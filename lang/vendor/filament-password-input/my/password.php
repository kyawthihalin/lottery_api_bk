<?php

declare(strict_types=1);

return [

    'actions' => [

        'copy' => [
            'tooltip' => 'ကော်ပီယူမည်',
        ],

        'regenerate' => [
            'tooltip' => 'အသစ်ထုတ်မည်',
            'success_message' => 'New password was generated!',
        ],

        'reveal' => [
            'show' => 'ကြည့်မည်',
            'hide' => 'ပိတ်မည်',
        ],

    ],

];
