<?php

return [

    'label' => 'ကိုယ်ရေးအကျဉ်း',

    'form' => [

        'email' => [
            'label' => 'Email address',
        ],

        'name' => [
            'label' => 'Name',
        ],

        'password' => [
            'label' => 'New password',
        ],

        'password_confirmation' => [
            'label' => 'Confirm new password',
        ],

        'actions' => [

            'save' => [
                'label' => 'သိမ်းမည်',
            ],

        ],

    ],

    'notifications' => [

        'saved' => [
            'title' => 'Saved',
        ],

    ],

    'actions' => [

        'cancel' => [
            'label' => 'မလုပ်တော့ပါ',
        ],

    ],

];
