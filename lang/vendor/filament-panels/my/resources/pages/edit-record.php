<?php

return [

    'title' => ':label ကိုပြင်မည်',

    'breadcrumb' => 'ပြင်မည်',

    'form' => [

        'actions' => [

            'cancel' => [
                'label' => 'မလုပ်တော့ပါ',
            ],

            'save' => [
                'label' => 'မှတ်မည်',
            ],

        ],

    ],

    'notifications' => [

        'saved' => [
            'title' => 'သိမ်းဆည်းလိုက်သည်',
        ],

    ],

];
