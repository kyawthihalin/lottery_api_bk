<?php

return [

    'title' => ':label ဖန်တီးမည်',

    'breadcrumb' => 'ဖန်တီးမည်',

    'form' => [

        'actions' => [

            'cancel' => [
                'label' => 'မလုပ်တော့ပါ',
            ],

            'create' => [
                'label' => 'ဖန်တီးမည်',
            ],

            'create_another' => [
                'label' => 'သိမ်းဆည်းပြီး နောက်တစ်ခုကို ဖန်တီးမည်',
            ],

        ],

    ],

    'notifications' => [

        'created' => [
            'title' => 'သိမ်းဆည်းပြီး',
        ],

    ],

];
