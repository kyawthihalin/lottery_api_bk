<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation & Resource
    |--------------------------------------------------------------------------
    */

    'nav.group' => 'ဆက်တင်',
    'nav.log.label' => 'လှုပ်ရှားမှုမှတ်တမ်း',
    'nav.log.icon' => 'heroicon-o-clipboard-document-list',
    'resource.label.log' => 'လှုပ်ရှားမှုမှတ်တမ်း',
    'resource.label.logs' => 'လှုပ်ရှားမှုမှတ်တမ်းများ',
    'resource.label.user' => 'ပြုလုပ်သူ',
    'resource.label.subject' => 'ပြုလုပ်သည့်အရာ',
    'resource.label.subject_type' => 'ပြုလုပ်သည့်အရာအမျိုးအစား',
    'resource.label.description' => 'အကျယ်',
    'resource.label.type' => 'အမျိုးအစား',
    'resource.label.event' => 'လုပ်ဆောင်ချက်',
    'resource.label.logged_at' => 'အချိန်',
    'resource.label.properties' => 'ပါဝင်သည့်အရာများ',
    'resource.label.old' => 'အဟောင်း',
    'resource.label.new' => 'အသစ်',
    'resource.label.old_value' => 'အဟောင်းတန်ဖိုး',
    'resource.label.new_value' => 'အသစ်တန်ဖိုး',
    'resource.label.properties_hint' => 'Can be key or value',
    'resource.label.old_attributes' => 'Old Attribute or Value: ',
    'resource.label.new_attributes' => 'New Attribute or Value: ',
];
