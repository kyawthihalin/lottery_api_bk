<?php

return [

    'confirmation' => 'ဒီလိုလုပ်ချင်တာ သေချာပါသလား',

    'actions' => [

        'cancel' => [
            'label' => 'မလုပ်တော့ပါ',
        ],

        'confirm' => [
            'label' => 'အတည်ပြုမည်',
        ],

        'submit' => [
            'label' => 'နှိပ်ပါ',
        ],

    ],

];
