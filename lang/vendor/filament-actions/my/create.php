<?php

return [

    'single' => [

        'label' => 'ဖန်တီးမည်',

        'modal' => [

            'heading' => ':label ဖန်တီးမည်',

            'actions' => [

                'create' => [
                    'label' => 'ဖန်တီးမည်',
                ],

                'create_another' => [
                    'label' => 'သိမ်းဆည်းပြီး နောက်တစ်ခုကို ဖန်တီးမည်',
                ],

            ],

        ],

        'notifications' => [

            'created' => [
                'title' => 'သိမ်းဆည်းပြီး',
            ],

        ],

    ],

];
