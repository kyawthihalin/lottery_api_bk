<?php

return [

    'single' => [

        'label' => 'ဖျက်မည်',

        'modal' => [

            'heading' => ':label ကိုဖျက်မည်',

            'actions' => [

                'delete' => [
                    'label' => 'ဖျက်မည်',
                ],

            ],

        ],

        'notifications' => [

            'deleted' => [
                'title' => 'ဖျက်ပြီးပါပြီ',
            ],

        ],

    ],

    'multiple' => [

        'label' => 'ရွေးထားတာတွေအားလုံး ဖျက်မည်',

        'modal' => [

            'heading' => ':label ကို ဖျက်မည်',

            'actions' => [

                'delete' => [
                    'label' => 'Delete selected',
                ],

            ],

        ],

        'notifications' => [

            'deleted' => [
                'title' => 'Deleted',
            ],

        ],

    ],

];
