<?php

return [
    'completed' => 'Settlement completed',
    'settlement' => 'Settlement',
    'stat' => 'Current stat',
    'history' => 'Settlement history',
    'actions' => [
        'settlement' => 'Settlement',
    ],
    'form' => [
        'balance_point' => [
            'label' => 'Balance point',
        ],
        'bought_point' => [
            'label' => 'Bought point',
        ],
        'rewithdrawn_point' => [
            'label' => 'Rewithdrawn point',
        ],
        'paid_point' => [
            'label' => 'Paid point',
        ],
        'regained_point' => [
            'label' => 'Regained point',
        ],
        'used_point' => [
            'label' => 'Used point',
        ],
        'profit' => [
            'label' => 'Profit',
        ],
        'paid_above' => [
            'label' => 'Paid above',
        ],
        'net_paid_above' => [
            'label' => 'Net paid above',
        ],
        'total_balance' => [
            'label' => 'Total balance',
        ],
    ],
];
