<?php

return [
    'notification' => [
        'title' => [
            'completed' => 'Deposit completed +:point',
            'completed_from' => 'Deposit completed -:point',
        ],
        'message' => [
            'completed' => 'You got :point deposit from :name.',
            'completed_from' => 'You deposited :point point for :name.',
        ],
    ],
    'completed' => 'Deposit completed',
];
