<?php

return [
    'agents_tab' => 'Senior/Master/Agent',
    'form' => [
        'fieldset' => [
            'password_rules' => 'Password rules',
            'secret_code_rules' => 'Secret code rules',
        ],
        'min' => [
            'label' => 'Minimum length',
        ],
        'letters' => [
            'label' => 'Letter',
            'helper' => 'Must contain at least one letter.',
        ],
        'mixed_case' => [
            'label' => 'Mixed case',
            'helper' => 'Must contain at least one uppercase and one lowercase letter.',
        ],
        'numbers' => [
            'label' => 'Numbers',
            'helper' => 'Must contain at least one number.',
        ],
        'symbols' => [
            'label' => 'Symbols',
            'helper' => 'Must contain at least one symbol.',
        ],
        'max_attempt' => [
            'label' => 'Max attempt',
            'helper' => 'How many invalid atempts in a day will lock the user?',
        ],
        'secret_code_interval' => [
            'label' => 'Secret code interval',
            'helper' => 'How many minutes to request secret code after user inactivity? (Web app only)',
        ],
    ],
];
