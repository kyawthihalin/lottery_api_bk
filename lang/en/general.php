<?php

return [
    'id' => 'ID',
    'admins' => 'Admin|Admins',
    'seniors' => 'Senior|Seniors',
    'masters' => 'Master|Masters',
    'agents' => 'Agent|Agents',
    'users' => 'User|Users',

    'app_types' => 'App type|App types',
    'website' => 'Website',
    'admin_app' => 'Admin website',
    'senior_app' => 'Senior app',
    'master_app' => 'Master app',
    'agent_app' => 'Agent app',
    'user_app' => 'User app',

    'app_logo' => 'App logo',
    'settings' => 'Settings',
    'authorization' => 'Authorization',
    'security' => 'Manage security',
    'manage_results' => 'Manage results',
    'user_management' => 'User management',
    'ticket_setting' => 'Ticket setting',
    'update_secret_code' => 'Update secret code',
    'update_password' => 'Update password',
    'update_user_password' => 'Update user password',
    'bets' => 'Bets',
    'deposit' => 'Deposit point',
    'withdraw' => 'Withdraw point',
    'deposits_withdraws' => 'Deposits & Withdraws',
    'manage_banners' => 'Manage banners',
    'manage_contacts' => 'Manage contacts',
    'manage_images' => 'Manage images',
    'manage_files' => 'Manage files',
    'faqs' => 'Faqs',
    'feedbacks' => 'Feedbacks',
    'results' => 'Results',
    'prizes' => 'Prizes',
    'states' => 'State|States',
    'townships' => 'Townships',
    'lottery_dates' => 'Lottery dates',
    'payment_accounts' => 'Payment accounts',

    'account_management' => 'Account management',
    'purchased_list' => 'Purchased list',

    'profile' => 'Profile',
    'name' => 'Name',
    'code' => 'Code',
    'agent_code' => 'Agent code',
    'user_name' => 'Username',
    'email' => 'Email',
    'phone' => 'Phone',
    'profile_image' => 'Profile image',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'nrc' => 'NRC',
    'nrc_front' => 'NRC front',
    'nrc_back' => 'NRC back',
    'nrc_number' => 'NRC number',
    'nrc_code' => 'NRC code',
    'password' => 'Password',
    'password_confirmation' => 'Password confirmation',
    'secret_code' => 'Secret code',
    'secret_code_confirmation' => 'Secret code confirmation',
    'number' => 'Number',

    'by' => 'By',
    'to' => 'To',
    'status' => 'Status',
    'reference_id' => 'Activity number',
    'detail' => 'Detail',
    'activity_logs' => 'Activity logs',

    'date' => 'Date',
    'time' => 'Time',
    'date_time' => 'Date & Time',
    'date_from' => 'Date from',
    'date_until' => 'Date until',

    'app_background' => 'App background',
    'website_background' => 'Website background',
    'group_ticket_frame_website' => 'Image frame of website for group ticket',
    'group_ticket_frame_mobile' => 'Image frame of mobile for group ticket',
    'ticket_background' => 'Ticket background',
    'won_image' => 'Won image',
    'lost_image' => 'Lost image',
    'default_avatar' => 'Default avatar',
    'draw_ids' => 'Lottery date|Lottery dates',
    'tickets' => 'Ticket|Tickets',
    'numbers' => 'Number|Numbers',
    'winners' => 'Winner|Winners',
    'total_winners' => 'Total winners',

    'price' => 'Price',
    'point' => 'Point',
    'point_before' => 'Point before',
    'point_after' => 'Point after',
    'point_rates' => 'Point rate|Point rates',
    'current_point_rate' => 'Current point rate',
    'total' => 'Total',
    'amount' => 'Amount',
    'purchase_ticket' => 'Purchase ticket',
    'total_won_lottery' => 'Total won lottery',

    'submit' => 'Submit',
    'minimum' => 'Minimum',
    'maximum' => 'Maximum',
    'as' => 'as',
    'type' => 'Type',
    'data' => 'Data',

    // notifications
    'by_users' => 'By users',
    'by_apps' => 'By apps',
    'read_at' => 'Read at',
    'send' => 'Send',
    'receivers' => 'Receivers',
    'send_notification' => 'Send notification',
    'title' => 'Title',
    'body' => 'Body',
    'content' => 'Content',
    'myanmar' => 'Myanmar',
    'english' => 'English',
    'your' => 'Your',
    'notification' => [
        'label' => 'Notification|Notifications',
        'title' => [
            'password_updated' => ':name password updated successfully.',
            'secret_code_updated' => ':name secret code updated successfully.',
            'point_rate_updated' => ':name point rate updated successfully.',
            'user' => [
                'lottery_won' => 'Congratulations...',
            ],
            'agent' => [
                'lottery_won' => ':name won the lottery',
            ],
        ],
        'message' => [
            'user' => [
                'lottery_won' => [
                    'first' => 'You just won the 1st Prize (:price ks) with ticket :tickets.',
                    'closest_first' => 'You just won the Up/Down prize (:price ks) with ticket :tickets.',
                    'second' => 'You just won the 2nd prize (:price ks) with ticket :tickets.',
                    'third' => 'You just won the 3rd prize (:price ks) with ticket :tickets.',
                    'forth' => 'You just won the 4th prize (:price ks) with ticket :tickets.',
                    'fifth' => 'You just won the 5th prize (:price ks) with ticket :tickets.',
                    'first_three' => 'You just won the First 3 Digits In Number prize (:price ks) with ticket :tickets.',
                    'last_three' => 'You just won the Last 3 Digits In Number prize (:price ks) with ticket :tickets.',
                    'last_two' => 'You just won the Last 2 Digits In Number prize (:price ks) with ticket :tickets.',
                ],
            ],
            'agent' => [
                'lottery_won' => [
                    'first' => ':name just won the 1st Prize (:price ks) with ticket :tickets.',
                    'closest_first' => ':name just won the Up/Down prize (:price ks) with ticket :tickets.',
                    'second' => ':name just won the 2nd prize (:price ks) with ticket :tickets.',
                    'third' => ':name just won the 3rd prize (:price ks) with ticket :tickets.',
                    'forth' => ':name just won the 4th prize (:price ks) with ticket :tickets.',
                    'fifth' => ':name just won the 5th prize (:price ks) with ticket :tickets.',
                    'first_three' => ':name just won the First 3 Digits In Number prize (:price ks) with ticket :tickets.',
                    'last_three' => ':name just won the Last 3 Digits In Number prize (:price ks) with ticket :tickets.',
                    'last_two' => ':name just won the Last 2 Digits In Number prize (:price ks) with ticket :tickets.',
                ],
            ],
        ],
    ],

    // forms
    'errors' => [
        'invalid' => [
            'user_name' => 'Invalid user name.',
            'agent_code' => 'Invalid agent code.',
        ],
        'ticket' => [
            'already_purchased' => 'Ticket is already purchased.',
        ],
        'point_not_enough' => 'Your point is not enough to :transaction_type.',
        'point_not_enough_user' => ":name's point is not enough to :transaction_type.",
        'purchase' => [
            'lottery_day' => 'Sorry, you can not purchase lottery on this day.',
        ]
    ],
    'filters' => [
        'unread' => 'Unread',
    ],
    'actions' => [
        'settlement' => 'Settlement',
        'read' => 'Read',
        'unlock' => 'Unlock',
        'lock' => 'Lock',
        'mark_all_as_read' => 'Mark all as read',
    ],
    'form' => [
        'deposit' => [
            'action' => 'Deposit',
        ],
        'withdraw' => [
            'action' => 'Withdraw',
        ],
        'question' => [
            'label' => 'Question',
        ],

        'your_password' => [
            'label' => 'Your password',
        ],
        'current_password' => [
            'label' => 'Current password',
        ],
        'new_password' => [
            'label' => 'New password',
        ],

        'answer' => [
            'label' => 'Answer',
        ],

        'order' => [
            'label' => 'Order',
        ],

        'restricted_tickets' => [
            'label' => 'Restricted tickets',
            'placeholder' => 'Enter new ticket',
            'description' => 'These tickets will be restricted from purchasing.',
        ],
        'purchase_limit_per_ticket' => [
            'label' => 'Purchase limit per ticket',
            'description' => 'How many times a ticket can be purchased on a draw?',
        ],
        'tickets_limit_per_purchase' => [
            'label' => 'Tickets limit per purchase',
            'description' => 'Number of tickets can select on each purchase.',
        ],
        'random_ticket_groups_count_per_draw_id' => [
            'label' => 'Ticket group count per draw',
            'description' => 'Application will randomly genenrate this number of ticket groups on each draw.',
        ],
        'lottery_dates' => [
            'hint' => 'Total dates - ',
            'label' => 'Dates of Thailand lottery',
            'placeholder' => 'Add new date here',
            'description' => 'The application will automatically fetch lottery results from api provider on these days starting from Myanmar Time 3:00 PM and will fetch every one hour until success, Please make sure the dates are correct!',
            'validation' => [
                'regex' => 'Please enter a valid date in the format MM-DD',
            ],
        ],
        'max_agent' => [
            'hint' => 'This value will be assumed as point rate for the user.',
        ],
        'live_link' => [
            'label' => 'YouTube live link'
        ],
    ],
    'get_lottery_results' => [
        'action' => 'Get lottery results',
        'helper' => 'Application will fetch lottery results with this date. If fetching resuts success, application will calculate winners and send notification to winners and related agents.',
        'succeed' => 'Get lottery results succeed.',
        'exist' => 'There are results for this lottery date.',
        'not_found' => 'There is no lottery results for this lottery date.',
        'failed' => 'Failed to fetch lottery results, please contact IT service.',
    ],
    'contacts' => [
        'viber' => 'Viber bot link',
        'facebook' => 'Facebook page link',
        'linkedin' => 'LinkedIn company link',
        'youtube' => 'Youtube channel link',
        'instagram' => 'Instagram account link',
        'twitter' => 'Twitter/X account link',
        'mail' => 'Mail address',
        'phone' => 'Phone number',
    ]
];
