<?php

return [
    'notification' => [
        'title' => [
            'completed' => 'Withdraw completed -:point',
            'completed_by' => 'Withdraw completed +:point',
        ],
        'message' => [
            'completed' => ':point point withdrawn by :name.',
            'completed_by' => 'You withdrew :point point from :name.',
        ],
    ],
    'completed' => 'Withdraw completed',
];
