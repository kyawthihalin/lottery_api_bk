<?php

return [
    'notification' => [
        'title' => [
            'user' => [
                'purchased' => 'Ticket Purchased -:point Point',
            ],
            'agent' => [
                'purchased' => 'Ticket Purchased +:point Point',
            ],
        ],
        'message' => [
            'user' => [
                'purchased' => 'You just purchased tickets :tickets, good luck.',
            ],
            'agent' => [
                'purchased' => ':name just purchased tickets :tickets',
            ],
        ],
    ],
    'already_purchased' => 'Ticket already purchased, please choose another number.',
    'purchased' => 'Ticket purchased successfully.',
];
