<?php

return [
    'types' => [
        'N' => 'N',
        'E' => 'E',
        'P' => 'P',
        'T' => 'T',
        'R' => 'R',
        'S' => 'S',
    ],
];
