<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'secret_code' => 'The provided secret code is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'failed_api' => [
        'user' => 'User name or password is incorrect. please try again.',
    ],

    'locked' => 'Your account is currently locked. please contact customer support.',

    'action' => [
        'notification' => [
            'locked' => ':name ၏ အကောင့်အား လော့ခ်ချခြင်းအောင်မြင်ပါသည်။',
            'unlocked' => ':name ၏ အကောင့်အား လော့ခ်ဖြေခြင်းအောင်မြင်ပါသည်။',
            'registered' => 'အကောင့်အား စာရင်းသွင်းခြင်းအောင်မြင်ပါသည်။',
        ],
    ],
];
