<?php

return [
    'completed' => 'စာရင်းရှင်းပြီးပါပြီ',
    'settlement' => 'စာရင်း',
    'stat' => 'လက်ရှိစာရင်း',
    'history' => 'ရှင်းထားသော စာရင်း',
    'actions' => [
        'settlement' => 'စာရင်းရှင်းမည်',
    ],
    'form' => [
        'balance_point' => [
            'label' => 'လက်ကျန်ပွိုင့်',
        ],
        'bought_point' => [
            'label' => 'ဝယ်ပွိုင့်',
        ],
        'rewithdrawn_point' => [
            'label' => 'ပြန်ထုတ်ပွိုင့်',
        ],
        'paid_point' => [
            'label' => 'ပေးပွိုင့်',
        ],
        'regained_point' => [
            'label' => 'ပြန်ရပွိုင့်',
        ],
        'used_point' => [
            'label' => 'အသုံးပြုပွိုင့်',
        ],
        'profit' => [
            'label' => 'အမြတ်',
        ],
        'paid_above' => [
            'label' => 'အပေါ်ပေး',
        ],
        'net_paid_above' => [
            'label' => 'အပေါ်ပေးအသားတင်',
        ],
        'total_balance' => [
            'label' => 'စုစုပေါင်းလက်ကျန်ပွိုင့်',
        ],
    ],
];
