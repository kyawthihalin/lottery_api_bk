<?php

return [
    'notification' => [
        'title' => [
            'completed' => 'ပွိုင့်ထည့်ခြင်းအောင်မြင်ပါသည် +:point ပွိုင့်',
            'completed_from' => 'ပွိုင့်ထည့်ခြင်းအောင်မြင်ပါသည် -:point ပွိုင့်',
        ],
        'message' => [
            'completed' => ':name မှ :point ပွိုင့် လက်ခံရရှိပါသည်။',
            'completed_from' => 'သင်သည် :name သို့ :point ပွိုင့် ထည့်လိုက်ပါသည်။',
        ],
    ],
    'completed' => 'ပွိုင့်ထည့်ခြင်းအောင်မြင်ပါသည်',
];
