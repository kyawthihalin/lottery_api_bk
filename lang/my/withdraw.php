<?php

return [
    'notification' => [
        'title' => [
            'completed' => 'ပွိုင့်ထုတ်ခြင်းအောင်မြင်ပါသည် -:point ပွိုင့်',
            'completed_by' => 'ပွိုင့်ထုတ်ခြင်းအောင်မြင်ပါသည် +:point ပွိုင့်',
        ],
        'message' => [
            'completed' => ':name မှ :point ပွိုင့် ထုတ်လိုက်ပါသည်။',
            'completed_by' => 'သင်သည် :name ထံမှ :point ပွိုင့် ထုတ်လိုက်ပါသည်။',
        ],
    ],
    'completed' => 'ပွိုင့်ထုတ်ခြင်းအောင်မြင်ပါသည်',
];
