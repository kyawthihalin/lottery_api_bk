<?php

namespace Database\Seeders;

use App\Models\Master;
use App\Models\Senior;
use Illuminate\Database\Seeder;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $masters = [
            [
                'name' => 'Naung Ye Htet (MS)',
                'user_name' => 'naungyehtet_ms',
                'senior_id' => 'naungyehtet_sn',
            ],
            [
                'name' => 'Wanna Min Paing (MS)',
                'user_name' => 'wannaminpaing_ms',
                'senior_id' => 'wannaminpaing_sn',
            ],
            [
                'name' => 'Aung Htet Myat (MS)',
                'user_name' => 'aunghtetmyat_ms',
                'senior_id' => 'aunghtetmyat_sn',
            ],
            [
                'name' => 'Nant Su Sandi (MS)',
                'user_name' => 'nantsusandi_ms',
                'senior_id' => 'nantsusandi_sn',
            ],
        ];

        foreach ($masters as $master) {
            $exist = Master::where('user_name', $master['user_name'])->first();
            $senior = Senior::where('user_name', $master['senior_id'])->first();

            $master['senior_id'] = $senior->id;

            if (! $exist) {
                Master::factory()
                    ->feedbacks()
                    ->create($master);
            }
        }
    }
}
