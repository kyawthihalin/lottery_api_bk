<?php

namespace Database\Seeders;

use App\Models\Agent;
use App\Models\Master;
use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $agents = [
            [
                'name' => 'Naung Ye Htet (AG)',
                'user_name' => 'naungyehtet_ag',
                'master_id' => 'naungyehtet_ms',
            ],
            [
                'name' => 'Wanna Min Paing (AG)',
                'user_name' => 'wannaminpaing_ag',
                'master_id' => 'wannaminpaing_ms',
            ],
            [
                'name' => 'Aung Htet Myat (AG)',
                'user_name' => 'aunghtetmyat_ag',
                'master_id' => 'aunghtetmyat_ms',
            ],
            [
                'name' => 'Nant Su Sandi (AG)',
                'user_name' => 'nantsusandi_ag',
                'master_id' => 'nantsusandi_ms',
            ],
        ];

        foreach ($agents as $agent) {
            $exist = Agent::where('user_name', $agent['user_name'])->first();
            $master = Master::where('user_name', $agent['master_id'])->first();

            $agent['master_id'] = $master->id;

            if (! $exist) {
                Agent::factory()
                    ->feedbacks()
                    ->create($agent);
            }
        }
    }
}
