<?php

namespace Database\Seeders;

use App\Constants\Factory\FactoryCount;
use App\Facades\Draw;
use App\Settings\TicketSettings;
use Illuminate\Database\Seeder;

class TicketGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $previousDrawCount = FactoryCount::DRAW_ID_COUNT;

        $drawIds = Draw::getPreviousDrawIds($previousDrawCount);

        foreach ($drawIds as $drawId) {
            \App\Models\TicketGroup::factory(app(TicketSettings::class)->random_ticket_groups_count_per_draw_id)->create([
                'draw_id' => $drawId,
            ]);
        }
    }
}
