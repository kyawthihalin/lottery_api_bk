<?php

namespace Database\Seeders;

use App\Constants\Factory\FactoryCount;
use App\Facades\Draw;
use App\Models\ResultType;
use Illuminate\Database\Seeder;

class ResultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $previousDrawCount = FactoryCount::DRAW_ID_COUNT;

        $drawIds = Draw::getPreviousDrawIds($previousDrawCount);
        $resultTypes = ResultType::all();

        foreach ($drawIds as $drawId) {
            foreach ($resultTypes as $resultType) {
                $resultType->results()->create([
                    'draw_id' => $drawId,
                    'numbers' => Draw::getFakeResultsByResultType($resultType->code),
                    'price' => $resultType->price,
                ]);
            }

            Draw::generateWinners($drawId);
        }
    }
}
