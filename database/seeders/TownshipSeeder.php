<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class TownshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $townships = json_decode(File::get(base_path('database/data/townships.json')));

        foreach ($townships as $township) {
            \App\Models\Township::create([
                'state_id' => $township->state_id,
                'name' => [
                    'en' => $township->name_my,
                    'my' => $township->name_my,
                ],
                'nrc_code' => [
                    'en' => $township->nrc_code_en,
                    'my' => $township->nrc_code_my,
                ],
            ]);
        }
    }
}
