<?php

namespace Database\Seeders;

use App\Models\ResultType;
use Illuminate\Database\Seeder;

class ResultTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $result_types = [
            [
                'name' => [
                    'en' => 'First Price',
                    'my' => 'ပထမဆု',
                ],
                'code' => 'first',
                'price' => 6000000,
            ],
            [
                'name' => [
                    'en' => 'Up/Down Price',
                    'my' => 'တွတ်တစ်လုံးလျော့/တိုး',
                ],
                'code' => 'closest_first',
                'price' => 2000000,
            ],
            [
                'name' => [
                    'en' => 'Second Price',
                    'my' => 'ဒုတိယဆု',
                ],
                'code' => 'second',
                'price' => 3000000,
            ],
            [
                'name' => [
                    'en' => 'Third Price',
                    'my' => 'တတိယဆု',
                ],
                'code' => 'third',
                'price' => 1500000,
            ],
            [
                'name' => [
                    'en' => 'Forth Price',
                    'my' => 'စတုတ္ထဆု',
                ],
                'code' => 'forth',
                'price' => 1000000,
            ],
            [
                'name' => [
                    'en' => 'Fifth Price',
                    'my' => 'ပဉ္စမဆု',
                ],
                'code' => 'fifth',
                'price' => 500000,
            ],
            [
                'name' => [
                    'en' => 'First 3 Digits In Number',
                    'my' => 'ရှေ့ (၃)လုံးတူ',
                ],
                'code' => 'first_three',
                'price' => 300000,
            ],
            [
                'name' => [
                    'en' => 'Last 3 Digits In Number',
                    'my' => 'နောက် (၃)လုံးတူ',
                ],
                'code' => 'last_three',
                'price' => 300000,
            ],
            [
                'name' => [
                    'en' => 'Last 2 Digits In Number',
                    'my' => 'နောက် (၂)လုံးတူ',
                ],
                'code' => 'last_two',
                'price' => 150000,
            ],
        ];

        foreach ($result_types as $result_type) {
            $exist = ResultType::whereCode($result_type['code'])->first();

            if (! $exist) {
                ResultType::create($result_type);
            }
        }
    }
}
