<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admins = [
            [
                'name' => 'Naung Ye Htet (AM)',
                'email' => 'naungyehtet.admin@gmail.com',
            ],
            [
                'name' => 'Wanna Min Paing (AM)',
                'email' => 'wannaminpaing.admin@gmail.com',
            ],
            [
                'name' => 'Aung Htet Myat (AM)',
                'email' => 'aunghtetmyat.admin@gmail.com',
            ],
            [
                'name' => 'Nant Su Sandi (AM)',
                'email' => 'nantsusandi.admin@gmail.com',
            ],
        ];

        foreach ($admins as $admin) {
            $exist = Admin::where('email', $admin['email'])->first();

            if (! $exist) {
                Admin::factory()->create($admin);
            }
        }
    }
}
