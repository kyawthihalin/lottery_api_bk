<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Constants\Factory\FactoryCount;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            StateSeeder::class,
            // TownshipSeeder::class,
            ResultTypeSeeder::class,
            AdminSeeder::class,
            PaymentAccountTypeSeeder::class,
        ]);

        $this->command->info('Seeding SeniorFactory...');
        \App\Models\Senior::factory(FactoryCount::MAX_SENIOR)
            ->feedbacks()
            ->masters()
            ->create();
        $this->call([
            SeniorSeeder::class,
            MasterSeeder::class,
            AgentSeeder::class,
            UserSeeder::class,
        ]);
        $this->command->info('Seeding FaqFactory...');
        \App\Models\Faq::factory(9)->create();
        $this->call([
            DepositAndBetSeeder::class,
            ResultSeeder::class,
        ]);

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
