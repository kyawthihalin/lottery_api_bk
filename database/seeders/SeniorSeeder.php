<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Senior;
use Illuminate\Database\Seeder;

class SeniorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $seniors = [
            [
                'name' => 'Naung Ye Htet (SN)',
                'user_name' => 'naungyehtet_sn',
                'admin_id' => 'naungyehtet.admin@gmail.com',
            ],
            [
                'name' => 'Wanna Min Paing (SN)',
                'user_name' => 'wannaminpaing_sn',
                'admin_id' => 'wannaminpaing.admin@gmail.com',
            ],
            [
                'name' => 'Aung Htet Myat (SN)',
                'user_name' => 'aunghtetmyat_sn',
                'admin_id' => 'aunghtetmyat.admin@gmail.com',
            ],
            [
                'name' => 'Nant Su Sandi (SN)',
                'user_name' => 'nantsusandi_sn',
                'admin_id' => 'nantsusandi.admin@gmail.com',
            ],
        ];

        foreach ($seniors as $senior) {
            $exist = Senior::where('user_name', $senior['user_name'])->first();
            $admin = Admin::where('email', $senior['admin_id'])->first();

            $senior['admin_id'] = $admin->id;

            if (! $exist) {
                Senior::factory()
                    ->feedbacks()
                    ->create($senior);
            }
        }
    }
}
