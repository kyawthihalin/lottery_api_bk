<?php

namespace Database\Seeders;

use App\Models\Agent;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => 'Naung Ye Htet (US)',
                'user_name' => 'naungyehtet_us',
                'agent_id' => 'naungyehtet_ag',
            ],
            [
                'name' => 'Wanna Min Paing (US)',
                'user_name' => 'wannaminpaing_us',
                'agent_id' => 'wannaminpaing_ag',
            ],
            [
                'name' => 'Aung Htet Myat (US)',
                'user_name' => 'aunghtetmyat_us',
                'agent_id' => 'aunghtetmyat_ag',
            ],
            [
                'name' => 'Nant Su Sandi (US)',
                'user_name' => 'nantsusandi_us',
                'agent_id' => 'nantsusandi_ag',
            ],
        ];

        foreach ($users as $user) {
            $exist = User::where('user_name', $user['user_name'])->first();
            $agent = Agent::where('user_name', $user['agent_id'])->first();

            $user['agent_id'] = $agent->id;

            if (! $exist) {
                User::factory()->create($user);
            }
        }
    }
}
