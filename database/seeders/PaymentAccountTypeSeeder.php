<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentAccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = ['KBZPay', 'KBZ Account', 'Yoma Account', 'CB Account', 'AYA Account', 'WaveMoney'];

        foreach($types as $type) {
            \App\Models\PaymentAccountType::create([
                'name' => $type
            ]);
        }
    }
}
