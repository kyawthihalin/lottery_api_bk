<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $states = [
            [
                'name' => [
                    'en' => 'Kachin',
                    'my' => 'ကချင်ပြည်နယ်',
                ],
                'code' => 'KC',
            ],
            [
                'name' => [
                    'en' => 'Kayah',
                    'my' => 'ကယားပြည်နယ်',
                ],
                'code' => 'KY',
            ],
            [
                'name' => [
                    'en' => 'Kayin',
                    'my' => 'ကရင်ပြည်နယ်',
                ],
                'code' => 'KN',
            ],
            [
                'name' => [
                    'en' => 'Chin',
                    'my' => 'ချင်းပြည်နယ်',
                ],
                'code' => 'CH',
            ],
            [
                'name' => [
                    'en' => 'Sagaing',
                    'my' => 'စစ်ကိုင်းတိုင်းဒေသကြီး',
                ],
                'code' => 'SG',
            ],
            [
                'name' => [
                    'en' => 'Tanintharyi',
                    'my' => 'တနင်္သာရီတိုင်းဒေသကြီး',
                ],
                'code' => 'TN',
            ],
            [
                'name' => [
                    'en' => 'Bago',
                    'my' => 'ပဲခူးတိုင်းဒေသကြီး',
                ],
                'code' => 'BG',
            ],
            [
                'name' => [
                    'en' => 'Magway',
                    'my' => 'မကွေးတိုင်းဒေသကြီး',
                ],
                'code' => 'MG',
            ],
            [
                'name' => [
                    'en' => 'Mandalay',
                    'my' => 'မန္တလေးတိုင်းဒေသကြီး',
                ],
                'code' => 'MY',
            ],
            [
                'name' => [
                    'en' => 'Mon',
                    'my' => 'မွန်ပြည်နယ်',
                ],
                'code' => 'MN',
            ],
            [
                'name' => [
                    'en' => 'Rakhine',
                    'mm' => 'ရခိုင်ပြည်နယ်',
                ],
                'code' => 'RK',
            ],
            [
                'name' => [
                    'en' => 'Yangon',
                    'my' => 'ရန်ကုန်တိုင်းဒေသကြီး',
                ],
                'code' => 'YG',
            ],
            [
                'name' => [
                    'en' => 'Shan',
                    'my' => 'ရှမ်းပြည်နယ်',
                ],
                'code' => 'SH',
            ],
            [
                'name' => [
                    'en' => 'Ayeyarwady',
                    'my' => 'ဧရာဝတီတိုင်းဒေသကြီး',
                ],
                'code' => 'AY',
            ],
        ];

        foreach ($states as $key => $state) {
            $exist = State::where('name->en', $state['name']['en'])->first();

            if (! $exist) {
                State::create([
                    'id' => $key + 1,
                    ...$state,
                ]);
            }
        }
    }
}
