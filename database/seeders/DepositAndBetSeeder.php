<?php

namespace Database\Seeders;

use App\Actions\MakeTransaction;
use App\Constants\Factory\FactoryCount;
use App\Enums\TransactionType;
use App\Events\DepositCompleted;
use App\Events\TicketPurchased;
use App\Facades\Draw;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use Illuminate\Database\Seeder;

class DepositAndBetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $masterMax = 888888 / 2;
        // $masterMin = $masterMax * 0.9;

        // dd("Min >>> " . $masterMin, "Max >>> ". $masterMax);

        $seniors = Senior::all();
        foreach ($seniors as $senior) {
            $seniorDeposit = (new MakeTransaction(
                transactionType: TransactionType::SeniorDeposit,
                point: get_stepped_random_number(10000, 11000, 50),
                user: $senior,
                otherFields: [
                    'point_rate' => $senior->point_rate,
                ]
            ))->execute();

            DepositCompleted::dispatch($seniorDeposit);
        }

        $masters = Master::all();
        $masterMax = 10000 / (FactoryCount::MAX_MASTER + 4);
        $masterMin = $masterMax * 0.9;

        // dd($masterMin, $masterMax);

        foreach ($masters as $master) {
            $senior = $master->senior;

            $masterDeposit = (new MakeTransaction(
                transactionType: TransactionType::MasterDeposit,
                point: get_stepped_random_number($masterMin, $masterMax, $masterMin / 5),
                user: $master,
                otherUser: $senior,
                otherFields: [
                    'point_rate' => $master->point_rate,
                ]
            ))->execute();

            DepositCompleted::dispatch($masterDeposit);
        }

        $agents = Agent::all();
        $agentMax = $masterMin / (FactoryCount::MAX_AGENT + 4);
        $agentMin = $agentMax * 0.9;

        foreach ($agents as $agent) {
            $master = $agent->master;
            $agentDeposit = (new MakeTransaction(
                transactionType: TransactionType::AgentDeposit,
                point: get_stepped_random_number($agentMin, $agentMax, $agentMin / 5),
                user: $agent,
                otherUser: $master,
                otherFields: [
                    'point_rate' => $agent->point_rate,
                ]
            ))->execute();

            DepositCompleted::dispatch($agentDeposit);
        }

        $users = User::all();
        $userMax = $agentMin / (FactoryCount::MAX_USER + 4);
        $userMin = $userMax * 0.9;

        foreach ($users as $user) {
            $agent = $user->agent;
            $userDeposit = (new MakeTransaction(
                transactionType: TransactionType::UserDeposit,
                point: get_stepped_random_number($userMin, $userMax, $userMin / 5),
                user: $user,
                otherUser: $agent,
            ))->execute();

            DepositCompleted::dispatch($userDeposit);

            $drawIds = Draw::getPreviousDrawIds(3);

            $betTimes = rand(1, 5);

            while ($betTimes > 0) {
                $drawDate = $drawIds[array_rand($drawIds)];
                $ticketCount = rand(5, 10);
                $tickets = get_random_digits(count: $ticketCount);

                if ($user->refresh()->point < $ticketCount) {
                    $betTimes--;

                    continue;
                }

                $bet = (new MakeTransaction(
                    transactionType: TransactionType::Bet,
                    point: $ticketCount,
                    user: $user,
                    otherFields: [
                        'draw_id' => $drawDate,
                        'tickets' => $tickets,
                    ]
                ))->execute();

                TicketPurchased::dispatch($bet);
                $betTimes--;
            }
        }
    }
}
