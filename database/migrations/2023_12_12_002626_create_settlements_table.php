<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('settlements', function (Blueprint $table) {
            $table->id();
            $table->morphs('user');
            $table->unsignedInteger('balance_point');
            $table->unsignedInteger('bought_point');
            $table->unsignedInteger('rewithdrawn_point');
            $table->unsignedInteger('paid_point');
            $table->unsignedInteger('regained_point');
            $table->unsignedInteger('used_point');
            $table->unsignedInteger('point_rate');
            $table->integer('profit');
            $table->unsignedInteger('paid_above');
            $table->unsignedInteger('net_paid_above');
            $table->unsignedInteger('total_balance_point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('settlements');
    }
};
