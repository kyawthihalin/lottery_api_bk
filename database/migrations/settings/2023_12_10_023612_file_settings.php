<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('file.user_app');
        $this->migrator->add('file.agent_app');
        $this->migrator->add('file.master_app');
        $this->migrator->add('file.senior_app');
    }
};
