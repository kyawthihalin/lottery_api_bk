<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('image.app_logo');
        $this->migrator->add('image.app_background');
        $this->migrator->add('image.website_background');
        $this->migrator->add('image.group_ticket_frame_website');
        $this->migrator->add('image.group_ticket_frame_mobile');
        $this->migrator->add('image.ticket_background');
        $this->migrator->add('image.won_image');
        $this->migrator->add('image.lost_image');
        $this->migrator->add('image.default_avatar');
        $this->migrator->add('banner.user_app', []);
        $this->migrator->add('banner.website', []);
    }
};
