<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('auth.admin_password_min', 8);
        $this->migrator->add('auth.agent_password_min', 6);
        $this->migrator->add('auth.user_password_min', 6);

        $this->migrator->add('auth.admin_password_letters', true);
        $this->migrator->add('auth.agent_password_letters', true);
        $this->migrator->add('auth.user_password_letters', true);

        $this->migrator->add('auth.admin_password_mixed_case', true);
        $this->migrator->add('auth.agent_password_mixed_case', true);
        $this->migrator->add('auth.user_password_mixed_case', true);

        $this->migrator->add('auth.admin_password_numbers', true);
        $this->migrator->add('auth.agent_password_numbers', true);
        $this->migrator->add('auth.user_password_numbers', true);

        $this->migrator->add('auth.admin_password_symbols', true);
        $this->migrator->add('auth.agent_password_symbols', true);
        $this->migrator->add('auth.user_password_symbols', true);

        $this->migrator->add('auth.agent_secret_code_min', 6);
        $this->migrator->add('auth.agent_secret_code_letters', true);
        $this->migrator->add('auth.agent_secret_code_mixed_case', true);
        $this->migrator->add('auth.agent_secret_code_numbers', true);
        $this->migrator->add('auth.agent_secret_code_symbols', true);

        $this->migrator->add('auth.agent_password_max_attempt', 5);
        $this->migrator->add('auth.admin_password_max_attempt', 3);
        $this->migrator->add('auth.user_password_max_attempt', 5);
        $this->migrator->add('auth.agent_secret_code_max_attempt', 5);
        $this->migrator->add('auth.agent_secret_code_interval', 5);
    }
};
