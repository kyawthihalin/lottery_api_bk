<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('point_rate.min_agent', 1700);
        $this->migrator->add('point_rate.max_agent', 3000);
        $this->migrator->add('point_rate.min_master', 1700);
        $this->migrator->add('point_rate.max_master', 3000);
        $this->migrator->add('point_rate.min_senior', 1700);
        $this->migrator->add('point_rate.max_senior', 3000);
    }
};
