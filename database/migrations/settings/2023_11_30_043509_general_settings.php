<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.force_use_secret_code', false);
        $this->migrator->add('general.lottery_dates', ['01-17', '02-01', '02-16', '03-01', '03-16', '04-01', '04-16', '05-02', '05-16', '06-01', '06-16', '07-01', '07-16', '07-31', '08-16', '09-01', '09-16', '10-01', '10-16', '11-01', '11-16', '12-01', '12-16',
        ]);
        $this->migrator->add('general.contact_viber', null);
        $this->migrator->add('general.contact_facebook', null);
        $this->migrator->add('general.contact_linkedin', null);
        $this->migrator->add('general.contact_youtube', null);
        $this->migrator->add('general.contact_instagram', null);
        $this->migrator->add('general.contact_twitter', null);
        $this->migrator->add('general.contact_mail', null);
        $this->migrator->add('general.contact_phone', null);
        $this->migrator->add('general.live_link', null);
    }
};
