<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('ticket.restricted_tickets', []);
        $this->migrator->add('ticket.purchase_limit_per_ticket', 2);
        $this->migrator->add('ticket.tickets_limit_per_purchase', 10);
        $this->migrator->add('ticket.random_ticket_groups_count_per_draw_id', 5);
    }
};
