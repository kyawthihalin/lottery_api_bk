<?php

use App\Enums\Gender;
use App\Enums\Language;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Agent::class)->nullable()->constrained()->nullOnDelete();

            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('password');
            $table->string('user_name')->unique();
            $table->unsignedInteger('point')->default(0);

            $table->string('language')->default(Language::Myanmar);
            $table->string('nrc')->nullable();
            $table->string('gender')->default(Gender::Male);
            $table->string('biometric_key')->nullable();
            $table->string('device_name')->nullable();
            $table->string('device_model')->nullable();
            $table->string('os_version')->nullable();
            $table->string('os_type')->nullable();
            $table->string('app_version')->nullable();
            $table->string('firebase_token')->nullable();
            $table->dateTime('locked_at')->nullable();
            $table->tinyInteger('password_mistakes_count')->default(0);
            $table->dateTime('password_mistook_at')->nullable();
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
