<?php

use App\Enums\Language;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seniors', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Admin::class)->nullable()->constrained();
            $table->foreignIdFor(\App\Models\State::class)->nullable()->constrained();

            $table->string('name');
            $table->string('user_name')->unique();
            $table->string('phone')->nullable();
            $table->string('password');
            $table->string('secret_code')->nullable();
            $table->string('biometric_key')->nullable();
            $table->unsignedInteger('point')->default(0);
            $table->unsignedInteger('point_rate');

            $table->tinyInteger('password_mistakes_count')->default(0);
            $table->dateTime('password_mistook_at')->nullable();
            $table->tinyInteger('secret_code_mistakes_count')->default(0);
            $table->dateTime('secret_code_mistook_at')->nullable();
            $table->boolean('is_secret_code_required')->default(false);
            $table->dateTime('locked_at')->nullable();

            $table->string('profile_image')->nullable();
            $table->unsignedTinyInteger('nrc_state')->nullable();
            $table->string('nrc_township', 9)->nullable();
            $table->string('nrc_type', 1)->nullable();
            $table->string('nrc_number', 6)->nullable();
            $table->string('nrc_front')->nullable();
            $table->string('nrc_back')->nullable();
            $table->dateTime('password_changed_at')->nullable();
            $table->string('language')->default(Language::English);
            // device details
            $table->string('firebase_token')->nullable();
            $table->string('device_name')->nullable();
            $table->string('device_model')->nullable();
            $table->string('os_version')->nullable();
            $table->string('os_type')->nullable();
            $table->string('app_version')->nullable();
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seniors');
    }
};
