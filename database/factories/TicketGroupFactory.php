<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Facades\Draw;
use App\Models\TicketGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\TicketGroup>
 */
final class TicketGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketGroup::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'numbers' => Draw::getRandomTickets(10),
        ];
    }
}
