<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Constants\Factory\Credential;
use App\Enums\Gender;
use App\Enums\OsType;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\User>
 */
final class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'agent_id' => \App\Models\Agent::all()->random(),
            'name' => fake()->name.' (US)',
            'phone' => fake()->phoneNumber,
            'password' => Credential::getPassword(),
            'user_name' => fake()->unique()->userName,
            'nrc' => fake()->optional()->word,
            'gender' => fake()->randomElement(Gender::cases()),
            'device_name' => fake()->word,
            'device_model' => fake()->word,
            'os_version' => fake()->word,
            'os_type' => fake()->randomElement(OsType::cases()),
            'app_version' => '1.0.0',
        ];
    }

    /**
     * Indicate that the company job has employee levels.
     */
    public function feedbacks(): Factory
    {
        return $this->afterCreating(function (User $user) {
            if (rand(0, 5) != 5) {
                return;
            }

            $count = rand(0, 5);

            Feedback::factory($count)->create([
                'user_id' => $user->id,
                'user_type' => get_class($user),
            ]);
        });
    }
}
