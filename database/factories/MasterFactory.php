<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Constants\Factory\Credential;
use App\Constants\Factory\FactoryCount;
use App\Enums\NrcType;
use App\Enums\OsType;
use App\Models\Feedback;
use App\Models\Master;
use App\Models\Senior;
use App\Models\State;
use App\Models\Township;
use App\Settings\PointRateSettings;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Master>
 */
final class MasterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Master::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $state = State::all()->random();

        //get random township by using state id
        if ($state->id == 15) {
            $nrcTownship = 'NaPaTa';
        } else {
            $township = Township::inRandomOrder()->where('state_id', $state->id)->first();
            $nrcTownship = $township ? $township->nrc_code : 'PhaMaNa';
        }

        $pointPrice = app(PointRateSettings::class);

        return [
            'state_id' => $state->id,
            'senior_id' => Senior::inRandomOrder()->first()->id,
            'name' => fake()->name.' (MS)',
            'user_name' => fake()->unique()->userName,
            'phone' => fake()->optional()->phoneNumber,
            'point_rate' => get_stepped_random_number(3000, 7000, 500),
            'point_rate' => get_stepped_random_number($pointPrice->min_master, $pointPrice->max_master, 500),
            'password' => Credential::getPassword(),
            'secret_code' => Credential::getSecretCode(),
            'nrc_state' => $state->id,
            'nrc_township' => $nrcTownship,
            'nrc_type' => fake()->randomElement(NrcType::cases()),
            'nrc_number' => get_random_digit(),
            'biometric_key' => fake()->optional()->word,
            'nrc_front' => fake()->optional()->word,
            'nrc_back' => fake()->optional()->word,
            'firebase_token' => fake()->optional()->word,
            'device_name' => fake()->optional()->word,
            'device_model' => fake()->optional()->word,
            'os_version' => fake()->optional()->word,
            'os_type' => fake()->randomElement(OsType::cases()),
            'app_version' => '1.0.0',
        ];
    }

    /**
     * Indicate that the company job has employee levels.
     */
    public function feedbacks(): Factory
    {
        return $this->afterCreating(function (Master $master) {
            $count = rand(0, 3);

            if (! $count) {
                return;
            }

            Feedback::factory($count)->create([
                'user_id' => $master->id,
                'user_type' => get_class($master),
            ]);
        });
    }

    public function agents(): Factory
    {
        return $this->afterCreating(function (Master $master) {
            $count = rand(FactoryCount::MIN_AGENT, FactoryCount::MAX_AGENT);

            if (! $count) {
                return;
            }

            \App\Models\Agent::factory($count)
                ->feedbacks()
                ->users()
                ->create([
                    'master_id' => $master->id,
                ]);
        });
    }
}
