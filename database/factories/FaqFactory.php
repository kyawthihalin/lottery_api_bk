<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Faq;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Faq>
 */
final class FaqFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Faq::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'order' => fake()->unique()->randomNumber(1),
            'question' => [
                'en' => fake()->sentence.'?',
                'my' => fake()->sentence.'?',
            ],
            'answer' => [
                'en' => fake()->sentence,
                'my' => fake()->sentence,
            ],
        ];
    }
}
