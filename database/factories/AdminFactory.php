<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Constants\Factory\Credential;
use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Admin>
 */
final class AdminFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Admin::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name.' (AM)',
            'user_name' => fake()->unique()->userName,
            'email' => fake()->unique()->safeEmail,
            'password' => Credential::getPassword(),
            'email_verified_at' => now(),
        ];
    }
}
