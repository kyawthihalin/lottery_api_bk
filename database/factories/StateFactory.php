<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\State;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\State>
 */
final class StateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = State::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => [
                'en' => ucfirst(fake()->word),
                'my' => ucfirst(fake()->word),
            ],
            'code' => strtoupper(\Illuminate\Support\Str::random('2')),
        ];
    }
}
