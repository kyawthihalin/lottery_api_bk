<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Actions\GenerateUserCode;
use App\Constants\Factory\Credential;
use App\Constants\Factory\FactoryCount;
use App\Enums\NrcType;
use App\Enums\OsType;
use App\Models\Agent;
use App\Models\Feedback;
use App\Models\Master;
use App\Models\State;
use App\Models\Township;
use App\Settings\PointRateSettings;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Agent>
 */
final class AgentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Agent::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $state = State::all()->random();

        //get random township by using state id
        if ($state->id == 15) {
            $nrcTownship = 'NaPaTa';
        } else {
            $township = Township::inRandomOrder()->where('state_id', $state->id)->first();
            $nrcTownship = $township ? $township->nrc_code : 'PhaMaNa';
        }

        $pointPrice = app(PointRateSettings::class);

        return [
            'state_id' => $state->id,
            'master_id' => Master::inRandomOrder()->first()->id,
            'name' => fake()->name.' (AG)',
            'code' => (new GenerateUserCode())->execute(Agent::class, $state->code),
            'user_name' => fake()->unique()->userName,
            'phone' => fake()->optional()->phoneNumber,
            'point_rate' => get_stepped_random_number($pointPrice->min_agent, $pointPrice->max_agent, 500),
            'password' => Credential::getPassword(),
            'secret_code' => Credential::getSecretCode(),
            'nrc_state' => $state->id,
            'nrc_township' => $nrcTownship,
            'nrc_type' => fake()->randomElement(NrcType::cases()),
            'nrc_number' => get_random_digit(),
            'biometric_key' => fake()->optional()->word,
            'nrc_front' => fake()->optional()->word,
            'nrc_back' => fake()->optional()->word,
            'firebase_token' => fake()->optional()->word,
            'device_name' => fake()->optional()->word,
            'device_model' => fake()->optional()->word,
            'os_version' => fake()->optional()->word,
            'os_type' => fake()->randomElement(OsType::cases()),
            'app_version' => '1.0.0',
        ];
    }

    public function feedbacks(): Factory
    {
        return $this->afterCreating(function (Agent $agent) {
            $count = rand(0, 3);

            if (! $count) {
                return;
            }

            Feedback::factory($count)->create([
                'user_id' => $agent->id,
                'user_type' => get_class($agent),
            ]);
        });
    }

    public function users(): Factory
    {
        return $this->afterCreating(function (Agent $agent) {
            $count = rand(FactoryCount::MIN_USER, FactoryCount::MAX_USER);

            if (! $count) {
                return;
            }

            \App\Models\User::factory($count)
                ->feedbacks()
                ->create([
                    'agent_id' => $agent->id,
                ]);
        });
    }
}
