<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Constants\Factory\Credential;
use App\Constants\Factory\FactoryCount;
use App\Enums\NrcType;
use App\Enums\OsType;
use App\Models\Admin;
use App\Models\Feedback;
use App\Models\Senior;
use App\Models\State;
use App\Models\Township;
use App\Settings\PointRateSettings;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Senior>
 */
final class SeniorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Senior::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $state = State::all()->random();

        //get random township by using state id
        if ($state->id == 15) {
            $nrcTownship = 'NaPaTa';
        } else {
            $township = Township::inRandomOrder()->where('state_id', $state->id)->first();
            $nrcTownship = $township ? $township->nrc_code : 'PhaMaNa';
        }

        $pointPrice = app(PointRateSettings::class);

        return [
            'state_id' => $state->id,
            'admin_id' => Admin::inRandomOrder()->first()->id,
            'name' => fake()->name.' (SN)',
            'user_name' => fake()->unique()->userName,
            'phone' => fake()->optional()->phoneNumber,
            'point_rate' => get_stepped_random_number($pointPrice->min_senior, $pointPrice->max_senior, 500),
            'password' => Credential::getPassword(),
            'secret_code' => Credential::getSecretCode(),
            'nrc_state' => $state->id,
            'nrc_township' => $nrcTownship,
            'nrc_type' => fake()->randomElement(NrcType::cases()),
            'nrc_number' => get_random_digit(),
            'biometric_key' => fake()->optional()->word,
            'nrc_front' => fake()->optional()->word,
            'nrc_back' => fake()->optional()->word,
            'firebase_token' => fake()->optional()->word,
            'device_name' => fake()->optional()->word,
            'device_model' => fake()->optional()->word,
            'os_version' => fake()->optional()->word,
            'os_type' => fake()->randomElement(OsType::cases()),
            'app_version' => '1.0.0',
        ];
    }

    /**
     * Indicate that the company job has employee levels.
     */
    public function feedbacks(): Factory
    {
        return $this->afterCreating(function (Senior $senior) {
            $count = rand(0, 3);

            if (! $count) {
                return;
            }

            Feedback::factory($count)->create([
                'user_id' => $senior->id,
                'user_type' => get_class($senior),
            ]);
        });
    }

    public function masters(): Factory
    {
        return $this->afterCreating(function (Senior $senior) {
            $count = rand(FactoryCount::MIN_MASTER, FactoryCount::MAX_MASTER);

            if (! $count) {
                return;
            }

            \App\Models\Master::factory($count)
                ->feedbacks()
                ->agents()
                ->create([
                    'senior_id' => $senior->id,
                ]);
        });
    }
}
