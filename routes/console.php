<?php

use App\Facades\Draw;
use App\Models\Agent;
use App\Models\TicketGroup;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Notification;
use NotificationChannels\Telegram\TelegramMessage;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    // dd(Draw::getRandomTickets(10), Draw::getPendingDrawId());
    // TicketGroup::factp

    // TelegramMessage::create()
    //     ->to('-1001911886196')
    //     ->content('dfasdfasdf')
    //     ->send();

    // Notification::send(User::where('user_name', 'shwe')->first(), new \App\Notifications\TestNotification());
    Notification::send(Agent::where('user_name', 'nantsusandi_ag')->first(), new \App\Notifications\TestNotification());
    

})->purpose('Display an inspiring quote');
