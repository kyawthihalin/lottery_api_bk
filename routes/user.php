<?php

use App\Http\Controllers\Api;
use App\Http\Controllers\Api\User;
use Illuminate\Support\Facades\Route;

Route::post('login', User\Auth\LoginController::class);
Route::post('register', User\Auth\RegisterController::class);
Route::post('countdown', User\CountDownController::class);
Route::post('assets', Api\AssetController::class);
Route::post('tickets/groups', [User\TicketController::class, 'groups']);
Route::post('results', [User\ResultController::class, 'index']);
Route::post('results/numbers', [User\ResultController::class, 'numbers']);
Route::post('results/check', [User\ResultController::class, 'check']);
Route::post('draw-ids', [User\DrawIdController::class, 'drawIds']);
Route::post('previous-draw', [User\DrawIdController::class, 'previousDraw']);
Route::post('months', [User\DrawIdController::class, 'months']);
Route::post('winners', User\WinnerController::class);
Route::post('faqs', User\FaqController::class);
Route::post('live', [User\DrawIdController::class, 'live']);

Route::middleware(['auth:sanctum', 'auth:user_api', 'prevent.locked'])->group(function () {
    Route::post('logout', User\Auth\LogoutController::class);
    Route::post('profile', [User\ProfileController::class, 'index']);
    Route::post('profile/update', [User\ProfileController::class, 'update']);
    Route::post('profile/update/password', [User\ProfileController::class, 'updatePassword']);
    Route::post('feedback', Api\FeedbackController::class);
    Route::post('won', [User\WinnerController::class, 'won']);

    Route::prefix('payment-accounts')->controller(Api\User\PaymentAccountController::class)->group(function () {
        Route::post('types', 'types');
        Route::post('/', 'index');
        Route::post('create', 'create');
        Route::post('delete', 'delete');
    });

    Route::post('purchase', [User\PurchaseController::class, 'purchase']);
    Route::post('purchase/histories', [User\PurchaseController::class, 'histories']);

    Route::prefix('notifications')->controller(Api\NotificationController::class)->group(function () {
        Route::post('/', 'index');
        Route::post('won', 'won');
        Route::post('read-won', 'readWon');
        Route::post('read', 'read');
        Route::post('read-all', 'readAll');
    });

    Route::post('deposit-withdraw/histories', [Api\DepositWithdrawController::class, 'userHistories']);
});
