<?php

use App\Http\Controllers\Api;
use App\Http\Controllers\Api\Agent;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\Master;
use Illuminate\Support\Facades\Route;

Route::post('login/password', [Master\Auth\LoginController::class, 'loginPassword']);
Route::post('login/secret', [Master\Auth\LoginController::class, 'loginSecret']);

Route::middleware(['auth:sanctum', 'auth:master_api', 'prevent.locked', 'force.secret_code'])->group(function () {
    Route::post('logout', Agent\Auth\LogoutController::class);
    Route::prefix('profile')->controller(Agent\ProfileController::class)->group(function () {
        Route::post('/', 'index');
        Route::post('update', 'update');
        Route::post('update/secret', 'updateSecret');
        Route::post('update/password', 'updatePassword');
    });

    Route::post('feedback', FeedbackController::class);

    Route::prefix('notifications')->controller(Api\NotificationController::class)->group(function () {
        Route::post('/', 'index');
        Route::post('read', 'read');
        Route::post('read-all', 'readAll');
    });

    Route::prefix('agents')->controller(Master\AgentController::class)->group(function () {
        Route::post('/', 'index');
        Route::post('create', 'store');
        Route::post('update', 'update');
    });

    Route::post('users', [Agent\UserController::class, 'index']);

    Route::prefix('deposit-withdraw')->controller(Api\DepositWithdrawController::class)->group(function () {
        Route::post('deposit', 'deposit');
        Route::post('withdraw', 'withdraw');
        Route::post('histories/user', 'userHistories');
        Route::post('histories/agent', 'agentHistories');
        Route::post('histories', 'masterHistories');
    });

    Route::prefix('settlement')->controller(Api\Master\SettlementController::class)->group(function () {
        Route::post('create', 'create');
        Route::post('current', 'current');
        Route::post('histories', 'histories');
    });

    Route::post('winners', Api\Agent\WinnerController::class);
    Route::post('purchase/histories', Api\Agent\PurchaseHistoryController::class);
});
