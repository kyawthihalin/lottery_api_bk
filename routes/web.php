<?php

use App\Livewire\Results;
use App\Livewire\UserInfo;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\TicketController;
use App\Http\Controllers\Web\NotificationController;

Route::view('/', 'home')->name('home');
Route::view('/login', 'login')->name('login');
Route::view('/register', 'register')->name('register');
Route::view('/purchase-tickets', 'purchase-tickets')->name('purchase-ticket');
Route::post('/purchase-tickets', [TicketController::class, 'goToCart'])->name('go-to-cart');
Route::post('/purchase', [TicketController::class, 'purchase'])->name('purchase');
Route::view('/live-details', 'live-details')->name('live-details');
Route::view('/results', 'result')->name('result');
Route::view('/winners', 'winner')->name('winner');
Route::view('/privacy-policy', 'privacy-policy')->name('privacy-policy');
Route::view('/terms-and-conditions', 'terms-and-conditions')->name('terms-and-conditions');
Route::view('/about', 'about')->name('about');

Route::middleware(['auth:user'])->group(function () {
    Route::view('/user-info', 'user-info')->name('user-info');
    Route::put('/notifications/{id}', [NotificationController::class, 'readNotification']);
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
Route::post('/register', [AuthController::class, 'register']);
Route::post('/remove-multi-input', [Results::class, 'removeArrayItem']);

Route::get('/lang/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
})->name('lang.change');
