<?php

namespace App\Console;

use App\Facades\Draw;
use App\Models\Result;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('generate:ticket-group')->when(function () {
            $currentDrawId = Draw::getPendingDrawId(now());
            $ticketGroup = \App\Models\TicketGroup::where('draw_id', $currentDrawId)->first();

            return ! (bool) $ticketGroup;
        })->hourly();

        $schedule->command('result:fetch')->when(function () {
            $currentDate = now()->format('Y-m-d');
            $isLotteryDate = Draw::isLotteryDate($currentDate);
            $resultExist = Result::where('draw_id', $currentDate)->first();

            return $isLotteryDate && ! $resultExist;
        })->everyFifteenMinutes();

        $schedule->command('activitylog:clean')->weekly();
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
