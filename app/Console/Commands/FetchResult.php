<?php

namespace App\Console\Commands;

use App\Services\LotteryApi\LotteryService;
use Illuminate\Console\Command;

class FetchResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'result:fetch {draw_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $drawId = $this->argument('draw_id') ? $this->argument('draw_id') : now()->format('Y-m-d'); 

        (new LotteryService())->fetchResults($drawId);
    }
}
