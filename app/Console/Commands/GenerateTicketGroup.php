<?php

namespace App\Console\Commands;

use App\Facades\Draw;
use App\Models\TicketGroup;
use App\Settings\TicketSettings;
use Illuminate\Console\Command;

class GenerateTicketGroup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:ticket-group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        TicketGroup::factory(app(TicketSettings::class)->random_ticket_groups_count_per_draw_id)->create([
            'draw_id' => Draw::getPendingDrawId(),
        ]);
    }
}
