<?php

use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use Carbon\Carbon;

if (! function_exists('get_stepped_random_number')) {
    function get_stepped_random_number(
        int $min,
        int $max,
        int $step = 1,
    ): int {
        if ($min >= $max) {
            throw new Exception('Min value cannot be greater than or equal max value');
        }

        if ($step <= 0) {
            throw new Exception('Step value cannot be less than or equal to zero');
        }

        if ($step > $max) {
            throw new Exception('Step value cannot be greater than max value');
        }

        // Calculate the range of possible values
        $range = ($max - $min) / $step;

        // Generate a random index within the range
        $randomIndex = rand(0, $range);

        // Calculate the random number based on the index and step
        return $min + ($randomIndex * $step);
    }
}

if (! function_exists('get_random_digit')) {
    function get_random_digit(
        int $length = 6,
    ): string {
        if ($length < 1) {
            throw new Exception('Length value cannot be less than one');
        }

        $max = '9';

        for ($i = 1; $i < $length; $i++) {
            $max .= '9';
        }

        return str_pad((string) mt_rand(0, (int) $max), $length, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('get_random_digits')) {
    function get_random_digits(
        int $length = 6,
        int $count = 1,
    ): array {
        if ($length < 1) {
            throw new Exception('Length value cannot be less than one');
        }

        if ($count < 1) {
            throw new Exception('Count value cannot be less than one');
        }

        $max = '9';

        for ($i = 1; $i < $length; $i++) {
            $max .= '9';
        }

        $numbers = [];

        for ($i = 0; $i < $count; $i++) {
            $numbers[] = str_pad((string) mt_rand(0, (int) $max), $length, '0', STR_PAD_LEFT);
        }

        return $numbers;
    }
}

if (! function_exists('get_path_by_model')) {
    function get_path_by_model(Admin|Senior|Master|Agent|User $user): string
    {
        return match (get_class($user)) {
            Admin::class => 'admins',
            Senior::class => 'seniors',
            Master::class => 'masters',
            Agent::class => 'agents',
            User::class => 'users',
        };
    }
}

if (! function_exists('get_route_prefix_by_model')) {
    function get_route_prefix_by_model(Admin|Senior|Master|Agent $user): string
    {
        return match (get_class($user)) {
            Admin::class => 'admin',
            Senior::class => 'senior',
            Master::class => 'master',
            Agent::class => 'agent',
        };
    }
}

if (! function_exists('get_model_by_route_prefix')) {
    function get_model_by_route_prefix(string $path): string
    {
        return match ($path) {
            'admin' => Admin::class,
            'senior' => Senior::class,
            'master' => Master::class,
            'agent' => Agent::class,
        };
    }
}

if (! function_exists('get_model_by_guard')) {
    function get_model_by_guard(string $guard): string
    {
        return match ($guard) {
            'admin' => Admin::class,
            'senior' => Senior::class,
            'master' => Master::class,
            'agent' => Agent::class,
            'user' => User::class,
        };
    }
}

if (! function_exists('is_json')) {
    function is_json(string|null $string): bool
    {
        if(!$string){
            return false;
        }

        json_decode($string);

        return json_last_error() === JSON_ERROR_NONE;
    }
}

if (! function_exists('convert_to_thailand_date')) {
    function convert_to_thailand_date(string $date): string
    {
        $date = Carbon::createFromFormat('Y-m-d', $date);

        // Convert the year to Thailand year
        $thailandYear = $date->year + 543;
        $date->year($thailandYear);

        // Format the date in the Thailand date format
        return $date->format('dmY');
    }
}

if (! function_exists('get_ticket_string')) {
    function get_ticket_string(array $tickets): string
    {
        $ticketStrings = [];
        foreach ($tickets as $resultTypeName => $tickets) {
            $ticketStr = implode(', ', $tickets);
            array_push($ticketStrings, "{$ticketStr} ".__('general.as')." {$resultTypeName}");
        }

        return implode(', ', $ticketStrings);
    }
}

if (! function_exists('format_model')) {
    function format_model(string $string): string
    {
        $parts = explode('\\', $string);
        $lastPart = end($parts);

        return preg_replace('/(?<!\s)(?=[A-Z])/', ' ', $lastPart);
    }
}

if (! function_exists('___')) {
    function ___($key, $number = 1, array $replace = [], $locale = null): string
    {
        return trans_choice($key, $number, $replace, $locale);
    }
}

if (! function_exists('get_child_function_by_user')) {
    function get_child_function_by_user(Senior|Master $user): string
    {
        return match (get_class($user)) {
            Senior::class => 'masters',
            Master::class => 'agents',
        };
    }
}

if (! function_exists('get_string_by_number')) {
    function get_string_by_number(?int $number): string
    {
        return match ($number) {
            0 => 'ZER',
            1 => 'ONE',
            2 => 'TWO',
            3 => 'THR',
            4 => 'FOR',
            5 => 'FIV',
            6 => 'SIX',
            7 => 'SEV',
            8 => 'EIG',
            9 => 'NIN',
            null => '',
        };
    }
}

if (! function_exists('is_api_request')) {
    function is_api_request(): bool
    {
        return str_starts_with(request()->path(), 'api');
    }
}
