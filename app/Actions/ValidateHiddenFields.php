<?php

namespace App\Actions;

use App\Exceptions\HiddenValidationException;
use Illuminate\Support\Facades\Validator;

class ValidateHiddenFields
{
    public function execute(array $rules)
    {
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            throw new HiddenValidationException($validator->errors()->toArray());
        }
    }
}
