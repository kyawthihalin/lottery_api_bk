<?php

namespace App\Actions;

use App\Exceptions\AccountLockedException;
use App\Exceptions\InvalidSecretCodeException;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Settings\AuthSettings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class CheckSecretCode
{
    private int $maxAttempt;

    public function __construct(private string $class)
    {
        $this->maxAttempt = app(AuthSettings::class)->getSecretCodeMaxAttemptByClass($class);
    }

    public function execute(Senior|Agent|Master $user, string $secret_code)
    {
        if (! Hash::check($secret_code, $user->secret_code)) {
            $this->invalidSecretCode($user);
        }

        $this->validSecretCode($user);
    }

    private function invalidSecretCode($user)
    {
        if (now()->diffInDays($user->secret_code_mistook_at) >= 1) {
            $user->update([
                'secret_code_mistakes_count' => 1,
                'secret_code_mistook_at' => now(),
            ]);

            $this->throwInvalidSecretCodeException();
        }

        $mistakesCount = $user->secret_code_mistakes_count + 1;

        if ($mistakesCount >= $this->maxAttempt) {
            $user->update([
                'secret_code_mistakes_count' => $mistakesCount,
                'secret_code_mistook_at' => now(),
                'locked_at' => now(),
            ]);

            $this->throwAccountLockedException($user);
        }

        $user->update([
            'secret_code_mistakes_count' => $mistakesCount,
            'secret_code_mistook_at' => now(),
        ]);

        $this->throwInvalidSecretCodeException();
    }

    private function throwInvalidSecretCodeException()
    {
        if (is_api_request()) {
            throw new InvalidSecretCodeException();
        }

        throw ValidationException::withMessages([
            'data.secret_code' => __('auth.secret_code'),
        ]);
    }

    private function throwAccountLockedException($user)
    {
        if (is_api_request()) {
            throw new AccountLockedException();
        }

        Auth::logout($user);
        throw ValidationException::withMessages([
            'data.secret_code' => __('filament-panels::pages/auth/login.messages.locked'),
        ]);
    }

    private function validSecretCode($user)
    {
        $user->update([
            'secret_code_mistakes_count' => 0,
            'secret_code_mistook_at' => null,
        ]);
    }
}
