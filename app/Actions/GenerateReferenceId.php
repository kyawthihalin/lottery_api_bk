<?php

namespace App\Actions;

class GenerateReferenceId
{
    public function execute(string $prefix, int $digit, int $pad_length = 13, int $random_int_length = 2)
    {
        $random_int = '';
        if ($random_int_length > 0) {
            $random_int = rand(pow(10, $random_int_length - 1), pow(10, $random_int_length) - 1);
        }

        return $prefix.
            sprintf("%0{$pad_length}d", $digit).
            $random_int;
    }
}
