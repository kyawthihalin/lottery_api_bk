<?php

namespace App\Actions;

use App\Exceptions\AccountLockedException;
use App\Exceptions\InvalidPasswordException;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use App\Settings\AuthSettings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class CheckPassword
{
    private int $maxAttempt;

    public function __construct(private string $class)
    {
        $this->maxAttempt = app(AuthSettings::class)->getPasswordMaxAttemptByClass($class);
    }

    public function execute(Admin|Senior|Agent|Master|User $user, string $password)
    {
        if (! Hash::check($password, $user->password)) {
            $this->invalidPassword($user);
        }

        $this->validPassword($user);
    }

    private function invalidPassword($user)
    {
        $attrName = $this->getIdentifierName($user);
        if (now()->diffInDays($user->password_mistook_at) >= 1) {
            $user->update([
                'password_mistakes_count' => 1,
                'password_mistook_at' => now(),
            ]);

            $this->throwInvalidPasswordException($attrName);
        }

        $mistakesCount = $user->password_mistakes_count + 1;

        if ($mistakesCount >= $this->maxAttempt) {
            $user->update([
                'password_mistakes_count' => $mistakesCount,
                'password_mistook_at' => now(),
                'locked_at' => now(),
            ]);

            $this->throwAccountLockedException($attrName);
        }

        $user->update([
            'password_mistakes_count' => $mistakesCount,
            'password_mistook_at' => now(),
        ]);

        $this->throwInvalidPasswordException($attrName);
    }

    private function validPassword($user)
    {
        $user->update([
            'password_mistakes_count' => 0,
            'password_mistook_at' => null,
        ]);
    }

    private function throwInvalidPasswordException(string $attrName)
    {
        if (is_api_request()) {
            throw new InvalidPasswordException();
        }

        throw ValidationException::withMessages([
            "data.{$attrName}" => __('filament-panels::pages/auth/login.messages.failed'),
        ]);
    }

    private function throwAccountLockedException(string $attrName)
    {
        if (is_api_request()) {
            throw new AccountLockedException();
        }

        throw ValidationException::withMessages([
            "data.{$attrName}" => __('filament-panels::pages/auth/login.messages.locked'),
        ]);
    }

    private function getIdentifierName($user)
    {
        return $user instanceof Admin ? 'email' : 'user_name';
    }
}
