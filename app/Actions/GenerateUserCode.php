<?php

namespace App\Actions;

class GenerateUserCode
{
    public function execute(string $class, string $prefix, int $length = 4, string $column = 'code'): string
    {
        $code = $prefix.get_random_digit($length);
        if ($class::where($column, '=', $code)->exists()) {
            return $this->execute($class, $prefix, $length, $column);
        }

        return $code;
    }
}
