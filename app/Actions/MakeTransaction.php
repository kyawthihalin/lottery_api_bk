<?php

namespace App\Actions;

use App\Contracts\Transaction;
use App\Contracts\TransactionUser;
use App\Enums\TransactionOperation;
use App\Enums\TransactionType;
use App\Exceptions\PointNotEnoughException;
use App\Models\TransactionLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MakeTransaction
{
    private Transaction $transaction;

    private TransactionLog $transactionLog;

    public function __construct(private TransactionType $transactionType, private int $point, private TransactionUser $user, private ?TransactionUser $otherUser = null, private array $otherFields = [])
    {
    }

    public function execute(): Transaction
    {
        DB::transaction(function () {
            if ($this->transactionType->isWithAdmin()) {
                [$beforePoint, $afterPoint] = $this->updatePoint($this->user, $this->transactionType->getUserOperation());

                $this->create($this->transactionType->getFunction())
                    ->log($this->user, $beforePoint, $afterPoint);

                return;
            }

            [$userBeforePoint, $userAfterPoint] = $this->updatePoint($this->user, $this->transactionType->getUserOperation());
            [$otherUserBeforePoint, $otherUserAfterPoint] = $this->updatePoint($this->otherUser, $this->transactionType->getOtherUserOperation());

            $this->create($this->transactionType->getFunction())
                ->log($this->user, $userBeforePoint, $userAfterPoint)
                ->log($this->otherUser, $otherUserBeforePoint, $otherUserAfterPoint);
        }, 5);

        return $this->transaction;
    }

    private function create(mixed $function)
    {
        $transaction = $this->user->$function()->create([
            'transaction_type' => $this->transactionType,
            'reference_id' => Str::uuid(),
            'point' => $this->point,
            ...$this->otherFields,
        ]);

        $transaction->refresh();

        $transaction->update([
            'reference_id' => (new GenerateReferenceId())->execute($transaction->transaction_type->getPrefix(), $transaction->id, random_int_length: 2),
        ]);

        $transaction->refresh();

        $this->transaction = $transaction;

        return $this;
    }

    private function updatePoint(TransactionUser $user, TransactionOperation $operation): array
    {
        $user = get_class($user)::lockForUpdate()->find($user->id);

        if ($operation == TransactionOperation::Substraction && $user->point < $this->point) {
            // dd(get_class($user), $user->point, $this->point, $this->transactionType);
            throw new PointNotEnoughException($this->transactionType);
        }

        $beforePoint = $user->point;
        $afterPoint = $operation == TransactionOperation::Addition ? $beforePoint + $this->point : $beforePoint - $this->point;

        $user->update([
            'point' => $afterPoint,
        ]);

        return [$beforePoint, $afterPoint];
    }

    private function log(TransactionUser $user, int $beforePoint, int $afterPoint)
    {
        $this->transactionLog = $user->transactionLogs()->create([
            'transaction_type' => $this->transactionType,
            'point' => $this->point,
            'before_point' => $beforePoint,
            'after_point' => $afterPoint,
            'loggable_id' => $this->transaction->id,
            'loggable_type' => get_class($this->transaction),
        ]);

        $this->transactionLog->refresh();

        return $this;
    }
}
