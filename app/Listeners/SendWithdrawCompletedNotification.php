<?php

namespace App\Listeners;

use App\Events\WithdrawCompleted;
use Filament\Notifications\Events\DatabaseNotificationsSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendWithdrawCompletedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(WithdrawCompleted $event): void
    {
        $user = $event->deposit->user;
        Notification::send($user, new \App\Notifications\User\WithdrawCompleted($event->deposit));
        event(new DatabaseNotificationsSent($user));
        Notification::send($user->parent, new \App\Notifications\Agent\WithdrawCompleted($event->deposit));
        event(new DatabaseNotificationsSent($user->parent));
    }
}
