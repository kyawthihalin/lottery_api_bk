<?php

namespace App\Listeners;

use App\Events\SettlementCompleted;
use App\Models\Admin;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendSettlementCompletedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(SettlementCompleted $event): void
    {
        $settlement = $event->settlement;

        $user = $settlement->user;
        $parent = $settlement->user->parent;

        if ($parent instanceof Admin) {
            $parent = Admin::whereNot('id', $parent->id)->get();
        }

        Notification::send($user, new \App\Notifications\SettlementCompleted($event->settlement));
        Notification::send($parent, new \App\Notifications\SettlementCompleted($event->settlement));
    }
}
