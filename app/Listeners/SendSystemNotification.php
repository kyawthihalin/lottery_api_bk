<?php

namespace App\Listeners;

use App\Events\SystemNotificationSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendSystemNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(SystemNotificationSent $event): void
    {
        Notification::send($event->users, new \App\Notifications\SystemNotification($event->data));
    }
}
