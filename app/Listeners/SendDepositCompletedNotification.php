<?php

namespace App\Listeners;

use App\Events\DepositCompleted;
use Filament\Notifications\Events\DatabaseNotificationsSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendDepositCompletedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(DepositCompleted $event): void
    {
        $user = $event->deposit->user;
        Notification::send($user, new \App\Notifications\User\DepositCompleted($event->deposit));
        event(new DatabaseNotificationsSent($user));
        Notification::send($user->parent, new \App\Notifications\Agent\DepositCompleted($event->deposit));
        event(new DatabaseNotificationsSent($user->parent));
    }
}
