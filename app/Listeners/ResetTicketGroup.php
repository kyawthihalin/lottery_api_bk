<?php

namespace App\Listeners;

use App\Events\TicketPurchased;
use App\Facades\Draw;
use App\Models\TicketGroup;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetTicketGroup implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TicketPurchased $event): void
    {
        $bet = $event->bet;

        foreach ($bet->tickets as $ticket) {
            $group = TicketGroup::where('draw_id', $bet->draw_id)->whereJsonContains('numbers', $ticket)->first();

            if ($group) {
                $group->update([
                    'numbers' => Draw::getRandomTickets(10),
                ]);
            }
        }
    }
}
