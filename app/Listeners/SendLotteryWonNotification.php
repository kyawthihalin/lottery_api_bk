<?php

namespace App\Listeners;

use App\Events\LotteryWon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendLotteryWonNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(LotteryWon $event): void
    {
        $user = $event->user;
        Notification::send($user, new \App\Notifications\User\LotteryWon($event->tickets));
        Notification::send($user->agent, new \App\Notifications\Agent\LotteryWon($user, $event->tickets));
        Notification::send($user->agent->master, new \App\Notifications\Agent\LotteryWon($user, $event->tickets));
        Notification::send($user->agent->master->senior, new \App\Notifications\Agent\LotteryWon($user, $event->tickets));
    }
}
