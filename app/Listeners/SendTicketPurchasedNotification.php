<?php

namespace App\Listeners;

use App\Events\TicketPurchased;
use Filament\Notifications\Events\DatabaseNotificationsSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendTicketPurchasedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TicketPurchased $event): void
    {
        $bet = $event->bet;
        $user = $bet->user;
        Notification::send($user, new \App\Notifications\User\TicketPurchased($event->bet));
        event(new DatabaseNotificationsSent($user));
        Notification::send($user->agent, new \App\Notifications\Agent\TicketPurchased($event->bet));
        event(new DatabaseNotificationsSent($user->agent));

    }
}
