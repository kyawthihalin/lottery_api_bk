<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $title = $this->data['title'];
        $body = $this->data['body'];

        if (is_json($title)) {
            $titleArr = json_decode($title, true);

            $title = __($titleArr['key'], $titleArr['replace']);
        }

        if (is_json($body)) {
            $bodyArr = json_decode($body, true);

            $body = __($bodyArr['key'], $bodyArr['replace']);
        }

        return [
            'id' => $this->id,
            'title' => $title,
            'body' => $body,
            'date' => $this->created_at->format('d M Y'),
            'read' => (bool) $this->read_at,
        ];
    }
}
