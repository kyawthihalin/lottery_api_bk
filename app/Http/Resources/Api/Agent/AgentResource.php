<?php

namespace App\Http\Resources\Api\Agent;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AgentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $parentType = $this->parent_type;

        return [
            'name' => $this->name,
            'point' => $this->point,
            'user_name' => $this->user_name,
            'phone' => $this->phone,
            'nrc' => $this->nrc,
            'language' => $this->language->value,
            "{$parentType}_user_name" => $this->parent->user_name,
            "{$parentType}_phone" => $this->parent->phone,
            'unread_noti_count' => $this->unread_noti_count > 9 ? '9+' : $this->unread_noti_count,
            // 'live_opening' => LiveOpening::first()->link,
            'profile_image' => $this->profile_image_url,
        ];
    }
}
