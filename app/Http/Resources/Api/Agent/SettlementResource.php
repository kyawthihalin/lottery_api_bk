<?php

namespace App\Http\Resources\Api\Agent;

use App\Facades\Draw;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SettlementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'date_time' => [
                'label' => __('general.date_time'),
                'value' => $this->created_at->format('d M Y h:i A'),
            ],
            'id' => [
                'label' => __('general.id'),
                'value' => $this->user->user_name,
            ],
            'balance_point' => [
                'label' => __('settlement.form.balance_point.label'),
                'value' => number_format($this->balance_point),
            ],
            'bought_point' => [
                'label' => __('settlement.form.bought_point.label'),
                'value' => number_format($this->bought_point),
            ],
            'rewithdrawn_point' => [
                'label' => __('settlement.form.rewithdrawn_point.label'),
                'value' => number_format($this->rewithdrawn_point),
            ],
            'paid_point' => [
                'label' => __('settlement.form.paid_point.label'),
                'value' => number_format($this->paid_point),
            ],
            'regained_point' => [
                'label' => __('settlement.form.regained_point.label'),
                'value' => number_format($this->regained_point),
            ],
            'used_point' => [
                'label' => __('settlement.form.used_point.label'),
                'value' => number_format($this->used_point),
            ],
            'point_rate' => [
                'label' => ___('general.point_rates'),
                'value' => number_format($this->point_rate),
            ],
            'profit' => [
                'label' => __('settlement.form.profit.label'),
                'value' => number_format($this->profit),
            ],
            'paid_above' => [
                'label' => __('settlement.form.paid_above.label'),
                'value' => number_format($this->paid_above),
            ],
            'net_paid_above' => [
                'label' => __('settlement.form.net_paid_above.label'),
                'value' => number_format($this->net_paid_above),
            ],
            'total_won_lottery' => [
                'label' => __('general.total_won_lottery'),
                'value' => number_format(Draw::getTotalWonLottery($this->user)),
            ],
            'total_balance_point' => [
                'label' => __('settlement.form.total_balance.label'),
                'value' => number_format($this->total_balance_point),
            ],
        ];
    }
}
