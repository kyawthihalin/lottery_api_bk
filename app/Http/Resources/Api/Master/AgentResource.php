<?php

namespace App\Http\Resources\Api\Master;

use App\Models\Agent;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AgentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            'name' => $this->name,
            'point' => $this->point,
            'user_name' => $this->user_name,
            'point_rate' => $this->point_rate,
        ];

        if ($this->resource instanceof Agent) {
            $data['code'] = $this->code;
        }

        return $data;
    }
}
