<?php

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->name,
            'point' => $this->point,
            'user_name' => $this->user_name,
            'phone' => $this->phone,
            'nrc' => $this->nrc,
            'language' => $this->language->value,
            'agent_code' => $this->agent->code,
            'agent_phone' => $this->agent->phone,
            'current_ticket_count' => $this->current_ticket_count,
            'unread_noti_count' => $this->unread_noti_count > 9 ? '9+' : $this->unread_noti_count,
            // 'live_opening' => LiveOpening::first()->link,
        ];
    }
}
