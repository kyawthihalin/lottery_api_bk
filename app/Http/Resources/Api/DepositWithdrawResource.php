<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DepositWithdrawResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $transaction = $this->loggable;
        $data = [
            'type' => $this->transaction_type->getBreifCode(),
            'by' => [
                'label' => __('general.by'),
                'value' => $transaction->user->parent->name,
            ],
            'to' => [
                'label' => __('general.to'),
                'value' => $this->user->name,
            ],
            'reference_id' => [
                'label' => __('general.reference_id'),
                'value' => $transaction->reference_id,
            ],
            'point' => [
                'label' => __('general.point'),
                'value' => number_format($this->point),
            ],
            'amount' => [
                'label' => __('general.amount'),
                'value' => number_format($transaction->point_rate * $this->point),
            ],
            'point_before' => [
                'label' => __('general.point_before'),
                'value' => number_format($this->before_point),
            ],
            'point_after' => [
                'label' => __('general.point_after'),
                'value' => number_format($this->after_point),
            ],
            'date_time' => [
                'label' => __('general.date_time'),
                'value' => $this->created_at->format('d M Y, h:i A'),
            ],
        ];

        return $data;
    }
}
