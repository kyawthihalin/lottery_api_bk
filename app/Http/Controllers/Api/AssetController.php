<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\StorageService;
use App\Settings\BannerSettings;
use App\Settings\GeneralSettings;
use App\Settings\ImageSettings;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $banners = app(BannerSettings::class)->getBannersByPath($request->path());

        $imageArray = [];

        if (count($banners)) {
            foreach ($banners as $path) {
                array_push($imageArray, StorageService::getUrl($path));
            }
        }

        return $this->responseSuccess([
            'banners' => $imageArray,
            'group_frame' => StorageService::getUrl(app(ImageSettings::class)->group_ticket_frame_mobile),
            'ticket_background' => StorageService::getUrl(app(ImageSettings::class)->ticket_background),
            'app_background' => StorageService::getUrl(app(ImageSettings::class)->app_background),
            'website_background' => StorageService::getUrl(app(ImageSettings::class)->website_background),
            'won_image' => app(ImageSettings::class)->won_image,
            'lost_image' => app(ImageSettings::class)->lost_image,
            'viber_contact' => app(GeneralSettings::class)->contact_viber,
        ]);
    }
}
