<?php

namespace App\Http\Controllers\Api;

use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\NotificationResource;
use App\Notifications\User\LotteryWon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $notifications = NotificationResource::collection($request->user()->notifications()->paginate($request->per_page ?? 15));

        return $this->responseSuccess([
            'notifications' => $notifications,
            'next' => $notifications->nextPageUrl() ?? null,
        ]);
    }

    public function read(Request $request)
    {
        $request->validate([
            'id' => ['required', 'uuid'],
        ]);

        $notification = $request->user()->notifications()->where('id', $request->id)->first();

        if (! $notification) {
            return $this->responseBadRequest('notification not found');
        }

        $notification->markAsRead();

        return $this->responseSuccess();
    }

    public function readAll(Request $request)
    {
        foreach ($request->user()->unreadNotifications as $notification) {
            $notification->markAsRead();
        }

        return $this->responseSuccess();
    }

    public function won(Request $request)
    {
        $previousDrawId = Draw::getPreviousDrawId();
        $user = $request->user();

        $wonNotifications = $user->notifications()->whereType(LotteryWon::class)->whereNull('read_at')->whereDate('created_at', '>=', $previousDrawId)->get();

        $messages = [];

        foreach ($wonNotifications as $notification) {
            $body = json_decode($notification->data['body'], true);

            $messages[] = __($body['key'], $body['replace']);
        }

        return $this->responseSuccess([
            'messages' => count($messages) ? $messages : null,
        ]);
    }

    public function readWon(Request $request)
    {
        $previousDrawId = Draw::getPreviousDrawId();
        $user = $request->user();

        $wonNotifications = $user->notifications()->whereType(LotteryWon::class)->whereNull('read_at')->whereDate('created_at', '>=', $previousDrawId)->get();

        foreach ($wonNotifications as $notification) {
            $notification->update([
                'read_at' => now(),
            ]);
        }

        return $this->responseSuccess();
    }
}
