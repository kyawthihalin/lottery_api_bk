<?php

namespace App\Http\Controllers\Api;

use App\Actions\MakeTransaction;
use App\Enums\TransactionType;
use App\Events\DepositCompleted;
use App\Events\WithdrawCompleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DepositRequest;
use App\Http\Requests\Api\WithdrawRequest;
use App\Http\Requests\DepositWithdraw\UserHistoryRequest;
use App\Http\Resources\Api\DepositWithdrawResource;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\TransactionLog;
use App\Models\User;
use App\Settings\PointRateSettings;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class DepositWithdrawController extends Controller
{
    public function userHistories(UserHistoryRequest $request)
    {
        $user = $request->user();
        $userIds = $user instanceof User ? [$user->id] : $user->users()->where(function (Builder $query) use ($request) {
            if ($request->has('user_name')) {
                $query->where('users.user_name', $request->user_name);
            }
        })->pluck('users.id')->toArray();

        $histories = TransactionLog::whereIn('transaction_type', [
            TransactionType::UserDeposit->value,
            TransactionType::UserWithdraw->value,
        ])
            ->whereIn('user_id', $userIds)
            ->whereUserType(User::class)
            ->where(function (Builder $query) use ($request) {
                if ($request->has('date')) {
                    $query->whereDate('created_at', Carbon::parse($request->date));
                }
            })
            ->orderBy('created_at', 'desc')->paginate($request->per_page ?? 10);

        return $this->responseSuccess([
            'histories' => DepositWithdrawResource::collection($histories),
            'next' => $histories->nextPageUrl() ?? null,
        ]);
    }

    public function agentHistories(UserHistoryRequest $request)
    {
        $user = $request->user();
        $userIds = $user instanceof Agent ? [$user->id] : $user->agents()->where(function (Builder $query) use ($request) {
            if ($request->has('code')) {
                $query->where('agents.user_name', $request->user_name);
            }
        })->pluck('agents.id')->toArray();

        $histories = TransactionLog::whereIn('transaction_type', [
            TransactionType::AgentDeposit->value,
            TransactionType::AgentWithdraw->value,
        ])
            ->whereIn('user_id', $userIds)
            ->whereUserType(Agent::class)
            ->where(function (Builder $query) use ($request) {
                if ($request->has('date')) {
                    $query->whereDate('created_at', Carbon::parse($request->date));
                }
            })
            ->orderBy('created_at', 'desc')->paginate($request->per_page ?? 10);

        return $this->responseSuccess([
            'histories' => DepositWithdrawResource::collection($histories),
            'next' => $histories->nextPageUrl() ?? null,
        ]);
    }

    public function masterHistories(UserHistoryRequest $request)
    {
        $user = $request->user();
        $userIds = $user instanceof Master ? [$user->id] : $user->masters()->where(function (Builder $query) use ($request) {
            if ($request->has('code')) {
                $query->where('masters.user_name', $request->user_name);
            }
        })->pluck('masters.id')->toArray();

        $histories = TransactionLog::whereIn('transaction_type', [
            TransactionType::MasterDeposit->value,
            TransactionType::MasterWithdraw->value,
        ])
            ->whereIn('user_id', $userIds)
            ->whereUserType(Master::class)
            ->where(function (Builder $query) use ($request) {
                if ($request->has('date')) {
                    $query->whereDate('created_at', Carbon::parse($request->date));
                }
            })
            ->orderBy('created_at', 'desc')->paginate($request->per_page ?? 10);

        return $this->responseSuccess([
            'histories' => DepositWithdrawResource::collection($histories),
            'next' => $histories->nextPageUrl() ?? null,
        ]);
    }

    public function seniorHistories(UserHistoryRequest $request)
    {
        $user = $request->user();
        $histories = TransactionLog::whereIn('transaction_type', [
            TransactionType::SeniorDeposit->value,
            TransactionType::SeniorWithdraw->value,
        ])
            ->where('user_id', $user->id)
            ->whereUserType(Senior::class)
            ->where(function (Builder $query) use ($request) {
                if ($request->has('date')) {
                    $query->whereDate('created_at', Carbon::parse($request->date));
                }
            })
            ->orderBy('created_at', 'desc')->paginate($request->per_page ?? 10);

        return $this->responseSuccess([
            'histories' => DepositWithdrawResource::collection($histories),
            'next' => $histories->nextPageUrl() ?? null,
        ]);
    }

    public function deposit(DepositRequest $request)
    {
        $otherUser = $request->user();
        $childFunction = $this->getChildFunctionByClass($otherUser);

        $user = $otherUser->$childFunction()->whereUserName($request->user_name)->first();

        if (! $user) {
            return $this->responseBadRequest('User not found.');
        }

        $deposit = (new MakeTransaction(
            transactionType: $this->getTransactionType(),
            point: $request->point,
            user: $user,
            otherUser: $otherUser,
            otherFields: [
                'point_rate' => $user instanceof User ? app(PointRateSettings::class)->max_agent : $user->point_rate,
            ]
        ))->execute();

        DepositCompleted::dispatch($deposit);

        return $this->responseSuccess(message: __('deposit.completed'));
    }

    public function withdraw(WithdrawRequest $request)
    {
        $otherUser = $request->user();
        $childFunction = $this->getChildFunctionByClass($otherUser);

        $user = $otherUser->$childFunction()->whereUserName($request->user_name)->first();

        if (! $user) {
            return $this->responseBadRequest('User not found.');
        }

        $withdraw = (new MakeTransaction(
            transactionType: $this->getTransactionType(false),
            point: $request->point,
            user: $user,
            otherUser: $otherUser,
            otherFields: [
                'point_rate' => $user instanceof User ? app(PointRateSettings::class)->max_agent : $user->point_rate,
            ]
        ))->execute();

        WithdrawCompleted::dispatch($withdraw);

        return $this->responseSuccess(message: __('withdraw.completed'));
    }

    private function getTransactionType($deposit = true): TransactionType
    {
        $user = auth()->user();

        if ($deposit) {
            return match (get_class($user)) {
                Agent::class => TransactionType::UserDeposit,
                Master::class => TransactionType::AgentDeposit,
                Senior::class => TransactionType::MasterDeposit,
            };
        }

        return match (get_class($user)) {
            Agent::class => TransactionType::UserWithdraw,
            Master::class => TransactionType::AgentWithdraw,
            Senior::class => TransactionType::MasterWithdraw,
        };
    }

    private function getChildFunctionByClass(Agent|Master|Senior $user): string
    {
        return match (get_class($user)) {
            Agent::class => 'users',
            Master::class => 'agents',
            Senior::class => 'masters',
        };
    }
}
