<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\User\TicketGroupResource;
use App\Models\TicketGroup;
use App\Settings\TicketSettings;

class TicketController extends Controller
{
    public function groups()
    {
        $ticketGroups = TicketGroup::all();

        return $this->responseSuccess([
            'groups' => TicketGroupResource::collection($ticketGroups),
            'purchase_limit' => app(TicketSettings::class)->tickets_limit_per_purchase,
        ]);
    }
}
