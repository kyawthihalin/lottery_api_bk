<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\MakeTransaction;
use App\Enums\TransactionType;
use App\Events\TicketPurchased;
use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\PurchaseHistoryRequest;
use App\Http\Requests\Api\User\PurchaseRequest;
use App\Http\Resources\Api\User\BetResource;

class PurchaseController extends Controller
{
    public function purchase(PurchaseRequest $request)
    {
        $tickets = $request->tickets;

        if (Draw::isLotteryDate(now())) {
            return $this->responseBadRequest(__('general.errors.purchase.lottery_day'));
        }

        $user = $request->user();

        if ($user->point < count($tickets)) {
            return $this->responseBadRequest(__('general.errors.point_not_enough', [
                'transaction_type' => TransactionType::Bet->getBreifLabel(),
            ]));
        }

        $purchasedTickets = [];
        $drawId = Draw::getPendingDrawId();

        foreach ($tickets as $ticket) {
            if (
                Draw::isPurchasedTicket($user, $ticket, $drawId) ||
                Draw::isPurchaseLimitReached($ticket, $drawId) ||
                Draw::isRestrictedTicket($ticket)
            ) {
                array_push($purchasedTickets, $ticket);
            }
        }

        if (count($purchasedTickets)) {
            return $this->responseError(__('ticket.already_purchased'), [
                'purchased_tickets' => array_unique($purchasedTickets),
            ], 422);
        }

        $transaction = (new MakeTransaction(
            transactionType: TransactionType::Bet,
            point: count($tickets),
            user: $user,
            otherFields: [
                'draw_id' => $drawId,
                'tickets' => $tickets,
            ]
        ))->execute();

        TicketPurchased::dispatch($transaction);

        return $this->responseSuccess(message: __('ticket.purchased'));
    }

    public function histories(PurchaseHistoryRequest $request)
    {
        $drawIds = [];
        $bets = $request->user()->bets()->where(function ($query) use ($request, $drawIds) {
            if ($request->has('draw_id')) {
                $drawIds[] = $request->draw_id;
                $query->whereDate('draw_id', $request->draw_id);

                return;
            }
            $drawIds = Draw::getPreviousDrawIds(24);

            $oldestDrawId = $drawIds[count($drawIds) - 1];
            $query->whereDate('draw_id', '>=', $oldestDrawId);
        })
        ->orderBy('draw_id', 'desc')
        ->get();

        $histories = BetResource::collection($bets)->groupBy(function ($data) {
            return $data->draw_id;
        });

        return $this->responseSuccess([
            'histories' => $histories,
        ]);
    }
}
