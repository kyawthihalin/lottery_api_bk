<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\ValidateHiddenFields;
use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\CheckResultRequest;
use App\Http\Resources\Api\User\ResultResource;
use App\Models\Result;
use App\Models\ResultType;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function index(Request $request)
    {
        $previousDrawId = Draw::getPreviousDrawId();
        $currentYear = Carbon::parse($previousDrawId)->year;

        (new ValidateHiddenFields())
            ->execute([
                'year' => ['required', 'integer', 'max:'.$currentYear, 'date_format:Y'],
                'month' => ['required', 'date_format:F'],
            ]);

        $resultTypes = ResultType::all();

        $results = [];
        $drawDate = Carbon::parse($request->year.'-'.$request->month);

        $drawIds = Draw::getDrawIdsByMonth($drawDate->format('Y-m-d'));

        foreach ($resultTypes as $resultType) {
            $resultsByDraw = $resultType->results()->whereIn('draw_id', $drawIds)->get();

            if (! count($resultsByDraw)) {
                continue;
            }

            foreach ($resultsByDraw as $result) {
                $results[$result->draw_id][$resultType->name] = new ResultResource($result);
            }
        }

        return $this->responseSuccess([
            'results' => count($results) ? $results : null,
        ]);
    }

    public function check(CheckResultRequest $request)
    {
        if ($request->has('from_ticket')) {
            $from = ltrim($request->from_ticket, '0');
            $to = ltrim($request->to_ticket, '0');

            if ($from >= $to) {
                return $this->responseBadRequest('To ticket number must be greater than from ticket number');
            }
        }

        $results = Result::whereDate('draw_id', $request->draw_id)->get();
        $resultTypes = ResultType::all();
        $filteredResults = [];
        foreach ($resultTypes as $resultType) {
            $filteredResults[$resultType->code->value] = [
                'code' => $resultType->code->value,
                'name' => $resultType->name,
                'amount' => null,
                'tickets' => [],
            ];
        }

        foreach ($results as $result) {
            $resultType = $result->resultType;
            $code = $result->resultType->code->value;

            $filteredResults[$code]['amount'] = $result->price;
            if ($request->has('ticket_numbers')) {
                $wonTickets = Draw::findWonNumbers($resultType->code, $result->numbers, $request->ticket_numbers);

                if (count($wonTickets)) {
                    array_push($filteredResults[$code]['tickets'], ...$wonTickets);
                }
            }

            if ($request->has('from_ticket')) {
                $from = (int) ltrim($request->from_ticket, '0');
                $to = (int) ltrim($request->to_ticket, '0');

                while ($from < $to) {
                    $ticketNumber = str_pad($from, 6, '0', STR_PAD_LEFT);

                    $wonTickets = Draw::findWonNumbers($resultType->code, $result->numbers, [$ticketNumber]);

                    if ($wonTickets) {
                        array_push($filteredResults[$code]['tickets'], ...$wonTickets);
                    }

                    $from++;
                }
            }

            $filteredResults[$code]['tickets'] = array_unique($filteredResults[$code]['tickets']);
        }

        foreach ($filteredResults as $key => $filteredResult) {
            if (! count($filteredResult['tickets'])) {
                unset($filteredResults[$key]);
            }
        }

        return $this->responseSuccess([
            'results' => array_values($filteredResults),
        ]);
    }

    public function numbers()
    {
        $last_draw = Draw::getPreviousDrawId();

        $results = Result::where('draw_id', Carbon::parse($last_draw))->get();
        $numbers = [];

        foreach ($results as $result) {
            $resultType = $result->resultType;

            $modifiedArray = array_map(function ($element) use ($resultType) {
                return $resultType->code->getPrefix().$element.$resultType->code->getSuffix();
            }, $result->numbers);
            $numbers = [...$numbers, ...$modifiedArray];
        }
        // $ticket_background_photo = AppFile::whereType('ticket background mobile')->pluck('file')->first();

        return $this->responseSuccess([
            // 'photo' => (new GetTicketPhotoUrl())->execute(),
            // 'ticket_background' => url('/') . '/' . $ticket_background_photo,
            'numbers' => $numbers,
        ]);
    }
}
