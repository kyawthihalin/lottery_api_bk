<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\ValidateHiddenFields;
use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Settings\GeneralSettings;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DrawIdController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function drawIds(Request $request)
    {
        (new ValidateHiddenFields())
            ->execute([
                'count' => ['numeric', 'integer', 'max:24'],
            ]);

        return $this->responseSuccess([
            'draw_ids' => Draw::getPreviousDrawIds($request->count ?? 6),
        ]);
    }

    public function previousDraw()
    {
        return $this->responseSuccess([
            'previous_draw' => Draw::getPreviousDrawId(),
        ]);
    }

    /**
     * Handle the incoming request.
     */
    public function months(Request $request)
    {
        $previousDrawId = Draw::getPreviousDrawId();
        $currentYear = Carbon::parse($previousDrawId)->year;

        (new ValidateHiddenFields())
            ->execute([
                'year' => ['required', 'integer', 'max:'.$currentYear, 'date_format:Y'],
            ]);

        if ($currentYear == $request->year) {
            $startDate = Carbon::parse($previousDrawId)->startOfMonth();
        } else {
            $startDate = Carbon::parse($request->year.'-12-01');
        }

        $startMonth = $startDate->month;

        while ($startMonth > 0) {
            $months[] = $startDate->format('F');
            $startDate->subMonth();
            $startMonth--;
        }

        return $this->responseSuccess([
            'months' => $months,
        ]);
    }

    public function live() {
        return $this->responseSuccess([
            'live' => app(GeneralSettings::class)->live_link,
        ]);
    }
}
