<?php

namespace App\Http\Controllers\Api\User;

use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserResult;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WinnerController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $previousDrawIds = Draw::getPreviousDrawIds(2);

        $results = [];

        foreach ($previousDrawIds as $drawId) {
            $userResults = UserResult::where('draw_id', Carbon::parse($drawId))->get();
            $drawResults = [];
            foreach ($userResults as $key => $betResult) {
                $resultType = $betResult->result->resultType;
                $user = $betResult->user;

                $drawResults[$resultType->name][] = [
                    'ticket_numbers' => $betResult->tickets,
                    'amount' => $betResult->result->price,
                    'name' => $user->name,
                    'agent_code' => $user->agent->code,
                ];
            }
            $results[$drawId] = count($drawResults) ? $drawResults : null;
        }

        return $this->responseSuccess([
            'results' => $results,
        ]);
    }

    public function won()
    {
        $user = User::find(auth()->user()->id);
        $previousDrawIds = Draw::getPreviousDrawIds(24);

        $results = [];

        foreach ($previousDrawIds as $drawId) {
            $userResults = UserResult::where('draw_id', Carbon::parse($drawId))->where('user_id', $user->id)->get();
            $drawResults = [];

            if (! count($userResults)) {
                continue;
            }

            foreach ($userResults as $key => $betResult) {
                $resultType = $betResult->result->resultType;
                $user = $betResult->user;

                $drawResults[$resultType->code->value][] = [
                    'ticket_numbers' => $betResult->tickets,
                    'amount' => $betResult->result->price,
                    'name' => $user->name,
                    'agent_code' => $user->agent->code,
                ];
            }

            $results[$drawId] = $drawResults;
        }

        return $this->responseSuccess([
            'won' => count($results) ? $results : null,
        ]);
    }
}
