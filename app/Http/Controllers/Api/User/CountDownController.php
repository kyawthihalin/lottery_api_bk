<?php

namespace App\Http\Controllers\Api\User;

use App\Facades\Draw;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CountDownController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $currentDraw = Carbon::parse(Draw::getPendingDrawId());

        return $this->responseSuccess([
            'day_diff' => $currentDraw->diffInDays(now()),
            'current_draw' => $currentDraw->format('Y-m-d'),
        ]);
    }
}
