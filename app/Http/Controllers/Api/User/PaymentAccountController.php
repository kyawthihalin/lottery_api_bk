<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\ValidateHiddenFields;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\CreatePaymentAccountRequest;
use App\Http\Resources\Api\User\PaymentAccountResource;
use App\Http\Resources\Api\User\PaymentAccountTypeResource;
use App\Models\PaymentAccountType;
use Illuminate\Http\Request;

class PaymentAccountController extends Controller
{
    public function types(Request $request)
    {
        return $this->responseSuccess([
            'types' => PaymentAccountTypeResource::collection(PaymentAccountType::all()),
        ]);
    }

    public function index(Request $request)
    {
        return $this->responseSuccess([
            'accounts' => PaymentAccountResource::collection($request->user()->paymentAccounts),
        ]);
    }

    public function create(CreatePaymentAccountRequest $request)
    {
        $type = PaymentAccountType::whereName($request->type)->first();
        if (! $type) {
            return $this->responseBadRequest('Type not found');
        }

        $exist = $request->user()->paymentAccounts()->where('number', $request->number)->where('payment_account_type_id', $type->id)->first();

        if($exist) {
            return $this->responseBadRequest('Account already exist with this number and account type.');
        }

        $paymentAccount = $request->user()->paymentAccounts()->create([
            'payment_account_type_id' => $type->id,
            'name' => $request->name,
            'number' => $request->number,
        ]);

        return $this->responseSuccess([
            'account' => new PaymentAccountResource($paymentAccount->refresh()),
        ]);
    }

    public function delete(Request $request){
        (new ValidateHiddenFields())->execute([
            'id' => ['required', 'uuid'],
        ]);

        $paymentAccount = $request->user()->paymentAccounts()->find($request->id);

        if(!$paymentAccount){
            return $this->responseBadRequest('Account not found');
        }

        $paymentAccount->delete();

        return $this->responseSuccess();
    }
}
