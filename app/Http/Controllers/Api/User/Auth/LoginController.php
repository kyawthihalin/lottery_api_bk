<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Actions\CheckPassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\LoginRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(LoginRequest $request)
    {
        $user = User::whereUserName($request->user_name)->first();

        if (! $user) {
            return $this->responseBadRequest(message: __('auth.failed_api.user'));
        }

        if ((bool) $user->locked_at) {
            return $this->responseLocked();
        }

        (new CheckPassword(User::class))->execute($user, $request->password);

        $user->update([
            'device_name' => $request->device_name,
            'device_model' => $request->device_model,
            'os_version' => $request->os_version,
            'os_type' => $request->os_type,
            'app_version' => $request->app_version,
            'firebase_token' => $request->firebase_token,
            'language' => $request->language,
        ]);

        return $this->responseSuccess(data: [
            'token' => $user->createToken('USER TOKEN')->plainTextToken,
            'user' => new UserResource($user),
        ]);
    }
}
