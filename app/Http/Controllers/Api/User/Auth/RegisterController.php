<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\Agent;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(RegisterRequest $request)
    {
        $agent = Agent::whereCode($request->agent_code)->first();

        if (! $agent) {
            return $this->responseBadRequest(__('general.errors.invalid.agent_code'));
        }

        $user = $agent->users()->create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'password' => bcrypt($request->password),
            'device_name' => $request->device_name,
            'device_model' => $request->device_model,
            'os_version' => $request->os_version,
            'os_type' => $request->os_type,
            'app_version' => $request->app_version,
            'firebase_token' => $request->firebase_token,
            'language' => $request->language,
        ]);

        return $this->responseSuccess(data: [
            'token' => $user->createToken('USER TOKEN')->plainTextToken,
            'user' => new UserResource($user->refresh()),
        ]);
    }
}
