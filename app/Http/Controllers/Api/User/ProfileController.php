<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\UpdateProfileRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function index(Request $request)
    {
        return $this->responseSuccess([
            'user' => new UserResource($request->user()),
        ]);
    }

    public function update(UpdateProfileRequest $request)
    {
        $user = User::find(auth()->id());
        $user->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'language' => $request->language,
            'nrc' => $request->nrc,
        ]);

        return $this->responseSuccess([
            'user' => new UserResource($user),
        ]);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required|string|current_password:user_api',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::find(auth()->id());
        $user->update([
            'password' => bcrypt($request->password),
        ]);

        return $this->responseSuccess([
            'user' => new UserResource($user),
        ]);
    }
}
