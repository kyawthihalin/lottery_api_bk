<?php

namespace App\Http\Controllers\Api;

use App\Enums\TransactionType;
use App\Exceptions\PointNotEnoughException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\WithdrawRequest;
use App\Http\Resources\Api\WithdrawResource;
use App\Models\Agent;
use App\Models\PaymentAccountType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WithdrawController extends Controller
{
    public function histories(Request $request)
    {
        $histories = WithdrawResource::collection($request->user()->withdraws()->orderBy('created_at', 'desc')->paginate(10));

        return $this->responseSuccess([
            'histories' => $histories,
            'next' => $histories->nextPageUrl() ?? null,
        ]);
    }

    public function withdraw(WithdrawRequest $request)
    {
        $paymentAccountType = PaymentAccountType::find($request->payment_account_type_id);

        if (! $paymentAccountType) {
            return $this->responseBadRequest('Invalid payment account type.');
        }

        DB::table(function () use ($request) {
            $userLocked = get_class($request->user())::lockForUpdate()->find(auth()->id());
            $agentLocked = Agent::lockForUpdate()->find($userLocked->agent_id);

            if ($userLocked->point < $request->point) {
                throw new PointNotEnoughException();
            }

            $userPointAfter = $userLocked->point - $request->point;
            $agentPointAfter = $agentLocked->point + $request->point;

            $userLocked->update([
                'point' => $userPointAfter,
            ]);

            $agentLocked->update([
                'point' => $agentPointAfter,
            ]);

            $withdraw = $userLocked->withdraws()->create([
                'transaction_type' => $this->getTransactionType(),
                'point' => $request->point,
                'reference_id' => Str::uuid(),
            ]);

            $file = $request->file('screenshot');
            $fileName = time().$file->getClientOriginalName();

            $path = 'deposit';

            $file->storeAs($path, $fileName);

            $deposit->refresh()->update([
                'reference_id' => (new GenerateReferenceId())->execute($deposit->transaction_type->getPrefix(), $deposit->id),
                'screenshot' => $path.'/'.$fileName,
            ]);

            DepositRequested::dispatch($deposit);
        });

        return $this->responseSuccess(message: __('recharge.requested'));
    }

    private function getTransactionType(): TransactionType
    {
        $user = auth()->user();

        return match (get_class($user)) {
            User::class => TransactionType::UserDeposit,
            Agent::class => TransactionType::AgentDeposit,
            Master::class => TransactionType::MasterDeposit,
            Senior::class => TransactionType::SeniorDeposit,
        };
    }
}
