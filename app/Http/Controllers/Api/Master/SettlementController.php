<?php

namespace App\Http\Controllers\Api\Master;

use App\Events\SettlementCompleted;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Agent\SettlementResource;
use App\Http\Resources\Api\Agent\SettlementStatResource;
use App\Models\Agent;
use App\Models\Settlement;
use Illuminate\Http\Request;

class SettlementController extends Controller
{
    public function current(Request $request)
    {
        $request->validate([
            'agent_ids' => ['array'],
            'agent_ids.*' => ['required', 'string'],
        ]);

        $master = $request->user();
        $users = [];

        if ($request->has('agent_ids')) {
            $users = Agent::whereMasterId($master->id)->whereIn('user_name', $request->agent_ids)->get();
        } else {
            $users = [$master];
        }

        return $this->responseSuccess([
            'histories' => SettlementStatResource::collection($users),
        ]);
    }

    public function histories(Request $request)
    {
        $request->validate([
            'agent_ids' => ['array'],
            'agent_ids.*' => ['required', 'string'],
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $master = $request->user();
        $settlements = null;

        if ($request->has('agent_ids')) {
            $agentIds = Agent::whereMasterId($master->id)->whereIn('user_name', $request->agent_ids)->pluck('id')->toArray();

            $settlements = Settlement::whereIn('user_id', $agentIds)->whereUserType(Agent::class)->orderBy('created_at', 'desc')->paginate($request->per_page ?? 5);
        } else {
            $settlements = $master->settlements()->orderBy('created_at', 'desc')->paginate($request->per_page ?? 5);
        }

        return $this->responseSuccess([
            'histories' => SettlementResource::collection($settlements),
            'next' => $settlements->nextPageUrl(),
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'agent_id' => ['required', 'string'],
        ]);

        $master = $request->user();
        $agent = Agent::whereMasterId($master->id)->where('user_name', $request->agent_id)->first();

        if (! $agent) {
            return $this->responseBadRequest('Agent not found');
        }

        $settlement = $agent->settlements()->create([
            'balance_point' => $agent->point,
            'bought_point' => $agent->bought_point,
            'rewithdrawn_point' => $agent->rewithdrawn_point,
            'paid_point' => $agent->paid_point,
            'regained_point' => $agent->regained_point,
            'used_point' => $agent->used_point,
            'point_rate' => $agent->point_rate,
            'profit' => $agent->profit,
            'paid_above' => $agent->paid_above,
            'net_paid_above' => $agent->net_paid_above,
            'total_balance_point' => $agent->total_balance,
        ]);

        SettlementCompleted::dispatch($settlement->refresh());

        return $this->responseSuccess(message: __('settlement.completed'));
    }
}
