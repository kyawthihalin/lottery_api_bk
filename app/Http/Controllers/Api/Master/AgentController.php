<?php

namespace App\Http\Controllers\Api\Master;

use App\Actions\GenerateUserCode;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Master\StoreAgentRequest;
use App\Http\Requests\Api\Master\UpdateAgentRequest;
use App\Http\Resources\Api\Master\AgentResource;
use App\Models\Agent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'search' => ['string'],
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $agents = $request->user()->agents()->where(function (Builder $query) use ($request) {
            $query->where('agents.user_name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('agents.name', 'LIKE', '%'.$request->search.'%');
        })->paginate($reuqest->per_page ?? 5);

        return $this->responseSuccess([
            'agents' => AgentResource::collection($agents),
            'next' => $agents->nextPageUrl() ?? null,
        ]);
    }

    public function store(StoreAgentRequest $request)
    {
        $agent = $request->user()->agents()->create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'password' => bcrypt($request->password),
            'secret_code' => bcrypt($request->secret_code),
            'point_rate' => $request->point_rate,
            'phone' => $request->phone,
            'code' => Str::uuid(),
        ]);

        $agent->update([
            'code' => (new GenerateUserCode())->execute(Agent::class, 'AG'),
        ]);

        return $this->responseSuccess([
            'agent' => new AgentResource($agent->refresh()),
        ]);
    }

    public function update(UpdateAgentRequest $request)
    {
        $agent = $request->user()->agents()->whereUserName($request->user_name)->first();

        if (! $agent) {
            return $this->responseBadRequest('Agent not found.');
        }

        $request->has('point_rate') && $agent->update([
            'point_rate' => $request->point_rate,
        ]);

        $request->has('name') && $agent->update([
            'name' => $request->name,
        ]);

        if ($request->has('password')) {
            $agent->update([
                'password' => bcrypt($request->password),
            ]);

            $agent->tokens()->delete();
        }

        if ($request->has('secret_code')) {
            $agent->update([
                'secret_code' => bcrypt($request->secret_code),
            ]);

            $agent->tokens()->delete();
        }

        return $this->responseSuccess([
            'agent' => new AgentResource($agent->refresh()),
        ]);
    }
}
