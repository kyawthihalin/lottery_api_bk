<?php

namespace App\Http\Controllers\Api\Master\Auth;

use App\Http\Controllers\Api\Agent\Auth\LoginController as AgentLoginController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Agent\Auth\LoginPasswordRequest;
use App\Http\Requests\Api\Agent\Auth\LoginSecretRequest;
use App\Models\Master;

class LoginController extends Controller
{
    public function loginPassword(LoginPasswordRequest $request)
    {
        return (new AgentLoginController())->loginPassword($request, Master::class);
    }

    public function loginSecret(LoginSecretRequest $request)
    {
        return (new AgentLoginController())->loginSecret($request, Master::class);
    }
}
