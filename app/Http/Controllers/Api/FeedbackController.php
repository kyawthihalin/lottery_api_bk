<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SubmitFeedbackRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class FeedbackController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(SubmitFeedbackRequest $request)
    {
        $feedback = $request->user()->feedbacks()->create([
            'title' => $request->title,
            'detail' => $request->detail,
        ]);

        Notification::send(Admin::all(), new \App\Notifications\FeedbackSubmitted($feedback->refresh()));

        return $this->responseSuccess();
    }
}
