<?php

namespace App\Http\Controllers\Api\Agent;

use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\PurchaseHistoryRequest;
use App\Models\UserResult;
use Carbon\Carbon;

class WinnerController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(PurchaseHistoryRequest $request)
    {
        $request->validate([
            'draw_ids.*' => ['array', 'min:1', 'max:4'],
            'draw_ids.*' => ['date', 'in:'.implode(',', Draw::getPreviousDrawIds(24))],
        ]);
        $drawIds = $request->draw_ids ?? Draw::getPreviousDrawIds(5);

        $results = [];

        $userIds = auth()->user()->users()->pluck('users.id');

        foreach ($drawIds as $drawId) {
            $userResults = UserResult::whereIn('user_id', $userIds)->where('draw_id', Carbon::parse($drawId))->get();
            $drawResults = [];
            foreach ($userResults as $key => $betResult) {
                $resultType = $betResult->result->resultType;
                $user = $betResult->user;

                $drawResults[$resultType->name][] = [
                    'ticket_numbers' => $betResult->tickets,
                    'amount' => $betResult->result->price,
                    'name' => $user->name,
                    'agent_code' => $user->agent->code,
                ];
            }
            $results[$drawId] = count($drawResults) ? $drawResults : null;
        }

        return $this->responseSuccess([
            'results' => $results,
        ]);
    }
}
