<?php

namespace App\Http\Controllers\Api\Agent\Auth;

use App\Actions\CheckPassword;
use App\Actions\CheckSecretCode;
use App\Enums\AppTokenAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Agent\Auth\LoginPasswordRequest;
use App\Http\Requests\Api\Agent\Auth\LoginSecretRequest;
use App\Http\Resources\Api\Agent\AgentResource;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Services\AppTokenService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function loginPassword(LoginPasswordRequest $request, $class = Agent::class)
    {
        $agent = $class::whereUserName($request->user_name)->first();

        if (! $agent) {
            return $this->responseBadRequest(message: __('auth.failed_api.user'));
        }

        if ((bool) $agent->locked_at) {
            return $this->responseLocked();
        }

        (new CheckPassword($class))->execute($agent, $request->password);

        return $this->responseSuccess(data: [
            'token' => (new AppTokenService($agent))->generate(AppTokenAction::LoginSecret),
            'secret' => true,
        ]);
    }

    public function loginSecret(LoginSecretRequest $request, $class = Agent::class)
    {
        $agent = $class::whereUserName($request->user_name)->first();

        if (! $agent) {
            return $this->responseBadRequest(message: __('auth.failed_api.user'));
        }

        if ((bool) $agent->locked_at) {
            return $this->responseLocked();
        }

        (new CheckSecretCode($class))->execute($agent, $request->secret_code);

        $this->loginSucceed($request, $agent);

        return $this->responseSuccess(data: [
            'token' => $agent->createToken('Agent TOKEN')->plainTextToken,
            'user' => new AgentResource($agent),
        ]);
    }

    private function loginSucceed(Request $request, Agent|Master|Senior $user)
    {
        $user->update([
            'device_name' => $request->device_name,
            'device_model' => $request->device_model,
            'os_version' => $request->os_version,
            'os_type' => $request->os_type,
            'app_version' => $request->app_version,
            'firebase_token' => $request->firebase_token,
            'language' => $request->language,
        ]);
    }
}
