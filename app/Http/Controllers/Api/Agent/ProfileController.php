<?php

namespace App\Http\Controllers\Api\Agent;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Agent\UpdateProfileRequest;
use App\Http\Resources\Api\Agent\AgentResource;
use App\Models\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function index(Request $request)
    {
        return $this->responseSuccess([
            'user' => new AgentResource($request->user()),
        ]);
    }

    public function update(UpdateProfileRequest $request)
    {
        $agent = Agent::find(auth()->id());
        $agent->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'language' => $request->language,
        ]);

        return $this->responseSuccess([
            'user' => new AgentResource($agent),
        ]);
    }

    // public function setupSecret(Request $request)
    // {
    //     $user = $request->user();

    //     if ($user->secret_code) {
    //         return $this->responseBadRequest('Already setted up secret code.');
    //     }

    //     $request->validate([
    //         'secret_code' => 'required|min:6|confirmed',
    //     ]);

    //     $user->update([
    //         'secret_code' => bcrypt($request->secret_code),
    //     ]);

    //     return $this->responseSuccess([
    //         'agent' => new AgentResource($user),
    //     ]);
    // }

    public function updateSecret(Request $request)
    {
        $request->validate([
            'password' => 'required|string|current_password:'.Auth::getDefaultDriver(),
            'secret_code' => 'required|min:6|confirmed',
        ]);

        $user = $request->user();

        $user->update([
            'secret_code' => bcrypt($request->secret_code),
        ]);

        return $this->responseSuccess([
            'user' => new AgentResource($user),
        ]);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required|string|current_password:'.Auth::getDefaultDriver(),
            'password' => 'required|min:6|confirmed',
        ]);

        $user = $request->user();

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        return $this->responseSuccess([
            'user' => new AgentResource($user),
        ]);
    }
}
