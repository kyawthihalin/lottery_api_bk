<?php

namespace App\Http\Controllers\Api\Agent;

use App\Facades\Draw;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\PurchaseHistoryRequest;
use App\Http\Resources\Api\User\BetResource;
use App\Models\Bet;

class PurchaseHistoryController extends Controller
{
    public function __invoke(PurchaseHistoryRequest $request)
    {
        $drawIds = [];
        $userIds = auth()->user()->users()->pluck('users.id');
        
        $bets = Bet::whereIn('user_id', $userIds)->where(function ($query) use ($request, $drawIds) {
            if ($request->has('draw_id')) {
                $drawIds[] = $request->draw_id;
                $query->whereDate('draw_id', $request->draw_id);

                return;
            }
            $drawIds = Draw::getPreviousDrawIds(24);

            $oldestDrawId = $drawIds[count($drawIds) - 1];
            $query->whereDate('draw_id', '>=', $oldestDrawId);
        })
        ->orderBy('draw_id', 'desc')
        ->get();

        $histories = BetResource::collection($bets)->groupBy(function ($data) {
            return $data->draw_id;
        });

        return $this->responseSuccess([
            'histories' => $histories,
        ]);
    }
}
