<?php

namespace App\Http\Controllers\Api\Agent;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Agent\SettlementResource;
use App\Http\Resources\Api\Agent\SettlementStatResource;
use Illuminate\Http\Request;

class SettlementController extends Controller
{
    public function current(Request $request)
    {
        $agent = $request->user();

        return $this->responseSuccess([
            'histories' => SettlementStatResource::collection([$agent]),
        ]);
    }

    public function histories(Request $request)
    {
        $request->validate([
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $agent = $request->user();
        $settlements = $agent->settlements()->orderBy('created_at', 'desc')->paginate($request->per_page ?? 5);

        return $this->responseSuccess([
            'histories' => SettlementResource::collection($settlements),
            'next' => $settlements->nextPageUrl(),
        ]);
    }
}
