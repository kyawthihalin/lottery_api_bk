<?php

namespace App\Http\Controllers\Api\Agent;

use App\Enums\Gender;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Agent\StoreUserRequest;
use App\Http\Requests\Api\Agent\UpdateUserRequest;
use App\Http\Resources\Api\Agent\UserResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'search' => ['string'],
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $users = $request->user()->users()->where(function (Builder $query) use ($request) {
            $query->where('users.user_name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('users.name', 'LIKE', '%'.$request->search.'%');
        })->paginate($request->per_page ?? 5);

        return $this->responseSuccess([
            'users' => UserResource::collection($users),
            'next' => $users->nextPageUrl() ?? null,
        ]);
    }

    public function store(StoreUserRequest $request)
    {
        $user = $request->user()->users()->create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'gender' => $request->gender ?? Gender::Male,
        ]);

        return $this->responseSuccess([
            'user' => new UserResource($user->refresh()),
        ]);
    }

    public function update(UpdateUserRequest $request)
    {
        $user = $request->user()->users()->whereUserName($request->user_name)->first();

        if (! $user) {
            return $this->responseBadRequest('User not found.');
        }

        $request->has('name') && $user->update([
            'name' => $request->name,
        ]);

        if ($request->has('password')) {
            $user->update([
                'password' => bcrypt($request->password),
            ]);

            $user->tokens()->delete();
        }

        return $this->responseSuccess([
            'user' => new UserResource($user->refresh()),
        ]);
    }
}
