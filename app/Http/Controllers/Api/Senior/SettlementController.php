<?php

namespace App\Http\Controllers\Api\Senior;

use App\Events\SettlementCompleted;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Agent\SettlementResource;
use App\Http\Resources\Api\Agent\SettlementStatResource;
use App\Models\Master;
use App\Models\Settlement;
use Illuminate\Http\Request;

class SettlementController extends Controller
{
    public function current(Request $request)
    {
        $request->validate([
            'master_ids' => ['array'],
            'master_ids.*' => ['required', 'string'],
        ]);

        $senior = $request->user();
        $users = [];

        if ($request->has('master_ids')) {
            $users = Master::whereSeniorId($senior->id)->whereIn('user_name', $request->master_ids)->get();
        } else {
            $users = [$senior];
        }

        return $this->responseSuccess([
            'histories' => SettlementStatResource::collection($users),
        ]);
    }

    public function histories(Request $request)
    {
        $request->validate([
            'master_ids' => ['array'],
            'master_ids.*' => ['required', 'string'],
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $senior = $request->user();
        $settlements = [];

        if ($request->has('master_ids')) {
            $agentIds = Master::whereSeniorId($senior->id)->whereIn('user_name', $request->master_ids)->pluck('id')->toArray();

            $settlements = Settlement::whereIn('user_id', $agentIds)->whereUserType(Master::class)->orderBy('created_at', 'desc')->paginate($request->per_page ?? 5);
        } else {
            $settlements = $senior->settlements()->orderBy('created_at', 'desc')->paginate($request->per_page ?? 5);
        }

        return $this->responseSuccess([
            'histories' => SettlementResource::collection($settlements),
            'next' => $settlements->nextPageUrl(),
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'master_id' => ['required', 'string'],
        ]);

        $senior = $request->user();
        $master = Master::whereSeniorId($senior->id)->where('user_name', $request->master_id)->first();

        if (! $master) {
            return $this->responseBadRequest('Master not found');
        }

        $settlement = $master->settlements()->create([
            'balance_point' => $master->point,
            'bought_point' => $master->bought_point,
            'rewithdrawn_point' => $master->rewithdrawn_point,
            'paid_point' => $master->paid_point,
            'regained_point' => $master->regained_point,
            'used_point' => $master->used_point,
            'point_rate' => $master->point_rate,
            'profit' => $master->profit,
            'paid_above' => $master->paid_above,
            'net_paid_above' => $master->net_paid_above,
            'total_balance_point' => $master->total_balance,
        ]);

        SettlementCompleted::dispatch($settlement->refresh());

        return $this->responseSuccess(message: __('settlement.completed'));
    }
}
