<?php

namespace App\Http\Controllers\Api\Senior;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Senior\StoreMasterRequest;
use App\Http\Requests\Api\Senior\UpdateMasterRequest;
use App\Http\Resources\Api\Master\AgentResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'search' => ['string'],
            'per_page' => ['integer', 'min:1', 'max:50'],
        ]);

        $masters = $request->user()->masters()->where(function (Builder $query) use ($request) {
            $query->where('masters.user_name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('masters.name', 'LIKE', '%'.$request->search.'%');
        })->paginate($request->per_page ?? 5);

        return $this->responseSuccess([
            'masters' => AgentResource::collection($masters),
            'next' => $masters->nextPageUrl() ?? null,
        ]);
    }

    public function store(StoreMasterRequest $request)
    {
        $master = $request->user()->masters()->create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'password' => bcrypt($request->password),
            'secret_code' => bcrypt($request->secret_code),
            'point_rate' => $request->point_rate,
            'phone' => $request->phone,
        ]);

        return $this->responseSuccess([
            'agent' => new AgentResource($master->refresh()),
        ]);
    }

    public function update(UpdateMasterRequest $request)
    {
        $master = $request->user()->masters()->whereUserName($request->user_name)->first();

        if (! $master) {
            return $this->responseBadRequest('Master not found.');
        }

        $request->has('point_rate') && $master->update([
            'point_rate' => $request->point_rate,
        ]);

        $request->has('name') && $master->update([
            'name' => $request->name,
        ]);

        if ($request->has('password')) {
            $master->update([
                'password' => bcrypt($request->password),
            ]);

            $master->tokens()->delete();
        }

        if ($request->has('secret_code')) {
            $master->update([
                'secret_code' => bcrypt($request->secret_code),
            ]);

            $master->tokens()->delete();
        }

        return $this->responseSuccess([
            'user' => new AgentResource($master->refresh()),
        ]);
    }
}
