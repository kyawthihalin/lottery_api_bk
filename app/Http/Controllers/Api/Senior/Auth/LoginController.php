<?php

namespace App\Http\Controllers\Api\Senior\Auth;

use App\Http\Controllers\Api\Agent\Auth\LoginController as AgentLoginController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Agent\Auth\LoginPasswordRequest;
use App\Http\Requests\Api\Agent\Auth\LoginSecretRequest;
use App\Models\Senior;

class LoginController extends Controller
{
    public function loginPassword(LoginPasswordRequest $request)
    {
        return (new AgentLoginController())->loginPassword($request, Senior::class);
    }

    public function loginSecret(LoginSecretRequest $request)
    {
        return (new AgentLoginController())->loginSecret($request, Senior::class);
    }
}
