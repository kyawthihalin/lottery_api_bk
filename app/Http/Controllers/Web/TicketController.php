<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Facades\Draw;
use Illuminate\Http\Request;
use App\Enums\TransactionType;
use App\Events\TicketPurchased;
use App\Actions\MakeTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class TicketController extends Controller
{
    public function goToCart(Request $request)
    {
        $carts = json_decode($request->input('carts'), true);

        return view('purchase-tickets', compact('carts'));
    }

    public function purchase(Request $request)
    {
        $messages = [
            'required' => __('validation.required'),
        ];

        try {
            $request->validate([
                'tickets' => ['required', 'array', 'min:1'],
                'tickets.*' => ['digits:6'],
            ], $messages);
        } catch (ValidationException $e) {
            return $this->responseError('', [
                'errors' => $e->errors(),
            ], 422);
        }

        $tickets = $request->tickets;

        if (Draw::isLotteryDate(now())) {
            return $this->responseError('', [
                'errors' => __('general.errors.purchase.lottery_day')
            ], 400);
        }

        $userId = auth()->guard('user')->user()->id;
        $user = User::find($userId);

        if ($user->point < count($tickets)) {
            return $this->responseError('', [
                'errors' => __('general.errors.point_not_enough', [
                    'transaction_type' => TransactionType::Bet->getBreifLabel(),
                ])
            ], 400);
        }

        $purchasedTickets = [];
        $drawId = Draw::getPendingDrawId();

        foreach ($tickets as $ticket) {
            if (
                Draw::isPurchasedTicket($user, $ticket, $drawId) ||
                Draw::isPurchaseLimitReached($ticket, $drawId) ||
                Draw::isRestrictedTicket($ticket)
            ) {
                array_push($purchasedTickets, $ticket);
            }
        }

        if (count($purchasedTickets)) {
            return $this->responseError('', [
                'errors' => __('ticket.already_purchased'), [
                    'purchased_tickets' => array_unique($purchasedTickets),
                ]
            ], 422);
        }

        $transaction = (new MakeTransaction(
            transactionType: TransactionType::Bet,
            point: count($tickets),
            user: $user,
            otherFields: [
                'draw_id' => $drawId,
                'tickets' => $tickets,
            ]
        ))->execute();

        TicketPurchased::dispatch($transaction);

        return $this->responseSuccess(message: __('ticket.purchased'));
    }
}
