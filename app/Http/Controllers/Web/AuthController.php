<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Agent;
use Illuminate\Http\Request;
use App\Actions\CheckPassword;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $messages = [
            'required' => __('validation.required'),
            'string' => __('validation.string'),
        ];

        try {
            $credentials = $request->validate([
                'user_name' => ['required', 'string'],
                'password' => ['required', 'string'],
            ], $messages);
        } catch (ValidationException $e) {
            if ($request->ajax) {
                return $this->responseError('', [
                    'errors' => $e->errors(),
                ], 422);
            } else {
                throw $e;
            }
        }

        $user = User::whereUserName($request->user_name)->first();

        if (!$user || (bool) $user->locked_at) {
            $errorText = $user ? 'locked' : 'failed_api.user';

            return $request->ajax
                ? $this->responseError('', [
                    'errors' => [__('auth.' . $errorText)],
                ], 422)
                : redirect()->back()->with('error', __('auth.' . $errorText));
        }

        if ($request->ajax) {
            if (!Hash::check($request->password, $user->password)) {
                return $this->responseError('', [
                    'errors' => [__('auth.failed_api.user')],
                ], 422);
            }
        } else {
            (new CheckPassword(User::class))->execute($user, $request->password);
        }

        if (Auth::guard('user')->attempt($credentials)) {
            $request->session()->regenerate();

            if ($request->ajax) {
                return $this->responseSuccess(data: [
                    'token' => $request->session()->token(),
                ]);
            }

            return redirect()->intended('/');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }

    public function register(Request $request)
    {
        $messages = [
            'required' => __('validation.required'),
            'string' => __('validation.string'),
        ];

        $credentials = $request->validate([
            'user_name' => ['required', 'string', 'unique:users'],
            'name' => ['required', 'string'],
            'password' => ['required', 'string'],
            'agent_code' => ['required', 'string'],
        ], $messages);

        $agent = Agent::whereCode($request->agent_code)->first();

        if (!$agent) {
            return redirect()->back()->with('error', __('general.errors.invalid.agent_code'));
        }

        $agent->users()->create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'password' => bcrypt($request->password),
            'app_version' => '1.0.0',
            'language' => config('app.locale'),
        ]);

        return redirect()->route('login')->with('success', __('auth.action.notification.registered'));
    }
}
