<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function readNotification(Request $request, $id)
    {
        $notification = auth('user')->user()->notifications()->where('id', $request->id)->first();
        $notification->markAsRead();

        return response()->json(['message' => 'Notification updated successfully']);
    }
}
