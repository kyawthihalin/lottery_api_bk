<?php

namespace App\Http\Requests\Api\Agent\Auth;

use App\Actions\ValidateHiddenFields;
use App\Enums\OsType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class LoginSecretRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenFields())
            ->execute([
                'device_name' => ['required', 'string', 'max:255'],
                'device_model' => ['required', 'string', 'max:255'],
                'os_version' => ['required', 'string'],
                'os_type' => ['required', new Enum(OsType::class)],
                'app_version' => ['required', 'string'],
                'firebase_token' => ['required', 'string', 'max:255'],
            ]);

        return [
            'secret_code' => ['required', 'string'],
        ];
    }
}
