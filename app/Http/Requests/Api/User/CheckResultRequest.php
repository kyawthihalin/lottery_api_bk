<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class CheckResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'ticket_numbers' => ['array', 'prohibits:from_ticket'],
            'ticket_numbers.*' => ['digits:6'],
            'from_ticket' => ['digits:6', 'prohibits:ticket_numbers', 'required_with:to_ticket'],
            'to_ticket' => ['digits:6', 'required_with:from_ticket'],
            'draw_id' => ['required', 'date', 'date_format:Y-m-d', 'before:tomorrow'],
        ];
    }
}
