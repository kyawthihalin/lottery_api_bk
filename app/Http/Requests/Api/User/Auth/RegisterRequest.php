<?php

namespace App\Http\Requests\Api\User\Auth;

use App\Actions\ValidateHiddenFields;
use App\Enums\Language;
use App\Enums\OsType;
use App\Settings\AuthSettings;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenFields())->execute([
            'device_name' => ['required', 'string', 'max:255'],
            'device_model' => ['required', 'string', 'max:255'],
            'os_version' => ['required', 'string', 'max:255'],
            'os_type' => ['required', 'string', new Enum(OsType::class)],
            'app_version' => ['required', 'string', 'max:255'],
            'firebase_token' => ['required', 'string', 'max:255'],
            'language' => ['required', 'string', new Enum(Language::class)],
        ]);

        return [
            'agent_code' => ['required', 'string'],
            'name' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'unique:users', 'alpha_dash'],
            'password' => [
                'required',
                'string',
                app(AuthSettings::class)->getPasswordRule(\App\Models\User::class),
                'confirmed',
            ],
        ];
    }
}
