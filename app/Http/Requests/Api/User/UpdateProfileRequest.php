<?php

namespace App\Http\Requests\Api\User;

use App\Enums\Language;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'language' => ['required', 'string', new Enum(Language::class)],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['string', 'max:20', 'min:5'],
            'nrc' => ['string', 'min:17', 'max:30'],
        ];
    }
}
