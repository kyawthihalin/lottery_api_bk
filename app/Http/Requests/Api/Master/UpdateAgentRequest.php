<?php

namespace App\Http\Requests\Api\Master;

use App\Actions\ValidateHiddenFields;
use App\Settings\AuthSettings;
use App\Settings\PointRateSettings;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenFields())->execute([
            'user_name' => ['required', 'alpha_dash'],
        ]);

        return [
            'name' => ['string', 'max:255'],
            'password' => [
                'string',
                app(AuthSettings::class)->getPasswordRule(\App\Models\Agent::class),
                'confirmed',
            ],
            'secret_code' => [
                'string',
                app(AuthSettings::class)->getSecretCodeRule(\App\Models\Agent::class),
                'confirmed',
            ],
            'point_rate' => [
                'integer',
                'min:'.app(PointRateSettings::class)->min_agent,
                'max:'.app(PointRateSettings::class)->max_agent,
            ],
        ];
    }
}
