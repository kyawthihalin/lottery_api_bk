<?php

namespace App\Http\Requests\Api\Senior;

use App\Settings\AuthSettings;
use App\Settings\PointRateSettings;
use Illuminate\Foundation\Http\FormRequest;

class StoreMasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'max:255', 'unique:masters,user_name'],
            'password' => [
                'required',
                'string',
                app(AuthSettings::class)->getPasswordRule(\App\Models\Agent::class),
                'confirmed',
            ],
            'secret_code' => [
                'required',
                'string',
                app(AuthSettings::class)->getSecretCodeRule(\App\Models\Agent::class),
                'confirmed',
            ],
            'point_rate' => [
                'required',
                'integer',
                'min:'.app(PointRateSettings::class)->min_master,
                'max:'.app(PointRateSettings::class)->max_master,
            ],
            'phone' => ['string'],
        ];
    }
}
