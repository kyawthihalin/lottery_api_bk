<?php

namespace App\Http\Requests\DepositWithdraw;

use Illuminate\Foundation\Http\FormRequest;

class UserHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_name' => ['string', 'alpha_dash'],
            'date' => ['date', 'date_format:Y-m-d'],
            'per_page' => ['integer', 'min:1', 'max:50'],
        ];
    }
}
