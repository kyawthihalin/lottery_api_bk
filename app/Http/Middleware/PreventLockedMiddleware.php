<?php

namespace App\Http\Middleware;

use App\Traits\ResponseHelper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PreventLockedMiddleware
{
    use ResponseHelper;

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = $request->user();

        if (! (bool) $user->locked_at) {
            return $next($request);
        }

        if ($this->isApiRequest()) {
            $user->tokens()->delete();

            return $this->responseLocked();
        }

        Auth::logout();

        return $next($request);
    }

    private function isApiRequest(): bool
    {
        return str_starts_with(request()->path(), 'api');
    }
}
