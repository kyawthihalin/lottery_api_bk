<?php

namespace App\Http\Middleware;

use App\Actions\ValidateHiddenFields;
use App\Enums\Language;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Enum;
use Symfony\Component\HttpFoundation\Response;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $language = 'en';

        if ($request->user()) {
            $language = $request->user()->language->value;
        } else {
            (new ValidateHiddenFields())->execute([
                'language' => ['required', 'string', new Enum(Language::class)],
            ]);
            $language = $request->language;
        }

        app()->setLocale($language);

        return $next($request);
    }
}
