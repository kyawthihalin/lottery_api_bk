<?php

namespace App\Http\Middleware;

use App\Actions\ValidateHiddenFields;
use App\Enums\AppTokenAction;
use App\Services\AppTokenService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AppTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string $action): Response
    {
        (new ValidateHiddenFields())->execute([
            'user_name' => ['required', 'string'],
            'app_token' => ['required', 'string'],
        ]);

        $model = get_model_by_route_prefix($request->segment(2));

        $user = $model::whereUserName($request->user_name)->first();
        $tokenAction = AppTokenAction::from($action);

        (new AppTokenService($user))->verify($tokenAction, $request->app_token);

        return $next($request);
    }
}
