<?php

namespace App\Http\Middleware;

use App\Settings\AuthSettings;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ForceSecretCodeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($this->isApiRequest()) {
            return $next($request);
        }

        if (! $this->isRequestSecretPath() && ! $this->isLiveWirePath()) {
            $user = $request->user();
            $session = DB::table('sessions')->where('id', '=', $request->session()->getId())->first();

            if (! $session) {
                Log::error('Found no session for loggedin user');
            }

            if ($session) {
                $lastActivity = Carbon::createFromTimestamp($session->last_activity);

                if ($lastActivity->diffInMinutes(Carbon::now()) >= app(AuthSettings::class)->agent_secret_code_interval) {
                    $user->update([
                        'is_secret_code_required' => true,
                    ]);
                }
            }
        }

        if ($request->user()->is_secret_code_required && ! $this->isRequestSecretPath()) {
            return redirect($this->getRequestSecretPath());
        }

        return $next($request);
    }

    private function isApiRequest(): bool
    {
        return str_starts_with(request()->path(), 'api');
    }

    // private function isSetupSecretPath(): bool
    // {
    //     return str_ends_with(request()->path(), 'setup-secret-code');
    // }

    // private function getSetupSecretPath(): string
    // {
    //     return get_route_prefix_by_model(request()->user()).'/'.'setup-secret-code';
    // }

    private function isRequestSecretPath(): bool
    {
        return str_ends_with(request()->path(), '/secret-code');
    }

    private function isLiveWirePath(): bool
    {
        return str_ends_with(request()->path(), 'livewire/update');
    }

    private function getRequestSecretPath(): string
    {
        return get_route_prefix_by_model(request()->user()).'/'.'secret-code';
    }
}
