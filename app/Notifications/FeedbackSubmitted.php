<?php

namespace App\Notifications;

use App\Models\Feedback;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;

class FeedbackSubmitted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Feedback $feedback)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the broadcast representation of the notification.
     */
    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        app()->setLocale($notifiable->language->value);

        return FilamentNotification::make()
            ->info()
            ->icon('heroicon-o-pencil-square')
            ->title($this->feedback->title)
            ->body($this->feedback->detail)
            ->getBroadcastMessage();
    }

    /**
     * Get the database representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->info()
            ->icon('heroicon-o-pencil-square')
            ->title($this->feedback->title)
            ->body($this->feedback->detail)
            ->getDatabaseMessage();
    }
}
