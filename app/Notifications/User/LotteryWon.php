<?php

namespace App\Notifications\User;

use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class LotteryWon extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public array $tickets)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('general.notification.title.user.lottery_won'),
            body: __("general.notification.message.user.lottery_won.{$this->tickets['code']}", [
                'tickets' => implode(', ', $this->tickets['numbers']),
                'price' => number_format($this->tickets['price']),
            ]),
        )))
            ->data([
                'tickets' => json_encode($this->tickets['numbers']),
                'price' => number_format($this->tickets['price']),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        app()->setLocale($notifiable->language->value);

        return FilamentNotification::make()
            ->success()
            ->title(__('general.notification.title.user.lottery_won'))
            ->body(json_encode([
                'key' => "general.notification.message.user.lottery_won.{$this->tickets['code']}",
                'replace' => [
                    'tickets' => implode(', ', $this->tickets['numbers']),
                    'price' => number_format($this->tickets['price']),
                ],
            ]))
            ->getDatabaseMessage();
    }
}
