<?php

namespace App\Notifications\User;

use App\Models\DepositWithdraw;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class DepositCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public DepositWithdraw $deposit)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
            'broadcast',
        ];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('deposit.notification.title.completed', [
                'point' => number_format($this->deposit->point),
            ]),
            body: __('deposit.notification.message.completed', [
                'point' => number_format($this->deposit->point),
                'name' => $this->deposit->user->parent->name,
            ]),
        )))
            ->data([
                'point' => number_format($this->deposit->point),
            ]);
    }

    /**
     * Get the broadcast representation of the notification.
     */
    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        app()->setLocale($notifiable->language->value);

        return FilamentNotification::make()
            ->success()
            ->title(__('deposit.notification.title.completed', [
                'point' => number_format($this->deposit->point),
            ]))
            ->body(__('deposit.notification.message.completed', [
                'point' => number_format($this->deposit->point),
                'name' => $this->deposit->user->parent->name,
            ]))
            ->getBroadcastMessage();
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->success()
            ->title(json_encode([
                'key' => 'deposit.notification.title.completed',
                'replace' => [
                    'point' => number_format($this->deposit->point),
                ],
            ]))
            ->body(json_encode([
                'key' => 'deposit.notification.message.completed',
                'replace' => [
                    'point' => number_format($this->deposit->point),
                    'name' => $this->deposit->user->parent->name,
                ],
            ]))
            ->getDatabaseMessage();
    }
}
