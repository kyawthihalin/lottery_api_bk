<?php

namespace App\Notifications\User;

use App\Models\Bet;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class TicketPurchased extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Bet $bet)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    public function toFcm($notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('ticket.notification.title.user.purchased', [
                'point' => number_format($this->bet->point),
            ]),
            body: __('ticket.notification.message.user.purchased', [
                'tickets' => implode(', ', $this->bet->tickets),
            ]),
            // image: 'http://example.com/url-to-image-here.png'
        )))
            ->data([
                'tickets' => json_encode($this->bet->tickets),
                'point' => number_format($this->bet->point),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->success()
            ->title(json_encode([
                'key' => 'ticket.notification.title.user.purchased',
                'replace' => [
                    'point' => number_format($this->bet->point),
                ],
            ]))
            ->body(json_encode([
                'key' => 'ticket.notification.message.user.purchased',
                'replace' => [
                    'tickets' => implode(', ', $this->bet->tickets),
                ],
            ]))
            ->getDatabaseMessage();
    }
}
