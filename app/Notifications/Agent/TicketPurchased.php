<?php

namespace App\Notifications\Agent;

use App\Models\Agent;
use App\Models\Bet;
use App\Models\Master;
use App\Models\Senior;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class TicketPurchased extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Bet $bet)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
            'broadcast',
        ];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('ticket.notification.title.agent.purchased', [
                'point' => number_format($this->bet->point),
            ]),
            body: __('ticket.notification.message.agent.purchased', [
                'name' => $this->bet->user->name,
                'tickets' => implode(', ', $this->bet->tickets),
            ]),
            // image: 'http://example.com/url-to-image-here.png'
        )))
            ->data([
                'tickets' => json_encode($this->bet->tickets),
                'point' => number_format($this->bet->point),
            ]);
    }

    // public function fcmProject($notifiable, $message)
    // {
    //     return 'agent';
    // }

    /**
     * Get the broadcast representation of the notification.
     */
    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        app()->setLocale($notifiable->language->value);

        return FilamentNotification::make()
            ->success()
            ->title(__('ticket.notification.title.agent.purchased', [
                'point' => number_format($this->bet->point),
            ]))
            ->body(__('ticket.notification.message.agent.purchased', [
                'name' => $this->bet->user->name,
                'tickets' => implode(', ', $this->bet->tickets),
            ]))
            ->getBroadcastMessage();
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->success()
            ->title(json_encode([
                'key' => 'ticket.notification.title.agent.purchased',
                'replace' => [
                    'point' => number_format($this->bet->point),
                ],
            ]))
            ->body(json_encode([
                'key' => 'ticket.notification.message.agent.purchased',
                'replace' => [
                    'name' => $this->bet->user->name,
                    'tickets' => implode(', ', $this->bet->tickets),
                ],
            ]))
            ->getDatabaseMessage();
    }
}
