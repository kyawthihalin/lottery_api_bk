<?php

namespace App\Notifications\Agent;

use App\Models\Agent;
use App\Models\DepositWithdraw;
use App\Models\Master;
use App\Models\Senior;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class WithdrawCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public DepositWithdraw $withdraw)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('withdraw.notification.title.completed_by', [
                'point' => number_format($this->withdraw->point),
            ]),
            body: __('withdraw.notification.message.completed_by', [
                'point' => number_format($this->withdraw->point),
                'name' => $this->withdraw->user->name,
            ]),
        )))
            ->data([
                'point' => number_format($this->withdraw->point),
            ]);
    }

    // public function fcmProject($notifiable, $message)
    // {
    //     $projectName = match (get_class($notifiable)) {
    //         Agent::class => 'agent',
    //         Master::class => 'master',
    //         Senior::class => 'senior',
    //     };

    //     return $projectName;
    // }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->success()
            ->title(json_encode([
                'key' => 'withdraw.notification.title.completed_by',
                'replace' => [
                    'point' => number_format($this->withdraw->point),
                ],
            ]))
            ->body(json_encode([
                'key' => 'withdraw.notification.message.completed_by',
                'replace' => [
                    'point' => number_format($this->withdraw->point),
                    'name' => $this->withdraw->user->name,
                ],
            ]))
            ->getDatabaseMessage();
    }
}
