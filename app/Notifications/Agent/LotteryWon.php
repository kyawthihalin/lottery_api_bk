<?php

namespace App\Notifications\Agent;

use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class LotteryWon extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public User $user, public array $tickets)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('general.notification.title.agent.lottery_won'),
            body: __("general.notification.message.agent.lottery_won.{$this->tickets['code']}", [
                'name' => $this->user->name,
                'tickets' => implode(', ', $this->tickets['numbers']),
                'price' => number_format($this->tickets['price']),
            ]),
        )))
            ->data([
                'name' => $this->user->name,
                'tickets' => json_encode($this->tickets['numbers']),
                'price' => number_format($this->tickets['price']),
            ]);
    }

    // public function fcmProject($notifiable, $message)
    // {
    //     $projectName = match (get_class($notifiable)) {
    //         Agent::class => 'agent',
    //         Master::class => 'master',
    //         Senior::class => 'senior',
    //     };

    //     return $projectName;
    // }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        app()->setLocale($notifiable->language->value);

        return FilamentNotification::make()
            ->success()
            ->title(__('general.notification.title.agent.lottery_won', [
                'name' => $this->user->name,
            ]))
            ->body(json_encode([
                'key' => "general.notification.message.agent.lottery_won.{$this->tickets['code']}",
                'replace' => [
                    'name' => $this->user->name,
                    'tickets' => implode(', ', $this->tickets['numbers']),
                    'price' => $this->tickets['price'],
                ],
            ]))
            ->getDatabaseMessage();
    }
}
