<?php

namespace App\Notifications\Agent;

use App\Models\Agent;
use App\Models\DepositWithdraw;
use App\Models\Master;
use App\Models\Senior;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class DepositCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public DepositWithdraw $deposit)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [
            FcmChannel::class,
            'database',
        ];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return (new FcmMessage(notification: new FcmNotification(
            title: __('deposit.notification.title.completed_from', [
                'point' => number_format($this->deposit->point),
            ]),
            body: __('deposit.notification.message.completed_from', [
                'point' => number_format($this->deposit->point),
                'name' => $this->deposit->user->name,
            ]),
        )))
            ->data([
                'point' => number_format($this->deposit->point),
            ]);
    }

    // public function fcmProject($notifiable, $message)
    // {
    //     $projectName = match (get_class($notifiable)) {
    //         Agent::class => 'agent',
    //         Master::class => 'master',
    //         Senior::class => 'senior',
    //     };

    //     return $projectName;
    // }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->success()
            ->title(json_encode([
                'key' => 'deposit.notification.title.completed_from',
                'replace' => [
                    'point' => number_format($this->deposit->point),
                ],
            ]))
            ->body(json_encode([
                'key' => 'deposit.notification.message.completed_from',
                'replace' => [
                    'point' => number_format($this->deposit->point),
                    'name' => $this->deposit->user->name,
                ],
            ]))
            ->getDatabaseMessage();
    }
}
