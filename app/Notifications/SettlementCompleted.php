<?php

namespace App\Notifications;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\Settlement;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class SettlementCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Settlement $settlement)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        $channels = [
            'database',
            'broadcast',
        ];

        if(!$notifiable instanceof Admin){
            $channels[] = FcmChannel::class;
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toFcm(object $notifiable): FcmMessage
    {
        app()->setLocale($notifiable->language->value);

        return new FcmMessage(notification: new FcmNotification(
            title: __('settlement.completed'),
        ));
    }

    // public function fcmProject($notifiable, $message)
    // {
    //     $projectName = match (get_class($notifiable)) {
    //         Agent::class => 'agent',
    //         Master::class => 'master',
    //         Senior::class => 'senior',
    //     };

    //     return $projectName;
    // }

    /**
     * Get the broadcast representation of the notification.
     */
    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        app()->setLocale($notifiable->language->value);

        return FilamentNotification::make()
            ->success()
            ->title(__('settlement.completed'))
            ->getBroadcastMessage();
    }

    public function toDatabase(object $notifiable): array
    {
        return FilamentNotification::make()
            ->success()
            ->title(json_encode([
                'key' => 'settlement.completed',
                'replace' => [],
            ]))
            ->getDatabaseMessage();
    }
}
