<?php

namespace App\Notifications;

use App\Models\Admin;
use Filament\Notifications\Notification as FilamentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class SystemNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public array $data)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        $channels = ['database', 'broadcast'];

        if (! $notifiable instanceof Admin) {
            $channels[] = FcmChannel::class;
        }

        return $channels;
    }

    public function toFcm($notifiable): FcmMessage
    {
        $language = $notifiable->language->value;

        return new FcmMessage(notification: new FcmNotification(
            title: $this->data["title_{$$language}"],
            body: $this->data["body_{$$language}"],
            // image: 'http://example.com/url-to-image-here.png'
        ));
    }

    /**
     * Get the broadcast representation of the notification.
     */
    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        $language = $notifiable->language->value;

        return FilamentNotification::make()
            ->title($this->data["title_{$$language}"])
            ->body($this->data["body_{$$language}"])
            ->getBroadcastMessage();
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toDatabase(object $notifiable): array
    {
        $language = $notifiable->language->value;

        return FilamentNotification::make()
            ->title($this->data["title_{$language}"])
            ->body($this->data["body_{$language}"])
            ->getDatabaseMessage();
    }
}
