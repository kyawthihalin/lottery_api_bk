<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string getPendingDrawId(string|null $date = null)
 * @method static string getPreviousDrawId(string|null $date = null)
 * @method static array getPreviousDrawIds(int $count = 1, string|null $date = null, bool $isOptions = false)
 * @method static bool isLotteryDate(string|null $date = null)
 * @method static array getDrawIdsByMonth(string|null $date = null)
 * @method static array findWonNumbers(ResultTypeCode $resultTypeCode, array $resultTickets, array $tickets)
 * @method static void generateWinners(string $drawId)
 * @method static bool isPurchasedTicket(User $user, string $ticketNumber, ?string $drawId = null)
 * @method static bool isPurchaseLimitReached(string $ticketNumber, ?string $drawId = null)
 * @method static bool isRestrictedTicket(string $ticketNumber)
 * @method static array getFakeResultsByResultType(ResultTypeCode $resultTypeCode)
 * @method static array getRandomTickets(int $count, bool $includeRestricted = false, bool $includePurchased = false, string|null $drawId = null)
 * @method static string getRandomTicket(bool $includeRestricted = false, bool $includePurchased = false, string|null $drawId = null)
 * @method static int getTotalWonLottery(Senior|Master|Agent $user)
 */
class Draw extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'draw';
    }
}
