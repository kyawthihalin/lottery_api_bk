<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string getBoughtTransactionType(Model $record)
 * @method static string getWithdrawnTransactionType(Model $record)
 * @method static string getPaidTransactionType(Model $record)
 * @method static string getRegainedTransactionType(Model $record)
 * @method static string getSettlementClosure(Model $record)
 * @method static string getBoughtPoint(Model $record)
 * @method static string getRewithdrawnPoint(Model $record)
 * @method static string getRegainedPoint(Model $record)
 * @method static string getPaidPoint(Model $record)
 * @method static string getUsedPoint(Model $record)
 * @method static string getPaidAbove(Model $record)
 * @method static string getNetPaidAbove(Model $record)
 */
class Settlement extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'settlement';
    }
}
