<?php

namespace App\Livewire;

use App\Facades\Draw;
use App\Models\UserResult;
use Carbon\Carbon;
use Livewire\Component;

class Winner extends Component
{
    public $winnerUsers;

    public $filterDates = [];

    public function render()
    {
        $previousDrawIds = Draw::getPreviousDrawIds(2);
        $this->filterDates = $previousDrawIds;

        $results = [];
        foreach ($previousDrawIds as $drawId) {
            $userResults = UserResult::where('draw_id', Carbon::parse($drawId))->get();

            $drawResults = [];
            foreach ($userResults as $key => $betResult) {
                $resultType = $betResult->result->resultType;
                $user = $betResult->user;

                $drawResults[$resultType->code->value][] = [
                    'ticket_numbers' => $betResult->tickets,
                    'amount' => $betResult->result->price,
                    'name' => $user->name,
                    'gender' => $user->gender,
                    'agent_code' => $user->agent->code,
                ];
            }
            $results[$drawId] = $drawResults;
        }

        $this->winnerUsers = $results;
        return view('livewire.winner');
    }
}
