<?php

namespace App\Livewire;

use App\Enums\ResultTypeCode;
use App\Facades\Draw;
use App\Models\Result;
use App\Models\ResultType;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Http\Request;

class Results extends Component
{
    public $results = [];

    public $month_count = '';

    public $changeType = 'single';

    public $number;

    public $multiInputs = [];

    public $winAmount = 0;

    public $showResult = false;

    public $showCheck = false;

    public $winOrLose = false;

    public $showWiningTicket = false;

    public $singleWinNumber = [];

    public $multiWinNumber = [];

    public $seriesFrom = '';

    public $seriesTo = '';

    public $onceUsed = true;

    public $onlyLastMonth = false;

    public function openResult()
    {
        $this->showResult = true;
    }

    public function closeResult()
    {
        $this->showResult = false;
    }

    public function openCheck()
    {
        $this->showCheck = true;
    }

    public function closeCheck()
    {
        $this->showCheck = false;
    }

    public function openWiningTicket()
    {
        $this->showWiningTicket = true;
    }

    public function closeWiningTicket()
    {
        $this->showWiningTicket = false;
    }

    public function changeCheckbox($value)
    {
        $this->changeType = $value;
    }

    public function filterResult($count, bool $lastMonth = false)
    {
        $this->month_count = $count;
        $this->getResult($count, $lastMonth);
    }

    public function checkSingleResult()
    {
        $customMessages = [
            'number.required' => 'The number field is required.',
            'number.numeric' => 'The number field must be numeric.',
            'number.digits' => 'The number field must be exactly 6 digits.',
        ];

        $this->validate([
            'number' => 'required|numeric|digits:6',
        ], $customMessages);

        $result = $this->CheckWinNumber($this->number);
        if ($result) {
            $this->winOrLose = true;
            $this->singleWinNumber = str_split($this->number);
            $this->winAmount = $result->price;
        } else {
            $this->winOrLose = false;
            $this->winAmount = 0;
        }
        $this->closeCheck();
        $this->openResult();

    }

    public function checkSeriesResult()
    {

        $customMessages = [
            'seriesFrom.required' => 'The number field is required.',
            'seriesFrom.*.numeric' => 'The number field must be numeric.',
            'seriesFrom.*.digits' => 'The number field must be exactly 6 digits.',
            'seriesTo.required' => 'The number field is required.',
            'seriesTo.*.numeric' => 'The number field must be numeric.',
            'seriesTo.*.digits' => 'The number field must be exactly 6 digits.',
        ];

        $this->validate([
            'seriesFrom' => 'required|numeric|digits:6',
            'seriesTo' => 'required|numeric|digits:6',
        ], $customMessages);

        if ($this->seriesFrom > $this->seriesTo) {
            $this->closeCheck();
            $this->openResult();
        }

        $this->winOrLose = false;
        $this->multiWinNumber = [];

        for ($i = $this->seriesFrom; $i <= $this->seriesTo; $i++) {
            $formattedNumber = sprintf('%06d', $i);

            $result = $this->CheckWinNumber($formattedNumber);
            if ($result) {
                $this->winOrLose = true;
                $result->winNumber = $formattedNumber;
                $this->multiWinNumber[] = $result;
            }
        }

        $this->closeCheck();
        $this->openResult();

    }

    public function checkMultipleResult()
    {
        $customMessages = [
            'multiInputs.required' => 'The number field is required.',
            'multiInputs.*.numeric' => 'The number field must be numeric.',
            'multiInputs.*.digits' => 'The number field must be exactly 6 digits.',
        ];

        $this->validate([
            'multiInputs' => 'required|array|min:1',
            'multiInputs.*' => 'required|numeric|digits:6',
        ], $customMessages);

        $this->multiWinNumber = [];
        $this->winOrLose = false;

        foreach ($this->multiInputs as $multiInput) {
            $result = $this->CheckWinNumber($multiInput);
            if ($result) {
                $this->winOrLose = true;
                $result->winNumber = $multiInput;
                $this->multiWinNumber[] = $result;
            }
        }

        $this->closeCheck();
        $this->openResult();
    }

    public function render()
    {
        if ($this->onceUsed) {
            $this->getResult(1, true);
            $this->onceUsed = false;
        }

        return view('livewire.results');
    }

    public function getResult(?int $count, bool $lastMonth = false)
    {
        $temp = [];
        $resultTypes = ResultType::all();
        $lotteryDates = Draw::getPreviousDrawIds(24);
        $lotteryDates = array_filter($lotteryDates, fn ($date) => Carbon::parse($date)->diffInMonths(now()) < $count);

        if ($lastMonth) {
            $lotteryDates = array_slice($lotteryDates, 0, 1);
        }

        foreach ($lotteryDates as $lotteryDate) {
            foreach ($resultTypes as $resultType) {
                $result = $resultType->results()->where('draw_id', $lotteryDate)->get();
                if (! $result) {
                    continue;
                }

                $temp[] = $result;
            }
        }

        $this->results = [];
        $this->results[] = $temp;
    }

    public function tryAgainCheck()
    {
        $this->closeResult();
        $this->openCheck();
    }

    public function viewWiningTicket()
    {
        $this->closeResult();
        $this->openWiningTicket();
    }

    public function removeMultiInput($key)
    {
            unset($this->multiInputs[$key]);
    }


    private function GetResultTypeID($resultTypeCode): int
    {
        return ResultType::where('code', $resultTypeCode)->pluck('id')->first();
    }

    private function CheckWinNumber($number): ?Result
    {
        $result = Result::where(function ($query) use ($number) {
            $query->whereJsonContains('numbers', $number)
                ->orwhere(function ($query) use ($number) {
                    $query->whereJsonContains('numbers', substr($number, -3))->where('result_type_id', $this->GetResultTypeID(ResultTypeCode::LastThree));
                })
                ->orwhere(function ($query) use ($number) {
                    $query->whereJsonContains('numbers', substr($number, 0, 3))->where('result_type_id', $this->GetResultTypeID(ResultTypeCode::FirstThree));
                })
                ->orwhere(function ($query) use ($number) {
                    $query->whereJsonContains('numbers', substr($number, -2))->where('result_type_id', $this->GetResultTypeID(ResultTypeCode::LastTwo));
                });
        })
            ->where('draw_id', function ($query) {
                $query->select('draw_id')
                    ->from('results')
                    ->latest('draw_id')
                    ->limit(1);
            })->first();

        return $result;
    }
}
