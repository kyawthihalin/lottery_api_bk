<?php

namespace App\Livewire;

use Carbon\Carbon;
use App\Models\Bet;
use App\Models\User;
use Livewire\Component;
use Illuminate\Http\Request;
use App\Enums\TransactionType;
use App\Models\TransactionLog;
use App\Models\PaymentAccountType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserInfo extends Component
{
    public $alert_title = '';
    public $alert_message = '';
    public $alert_type = '';
    public $showMessage = false;
    public $user_name = "";
    public $phone = "";
    public $gender = "";
    public $first_time = true;
    public $old_password = '';
    public $new_password = '';
    public $confirm_password = '';
    public $bet_infos = [];
    public $last_date = '';
    public $bet_count = 0;
    public $showTicketBox = false;
    public $showTicketNumbers = [];
    public $showTicketDate = '';
    public $showTicketCount = 0;
    public $depositWithdrawes = [];
    public $depositWithdrawCount = 0;
    public $showPurchaseOneTicket = false;
    public $paymentTypes = [];
    public $authUserPaymentTypes = [];
    public $kbzPayName = '';
    public $kbzPayNumber = '';
    public $kbzAccountName = '';
    public $kbzAccountNumber = '';
    public $yomaAccountName = '';
    public $yomaAccountNumber = '';
    public $cbAccountName = '';
    public $cdAccountNumber = '';
    public $ayaAccountName = '';
    public $ayaAccountNumber = '';
    public $waveMoneyAccountName = '';
    public $waveMoneyAccountNumber = '';

    public function updateInfo()
    {

        $validator = Validator::make([
            'user_name' => $this->user_name,
            'phone_number' => $this->phone,]
            ,[
            'user_name' => 'required|string',
            'phone_number' => 'required|string|unique:users,phone,' . auth('user')->id(),
        ]);

        if ($validator->fails()) {

            if ($validator->errors()->has('user_name')) {
                $this->alert('error',"Error",$validator->errors()->get('user_name'));
                return;
            }

            if ($validator->errors()->has('phone_number')) {
                $this->alert('error',"Error",$validator->errors()->get('phone_number'));
                return;
            }
        }
        else{
            $user = auth('user')->user();
            $user->name = $this->user_name;
            $user->phone = $this->phone;
            $user->gender = $this->gender;
            $user->save();
            $this->alert('success',__('web.update_profile_success_title'),__('web.update_profile_success_message'));
        }
    }

    public function updatePassword()
    {
        $validator = Validator::make([
            'old_password' => $this->old_password,
            'new_password' => $this->new_password,
            'confirm_password' => $this->confirm_password,]
            ,[
            'old_password' => 'required|min:6',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:new_password',
        ]);

        if ($validator->fails()) {

            if ($validator->errors()->has('old_password')) {
                $this->alert('error',"Error",$validator->errors()->get('old_password'));
                return;
            }

            if ($validator->errors()->has('new_password')) {
                $this->alert('error',"Error",$validator->errors()->get('new_password'));
                return;
            }

            if ($validator->errors()->has('confirm_password')) {
                $this->alert('error',"Error",$validator->errors()->get('confirm_password'));
                return;
            }
        }
        else{
            $user = auth('user')->user();

            if (!Hash::check($this->old_password, $user->password)) {
                    $this->alert('error',"Error",__('web.incorrect_password'));
                    return;
            }

            $user->update([
                'password' => Hash::make($this->new_password),
            ]);

            $this->old_password = '';
            $this->new_password = '';
            $this->confirm_password = '';
            $this->alert('success',__('web.update_profile_success_title'),__('web.update_password_success'));
        }
    }

    public function render()
    {

        if($this->first_time) {
            $this->user_name = auth('user')->user()->name;
            $this->phone = auth('user')->user()->phone;
            $this->gender = auth('user')->user()->gender->value;
            $this->first_time = false;
            $this->bet_infos = auth('user')->user()->bets()->get();
            $this->last_date = Carbon::parse(auth('user')->user()->bets()->latest()->pluck('created_at')->first())->format('M j, Y');
            $this->bet_count =  auth('user')->user()->bets()->sum('point');
            $this->depositWithdrawes = auth('user')->user()->depositWithdraws()->get();
            $this->depositWithdrawCount = auth('user')->user()->depositWithdraws()->sum('point');
            $this->paymentTypes = PaymentAccountType::all();
            $this->authUserPaymentTypes = auth('user')->user()->paymentAccounts()->get();

            $accountTypeMappings = [
                'KBZPay' => ['kbzPayName', 'kbzPayNumber'],
                'KBZ Account' => ['kbzAccountName', 'kbzAccountNumber'],
                'Yoma Account' => ['yomaAccountName', 'yomaAccountNumber'],
                'CB Account' => ['cbAccountName', 'cdAccountNumber'],
                'AYA Account' => ['ayaAccountName', 'ayaAccountNumber'],
                'WaveMoney' => ['waveMoneyAccountName', 'waveMoneyAccountNumber'],
            ];

            foreach ($this->authUserPaymentTypes as $authUserPaymentType) {
                $accountType = $authUserPaymentType->accountType->name;

                if (isset($accountTypeMappings[$accountType])) {
                    list($nameProperty, $numberProperty) = $accountTypeMappings[$accountType];

                    $this->$nameProperty = $authUserPaymentType->name;
                    $this->$numberProperty = $authUserPaymentType->number;
                }
            }

        }


        return view('livewire.user-info');
    }

    private function alert($type,$title,$message)
    {
        $this->alert_type = $type;
        $this->alert_title = $title;
        $this->alert_message = $message;
        $this->showMessage = true;
    }

    public function showTickets($id)
    {
        $betData = Bet::find($id);
        $ticketNumbers = [];
        foreach ($betData->tickets as $ticket) {
            $ticketNumbers[] = array_map('intval', str_split($ticket));
        }
        $this->showTicketNumbers = $ticketNumbers;
        $this->showTicketDate = Carbon::parse($betData->created_at)->format('d M Y ');
        $this->showTicketCount =count($betData->tickets)-1;
        count($betData->tickets) > 1 ? $this->showTicketBox = true : $this->showPurchaseOneTicket = true;
    }

    public function updateBankInfo()
    {
        $this->updatePaymentAccount('KBZPay', $this->kbzPayName, $this->kbzPayNumber);
        $this->updatePaymentAccount('KBZ Account', $this->kbzAccountName, $this->kbzAccountNumber);
        $this->updatePaymentAccount('Yoma Account', $this->yomaAccountName, $this->yomaAccountNumber);
        $this->updatePaymentAccount('CB Account', $this->cbAccountName, $this->cdAccountNumber);
        $this->updatePaymentAccount('AYA Account', $this->ayaAccountName, $this->ayaAccountNumber);
        $this->updatePaymentAccount('WaveMoney', $this->waveMoneyAccountName, $this->waveMoneyAccountNumber);

        $this->alert('success',__('web.update_profile_success_title'),__('web.update_profile_success_message'));

    }

    public function deletePaymentAccount($name)
    {
        $accountProperties = [
            'KBZPay' => ['kbzPayName', 'kbzPayNumber'],
            'KBZ Account' => ['kbzAccountName', 'kbzAccountNumber'],
            'Yoma Account' => ['yomaAccountName', 'yomaAccountNumber'],
            'CB Account' => ['cbAccountName', 'cdAccountNumber'],
            'AYA Account' => ['ayaAccountName', 'ayaAccountNumber'],
            'WaveMoney' => ['waveMoneyAccountName', 'waveMoneyAccountNumber'],
        ];

        if (array_key_exists($name, $accountProperties)) {
            list($propertyName1, $propertyName2) = $accountProperties[$name];

            $this->$propertyName1 = '';
            $this->$propertyName2 = '';
        }
    }


    private function updatePaymentAccount($accountTypeName, $name, $number)
    {
        $accountTypeId = PaymentAccountType::where('name', $accountTypeName)->pluck('id')->first();

        if ($name !== null && $number !== null) {
            auth('user')->user()->paymentAccounts()->updateOrCreate(
                ['payment_account_type_id' => $accountTypeId],
                ['name' => $name, 'number' => $number]
            );
        }

        if ($name == null && $number == null) {
            $exit = auth('user')->user()->paymentAccounts()->where('payment_account_type_id',$accountTypeId)->first();
            $exit->delete();
        }
    }
}
