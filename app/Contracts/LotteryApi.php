<?php

namespace App\Contracts;

interface LotteryApi
{
    public function health();

    public function fetchResults(string $draw_id);
}
