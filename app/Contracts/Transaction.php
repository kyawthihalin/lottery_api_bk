<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

interface Transaction
{
    public function user(): MorphTo|BelongsTo;

    public function logs(): MorphMany;
}
