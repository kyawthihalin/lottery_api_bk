<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

interface TransactionUser
{
    public function transactionLogs(): HasMany|MorphMany;
}
