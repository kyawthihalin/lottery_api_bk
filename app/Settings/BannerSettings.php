<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class BannerSettings extends Settings
{
    public array $user_app;

    public array $website;

    public static function group(): string
    {
        return 'banner';
    }

    public function getBannersByPath(string $path)
    {
        return match ($path) {
            'api/user/assets' => $this->user_app,
            'banners' => $this->website,
        };
    }
}
