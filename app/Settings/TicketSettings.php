<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class TicketSettings extends Settings
{
    public array $restricted_tickets;

    public int $purchase_limit_per_ticket;

    public int $tickets_limit_per_purchase;

    public int $random_ticket_groups_count_per_draw_id;

    public static function group(): string
    {
        return 'ticket';
    }
}
