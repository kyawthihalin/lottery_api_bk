<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class PointRateSettings extends Settings
{
    public int $min_agent;

    public int $max_agent;

    public int $min_master;

    public int $max_master;

    public int $min_senior;

    public int $max_senior;

    public static function group(): string
    {
        return 'point_rate';
    }
}
