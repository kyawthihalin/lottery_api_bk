<?php

namespace App\Settings;

use Illuminate\Validation\Rules\Password;
use Spatie\LaravelSettings\Settings;

class AuthSettings extends Settings
{
    public int $admin_password_min;

    public int $agent_password_min;

    public int $user_password_min;

    public bool $admin_password_letters;

    public bool $agent_password_letters;

    public bool $user_password_letters;

    public bool $admin_password_mixed_case;

    public bool $agent_password_mixed_case;

    public bool $user_password_mixed_case;

    public bool $admin_password_numbers;

    public bool $agent_password_numbers;

    public bool $user_password_numbers;

    public bool $admin_password_symbols;

    public bool $agent_password_symbols;

    public bool $user_password_symbols;

    public int $agent_secret_code_min;

    public bool $agent_secret_code_letters;

    public bool $agent_secret_code_mixed_case;

    public bool $agent_secret_code_numbers;

    public bool $agent_secret_code_symbols;

    public int $agent_password_max_attempt;

    public int $admin_password_max_attempt;

    public int $user_password_max_attempt;

    public int $agent_secret_code_max_attempt;

    public int $agent_secret_code_interval;

    public static function group(): string
    {
        return 'auth';
    }

    public function getPasswordRule(string $class)
    {
        $attrPrefix = $this->getAttrPrefixByClass($class);
        $attrMinName = $attrPrefix.'password_min';
        $rule = Password::min($this->$attrMinName);

        $attrLetterName = $attrPrefix.'password_letters';

        if ($this->$attrLetterName) {
            $rule->letters();
        }
        $attrMixedCaseName = $attrPrefix.'password_mixed_case';
        if ($this->$attrMixedCaseName) {
            $rule->mixedCase();
        }
        $attrNumberName = $attrPrefix.'password_numbers';
        if ($this->$attrNumberName) {
            $rule->numbers();
        }
        $attrSymbolName = $attrPrefix.'password_symbols';
        if ($this->$attrSymbolName) {
            $rule->symbols();
        }

        return $rule;
    }

    public function getSecretCodeRule(string $class)
    {
        $attrPrefix = $this->getAttrPrefixByClass($class);
        $attrMinName = $attrPrefix.'secret_code_min';
        $rule = Password::min($this->$attrMinName);

        $attrLetterName = $attrPrefix.'secret_code_letters';

        if ($this->$attrLetterName) {
            $rule->letters();
        }
        $attrMixedCaseName = $attrPrefix.'secret_code_mixed_case';
        if ($this->$attrMixedCaseName) {
            $rule->mixedCase();
        }
        $attrNumberName = $attrPrefix.'secret_code_numbers';
        if ($this->$attrNumberName) {
            $rule->numbers();
        }
        $attrSymbolName = $attrPrefix.'secret_code_symbols';
        if ($this->$attrSymbolName) {
            $rule->symbols();
        }

        return $rule;
    }

    public function getPasswordMaxAttemptByClass(string $class)
    {
        $attrPrefix = $this->getAttrPrefixByClass($class);
        $attrName = $attrPrefix.'password_max_attempt';

        return $this->$attrName;
    }

    public function getSecretCodeMaxAttemptByClass(string $class)
    {
        $attrPrefix = $this->getAttrPrefixByClass($class);
        $attrName = $attrPrefix.'secret_code_max_attempt';

        return $this->$attrName;
    }

    private function getAttrPrefixByClass(string $class): string
    {
        return match ($class) {
            \App\Models\Admin::class => 'admin_',
            \App\Models\Agent::class => 'agent_',
            \App\Models\Master::class => 'agent_',
            \App\Models\Senior::class => 'agent_',
            \App\Models\User::class => 'user_',
        };
    }
}
