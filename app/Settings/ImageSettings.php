<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class ImageSettings extends Settings
{
    public ?string $app_logo;

    public ?string $app_background;

    public ?string $website_background;

    public ?string $group_ticket_frame_website;

    public ?string $group_ticket_frame_mobile;

    public ?string $ticket_background;

    public ?string $won_image;

    public ?string $lost_image;

    public ?string $default_avatar;

    public static function group(): string
    {
        return 'image';
    }
}
