<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class FileSettings extends Settings
{
    public ?string $user_app;

    public ?string $agent_app;

    public ?string $master_app;

    public ?string $senior_app;

    public static function group(): string
    {
        return 'file';
    }
}
