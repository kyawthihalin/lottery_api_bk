<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class GeneralSettings extends Settings
{
    public bool $force_use_secret_code;

    public array $lottery_dates;

    public ?string $contact_viber;

    public ?string $contact_facebook;

    public ?string $contact_linkedin;

    public ?string $contact_youtube;

    public ?string $contact_instagram;

    public ?string $contact_twitter;

    public ?string $contact_mail;

    public ?string $contact_phone;

    public ?string $live_link;

    public static function group(): string
    {
        return 'general';
    }
}
