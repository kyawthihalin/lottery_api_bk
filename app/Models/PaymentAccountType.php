<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentAccountType extends Model
{
    use HasFactory;
    protected $table = 'payment_account_types';

    public function accounts()
    {
        return $this->hasMany(PaymentAccount::class, 'payment_account_type_id');
    }
}
