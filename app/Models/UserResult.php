<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserResult extends Model
{
    protected $casts = [
        'tickets' => 'array',
    ];

    protected $table = 'user_result';

    public function result(): BelongsTo
    {
        return $this->belongsTo(Result::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
