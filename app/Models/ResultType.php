<?php

namespace App\Models;

use App\Enums\ResultTypeCode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class ResultType extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['name'];

    protected $casts = [
        'name' => 'array',
        'code' => ResultTypeCode::class,
    ];

    public function results(): HasMany
    {
        return $this->hasMany(Result::class, 'result_type_id');
    }
}
