<?php

namespace App\Models;

use App\Contracts\Transaction;
use App\Enums\TransactionType;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Bet extends Model implements Transaction
{
    use HasFactory;

    protected $casts = [
        'transaction_type' => TransactionType::class,
        'tickets' => 'array',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function logs(): MorphMany
    {
        return $this->morphMany(TransactionLog::class, 'loggable');
    }

    public function ticketsCount(): Attribute
    {
        return Attribute::make(
            get: fn () => count($this->tickets)
        );
    }
}
