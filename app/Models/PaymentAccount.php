<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class PaymentAccount extends Model
{
    use HasFactory, HasUuids;

    protected $primaryKey = 'uuid';

    public function user(): MorphTo
    {
        return $this->morphTo();
    }

    public function accountType(): BelongsTo
    {
        return $this->belongsTo(PaymentAccountType::class, 'payment_account_type_id');
    }
}
