<?php

namespace App\Models;

use App\Enums\AppTokenAction;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class AppToken extends Model
{
    use HasFactory, HasUuids;

    protected $primaryKey = 'uuid';

    protected $casts = [
        'action' => AppTokenAction::class,
    ];

    public function user(): MorphTo
    {
        return $this->morphTo();
    }
}
