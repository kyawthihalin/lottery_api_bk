<?php

namespace App\Models;

use App\Contracts\TransactionUser;
use App\Enums\Language;
use App\Enums\NrcType;
use App\Enums\OsType;
use App\Facades\Draw;
use App\Facades\Settlement as SettlementFacade;
use App\Services\StorageService;
use App\Settings\ImageSettings;
use App\Traits\HasAppTokens;
use App\Traits\HasNrc;
use App\Traits\HasParentAttributes;
use App\Traits\HasSettlements;
use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasAvatar;
use Filament\Panel;
use Filament\Panel\Concerns\HasNotifications;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Senior extends PolicyUser implements FilamentUser, HasAvatar, TransactionUser
{
    use HasApiTokens;
    use HasAppTokens;
    use HasFactory;
    use HasNotifications;
    use HasNrc;
    use HasParentAttributes;
    use HasRelationships;
    use HasSettlements;
    use Notifiable;

    protected $casts = [
        'is_secret_code_required' => 'boolean',
        'language' => Language::class,
        'os_type' => OsType::class,
        'nrc_type' => NrcType::class,
        'password_mistook_at' => 'datetime',
        'secret_code_mistook_at' => 'datetime',
        'locked_at' => 'datetime',
    ];

    protected $hidden = [
        'password',
        'secret_code',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'user_name';
    }

    public function canImpersonate()
    {
        return false;
    }

    public function canAccessPanel(Panel $panel): bool
    {
        return $panel->getId() === 'senior';
    }

    public function getFilamentAvatarUrl(): ?string
    {
        return StorageService::getUrl($this->profile_image ?? app(ImageSettings::class)->default_avatar);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(Admin::class);
    }

    public function masters(): HasMany
    {
        return $this->hasMany(Master::class);
    }

    public function agents(): HasManyThrough
    {
        return $this->hasManyThrough(Agent::class, Master::class);
    }

    public function users(): HasManyDeep
    {
        return $this->hasManyDeep(User::class, [Master::class, Agent::class]);
    }

    public function depositWithdraws(): MorphMany
    {
        return $this->morphMany(DepositWithdraw::class, 'user');
    }

    public function settlements(): MorphMany
    {
        return $this->morphMany(Settlement::class, 'user');
    }

    public function transactionLogs(): MorphMany
    {
        return $this->morphMany(TransactionLog::class, 'user');
    }

    public function feedbacks(): MorphMany
    {
        return $this->morphMany(Feedback::class, 'user');
    }

    protected function nrc(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                return "{$attributes['nrc_state']}/{$attributes['nrc_township']}({$attributes['nrc_type']}){$attributes['nrc_number']}";
            },
        );
    }

    protected function totalBalance(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                $masters = Master::where('senior_id', $attributes['id'])->get();
                $agents = Agent::whereIn('master_id', $masters->pluck('id'))->get();

                return $attributes['point'] + $masters->sum('point') + $agents->sum('point');
            },
        );
    }

    protected function profit(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                $senior = Senior::find($attributes['id']);

                $boughPoint = SettlementFacade::getBoughtPoint($senior);

                $pointRate = $senior->point_rate;

                $usedPoint = SettlementFacade::getUsedPoint($senior);

                return ($boughPoint - $pointRate) * $usedPoint;
            },
        );
    }

    protected function totalWonLottery(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Draw::getTotalWonLottery(self::find($attributes['id'])),
        );
    }

    /**
     * Specifies the user's FCM token
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
        return $this->firebase_token;
    }

    protected function profileImageUrl(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => StorageService::getUrl($attributes['profile_image'] ?? app(ImageSettings::class)->default_avatar),
        );
    }
}
