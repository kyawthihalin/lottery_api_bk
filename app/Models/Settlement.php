<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Settlement extends Model
{
    use HasFactory;

    public function user(): MorphTo
    {
        return $this->morphTo();
    }
}
