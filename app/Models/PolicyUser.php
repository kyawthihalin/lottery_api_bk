<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class PolicyUser extends Authenticatable
{
    use HasRoles;
}
