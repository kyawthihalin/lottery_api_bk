<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Faq extends Model
{
    use HasFactory, HasTranslations, HasUuids;

    protected $primaryKey = 'uuid';

    protected $fillable = ['order'];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public $translatable = ['question', 'answer'];
}
