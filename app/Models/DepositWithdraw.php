<?php

namespace App\Models;

use App\Contracts\Transaction;
use App\Enums\TransactionType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class DepositWithdraw extends Model implements Transaction
{
    use HasFactory;

    protected $casts = [
        'transaction_type' => TransactionType::class,
    ];

    public function user(): MorphTo
    {
        return $this->morphTo();
    }

    public function logs(): MorphMany
    {
        return $this->morphMany(TransactionLog::class, 'loggable');
    }
}
