<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Contracts\TransactionUser;
use App\Enums\Gender;
use App\Enums\Language;
use App\Enums\OsType;
use App\Facades\Draw;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements TransactionUser
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'language' => Language::class,
        'gender' => Gender::class,
        'os_type' => OsType::class,
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'locked_at' => 'datetime',
        'password_mistook_at' => 'datetime',
    ];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function agent(): BelongsTo
    {
        return $this->belongsTo(Agent::class);
    }

    public function bets(): HasMany
    {
        return $this->hasMany(Bet::class);
    }

    public function depositWithdraws(): MorphMany
    {
        return $this->morphMany(DepositWithdraw::class, 'user');
    }

    public function transactionLogs(): MorphMany
    {
        return $this->morphMany(TransactionLog::class, 'user');
    }

    public function feedbacks(): MorphMany
    {
        return $this->morphMany(Feedback::class, 'user');
    }

    public function paymentAccounts(): MorphMany
    {
        return $this->morphMany(PaymentAccount::class, 'user');
    }

    public function results(): BelongsToMany
    {
        return $this->belongsToMany(Result::class, 'user_result', 'user_id', 'result_uuid')->withPivot('tickets', 'draw_id');
    }

    /**
     * Specifies the user's FCM token
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
        return $this->firebase_token;
    }

    protected function currentTicketCount(): Attribute
    {
        return Attribute::make(
            get: function () {
                $currentTickets = $this->bets()->where('draw_id', Draw::getPreviousDrawId())->pluck('tickets');

                $currentTicketsCount = 0;
                foreach ($currentTickets as $ticket) {
                    $currentTicketsCount += count($ticket);
                }

                return $currentTicketsCount;
            }
        );
    }

    protected function unreadNotiCount(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->unreadNotifications()->count()
        );
    }
}
