<?php

namespace App\Models;

use App\Enums\Language;
use BezhanSalleh\FilamentShield\Traits\HasPanelShield;
use Filament\Models\Contracts\FilamentUser;
use Filament\Panel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Spatie\Permission\Traits\HasRoles;

class Admin extends PolicyUser implements FilamentUser
{
    use HasFactory, HasPanelShield, HasRoles, Notifiable;

    protected $casts = [
        'password' => 'hashed',
        'password_mistook_at' => 'datetime',
        'locked_at' => 'datetime',
    ];

    protected $hidden = [
        'password',
    ];

    public function canImpersonate()
    {
        // Let's prevent impersonating other users at our own company
        return Str::startsWith($this->email, [
            'naungyehtet',
            'wannaminpaing',
            'aunghtetmyat',
            'nantsusandi',
        ]);
    }

    public function getFilamentName(): string
    {
        return $this->name;
    }

    public function seniors(): HasMany
    {
        return $this->hasMany(Senior::class);
    }

    public function canAccessPanel(Panel $panel): bool
    {
        return true;
    }

    protected function language(): Attribute
    {
        return Attribute::make(
            get: fn () => Language::from('my'),
        );
    }
}
