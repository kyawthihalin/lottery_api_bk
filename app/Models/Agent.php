<?php

namespace App\Models;

use App\Contracts\TransactionUser;
use App\Enums\Language;
use App\Enums\NrcType;
use App\Enums\OsType;
use App\Facades\Draw;
use App\Facades\Settlement as SettlementFacade;
use App\Services\StorageService;
use App\Settings\ImageSettings;
use App\Settings\PointRateSettings;
use App\Traits\HasAppTokens;
use App\Traits\HasNrc;
use App\Traits\HasParentAttributes;
use App\Traits\HasSettlements;
use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasAvatar;
use Filament\Models\Contracts\HasName;
use Filament\Panel;
use Filament\Panel\Concerns\HasNotifications;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Agent extends PolicyUser implements FilamentUser, HasAvatar, HasName, TransactionUser
{
    use HasApiTokens;
    use HasAppTokens;
    use HasFactory;
    use HasNotifications;
    use HasNrc;
    use HasParentAttributes;
    use HasSettlements;
    use Notifiable;

    protected $casts = [
        'is_secret_code_required' => 'boolean',
        'language' => Language::class,
        'os_type' => OsType::class,
        'nrc_type' => NrcType::class,
        'password_mistook_at' => 'datetime',
        'secret_code_mistook_at' => 'datetime',
        'locked_at' => 'datetime',
    ];

    protected $hidden = [
        'password',
        'secret_code',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'user_name';
    }

    public function canImpersonate()
    {
        return false;
    }

    public function canAccessPanel(Panel $panel): bool
    {
        return $panel->getId() === 'agent';
    }

    public function getFilamentName(): string
    {
        return $this->name;
    }

    public function getFilamentAvatarUrl(): ?string
    {
        return StorageService::getUrl($this->profile_image ?? app(ImageSettings::class)->default_avatar);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Master::class, 'master_id');
    }

    public function master(): BelongsTo
    {
        return $this->belongsTo(Master::class);
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function depositWithdraws(): MorphMany
    {
        return $this->morphMany(DepositWithdraw::class, 'user');
    }

    public function settlements(): MorphMany
    {
        return $this->morphMany(Settlement::class, 'user');
    }

    public function transactionLogs(): MorphMany
    {
        return $this->morphMany(TransactionLog::class, 'user');
    }

    public function feedbacks(): MorphMany
    {
        return $this->morphMany(Feedback::class, 'user');
    }

    /**
     * Specifies the user's FCM token
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
        return $this->firebase_token;
    }

    protected function totalBalance(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                return $attributes['point'];
            },
        );
    }

    protected function profit(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                $agent = Agent::find($attributes['id']);

                $maximumPointRate = app(PointRateSettings::class)->max_agent;

                $usedPoint = SettlementFacade::getUsedPoint($agent);

                return ($maximumPointRate - $agent->point_rate) * $usedPoint;
            },
        );
    }

    protected function totalWonLottery(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Draw::getTotalWonLottery(self::find($attributes['id'])),
        );
    }

    protected function profileImageUrl(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => StorageService::getUrl($attributes['profile_image'] ?? app(ImageSettings::class)->default_avatar),
        );
    }
}
