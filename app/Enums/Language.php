<?php

namespace App\Enums;

enum Language: string
{
    case English = 'en';
    case Myanmar = 'my';
    // case Thailand = 'th';
}
