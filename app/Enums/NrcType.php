<?php

namespace App\Enums;

use Filament\Support\Contracts\HasLabel;

enum NrcType: string implements HasLabel
{
    case N = 'N';
    case E = 'E';
    case P = 'P';
    case T = 'T';
    case R = 'R';
    case S = 'S';

    public function getLabel(): string
    {
        return match ($this) {
            self::N => __('nrc.types.N'),
            self::E => __('nrc.types.E'),
            self::P => __('nrc.types.P'),
            self::T => __('nrc.types.T'),
            self::R => __('nrc.types.R'),
            self::S => __('nrc.types.S'),
        };
    }
}
