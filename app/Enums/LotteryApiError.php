<?php

namespace App\Enums;

enum LotteryApiError: string
{
    case NOT_FOUND = 'not_found';
    case FAILED = 'failed';

    public function getMessage(): string
    {
        return match ($this) {
            self::NOT_FOUND => __('general.get_lottery_results.not_found'),
            self::FAILED => __('general.get_lottery_results.failed'),
        };
    }
}
