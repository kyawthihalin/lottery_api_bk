<?php

namespace App\Enums;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;

enum TransactionType: string
{
    case SeniorDeposit = 'Senior Deposit';
    case MasterDeposit = 'Master Deposit';
    case AgentDeposit = 'Agent Deposit';
    case SeniorWithdraw = 'Senior Withdraw';
    case MasterWithdraw = 'Master Withdraw';
    case AgentWithdraw = 'Agent Withdraw';
    case UserDeposit = 'User Deposit';
    case UserWithdraw = 'User Withdraw';
    case Bet = 'Bet';

    public function getPrefix()
    {
        $words = explode(' ', strtolower($this->value));
        $prefix = '';
        foreach ($words as $word) {
            $prefix .= substr($word, 0, 1);
        }

        return strtoupper($prefix);
    }

    public function isWithAdmin(): bool
    {
        return match ($this) {
            self::SeniorDeposit => true,
            self::SeniorWithdraw => true,
            self::Bet => true,
            default => false,
        };
    }

    public function getUserOperation(): TransactionOperation
    {
        return match ($this) {
            self::SeniorDeposit => TransactionOperation::Addition,
            self::SeniorWithdraw => TransactionOperation::Substraction,
            self::MasterDeposit => TransactionOperation::Addition,
            self::MasterWithdraw => TransactionOperation::Substraction,
            self::AgentDeposit => TransactionOperation::Addition,
            self::AgentWithdraw => TransactionOperation::Substraction,
            self::UserDeposit => TransactionOperation::Addition,
            self::UserWithdraw => TransactionOperation::Substraction,
            self::Bet => TransactionOperation::Substraction,
        };
    }

    public function getOtherUserOperation(): TransactionOperation
    {
        return match ($this) {
            self::MasterDeposit => TransactionOperation::Substraction,
            self::MasterWithdraw => TransactionOperation::Addition,
            self::AgentDeposit => TransactionOperation::Substraction,
            self::AgentWithdraw => TransactionOperation::Addition,
            self::UserDeposit => TransactionOperation::Substraction,
            self::UserWithdraw => TransactionOperation::Addition,
        };
    }

    public static function getFilterableValuesByUser(User|Agent|Master|Senior|Admin $user): array
    {
        return match (get_class($user)) {
            \App\Models\User::class => [
                self::UserDeposit->value,
                self::UserWithdraw->value,
            ],
            \App\Models\Agent::class => [
                self::AgentDeposit->value,
                self::AgentWithdraw->value,
                self::UserDeposit->value,
                self::UserWithdraw->value,
            ],
            \App\Models\Master::class => [
                self::MasterDeposit->value,
                self::MasterWithdraw->value,
                self::AgentDeposit->value,
                self::AgentWithdraw->value,
                self::UserDeposit->value,
                self::UserWithdraw->value,
            ],
            default => [
                self::SeniorDeposit->value,
                self::SeniorWithdraw->value,
                self::MasterDeposit->value,
                self::MasterWithdraw->value,
                self::AgentDeposit->value,
                self::AgentWithdraw->value,
                self::UserDeposit->value,
                self::UserWithdraw->value,
            ],
        };
    }

    public function getFunction(): string
    {
        return match ($this) {
            self::Bet => 'bets',
            default => 'depositWithdraws',
        };
    }

    public function getBreifLabel(): string
    {
        return match ($this) {
            self::SeniorDeposit => strtolower(__('general.deposit')),
            self::SeniorWithdraw => strtolower(__('general.withdraw')),
            self::MasterDeposit => strtolower(__('general.deposit')),
            self::MasterWithdraw => strtolower(__('general.withdraw')),
            self::AgentDeposit => strtolower(__('general.deposit')),
            self::AgentWithdraw => strtolower(__('general.withdraw')),
            self::UserDeposit => strtolower(__('general.deposit')),
            self::UserWithdraw => strtolower(__('general.withdraw')),
            self::Bet => strtolower(__('general.purchase_ticket')),
        };
    }

    public function getBreifCode(): string
    {
        return match ($this) {
            self::SeniorDeposit => 'Deposit',
            self::SeniorWithdraw => 'Withdrawal',
            self::MasterDeposit => 'Deposit',
            self::MasterWithdraw => 'Withdrawal',
            self::AgentDeposit => 'Deposit',
            self::AgentWithdraw => 'Withdrawal',
            self::UserDeposit => 'Deposit',
            self::UserWithdraw => 'Withdrawal',
            self::Bet => 'Purchase Ticket',
        };
    }
}
