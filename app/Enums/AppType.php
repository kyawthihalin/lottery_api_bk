<?php

namespace App\Enums;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use Filament\Support\Contracts\HasLabel;

enum AppType: string implements HasLabel
{
    case Senior = 'Senior';
    case Master = 'Master';
    case Agent = 'Agent';
    case User = 'User';
    case Admin = 'Admin';

    public function getModel(): string
    {
        return match ($this) {
            self::Senior => Senior::class,
            self::Master => Master::class,
            self::Agent => Agent::class,
            self::User => User::class,
            self::Admin => Admin::class,
        };
    }

    public function getLabel(): string
    {
        return match ($this) {
            self::Senior => __('general.senior_app'),
            self::Master => __('general.master_app'),
            self::Agent => __('general.agent_app'),
            self::User => __('general.user_app'),
            self::Admin => __('general.admin_app'),
        };
    }
}
