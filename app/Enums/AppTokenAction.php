<?php

namespace App\Enums;

enum AppTokenAction: string
{
    case LoginSecret = 'login_secret';

    public function getLifetime(): int
    {
        return match ($this) {
            self::LoginSecret => 5,
        };
    }
}
