<?php

namespace App\Enums;

enum ResultTypeCode: string
{
    case First = 'first';
    case ClosetFirst = 'closest_first';
    case Second = 'second';
    case Third = 'third';
    case Forth = 'forth';
    case Fifth = 'fifth';
    case FirstThree = 'first_three';
    case LastThree = 'last_three';
    case LastTwo = 'last_two';

    public function getResultCount(): int
    {
        return match ($this) {
            self::First => 1,
            self::ClosetFirst => 2,
            self::Second => 5,
            self::Third => 10,
            self::Forth => 50,
            self::Fifth => 100,
            self::FirstThree => 2,
            self::LastThree => 2,
            self::LastTwo => 1,
        };
    }

    public function getDigitCount(): int
    {
        return match ($this) {
            self::FirstThree => 3,
            self::LastThree => 3,
            self::LastTwo => 2,
            default => 6,
        };
    }

    public function getPrefix(): string
    {
        return match ($this) {
            self::FirstThree => '---',
            self::LastTwo => '----',
            default => '',
        };
    }

    public function getSuffix(): string
    {
        return match ($this) {
            self::LastThree => '---',
            default => '',
        };
    }
}
