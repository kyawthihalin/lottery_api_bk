<?php

namespace App\Enums;

enum OsType: string
{
    case Android = 'Android';
    case iOS = 'iOS';
}
