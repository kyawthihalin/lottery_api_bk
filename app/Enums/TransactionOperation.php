<?php

namespace App\Enums;

enum TransactionOperation
{
    case Addition;
    case Substraction;
}
