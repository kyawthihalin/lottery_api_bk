<?php

namespace App\Filament\Master\Resources\AgentResource\Pages;

use App\Actions\GenerateUserCode;
use App\Filament\Master\Resources\AgentResource;
use App\Models\Agent;
use App\Models\State;
use Filament\Actions;
use Filament\Facades\Filament;
use Filament\Resources\Pages\ListRecords;

class ListAgents extends ListRecords
{
    protected static string $resource = AgentResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->mutateFormDataUsing(function (array $data) {
                    $state = State::find($data['state_id']);
                    $data['code'] = (new GenerateUserCode())->execute(Agent::class, $state->code);
                    $data['master_id'] = Filament::auth()->id();

                    return $data;
                }),
        ];
    }
}
