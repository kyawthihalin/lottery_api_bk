<?php

namespace App\Filament\Master\Resources;

use App\Filament\Master\Resources\AgentResource\Pages;
use App\Filament\Resources\AgentResource as AdminAgentResource;
use App\Models\Agent;
use Filament\Facades\Filament;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class AgentResource extends Resource
{
    protected static ?string $model = Agent::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    protected static ?string $slug = 'account-management';

    public static function getNavigationLabel(): string
    {
        return __('general.account_management');
    }

    public static function getModelLabel(): string
    {
        return ___('general.agents');
    }

    public static function form(Form $form): Form
    {
        return AdminAgentResource::form($form);
    }

    public static function table(Table $table): Table
    {
        return AdminAgentResource::table($table)
            ->modifyQueryUsing(fn (Builder $query) => $query->where('master_id', Filament::auth()->id()))
            ->actions([
                Tables\Actions\Action::make('settlement')
                    ->label(___('general.actions.settlement'))
                    ->requiresConfirmation(),
                Tables\Actions\EditAction::make()
                    ->iconButton(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAgents::route('/'),
        ];
    }
}
