<?php

namespace App\Filament\Master\Resources\DepositWithdrawResource\Pages;

use App\Actions\MakeTransaction;
use App\Enums\TransactionType;
use App\Events\DepositCompleted;
use App\Filament\Master\Resources\DepositWithdrawResource;
use App\Models\Agent;
use App\Models\Master;
use Filament\Actions;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Get;
use Filament\Notifications\Notification;
use Filament\Resources\Components\Tab;
use Filament\Resources\Pages\ListRecords;
use Filament\Support\Enums\MaxWidth;
use Illuminate\Database\Eloquent\Builder;

class ListDepositWithdraws extends ListRecords
{
    protected static string $resource = DepositWithdrawResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\Action::make('deposit')
                ->label(__('general.form.deposit.action'))
                ->color('success')
                ->modalHeading(__('general.point').' '.__('general.form.deposit.action'))
                ->modalSubmitActionLabel(__('general.form.deposit.action'))
                ->form($this->getActionForm())
                ->action(function (array $data): void {
                    $master = Master::find(Filament::auth()->id());
                    $agent = $master->agents()->where('id', $data['agent_id'])->first();

                    if (! $agent) {
                        abort(404, 'Agent does not exist');
                    }

                    if ($master->point < $data['point']) {
                        Notification::make()
                            ->title(__('general.errors.point_not_enough', [
                                'transaction_type' => TransactionType::AgentDeposit->getBreifLabel(),
                            ]))
                            ->danger()
                            ->send();

                        return;
                    }

                    $deposit = (new MakeTransaction(
                        transactionType: TransactionType::AgentDeposit,
                        point: $data['point'],
                        user: $agent,
                        otherUser: $master,
                        otherFields: [
                            'point_rate' => $agent->point_rate,
                        ]
                    ))->execute();

                    DepositCompleted::dispatch($deposit);

                    Notification::make()
                        ->success()
                        ->title(__('deposit.notification.title.completed_from', [
                            'point' => number_format($data['point']),
                        ]))
                        ->body(__('deposit.notification.message.completed_from', [
                            'point' => number_format($data['point']),
                            'name' => $agent->name,
                        ]))
                        ->send();
                })->modalWidth(MaxWidth::Medium),
            Actions\Action::make('withdraw')
                ->label(__('general.form.withdraw.action'))
                ->modalHeading(__('general.point').' '.__('general.form.withdraw.action'))
                ->form($this->getActionForm())
                ->modalSubmitActionLabel(__('general.form.withdraw.action'))
                ->action(function (array $data): void {
                    $master = Master::find(Filament::auth()->id());
                    $agent = $master->agents()->where('id', $data['agent_id'])->first();

                    if (! $agent) {
                        abort(404, 'Agent does not exist');
                    }

                    if ($agent->point < $data['point']) {
                        Notification::make()
                            ->title(__('general.errors.point_not_enough_user', [
                                'name' => $agent->name,
                                'transaction_type' => TransactionType::AgentWithdraw->getBreifLabel(),
                            ]))
                            ->danger()
                            ->send();

                        return;
                    }

                    $withdraw = (new MakeTransaction(
                        transactionType: TransactionType::AgentWithdraw,
                        point: $data['point'],
                        user: $agent,
                        otherUser: $master,
                        otherFields: [
                            'point_rate' => $agent->point_rate,
                        ]
                    ))->execute();

                    DepositCompleted::dispatch($withdraw);

                    Notification::make()
                        ->success()
                        ->title(__('withdraw.notification.title.completed_by', [
                            'point' => number_format($data['point']),
                        ]))
                        ->body(__('withdraw.notification.message.completed_by', [
                            'point' => number_format($data['point']),
                            'name' => $agent->name,
                        ]))
                        ->send();
                })->modalWidth(MaxWidth::Medium),
        ];
    }

    protected function getActionForm(): array
    {
        return [
            Forms\Components\Select::make('agent_id')
                ->label(___('general.agents'))
                ->hint(fn (Get $get) => Agent::find($get('agent_id'))?->point)
                ->hintColor('warning')
                ->hintIcon('gmdi-monetization-on-o')
                ->selectablePlaceholder(false)
                ->options(Master::find(Filament::auth()->id())->agents()->pluck('name', 'id'))
                ->searchable()
                ->live()
                ->required(),
            Forms\Components\TextInput::make('point')
                ->label(__('general.point'))
                ->numeric()
                ->rules([
                    'integer',
                    'min:1',
                ])
                ->required(),
        ];
    }

    public function getTabs(): array
    {
        return [
            'Master' => Tab::make(___('general.masters'))
                ->modifyQueryUsing(function (Builder $query) {
                    $query->where('user_id', Filament::auth()->id())
                        ->where('user_type', \App\Models\Master::class)
                        ->whereIn('transaction_type', [
                            TransactionType::MasterDeposit->value,
                            TransactionType::MasterWithdraw->value,
                        ]);
                }),
            'Agents' => Tab::make(___('general.agents'))
                ->modifyQueryUsing(function (Builder $query) {
                    $agentIds = Master::find(Filament::auth()->id())->agents()->pluck('id')->toArray();

                    $query->whereIn('user_id', $agentIds)
                        ->where('user_type', \App\Models\Agent::class)
                        ->whereIn('transaction_type', [
                            TransactionType::AgentDeposit->value,
                            TransactionType::AgentWithdraw->value,
                        ]);
                }),
            'Users' => Tab::make(___('general.users'))
                ->modifyQueryUsing(function (Builder $query) {
                    $userIds = Master::find(Filament::auth()->id())->users()->pluck('users.id')->toArray();

                    $query->whereIn('user_id', $userIds)
                        ->where('user_type', \App\Models\User::class)
                        ->whereIn('transaction_type', [
                            TransactionType::UserDeposit->value,
                            TransactionType::UserWithdraw->value,
                        ]);
                }),
        ];
    }
}
