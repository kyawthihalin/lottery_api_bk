<?php

namespace App\Filament\Master\Resources;

use App\Filament\Master\Resources\DepositWithdrawResource\Pages;
use App\Filament\Resources\DepositWithdrawResource as AdminDepositWithdrawResource;
use App\Models\TransactionLog;
use Filament\Resources\Resource;
use Filament\Tables\Table;

class DepositWithdrawResource extends Resource
{
    protected static ?string $model = TransactionLog::class;

    protected static ?string $navigationIcon = 'heroicon-m-arrows-up-down';

    public static function getModelLabel(): string
    {
        return __('general.deposits_withdraws');
    }

    public static function table(Table $table): Table
    {
        return AdminDepositWithdrawResource::table($table);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDepositWithdraws::route('/'),
        ];
    }
}
