<?php

namespace App\Filament\Master\Resources\BetResource\Pages;

use App\Filament\Master\Resources\BetResource;
use Filament\Resources\Pages\ListRecords;

class ListBets extends ListRecords
{
    protected static string $resource = BetResource::class;
}
