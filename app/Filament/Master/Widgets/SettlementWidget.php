<?php

namespace App\Filament\Master\Widgets;

use App\Filament\Widgets\SettlementWidget as AdminSettlementWidget;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Settlement;
use Filament\Facades\Filament;
use Filament\Tables\Table;
use Filament\Widgets\Concerns\InteractsWithPageFilters;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementWidget extends BaseWidget
{
    use InteractsWithPageFilters;

    protected int|string|array $columnSpan = 'full';

    protected static bool $isLazy = false;

    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.history');
    }

    public function table(Table $table): Table
    {
        $master = Master::find(Filament::auth()->id());
        $agentUserNames = $this->filters['agent_ids'] ?? null;

        if ($agentUserNames) {
            $userType = Agent::class;
            $usersIds = $master->agents()->whereIn('user_name', $agentUserNames)->pluck('id')->toArray();
        } else {
            $userType = Master::class;
            $usersIds = [$master->id];
        }

        return $table
            ->query(Settlement::whereIn('user_id', $usersIds)->whereUserType($userType))
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(AdminSettlementWidget::getColumns())
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.history').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }
}
