<?php

namespace App\Filament\Master\Widgets;

use App\Filament\Widgets\SettlementStatWidget as AdminSettlementStatWidget;
use App\Models\Agent;
use App\Models\Master;
use Filament\Facades\Filament;
use Filament\Tables\Enums\ActionsPosition;
use Filament\Tables\Table;
use Filament\Widgets\Concerns\InteractsWithPageFilters;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementStatWidget extends BaseWidget
{
    use InteractsWithPageFilters;

    protected int|string|array $columnSpan = 'full';

    protected static bool $isLazy = false;

    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.stat');
    }

    public function table(Table $table): Table
    {
        $master = Master::find(Filament::auth()->id());
        $agentUserNames = $this->filters['agent_ids'] ?? null;
        $actions = [];

        if ($agentUserNames) {
            $actions[] = AdminSettlementStatWidget::getCreateAction();
            $tableQuery = Agent::whereMasterId($master->id)->whereIn('user_name', $agentUserNames);
        } else {
            $tableQuery = Master::where('id', $master->id);
        }

        return $table
            ->query($tableQuery)
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(AdminSettlementStatWidget::getStatColumns())
            ->actions($actions, position: ActionsPosition::BeforeColumns)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.stat').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }
}
