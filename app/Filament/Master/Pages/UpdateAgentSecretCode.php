<?php

namespace App\Filament\Master\Pages;

use App\Models\Master;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Illuminate\Contracts\Support\Htmlable;
use Rawilk\FilamentPasswordInput\Password;

class UpdateAgentSecretCode extends Page
{
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    protected static ?string $navigationGroup = 'Update Agent';

    protected static string $layout = 'filament-panels::components.layout.index';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.secret_code');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.secret_code');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(2)->schema([
                \Filament\Forms\Components\Select::make('agent_id')
                    ->label(___('general.agents'))
                    ->options(Master::find(Filament::auth()->id())->agents()->pluck('name', 'id'))
                    ->rules([
                        'required',
                        'string',
                    ])
                    ->searchable()
                    ->preload()
                    ->selectablePlaceholder(false)
                    ->required(),
                \Filament\Forms\Components\TextInput::make('your_password')
                    ->label(__('general.form.your_password.label'))
                    ->password()
                    ->rules([
                        'required',
                        'current_password',
                    ])
                    ->required(),
                Password::make('secret_code')
                    ->label(__('general.secret_code'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getSecretCodeRule(\App\Models\Agent::class),
                    ]),
            ]),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $master = Master::find(Filament::auth()->id());
        $agent = $master->agents()->find($data['agent_id']);

        $agent->update([
            'secret_code' => bcrypt($data['secret_code']),
        ]);

        Notification::make()
            ->title(__('general.notification.title.secret_code_updated', [
                'name' => $agent->name,
            ]))
            ->success()
            ->send();

        return redirect(self::getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }
}
