<?php

namespace App\Filament\Master\Pages;

use App\Models\Master;
use App\Settings\PointRateSettings;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Illuminate\Contracts\Support\Htmlable;

class UpdateAgentPointRate extends Page
{
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    protected static ?string $navigationGroup = 'Update Agent';

    protected static string $layout = 'filament-panels::components.layout.index';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.point_rates');
    }

    public static function getNavigationLabel(): string
    {
        return ___('general.point_rates');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(2)->schema([
                \Filament\Forms\Components\Select::make('agent_id')
                    ->label(___('general.agents'))
                    ->options(Master::find(Filament::auth()->id())->agents()->pluck('name', 'id'))
                    ->rules([
                        'required',
                        'string',
                    ])
                    ->searchable()
                    ->preload()
                    ->selectablePlaceholder(false)
                    ->live()
                    ->required(),
                \Filament\Forms\Components\TextInput::make('point_rate')
                    ->label(___('general.point_rates'))
                    ->hint(fn (Get $get) => $get('agent_id') ? __('general.current_point_rate').' - '.Master::find(Filament::auth()->id())->agents()->find($get('agent_id'))->point_rate : '')
                    ->helperText(
                        __('general.minimum').' - '.app(PointRateSettings::class)->min_agent.' | '.
                        __('general.maximum').' - '.app(PointRateSettings::class)->max_agent
                    )
                    ->hintColor('primary')
                    ->numeric()
                    ->rules([
                        'integer',
                        'min:'.app(PointRateSettings::class)->min_agent,
                        'max:'.app(PointRateSettings::class)->max_agent,
                    ])
                    ->required(),
            ]),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $master = Master::find(Filament::auth()->id());
        $agent = $master->agents()->find($data['agent_id']);

        $agent->update([
            'point_rate' => $data['point_rate'],
        ]);

        Notification::make()
            ->title(__('general.notification.title.point_rate_updated', [
                'name' => $agent->name,
            ]))
            ->success()
            ->send();

        return redirect(self::getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }
}
