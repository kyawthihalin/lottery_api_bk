<?php

namespace App\Filament\Master\Pages;

use App\Models\Master;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Components\Section;
use Filament\Forms\Form;
use Filament\Pages\Dashboard as BaseDashboard;
use Filament\Pages\Dashboard\Concerns\HasFiltersForm;

class Dashboard extends BaseDashboard
{
    use HasFiltersForm;

    public function filtersForm(Form $form): Form
    {
        $master = Master::find(Filament::auth()->id());

        return $form
            ->schema([
                Section::make()
                    ->schema([
                        Forms\Components\Select::make('agent_ids')
                            ->label(___('general.agents'))
                            ->options($master->agents()->pluck('name', 'user_name'))
                            ->multiple()
                            ->searchable(),
                    ])
                    ->columns(3),
            ]);
    }
}
