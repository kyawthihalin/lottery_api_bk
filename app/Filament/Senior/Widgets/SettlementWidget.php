<?php

namespace App\Filament\Senior\Widgets;

use App\Filament\Widgets\SettlementWidget as AdminSettlementWidget;
use App\Models\Master;
use App\Models\Senior;
use App\Models\Settlement;
use Filament\Facades\Filament;
use Filament\Tables\Table;
use Filament\Widgets\Concerns\InteractsWithPageFilters;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementWidget extends BaseWidget
{
    use InteractsWithPageFilters;

    protected int|string|array $columnSpan = 'full';

    protected static bool $isLazy = false;

    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.history');
    }

    public function table(Table $table): Table
    {
        $senior = Senior::find(Filament::auth()->id());
        $masterUserNames = $this->filters['master_ids'] ?? null;

        if ($masterUserNames) {
            $userType = Master::class;
            $usersIds = $senior->masters()->whereIn('user_name', $masterUserNames)->pluck('id')->toArray();
        } else {
            $userType = Senior::class;
            $usersIds = [$senior->id];
        }

        return $table
            ->query(Settlement::whereIn('user_id', $usersIds)->whereUserType($userType))
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(AdminSettlementWidget::getColumns())
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.history').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }
}
