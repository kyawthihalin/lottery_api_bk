<?php

namespace App\Filament\Senior\Resources;

use App\Filament\Resources\AgentResource as AdminAgentResource;
use App\Filament\Senior\Resources\MasterResource\Pages;
use App\Models\Master;
use Filament\Facades\Filament;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class MasterResource extends Resource
{
    protected static ?string $model = Master::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    protected static ?string $slug = 'account-management';

    public static function getNavigationLabel(): string
    {
        return __('general.account_management');
    }

    public static function getModelLabel(): string
    {
        return ___('general.masters');
    }

    public static function form(Form $form): Form
    {
        return AdminAgentResource::form($form);
    }

    public static function table(Table $table): Table
    {
        return AdminAgentResource::table($table)
            ->modifyQueryUsing(fn (Builder $query) => $query->where('senior_id', Filament::auth()->id()))
            ->actions([
                Tables\Actions\Action::make('settlement')
                    ->label(___('general.actions.settlement'))
                    ->requiresConfirmation(),
                Tables\Actions\EditAction::make()
                    ->iconButton(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMasters::route('/'),
        ];
    }
}
