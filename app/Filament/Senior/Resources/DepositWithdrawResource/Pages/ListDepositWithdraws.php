<?php

namespace App\Filament\Senior\Resources\DepositWithdrawResource\Pages;

use App\Actions\MakeTransaction;
use App\Enums\TransactionType;
use App\Events\DepositCompleted;
use App\Filament\Senior\Resources\DepositWithdrawResource;
use App\Models\Master;
use App\Models\Senior;
use Filament\Actions;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Get;
use Filament\Notifications\Notification;
use Filament\Resources\Components\Tab;
use Filament\Resources\Pages\ListRecords;
use Filament\Support\Enums\MaxWidth;
use Illuminate\Database\Eloquent\Builder;

class ListDepositWithdraws extends ListRecords
{
    protected static string $resource = DepositWithdrawResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\Action::make('deposit')
                ->label(__('general.form.deposit.action'))
                ->color('success')
                ->modalHeading(__('general.point').' '.__('general.form.deposit.action'))
                ->modalSubmitActionLabel(__('general.form.deposit.action'))
                ->form($this->getActionForm())
                ->action(function (array $data): void {
                    $senior = Senior::find(Filament::auth()->id());
                    $master = $senior->masters()->where('id', $data['master_id'])->first();

                    if (! $master) {
                        abort(404, 'Master does not exist');
                    }

                    if ($senior->point < $data['point']) {
                        Notification::make()
                            ->title(__('general.errors.point_not_enough', [
                                'transaction_type' => TransactionType::MasterDeposit->getBreifLabel(),
                            ]))
                            ->danger()
                            ->send();

                        return;
                    }

                    $deposit = (new MakeTransaction(
                        transactionType: TransactionType::MasterDeposit,
                        point: $data['point'],
                        user: $master,
                        otherUser: $senior,
                        otherFields: [
                            'point_rate' => $master->point_rate,
                        ]
                    ))->execute();

                    DepositCompleted::dispatch($deposit);

                    Notification::make()
                        ->success()
                        ->title(__('deposit.notification.title.completed_from', [
                            'point' => number_format($data['point']),
                        ]))
                        ->body(__('deposit.notification.message.completed_from', [
                            'point' => number_format($data['point']),
                            'name' => $master->name,
                        ]))
                        ->send();
                })->modalWidth(MaxWidth::Medium),
            Actions\Action::make('withdraw')
                ->label(__('general.form.withdraw.action'))
                ->modalHeading(__('general.point').' '.__('general.form.withdraw.action'))
                ->form($this->getActionForm())
                ->modalSubmitActionLabel(__('general.form.withdraw.action'))
                ->action(function (array $data): void {
                    $senior = Senior::find(Filament::auth()->id());
                    $master = $senior->masters()->where('id', $data['master_id'])->first();

                    if (! $master) {
                        abort(404, 'Master does not exist');
                    }

                    if ($master->point < $data['point']) {
                        Notification::make()
                            ->title(__('general.errors.point_not_enough_user', [
                                'name' => $master->name,
                                'transaction_type' => TransactionType::MasterWithdraw->getBreifLabel(),
                            ]))
                            ->danger()
                            ->send();

                        return;
                    }

                    $withdraw = (new MakeTransaction(
                        transactionType: TransactionType::MasterWithdraw,
                        point: $data['point'],
                        user: $master,
                        otherUser: $senior,
                        otherFields: [
                            'point_rate' => $master->point_rate,
                        ]
                    ))->execute();

                    DepositCompleted::dispatch($withdraw);

                    Notification::make()
                        ->success()
                        ->title(__('withdraw.notification.title.completed_by', [
                            'point' => number_format($data['point']),
                        ]))
                        ->body(__('withdraw.notification.message.completed_by', [
                            'point' => number_format($data['point']),
                            'name' => $master->name,
                        ]))
                        ->send();
                })->modalWidth(MaxWidth::Medium),
        ];
    }

    protected function getActionForm(): array
    {
        return [
            Forms\Components\Select::make('master_id')
                ->label(___('general.masters'))
                ->hint(fn (Get $get) => Master::find($get('master_id'))?->point)
                ->hintColor('warning')
                ->hintIcon('gmdi-monetization-on-o')
                ->selectablePlaceholder(false)
                ->options(Senior::find(Filament::auth()->id())->masters()->pluck('name', 'id'))
                ->searchable()
                ->live()
                ->required(),
            Forms\Components\TextInput::make('point')
                ->label(__('general.point'))
                ->numeric()
                ->rules([
                    'integer',
                    'min:1',
                ])
                ->required(),
        ];
    }

    public function getTabs(): array
    {
        $senior = Senior::find(Filament::auth()->id());

        return [
            'Senior' => Tab::make(___('general.seniors'))
                ->modifyQueryUsing(function (Builder $query) use ($senior) {
                    $query->where('user_id', $senior->id)
                        ->where('user_type', \App\Models\Senior::class)
                        ->whereIn('transaction_type', [
                            TransactionType::SeniorDeposit->value,
                            TransactionType::SeniorWithdraw->value,
                        ]);
                }),
            'Master' => Tab::make(___('general.masters'))
                ->modifyQueryUsing(function (Builder $query) use ($senior) {
                    $query->whereIn('user_id', $senior->masters()->pluck('id')->toArray())
                        ->where('user_type', \App\Models\Master::class)
                        ->whereIn('transaction_type', [
                            TransactionType::MasterDeposit->value,
                            TransactionType::MasterWithdraw->value,
                        ]);
                }),
            'Agent' => Tab::make(___('general.agents'))
                ->modifyQueryUsing(function (Builder $query) use ($senior) {
                    $userIds = $senior->agents()->pluck('agents.id')->toArray();

                    $query->whereIn('user_id', $userIds)
                        ->where('user_type', \App\Models\Agent::class)
                        ->whereIn('transaction_type', [
                            TransactionType::AgentDeposit->value,
                            TransactionType::AgentWithdraw->value,
                        ]);
                }),
            'Users' => Tab::make(___('general.users'))
                ->modifyQueryUsing(function (Builder $query) use ($senior) {
                    $userIds = $senior->users()->pluck('users.id')->toArray();

                    $query->whereIn('user_id', $userIds)
                        ->where('user_type', \App\Models\User::class)
                        ->whereIn('transaction_type', [
                            TransactionType::UserDeposit->value,
                            TransactionType::UserWithdraw->value,
                        ]);
                }),
        ];
    }
}
