<?php

namespace App\Filament\Senior\Resources\MasterResource\Pages;

use App\Filament\Senior\Resources\MasterResource;
use Filament\Actions;
use Filament\Facades\Filament;
use Filament\Resources\Pages\ListRecords;

class ListMasters extends ListRecords
{
    protected static string $resource = MasterResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->mutateFormDataUsing(function (array $data) {
                    $data['senior_id'] = Filament::auth()->id();

                    return $data;
                }),
        ];
    }
}
