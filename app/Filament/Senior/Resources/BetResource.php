<?php

namespace App\Filament\Senior\Resources;

use App\Filament\Resources\BetResource as AdminBetResource;
use App\Filament\Senior\Resources\BetResource\Pages;
use App\Models\Bet;
use Filament\Facades\Filament;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class BetResource extends Resource
{
    protected static ?string $model = Bet::class;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    protected static ?string $slug = 'purchased-list';

    public static function getModelLabel(): string
    {
        return __('general.purchased_list');
    }

    public static function table(Table $table): Table
    {
        return AdminBetResource::table($table)
            ->modifyQueryUsing(fn (Builder $query) => $query->where('user_id', Filament::auth()->user()->users()->pluck('users.id')->toArray()));
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBets::route('/'),
        ];
    }
}
