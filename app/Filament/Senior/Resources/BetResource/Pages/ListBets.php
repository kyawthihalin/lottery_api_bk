<?php

namespace App\Filament\Senior\Resources\BetResource\Pages;

use App\Filament\Senior\Resources\BetResource;
use Filament\Resources\Pages\ListRecords;

class ListBets extends ListRecords
{
    protected static string $resource = BetResource::class;
}
