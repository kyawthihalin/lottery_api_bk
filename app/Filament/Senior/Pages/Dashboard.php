<?php

namespace App\Filament\Senior\Pages;

use App\Models\Senior;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Components\Section;
use Filament\Forms\Form;
use Filament\Pages\Dashboard as BaseDashboard;
use Filament\Pages\Dashboard\Concerns\HasFiltersForm;

class Dashboard extends BaseDashboard
{
    use HasFiltersForm;

    public function filtersForm(Form $form): Form
    {
        $senior = Senior::find(Filament::auth()->id());

        return $form
            ->schema([
                Section::make()
                    ->schema([
                        Forms\Components\Select::make('master_ids')
                            ->label(___('general.masters'))
                            ->options($senior->masters()->pluck('name', 'user_name'))
                            ->multiple()
                            ->searchable(),
                    ])
                    ->columns(3),
            ]);
    }
}
