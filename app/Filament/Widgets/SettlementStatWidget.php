<?php

namespace App\Filament\Widgets;

use App\Events\SettlementCompleted;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use BezhanSalleh\FilamentShield\Traits\HasWidgetShield;
use Filament\Facades\Filament;
use Filament\Notifications\Notification;
use Filament\Tables;
use Filament\Tables\Enums\ActionsPosition;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementStatWidget extends BaseWidget
{
    use HasWidgetShield;

    protected int|string|array $columnSpan = 'full';

    /**
     * @deprecated Override the `table()` method to configure the table.
     */
    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.stat');
    }

    public function table(Table $table): Table
    {
        $authUser = Filament::auth()->user();
        $childQuery = match (get_class($authUser)) {
            Admin::class => Senior::query(),
            Senior::class => Master::where('senior_id', $authUser->id),
            Master::class => Agent::where('master_id', $authUser->id),
        };

        return $table
            ->query($childQuery)
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(self::getStatColumns())
            ->actions([
                self::getCreateAction(),
            ], position: ActionsPosition::BeforeColumns)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.stat').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getCreateAction(): Tables\Actions\Action
    {
        return Tables\Actions\Action::make('settlement')
            ->visible(function (Model $record): bool {
                if (Filament::getAuthGuard() != 'admin') {
                    return true;
                }

                return Admin::find(Filament::auth()->id())->can('settlement_settlement');
            })
            ->label(__('settlement.actions.settlement'))
            ->requiresConfirmation()
            ->action(function (Model $record) {
                $settlement = $record->settlements()->create([
                    'balance_point' => $record->point,
                    'bought_point' => $record->bought_point,
                    'rewithdrawn_point' => $record->rewithdrawn_point,
                    'paid_point' => $record->paid_point,
                    'regained_point' => $record->regained_point,
                    'used_point' => $record->used_point,
                    'point_rate' => $record->point_rate,
                    'profit' => $record->profit,
                    'paid_above' => $record->paid_above,
                    'net_paid_above' => $record->net_paid_above,
                    'total_balance_point' => $record->total_balance,
                ]);

                Notification::make('settlement_completed')
                    ->success()
                    ->title(__('settlement.completed'))
                    ->send();

                SettlementCompleted::dispatch($settlement->refresh());

                return redirect(get_route_prefix_by_model(Filament::auth()->user()));
            });
    }

    public static function getStatColumns(): array
    {
        return [
            Tables\Columns\TextColumn::make('name')
                ->searchable()
                ->label('User')
                ->sortable(),
            Tables\Columns\TextColumn::make('point')
                ->label(__('settlement.form.balance_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('bought_point')
                ->label(__('settlement.form.bought_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('rewithdrawn_point')
                ->label(__('settlement.form.rewithdrawn_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('paid_point')
                ->label(__('settlement.form.paid_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('regained_point')
                ->label(__('settlement.form.regained_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('used_point')
                ->label(__('settlement.form.used_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('point_rate')
                ->label(___('general.point_rates'))
                ->numeric(),
            Tables\Columns\TextColumn::make('profit')
                ->label(__('settlement.form.profit.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('paid_above')
                ->label(__('settlement.form.paid_above.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('net_paid_above')
                ->label(__('settlement.form.net_paid_above.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('total_won_lottery')
                ->label(__('general.total_won_lottery'))
                ->numeric(),
            Tables\Columns\TextColumn::make('total_balance')
                ->label(__('settlement.form.total_balance.label'))
                ->numeric(),
        ];
    }
}
