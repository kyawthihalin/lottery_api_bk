<?php

namespace App\Filament\Widgets;

use App\Facades\Draw;
use App\Models\Senior;
use App\Models\Settlement;
use Filament\Forms;
use Filament\Tables;
use Filament\Tables\Enums\FiltersLayout;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementWidget extends BaseWidget
{
    protected int|string|array $columnSpan = 'full';

    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.history');
    }

    public function table(Table $table): Table
    {
        return $table
            ->query(Settlement::whereIn('user_id', Senior::all()->pluck('id'))->whereUserType(Senior::class))
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(self::getColumns())
            ->filters([
                Tables\Filters\Filter::make('created_at')
                    ->form([
                        Forms\Components\DateTimePicker::make('date_from')
                            ->label(__('general.date_from')),
                        Forms\Components\DateTimePicker::make('date_until')
                            ->label(__('general.date_until')),
                    ])
                    ->columns(2)
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['date_from'],
                                fn (Builder $query, $date): Builder => $query->where('created_at', '>=', $date),
                            )
                            ->when(
                                $data['date_until'],
                                fn (Builder $query, $date): Builder => $query->where('created_at', '<=', $date),
                            );
                    })
                    ->columnSpanFull(),
            ], layout: FiltersLayout::AboveContent)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.history').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getColumns(): array
    {
        return [
            Tables\Columns\TextColumn::make('created_at')
                ->label(__('general.date'))
                ->dateTime(),
            Tables\Columns\TextColumn::make('user.name')
                ->searchable()
                // ->label(fn (Model $record) => format_model($record->user_type))
                ->sortable(),
            Tables\Columns\TextColumn::make('balance_point')
                ->label(__('settlement.form.balance_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('bought_point')
                ->label(__('settlement.form.bought_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('rewithdrawn_point')
                ->label(__('settlement.form.rewithdrawn_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('paid_point')
                ->label(__('settlement.form.paid_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('regained_point')
                ->label(__('settlement.form.regained_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('used_point')
                ->label(__('settlement.form.used_point.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('point_rate')
                ->label(__('general.point_rates'))
                ->numeric(),
            Tables\Columns\TextColumn::make('profit')
                ->label(__('settlement.form.profit.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('paid_above')
                ->label(__('settlement.form.paid_above.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('net_paid_above')
                ->label(__('settlement.form.net_paid_above.label'))
                ->numeric(),
            Tables\Columns\TextColumn::make('user.total_won_lottery')
                ->label(__('general.total_won_lottery'))
                ->numeric(),
            Tables\Columns\TextColumn::make('total_balance_point')
                ->label(__('settlement.form.total_balance.label'))
                ->numeric(),
        ];
    }
}
