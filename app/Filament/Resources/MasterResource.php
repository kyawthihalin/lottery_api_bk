<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MasterResource\Pages;
use App\Models\Master;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Tables\Table;

class MasterResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Master::class;

    protected static ?string $navigationIcon = 'gmdi-support-agent-o';

    protected static ?string $navigationGroup = 'User Management';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'lock',
        ];
    }

    public static function getModelLabel(): string
    {
        return ___('general.masters');
    }

    public static function infolist(Infolist $infolist): Infolist
    {
        return AgentResource::infolist($infolist);
    }

    public static function table(Table $table): Table
    {
        return AgentResource::table($table)
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMasters::route('/'),
        ];
    }
}
