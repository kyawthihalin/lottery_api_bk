<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ResultTypeResource\Pages;
use App\Models\ResultType;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class ResultTypeResource extends Resource implements HasShieldPermissions
{
    use Translatable;

    protected static ?string $model = ResultType::class;

    protected static ?string $navigationIcon = 'heroicon-o-trophy';

    protected static ?string $slug = 'prize';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'update',
        ];
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getModelLabel(): string
    {
        return __('general.prizes');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label(__('general.name'))
                    ->required(),
                Forms\Components\TextInput::make('price')
                    ->label(__('general.price'))
                    ->numeric()
                    ->rules([
                        'integer',
                        'max:999999999'
                    ])
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label(__('general.name')),
                Tables\Columns\TextColumn::make('price')
                    ->label(__('general.price'))
                    ->numeric(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make('table')->withFilename(fn ($resource) => $resource::getModelLabel().' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListResultTypes::route('/'),
            'edit' => Pages\EditResultType::route('/{record}/edit'),
        ];
    }
}
