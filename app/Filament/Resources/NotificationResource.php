<?php

namespace App\Filament\Resources;

use App\Enums\AppType;
use App\Events\SystemNotificationSent;
use App\Filament\Resources\NotificationResource\Pages;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Notification;
use App\Models\Senior;
use App\Models\User;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class NotificationResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Notification::class;

    protected static ?string $navigationIcon = 'heroicon-o-bell';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'create',
        ];
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getModelLabel(): string
    {
        return ___('general.notification.label');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Wizard::make([
                    Forms\Components\Wizard\Step::make(__('general.receivers'))
                        ->schema([
                            Forms\Components\Select::make('type')
                                ->label(__('general.type'))
                                ->options([
                                    'by_apps' => __('general.by_apps'),
                                    'by_users' => __('general.by_users'),
                                ])
                                ->default('by_apps')
                                ->live()
                                ->selectablePlaceholder(false)
                                ->native(false)
                                ->required(),
                            Forms\Components\Select::make('app_types')
                                ->label(___('general.app_types', 2))
                                ->options(AppType::class)
                                ->default([AppType::User->value])
                                ->hidden(fn (Get $get) => $get('type') != 'by_apps')
                                ->native(false)
                                ->multiple()
                                ->required(),
                            Forms\Components\Fieldset::make('All users')
                                ->schema([
                                    Forms\Components\Select::make('users')
                                        ->label(___('general.users', 2))
                                        ->options(User::all()->pluck('name', 'id'))
                                        ->searchable()
                                        ->multiple()
                                        ->preload(),
                                    Forms\Components\Select::make('agents')
                                        ->label(___('general.agents', 2))
                                        ->options(Agent::all()->pluck('name', 'id'))
                                        ->searchable()
                                        ->multiple()
                                        ->preload(),
                                    Forms\Components\Select::make('masters')
                                        ->label(___('general.masters', 2))
                                        ->options(Master::all()->pluck('name', 'id'))
                                        ->searchable()
                                        ->multiple()
                                        ->preload(),
                                    Forms\Components\Select::make('seniors')
                                        ->label(___('general.seniors', 2))
                                        ->options(Senior::all()->pluck('name', 'id'))
                                        ->searchable()
                                        ->multiple()
                                        ->preload(),
                                    Forms\Components\Select::make('admins')
                                        ->label(___('general.admins', 2))
                                        ->options(Admin::all()->pluck('name', 'id'))
                                        ->searchable()
                                        ->multiple()
                                        ->preload(),
                                ])
                                ->hidden(fn (Get $get) => $get('type') != 'by_users')
                                ->columnSpanFull(),
                        ])
                        ->columns(2),
                    Forms\Components\Wizard\Step::make(__('general.content'))
                        ->schema([
                            Forms\Components\Tabs::make('Language')
                                ->schema([
                                    Forms\Components\Tabs\Tab::make(__('general.english'))
                                        ->schema([
                                            Forms\Components\TextInput::make('title_en')
                                                ->label(__('general.title'))
                                                ->default(fake()->word)
                                                ->rules([
                                                    'string',
                                                    'max:255',
                                                ])
                                                ->required(),
                                            Forms\Components\Textarea::make('body_en')
                                                ->label(__('general.body'))
                                                ->default(fake()->paragraph)
                                                ->rules([
                                                    'string',
                                                    'max:500',
                                                ])
                                                ->required(),
                                        ]),
                                    Forms\Components\Tabs\Tab::make(__('general.myanmar'))
                                        ->schema([
                                            Forms\Components\TextInput::make('title_my')
                                                ->label(__('general.title'))
                                                ->default(fake()->word)
                                                ->rules([
                                                    'string',
                                                    'max:255',
                                                ])
                                                ->required(),

                                            Forms\Components\Textarea::make('body_my')
                                                ->label(__('general.body'))
                                                ->default(fake()->paragraph)
                                                ->rules([
                                                    'string',
                                                    'max:500',
                                                ])
                                                ->required(),
                                        ]),
                                ]),

                        ]),
                ]),
            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('notifiable.name')
                    ->description(fn (Model $record) => format_model($record->notifiable_type))
                    ->searchable()
                    ->label(__('general.to')),
                Tables\Columns\TextColumn::make('type')
                    ->label(__('general.type'))
                    ->formatStateUsing(function (string $state): string {
                        $parts = explode('\\', $state);
                        $lastPart = end($parts);

                        return preg_replace('/(?<!\s)(?=[A-Z])/', ' ', $lastPart);
                    })
                    ->searchable(),
                Tables\Columns\TextColumn::make('data')
                    ->label(__('general.data'))
                    ->formatStateUsing(function (string $state) {
                        $data = json_decode($state, true);

                        $titleString = '';

                        if (is_json($data['title'])) {
                            $title = json_decode($data['title'], true);
                            $titleString = __($title['key'], $title['replace']);
                        } else {
                            $titleString = $data['title'];
                        }

                        $bodyString = '';
                        if (is_json($data['body'])) {
                            $body = json_decode($data['body'], true);
                            $bodyString = __($body['key'], $body['replace']);
                        } else {
                            $bodyString = $data['body'];
                        }

                        return $titleString.'<br>'.$bodyString;
                    })
                    ->wrap()
                    ->html(),
                Tables\Columns\TextColumn::make('read_at')
                    ->label(__('general.read_at'))
                    ->dateTime(),
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make('create')
                    ->modalHeading(__('general.send_notification'))
                    ->label(__('general.send_notification'))
                    ->createAnother(false)
                    ->modalSubmitActionLabel(__('general.send'))
                    ->action(function (array $data): void {
                        $targetUsers = new Collection();
                        if ($data['type'] == 'by_apps') {
                            $appTypes = $data['app_types'];

                            foreach ($appTypes as $appType) {
                                $model = AppType::from($appType)->getModel();
                                $users = $model::all();

                                $targetUsers = $targetUsers->merge($users);
                            }
                        }

                        if ($data['type'] == 'by_users') {
                            $admins = Admin::find($data['admins']);
                            $seniors = Senior::find($data['seniors']);
                            $masters = Master::find($data['masters']);
                            $agents = Agent::find($data['agents']);
                            $users = User::find($data['users']);

                            $targetUsers = $targetUsers->merge($admins);

                            $targetUsers = $targetUsers->merge($seniors);
                            $targetUsers = $targetUsers->merge($masters);
                            $targetUsers = $targetUsers->merge($agents);
                            $targetUsers = $targetUsers->merge($users);
                        }

                        event(new SystemNotificationSent($targetUsers, $data));
                    }),
            ])
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListNotifications::route('/'),
        ];
    }
}
