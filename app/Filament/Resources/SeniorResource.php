<?php

namespace App\Filament\Resources;

use App\Filament\Resources\SeniorResource\Pages;
use App\Models\Senior;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class SeniorResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Senior::class;

    protected static ?string $navigationIcon = 'gmdi-support-agent-o';

    protected static ?string $navigationGroup = 'User Management';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'create',
            'update',
            'lock',
        ];
    }

    public static function getModelLabel(): string
    {
        return ___('general.seniors');
    }

    public static function form(Form $form): Form
    {
        return AgentResource::form($form);
    }

    public static function table(Table $table): Table
    {
        return AgentResource::table($table)
            ->filters([
                //
            ])
            ->actions([
                AdminResource::getLockAction(),
                Tables\Actions\EditAction::make()
                    ->iconButton(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSeniors::route('/'),
        ];
    }
}
