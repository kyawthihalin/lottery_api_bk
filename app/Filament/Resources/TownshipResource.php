<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TownshipResource\Pages;
use App\Models\Township;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;

class TownshipResource extends Resource implements HasShieldPermissions
{
    use Translatable;

    protected static ?string $model = Township::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'create',
            'update',
            'delete',
        ];
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getModelLabel(): string
    {
        return __('general.townships');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('state_id')
                    ->label(___('general.states'))
                    ->relationship('state')
                    ->searchable()
                    ->preload()
                    ->getOptionLabelFromRecordUsing(fn (Model $record) => $record->name)
                    ->required(),
                Forms\Components\TextInput::make('name')
                    ->label(__('general.name'))
                    ->rules([
                        'string',
                        'max:255',
                    ])
                    ->required(),
                Forms\Components\TextInput::make('nrc_code')
                    ->label(__('general.nrc_code'))
                    ->rules([
                        'string',
                        'min:6',
                        'max:9',
                    ])
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label(__('general.name')),
                Tables\Columns\TextColumn::make('state.name')
                    ->label(___('general.states')),
                Tables\Columns\TextColumn::make('nrc_code')
                    ->label(__('general.nrc_code')),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTownships::route('/'),
            'create' => Pages\CreateTownship::route('/create'),
            'edit' => Pages\EditTownship::route('/{record}/edit'),
        ];
    }
}
