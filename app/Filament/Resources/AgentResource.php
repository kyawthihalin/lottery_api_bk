<?php

namespace App\Filament\Resources;

use App\Enums\NrcType;
use App\Filament\Resources\AgentResource\Pages;
use App\Models\Agent;
use App\Settings\AuthSettings;
use App\Settings\PointRateSettings;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Infolists;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Support\Enums\IconPosition;
use Filament\Tables;
use Filament\Tables\Table;
use Guava\FilamentClusters\Forms\Cluster;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;
use Rawilk\FilamentPasswordInput\Password;
use STS\FilamentImpersonate\Tables\Actions\Impersonate;

class AgentResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Agent::class;

    protected static ?string $navigationIcon = 'gmdi-support-agent-o';

    protected static ?string $navigationGroup = 'User Management';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'lock',
        ];
    }

    public static function getModelLabel(): string
    {
        return ___('general.agents');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Tabs::make('Form')
                    ->schema([
                        self::getProfileTab(),
                        self::getNrcTab(),
                    ])->columns(3),
            ])
            ->columns(1);
    }

    public static function getProfileTab(): Forms\Components\Tabs\Tab
    {
        return Forms\Components\Tabs\Tab::make(__('general.profile'))
            ->schema([
                Forms\Components\FileUpload::make('profile_image')
                    ->label(__('general.profile_image'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images/profile_image')
                    ->avatar()
                    ->maxSize(5000)
                    ->columnSpanFull(),
                Forms\Components\Select::make('state_id')
                    ->label(___('general.states'))
                    ->relationship('state', 'name')
                    ->getOptionLabelFromRecordUsing(fn (Model $record) => $record->name)
                    ->searchable()
                    ->preload()
                    ->required(),
                Forms\Components\TextInput::make('name')
                    ->label(__('general.name'))
                    ->rules([
                        'string',
                        'max:255',
                    ])
                    ->required(),
                Forms\Components\TextInput::make('user_name')
                    ->label(__('general.user_name'))
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->unique(ignoreRecord: true)
                    ->rules([
                        'string',
                        'max:255',
                        'alpha_dash',
                    ])
                    ->required(),
                Forms\Components\TextInput::make('phone')
                    ->label(__('general.phone'))
                    ->tel(),
                Password::make('password')
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->label(__('general.password'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->dehydrateStateUsing(fn (string $state): string => bcrypt($state))
                    ->dehydrated(fn (?string $state): bool => filled($state))
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getPasswordRule(Agent::class),
                    ]),
                Password::make('secret_code')
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->label(__('general.secret_code'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->dehydrateStateUsing(fn (string $state): string => bcrypt($state))
                    ->dehydrated(fn (?string $state): bool => filled($state))
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getSecretCodeRule(Agent::class),
                    ]),
                Forms\Components\TextInput::make('point_rate')
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->label(___('general.point_rates'))
                    ->numeric()
                    ->rules([
                        'integer',
                        'min:'.app(PointRateSettings::class)->min_senior,
                        'max:'.app(PointRateSettings::class)->max_senior,
                    ])
                    ->required(),
            ]);
    }

    public static function getNrcTab(): Forms\Components\Tabs\Tab
    {
        return Forms\Components\Tabs\Tab::make(__('general.nrc'))->schema([
            Cluster::make([
                Forms\Components\Select::make('nrc_state')
                    ->live()
                    ->native(false)
                    ->default(1)
                    ->options(\App\Models\State::all()->pluck('id', 'id'))
                    ->selectablePlaceholder(false)
                    ->columnSpan(1),
                Forms\Components\Select::make('nrc_township')
                    ->options(fn (Get $get) => \App\Models\Township::where('state_id', $get('nrc_state'))->pluck('nrc_code', 'nrc_code'))
                    ->selectablePlaceholder(false)
                    ->placeholder('')
                    ->searchable()
                    ->columnSpan(2),
                Forms\Components\Select::make('nrc_type')
                    ->default(NrcType::N)
                    ->options(NrcType::class)
                    ->selectablePlaceholder(false)
                    ->native(false)
                    ->columnSpan(2),
                Forms\Components\TextInput::make('nrc_number')
                    ->label(__('general.nrc_number'))
                    ->numeric()
                    ->maxLength(6)
                    ->columnSpan(3),
            ])
                ->label(__('general.nrc_number'))
                ->columnSpan(2)
                ->columns(8),
            Forms\Components\Grid::make()->schema([
                Forms\Components\FileUpload::make('nrc_front')
                    ->label(__('general.nrc_front'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images/nrc')
                    ->maxSize(5000),
                Forms\Components\FileUpload::make('nrc_back')
                    ->label(__('general.nrc_back'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images/nrc')
                    ->maxSize(500),
            ])->columnSpanFull(),
        ]);
    }

    public static function infolist(Infolist $infolist): Infolist
    {
        return $infolist->schema([
            Infolists\Components\TextEntry::make('name')
                ->label(__('general.name')),
            Infolists\Components\TextEntry::make('state.name')
                ->label(__('general.state')),
            Infolists\Components\TextEntry::make('master.name')
                ->label(__('general.masters'))
                ->badge(),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label(__('general.name'))
                    ->wrap(),
                Tables\Columns\TextColumn::make('user_name')
                    ->label(__('general.user_name'))
                    ->description(fn (Model $record) => $record instanceof Agent ? __('general.code').' - '.$record->code : '')
                    ->copyable()
                    ->icon('gmdi-copy-all-o')
                    ->iconPosition(IconPosition::After)
                    ->iconColor('success')
                    ->searchable(),
                Tables\Columns\TextColumn::make('point')
                    ->label(__('general.point'))
                    ->numeric(),
                Tables\Columns\TextColumn::make('point_rate')
                    ->label(___('general.point_rates'))
                    ->numeric(),
                Tables\Columns\TextColumn::make('state.name')
                    ->label(___('general.states', 0))
                    ->wrap(),
                Tables\Columns\TextColumn::make('phone')
                    ->label(__('general.phone')),
            ])
            ->filters([
            ])
            ->actions([
                AdminResource::getLockAction(),
                Impersonate::make()
                    ->guard('agent')
                    ->redirectTo('/agent'),
            ])
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make('table')->withFilename(fn ($resource) => $resource::getModelLabel().' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);;
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAgents::route('/'),
        ];
    }
}
