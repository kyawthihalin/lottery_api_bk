<?php

namespace App\Filament\Resources\FeedbackResource\Pages;

use App\Filament\Resources\FeedbackResource;
use Filament\Resources\Components\Tab;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Database\Eloquent\Builder;

class ListFeedback extends ListRecords
{
    protected static string $resource = FeedbackResource::class;

    public function getTabs(): array
    {
        return [
            'senior' => Tab::make(___('general.seniors'))
                ->badge(fn () => self::$resource::getModel()::whereUserType(\App\Models\Senior::class)->whereNull('read_at')->count())
                ->modifyQueryUsing(fn (Builder $query) => $query->whereUserType(\App\Models\Senior::class)),
            'master' => Tab::make(___('general.masters'))
                ->badge(fn () => self::$resource::getModel()::whereUserType(\App\Models\Master::class)->whereNull('read_at')->count())
                ->modifyQueryUsing(fn (Builder $query) => $query->whereUserType(\App\Models\Master::class)),
            'agent' => Tab::make(___('general.agents'))
                ->badge(fn () => self::$resource::getModel()::whereUserType(\App\Models\Agent::class)->whereNull('read_at')->count())
                ->modifyQueryUsing(fn (Builder $query) => $query->whereUserType(\App\Models\Agent::class)),
            'user' => Tab::make(___('general.users'))
                ->badge(fn () => self::$resource::getModel()::whereUserType(\App\Models\User::class)->whereNull('read_at')->count())
                ->modifyQueryUsing(fn (Builder $query) => $query->whereUserType(\App\Models\User::class)),
        ];
    }
}
