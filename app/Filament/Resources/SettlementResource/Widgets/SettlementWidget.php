<?php

namespace App\Filament\Resources\SettlementResource\Widgets;

use App\Filament\Resources\SettlementResource;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;

class SettlementWidget extends BaseWidget
{
    protected int|string|array $columnSpan = 'full';

    public function table(Table $table): Table
    {
        return $table
            ->query(SettlementResource::getEloquentQuery())
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns([
                // ...
            ]);
    }
}
