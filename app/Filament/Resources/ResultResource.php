<?php

namespace App\Filament\Resources;

use App\Facades\Draw;
use App\Filament\Resources\ResultResource\Pages;
use App\Models\Admin;
use App\Models\Result;
use App\Services\LotteryApi\LotteryService;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Support\Enums\MaxWidth;
use Filament\Tables;
use Filament\Tables\Enums\FiltersLayout;
use Filament\Tables\Filters;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class ResultResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Result::class;

    protected static ?string $navigationIcon = 'heroicon-o-clipboard-document-list';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'get_lottery_results',
        ];
    }

    public static function getModelLabel(): string
    {
        return __('general.results');
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('resultType.name')
                    ->label(__('general.type')),
                Tables\Columns\TextColumn::make('draw_id')
                    ->label(___('general.draw_ids')),
                Tables\Columns\TextColumn::make('users_count')
                    ->label(__('general.total_winners'))
                    ->counts('users'),
                Tables\Columns\TextColumn::make('numbers')
                    ->label(___('general.numbers'))
                    ->searchable(isIndividual: true)
                    ->listWithLineBreaks()
                    ->limitList()
                    ->expandableLimitedList(),
                Tables\Columns\TextColumn::make('price')
                    ->label(__('general.price'))
                    ->numeric(),
            ])
            ->headerActions([
                Tables\Actions\Action::make('get_lottery_results')
                    ->visible(fn (): bool => Admin::find(Filament::auth()->id())->can('get_lottery_results_result'))
                    ->label(__('general.get_lottery_results.action'))
                    ->form([
                        Forms\Components\DatePicker::make('draw_id')
                            ->label(___('general.draw_ids'))
                            ->helperText(___('general.get_lottery_results.helper'))
                            ->required()
                            ->native(false)
                            ->displayFormat('Y-m-d')
                            ->rules([
                                'date',
                            ]),
                    ])
                    ->modalWidth(MaxWidth::Medium)
                    ->modalSubmitActionLabel(__('general.get_lottery_results.action'))
                    ->action(function (array $data) {
                        $drawId = $data['draw_id'];
                        $resultExist = Result::where('draw_id', $drawId)->first();

                        if ($resultExist) {
                            Notification::make('error')
                                ->danger()
                                ->title(__('general.get_lottery_results.exist'))
                                ->send();

                            return;
                        }

                        $error = (new LotteryService())->fetchResults($drawId);

                        if ($error) {
                            Notification::make('error')
                                ->danger()
                                ->title($error->getMessage())
                                ->send();

                            return;
                        }

                        Notification::make('success')
                            ->success()
                            ->title(__('general.get_lottery_results.succeed'))
                            ->send();
                    }),
            ])
            ->filters([
                Filters\SelectFilter::make('resultType')
                    ->label(__('general.type'))
                    ->relationship('resultType', 'name')
                    ->getOptionLabelFromRecordUsing(fn (Model $record) => $record->name)
                    ->searchable()
                    ->preload(),
                Filters\SelectFilter::make('draw_ids')
                    ->label(___('general.draw_ids'))
                    ->multiple()
                    ->default([Draw::getPreviousDrawId()])
                    ->options(fn () => Draw::getPreviousDrawIds(count: 24, isOptions: true))
                    ->attribute('draw_id'),
            ], layout: FiltersLayout::AboveContent)
            ->searchable(false)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make('table')->withFilename(fn ($resource) => $resource::getModelLabel().' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListResults::route('/'),
        ];
    }
}
