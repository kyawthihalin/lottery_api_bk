<?php

namespace App\Filament\Resources;

use App\Facades\Draw;
use App\Filament\Resources\BetResource\Pages;
use App\Models\Bet;
use Filament\Facades\Filament;
use Filament\Resources\Resource;
use Filament\Support\Enums\IconPosition;
use Filament\Tables;
use Filament\Tables\Enums\FiltersLayout;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class BetResource extends Resource
{
    protected static ?string $model = Bet::class;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    protected static ?string $slug = 'purchased-list';

    public static function getModelLabel(): string
    {
        return __('general.bets');
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('user.name')
                    ->label(__('general.name'))
                    ->searchable(isIndividual: true)
                    ->description(fn (Model $record): string => $record->user->user_name)
                    ->url(function (Model $record) {
                        if (Filament::getAuthGuard() != 'admin') {
                            return null;
                        }

                        return route('filament.admin.resources.users.index', [
                            'tableSearch' => $record->user->user_name,
                        ]);
                    }, shouldOpenInNewTab: true),
                Tables\Columns\TextColumn::make('reference_id')
                    ->label(__('general.reference_id'))
                    ->searchable(isIndividual: true)
                    ->copyable()
                    ->icon('gmdi-copy-all-o')
                    ->iconPosition(IconPosition::After)
                    ->iconColor('success'),
                Tables\Columns\TextColumn::make('tickets')
                    ->listWithLineBreaks()
                    ->limitList()
                    ->expandableLimitedList()
                    ->label(trans_choice('general.tickets', 2))
                    ->searchable(isIndividual: true)
                    ->wrap(),
                Tables\Columns\TextColumn::make('draw_id')
                    ->label(___('general.draw_ids')),
                Tables\Columns\TextColumn::make('point')
                    ->label(__('general.point'))
                    ->numeric(),
            ])
            ->searchable(false)
            ->filters([
                Tables\Filters\SelectFilter::make('draw_ids')
                    ->label(___('general.draw_ids', 2))
                    ->multiple()
                    ->options(fn () => Draw::getPreviousDrawIds(count: 24, isOptions: true))
                    ->attribute('draw_id'),
            ], layout: FiltersLayout::AboveContent)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn ($resource) => $resource::getModelLabel().' '.now()->format('Y-m-d h-i A')),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBets::route('/'),
        ];
    }
}
