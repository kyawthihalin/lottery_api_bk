<?php

namespace App\Filament\Resources;

use App\Facades\Draw;
use App\Filament\Resources\WinnerResource\Pages;
use App\Models\Admin;
use App\Models\User;
use App\Models\UserResult;
use Filament\Facades\Filament;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Enums\FiltersLayout;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class WinnerResource extends Resource
{
    protected static ?string $model = UserResult::class;

    protected static ?string $navigationIcon = 'gmdi-supervised-user-circle-o';

    public static function getModelLabel(): string
    {
        return ___('general.winners');
    }

    public static function table(Table $table): Table
    {
        $authUser = Filament::auth()->user();
        if ($authUser instanceof Admin) {
            $userIds = User::all()->pluck('id')->toArray();
        } else {
            $userIds = $authUser->users()->pluck('id')->toArray();
        }

        return $table
            ->modifyQueryUsing(fn (Builder $query) => $query->whereIn('id', $userIds))
            ->columns([
                Tables\Columns\TextColumn::make('user.name')
                    ->label(__('general.name'))
                    ->searchable(isIndividual: true)
                    ->description(fn (Model $record): string => $record->user->user_name)
                    ->url(function (Model $record) {
                        if (Filament::getAuthGuard() != 'admin') {
                            return null;
                        }

                        return route('filament.admin.resources.users.index', [
                            'tableSearch' => $record->user->user_name,
                        ]);
                    }, shouldOpenInNewTab: true),
                Tables\Columns\TextColumn::make('result.resultType.name')
                    ->label(__('general.type')),
                Tables\Columns\TextColumn::make('tickets')
                    ->listWithLineBreaks()
                    ->limitList()
                    ->expandableLimitedList()
                    ->label(trans_choice('general.tickets', 2))
                    ->searchable(isIndividual: true)
                    ->wrap(),
                Tables\Columns\TextColumn::make('draw_id')
                    ->label(___('general.draw_ids')),
                Tables\Columns\TextColumn::make('result.price')
                    ->label(__('general.price'))
                    ->numeric(),
            ])
            ->filters([
                Tables\Filters\SelectFilter::make('draw_ids')
                    ->label(___('general.draw_ids', 2))
                    ->multiple()
                    ->options(fn () => Draw::getPreviousDrawIds(count: 24, isOptions: true))
                    ->attribute('draw_id'),
            ], layout: FiltersLayout::AboveContent)
            ->searchable(false)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make('table')->withFilename(fn ($resource) => $resource::getModelLabel().' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListWinners::route('/'),
        ];
    }
}
