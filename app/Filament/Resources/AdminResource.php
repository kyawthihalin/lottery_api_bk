<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AdminResource\Pages;
use App\Models\Admin;
use App\Settings\AuthSettings;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;
use Rawilk\FilamentPasswordInput\Password;

class AdminResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Admin::class;

    protected static ?string $navigationIcon = 'gmdi-supervised-user-circle-o';

    protected static ?string $navigationGroup = 'Authorization';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'create',
            'update',
            'lock',
        ];
    }

    public static function getModelLabel(): string
    {
        return ___('general.admins');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label(__('general.name'))
                    ->required()
                    ->rules([
                        'string',
                        'max:255',
                    ]),
                Forms\Components\Select::make('roles')
                    ->relationship('roles', 'name')
                    ->multiple()
                    ->preload()
                    ->searchable()
                    ->required(),
                Forms\Components\TextInput::make('email')
                    ->label(__('general.email'))
                    ->unique(ignoreRecord: true)
                    ->email()
                    ->required()
                    ->rules([
                        'string',
                        'email',
                        'max:255',
                    ]),
                Forms\Components\TextInput::make('user_name')
                    ->label(__('general.user_name'))
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->unique(ignoreRecord: true)
                    ->rules([
                        'string',
                        'max:255',
                        'alpha_dash',
                    ])
                    ->required(),
                Password::make('password')
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->label(__('general.password'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->dehydrateStateUsing(fn (string $state): string => bcrypt($state))
                    ->dehydrated(fn (?string $state): bool => filled($state))
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getPasswordRule(\App\Models\Admin::class),
                    ]),
            ])
            ->columns(3);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('general.name')),
                Tables\Columns\TextColumn::make('roles.name')
                    ->badge(),
                Tables\Columns\TextColumn::make('email')
                    ->searchable()
                    ->label(__('general.email'))
                    ->copyable(),
            ])
            ->actions([
                self::getLockAction(),
                Tables\Actions\EditAction::make()
                    ->iconButton(),
            ]);
    }

    public static function getLockAction(): Tables\Actions\Action
    {
        return Tables\Actions\Action::make('lock')
            ->visible(fn (Model $record): bool => Admin::find(Filament::auth()->id())->can('lock', $record))
            ->modalHeading(fn (Model $record) => $record->locked_at ? __('general.actions.unlock') : __('general.actions.lock'))
            ->tooltip(fn (Model $record) => $record->locked_at ? __('general.actions.unlock') : __('general.actions.lock'))
            ->color(fn (Model $record) => $record->locked_at ? 'gray' : 'danger')
            ->iconButton()
            ->icon(fn (Model $record) => $record->locked_at ? 'heroicon-o-lock-closed' : 'heroicon-o-lock-open')
            ->requiresConfirmation()
            ->action(function (Model $record): void {
                $state = $record->locked_at ? 'unlocked' : 'locked';

                $record->update([
                    'locked_at' => $record->locked_at ? null : now(),
                ]);

                Notification::make()
                    ->title(__("auth.action.notification.{$state}", [
                        'name' => $record->name,
                    ]))
                    ->success()
                    ->send();
            });
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAdmins::route('/'),
            'create' => Pages\CreateAdmin::route('/create'),
            'edit' => Pages\EditAdmin::route('/{record}/edit'),
        ];
    }
}
