<?php

namespace App\Filament\Resources\DepositWithdrawResource\Pages;

use App\Filament\Resources\DepositWithdrawResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDepositWithdraw extends EditRecord
{
    protected static string $resource = DepositWithdrawResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
