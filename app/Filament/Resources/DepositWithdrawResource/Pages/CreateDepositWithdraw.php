<?php

namespace App\Filament\Resources\DepositWithdrawResource\Pages;

use App\Filament\Resources\DepositWithdrawResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDepositWithdraw extends CreateRecord
{
    protected static string $resource = DepositWithdrawResource::class;
}
