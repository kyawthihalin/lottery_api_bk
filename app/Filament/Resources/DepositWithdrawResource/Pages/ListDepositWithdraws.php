<?php

namespace App\Filament\Resources\DepositWithdrawResource\Pages;

use App\Actions\MakeTransaction;
use App\Enums\TransactionType;
use App\Events\DepositCompleted;
use App\Filament\Resources\DepositWithdrawResource;
use App\Models\Admin;
use App\Models\Senior;
use App\Models\TransactionLog;
use Filament\Actions;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Get;
use Filament\Notifications\Notification;
use Filament\Resources\Components\Tab;
use Filament\Resources\Pages\ListRecords;
use Filament\Support\Enums\MaxWidth;
use Illuminate\Database\Eloquent\Builder;

class ListDepositWithdraws extends ListRecords
{
    protected static string $resource = DepositWithdrawResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\Action::make('deposit')
                ->visible(fn (): bool => Admin::find(Filament::auth()->id())->can('deposit', TransactionLog::class))
                ->label(__('general.form.deposit.action'))
                ->modalHeading(__('general.point').' '.__('general.form.deposit.action'))
                ->color('success')
                ->modalSubmitActionLabel(__('general.form.deposit.action'))
                ->form($this->getActionForm())
                ->action(function (array $data): void {
                    $senior = Senior::where('id', $data['senior_id'])->first();

                    if (! $senior) {
                        abort(404, 'Senior does not exist');
                    }

                    $deposit = (new MakeTransaction(
                        transactionType: TransactionType::SeniorDeposit,
                        point: $data['point'],
                        user: $senior,
                        otherFields: [
                            'point_rate' => $senior->point_rate,
                        ]
                    ))->execute();

                    DepositCompleted::dispatch($deposit);

                    Notification::make()
                        ->success()
                        ->title(__('deposit.notification.title.completed_from', [
                            'point' => number_format($data['point']),
                        ]))
                        ->body(__('deposit.notification.message.completed_from', [
                            'point' => number_format($data['point']),
                            'name' => $senior->name,
                        ]))
                        ->send();
                })->modalWidth(MaxWidth::Medium),
            Actions\Action::make('withdraw')
                ->visible(fn (): bool => Admin::find(Filament::auth()->id())->can('withdraw', TransactionLog::class))
                ->label(__('general.form.withdraw.action'))
                ->modalHeading(__('general.point').' '.__('general.form.withdraw.action'))
                ->modalSubmitActionLabel(__('general.form.withdraw.action'))
                ->form($this->getActionForm())
                ->action(function (array $data): void {
                    $senior = Senior::where('id', $data['senior_id'])->first();

                    if (! $senior) {
                        abort(404, 'Senior does not exist');
                    }

                    if ($senior->point < $data['point']) {
                        Notification::make()
                            ->title(__('general.errors.point_not_enough_user', [
                                'name' => $senior->name,
                                'transaction_type' => TransactionType::SeniorWithdraw->getBreifLabel(),
                            ]))
                            ->danger()
                            ->send();

                        return;
                    }

                    $withdraw = (new MakeTransaction(
                        transactionType: TransactionType::SeniorWithdraw,
                        point: $data['point'],
                        user: $senior,
                        otherFields: [
                            'point_rate' => $senior->point_rate,
                        ]
                    ))->execute();

                    DepositCompleted::dispatch($withdraw);

                    Notification::make()
                        ->success()
                        ->title(__('withdraw.notification.title.completed_by', [
                            'point' => number_format($data['point']),
                        ]))
                        ->body(__('withdraw.notification.message.completed_by', [
                            'point' => number_format($data['point']),
                            'name' => $senior->name,
                        ]))
                        ->send();
                })->modalWidth(MaxWidth::Medium),
        ];
    }

    protected function getActionForm(): array
    {
        return [
            Forms\Components\Select::make('senior_id')
                ->label(___('general.seniors'))
                ->hint(fn (Get $get) => Senior::find($get('senior_id'))?->point)
                ->hintColor('warning')
                ->hintIcon('gmdi-monetization-on-o')
                ->selectablePlaceholder(false)
                ->options(Senior::pluck('name', 'id'))
                ->searchable()
                ->live()
                ->required(),
            Forms\Components\TextInput::make('point')
                ->label(__('general.point'))
                ->numeric()
                ->rules([
                    'integer',
                    'min:1',
                ])
                ->required(),
        ];
    }

    public function getTabs(): array
    {
        return [
            'senior' => Tab::make()
                ->label(___('general.seniors'))
                ->modifyQueryUsing(fn (Builder $query) => $query
                    ->where('user_type', \App\Models\Senior::class)
                    ->whereIn('transaction_type', [
                        TransactionType::SeniorDeposit->value,
                        TransactionType::SeniorWithdraw->value,
                    ])),
            'master' => Tab::make()
                ->label(___('general.masters'))
                ->modifyQueryUsing(fn (Builder $query) => $query
                    ->where('user_type', \App\Models\Master::class)
                    ->whereIn('transaction_type', [
                        TransactionType::MasterDeposit->value,
                        TransactionType::MasterWithdraw->value,
                    ])),
            'agent' => Tab::make()
                ->label(___('general.agents'))
                ->modifyQueryUsing(fn (Builder $query) => $query
                    ->where('user_type', \App\Models\Agent::class)
                    ->whereIn('transaction_type', [
                        TransactionType::AgentDeposit->value,
                        TransactionType::AgentWithdraw->value,
                    ])),
            'user' => Tab::make()
                ->label(___('general.users'))
                ->modifyQueryUsing(fn (Builder $query) => $query
                    ->where('user_type', \App\Models\User::class)
                    ->whereIn('transaction_type', [
                        TransactionType::UserDeposit->value,
                        TransactionType::UserDeposit->value,
                    ])),
        ];
    }
}
