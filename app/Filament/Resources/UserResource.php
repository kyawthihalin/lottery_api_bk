<?php

namespace App\Filament\Resources;

use App\Filament\Resources\UserResource\Pages;
use App\Models\User;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Resources\Resource;
use Filament\Support\Enums\IconPosition;
use Filament\Tables;
use Filament\Tables\Table;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class UserResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    protected static ?string $navigationGroup = 'User Management';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'lock',
        ];
    }

    public static function getModelLabel(): string
    {
        return ___('general.users');
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label(__('general.name'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('user_name')
                    ->label(__('general.user_name'))
                    ->searchable()
                    ->copyable()
                    ->icon('gmdi-copy-all-o')
                    ->iconPosition(IconPosition::After)
                    ->iconColor('success'),
                Tables\Columns\TextColumn::make('agent.name')
                    ->label(___('general.agents'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('phone')
                    ->label(__('general.phone'))
                    ->searchable()
                    ->copyable()
                    ->icon('gmdi-copy-all-o')
                    ->iconPosition(IconPosition::After)
                    ->iconColor('success'),
                Tables\Columns\TextColumn::make('nrc')
                    ->label(__('general.nrc')),
                Tables\Columns\TextColumn::make('point')
                    ->label(__('general.point'))
                    ->numeric(),
            ])
            ->filters([
                //
            ])
            ->actions([
                AdminResource::getLockAction(),
            ])
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make('table')->withFilename(fn ($resource) => $resource::getModelLabel().' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
        ];
    }
}
