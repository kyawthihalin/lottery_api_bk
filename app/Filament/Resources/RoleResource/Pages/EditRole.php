<?php

namespace App\Filament\Resources\RoleResource\Pages;

use BezhanSalleh\FilamentShield\Resources\RoleResource\Pages\EditRole as ShieldEditRole;
use Filament\Forms\Components\Component;
use Filament\Forms\Components\TextInput;

class EditRole extends ShieldEditRole
{
    protected function afterFill(): void
    {
        collect($this->form->getFlatComponents())
            ->each(function (Component $component) {
                if ($component instanceof TextInput) {
                    $component->getName() === 'guard_name' && $component->hidden()
                        ->dehydrated(false);
                }
            });
    }
}
