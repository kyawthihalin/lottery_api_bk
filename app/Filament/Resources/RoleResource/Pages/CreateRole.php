<?php

namespace App\Filament\Resources\RoleResource\Pages;

use BezhanSalleh\FilamentShield\Resources\RoleResource\Pages\CreateRole as ShieldCreateRole;
use Filament\Forms\Components\Component;
use Filament\Forms\Components\TextInput;
use Illuminate\Support\Arr;

class CreateRole extends ShieldCreateRole
{
    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $data['guard_name'] = 'admin';
        $this->permissions = collect($data)
            ->filter(function ($permission, $key) {
                return ! in_array($key, ['name', 'guard_name', 'select_all']);
            })
            ->values()
            ->flatten();

        return Arr::only($data, ['name', 'guard_name']);
    }

    protected function afterFill(): void
    {
        collect($this->form->getFlatComponents())
            ->each(function (Component $component) {
                if ($component instanceof TextInput) {
                    $component->getName() === 'guard_name' && $component->hidden()
                        ->dehydrated(false);
                }
            });
    }
}
