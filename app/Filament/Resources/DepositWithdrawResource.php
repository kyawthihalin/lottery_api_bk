<?php

namespace App\Filament\Resources;

use App\Filament\Resources\DepositWithdrawResource\Pages;
use App\Models\DepositWithdraw;
use App\Models\TransactionLog;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Resources\Resource;
use Filament\Support\Enums\IconPosition;
use Filament\Tables;
use Filament\Tables\Enums\ActionsPosition;
use Filament\Tables\Enums\FiltersLayout;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class DepositWithdrawResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = TransactionLog::class;

    protected static ?string $navigationIcon = 'heroicon-m-arrows-up-down';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'deposit',
            'withdraw',
        ];
    }

    public static function getModelLabel(): string
    {
        return __('general.deposits_withdraws');
    }

    public static function table(Table $table): Table
    {
        return $table
            ->modifyQueryUsing(fn (Builder $query) => $query->whereHasMorph('loggable', [
                DepositWithdraw::class,
            ]))
            ->columns([
                Tables\Columns\TextColumn::make('transaction_type')
                    ->label(__('general.type')),
                Tables\Columns\TextColumn::make('user.parent.name')
                    ->label(__('general.by'))
                    ->searchable(isIndividual: true),
                // ->description(fn (Model $record): string => $record->user->parent->user_name)
                // ->url(function (Model $record) {
                //     if (Filament::getAuthGuard() != 'admin') {
                //         return null;
                //     }
                //     $parent = $record->user->parent;

                //     $path = get_path_by_model($parent);

                //     return route("filament.admin.resources.{$path}.index", [
                //         'tableSearch' => $parent->user_name,
                //     ]);
                // }, shouldOpenInNewTab: true),
                Tables\Columns\TextColumn::make('user.name')
                    ->label(__('general.to'))
                    ->searchable(isIndividual: true)
                    ->description(fn (Model $record): string => $record->user->user_name)
                    ->url(function (Model $record) {
                        if (Filament::getAuthGuard() != 'admin') {
                            return null;
                        }
                        $user = $record->user;

                        $path = get_path_by_model($user);

                        return route("filament.admin.resources.{$path}.index", [
                            'tableSearch' => $user->user_name,
                        ]);
                    }, shouldOpenInNewTab: true),
                Tables\Columns\TextColumn::make('loggable.reference_id')
                    ->label(__('general.reference_id'))
                    ->searchable(isIndividual: true)
                    ->copyable()
                    ->icon('gmdi-copy-all-o')
                    ->iconPosition(IconPosition::After)
                    ->iconColor('success'),
                Tables\Columns\TextColumn::make('point')
                    ->label(__('general.point'))
                    ->numeric(),
                Tables\Columns\TextColumn::make('point_rate')
                    ->label(___('general.point_rates'))
                    ->numeric(),
                Tables\Columns\TextColumn::make('before_point')
                    ->label(__('general.point_before'))
                    ->numeric(),
                Tables\Columns\TextColumn::make('after_point')
                    ->label(__('general.point_after'))
                    ->numeric(),
                Tables\Columns\TextColumn::make('created_at')
                    ->label(__('general.date_time'))
                    ->dateTime(),
            ])
            ->filters([
                Tables\Filters\Filter::make('created_at')
                    ->form([
                        Forms\Components\DateTimePicker::make('date_from')
                            ->label(__('general.date_from')),
                        Forms\Components\DateTimePicker::make('date_until')
                            ->label(__('general.date_until')),
                    ])
                    ->columns(2)
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['date_from'],
                                fn (Builder $query, $date): Builder => $query->where('created_at', '>=', $date),
                            )
                            ->when(
                                $data['date_until'],
                                fn (Builder $query, $date): Builder => $query->where('created_at', '<=', $date),
                            );
                    })
                    ->columnSpanFull(),
            ], layout: FiltersLayout::AboveContent)
            ->actions([
                //
            ], position: ActionsPosition::BeforeColumns)
            ->searchable(false)
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn ($resource) => str_replace('/', '-', $resource::getModelLabel()).' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDepositWithdraws::route('/'),
        ];
    }
}
