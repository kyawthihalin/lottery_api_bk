<?php

namespace App\Filament\Resources;

use App\Filament\Resources\FeedbackResource\Pages;
use App\Models\Feedback;
use BezhanSalleh\FilamentShield\Contracts\HasShieldPermissions;
use Filament\Infolists;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Enums\FiltersLayout;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class FeedbackResource extends Resource implements HasShieldPermissions
{
    protected static ?string $model = Feedback::class;

    protected static ?string $navigationIcon = 'heroicon-o-pencil-square';

    public static function getPermissionPrefixes(): array
    {
        return [
            'view_any',
            'delete',
            'delete_any',
        ];
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getModelLabel(): string
    {
        return __('general.feedbacks');
    }

    public static function getNavigationBadge(): ?string
    {
        return static::getModel()::whereNull('read_at')->count();
    }

    public static function infolist(Infolist $infolist): Infolist
    {
        return $infolist->schema([
            Infolists\Components\TextEntry::make('user.name'),
            Infolists\Components\TextEntry::make('title'),
            Infolists\Components\TextEntry::make('detail'),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\Layout\Stack::make([
                    Tables\Columns\TextColumn::make('user.name')
                        ->searchable()
                        ->label(___('general.users'))
                        ->description(fn (Model $record): string => $record->user->user_name)
                        ->url(function (Model $record) {
                            $user = $record->user;

                            $path = get_path_by_model($user);

                            return route("filament.admin.resources.{$path}.index", [
                                'tableSearch' => $user->user_name,
                            ]);
                        }, shouldOpenInNewTab: true),
                    Tables\Columns\TextColumn::make('title')
                        ->label(___('general.title')),
                    Tables\Columns\TextColumn::make('detail')
                        ->label(___('general.detail'))
                        ->limit(200)
                        ->tooltip(function (TextColumn $column): ?string {
                            $state = $column->getState();

                            if (strlen($state) <= $column->getCharacterLimit()) {
                                return null;
                            }

                            // Only render the tooltip if the column content exceeds the length limit.
                            return $state;
                        })
                        ->wrap(),
                ]),
            ])
            ->contentGrid([
                'md' => 2,
                'xl' => 2,
            ])
            ->filters([
                Tables\Filters\Filter::make('read_at')
                    ->toggle()
                    ->label(__('general.filters.unread'))
                    ->baseQuery(fn (Builder $query) => $query->whereNull('read_at')),
            ], layout: FiltersLayout::AboveContent)
            ->actions([
                Tables\Actions\ViewAction::make()
                    ->icon('heroicon-o-eye')
                    ->color(fn (Model $record) => (bool) $record->read_at ? 'gray' : 'primary')
                    ->mutateRecordDataUsing(function (Model $record, array $data): array {
                        $record->update([
                            'read_at' => now(),
                        ]);

                        return $data;
                    })
                    ->iconButton(),
                Tables\Actions\DeleteAction::make()
                    ->iconButton(),
            ])
            ->bulkActions([
                Tables\Actions\BulkAction::make('mark_all_as_read')
                    ->label(__('general.actions.mark_all_as_read'))
                    ->action(fn (Collection $records) => $records->each->update([
                        'read_at' => now(),
                    ])),
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFeedback::route('/'),
        ];
    }
}
