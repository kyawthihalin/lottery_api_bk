<?php

namespace App\Filament\Pages;

use App\Models\Senior;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Actions\Action;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Illuminate\Contracts\Support\Htmlable;

class UpdateSeniorSecretCode extends Page
{
    use HasPageShield;
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    protected static ?string $navigationGroup = 'Update Senior';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.seniors').' '.___('general.secret_code');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.secret_code');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(2)->schema([
                \Filament\Forms\Components\Select::make('senior_id')
                    ->label(___('general.seniors'))
                    ->options(Senior::all()->pluck('name', 'id'))
                    ->rules([
                        'required',
                        'string',
                    ])
                    ->searchable()
                    ->preload()
                    ->selectablePlaceholder(false)
                    ->required(),
                \Filament\Forms\Components\TextInput::make('your_password')
                    ->label(__('general.form.your_password.label'))
                    ->password()
                    ->rules([
                        'required',
                        'current_password',
                    ])
                    ->required(),
                \Filament\Forms\Components\TextInput::make('secret_code')
                    ->label(__('general.secret_code'))
                    ->password()
                    ->confirmed()
                    ->rules([
                        'required',
                        'string',
                        'min:6',
                        'confirmed',
                    ])
                    ->required(),
                \Filament\Forms\Components\TextInput::make('secret_code_confirmation')
                    ->label(__('general.secret_code_confirmation'))
                    ->password()
                    ->required(),
            ]),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $senior = Senior::find($data['senior_id']);

        $senior->update([
            'secret_code' => bcrypt($data['secret_code']),
        ]);

        Notification::make()
            ->title(__('general.notification.title.secret_code_updated', [
                'name' => $senior->name,
            ]))
            ->success()
            ->send();

        return redirect(self::getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }
}
