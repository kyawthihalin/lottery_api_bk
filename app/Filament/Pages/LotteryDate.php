<?php

namespace App\Filament\Pages;

use App\Settings\GeneralSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class LotteryDate extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-calendar-days';

    protected static string $settings = GeneralSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.lottery_dates');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.lottery_dates');
    }

    public function form(Form $form): Form
    {
        $langPrefix = 'general.form.lottery_dates';

        return $form
            ->schema([
                Forms\Components\TagsInput::make('lottery_dates')
                    ->hint(fn () => __("{$langPrefix}.hint").count(app(GeneralSettings::class)->lottery_dates))
                    ->hintColor('success')
                    ->label(__("{$langPrefix}.label"))
                    ->helperText(__("{$langPrefix}.description"))
                    ->placeholder(__("{$langPrefix}.placeholder"))
                    ->nestedRecursiveRules([
                        'regex:/^(0[1-9]|1[0-7])-(0[1-9]|[12][0-9]|3[01])$/',
                    ])
                    ->validationMessages([
                        '*.regex' => __("{$langPrefix}.validation.regex"),
                    ])
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('live_link')
                    ->label('YouTube live link')
                    ->url(),
            ]);
    }
}
