<?php

namespace App\Filament\Pages;

use App\Actions\CheckSecretCode;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns;
use Filament\Pages\Page;
use Illuminate\Validation\ValidationException;
use Livewire\Features\SupportRedirects\Redirector;

class SecretCode extends Page
{
    use Concerns\InteractsWithFormActions;
    use WithRateLimiting;

    protected static string $layout = 'filament-panels::components.layout.base';

    protected static string $routePath = '/secret-code';

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.agent.pages.secret-code';

    protected static bool $shouldRegisterNavigation = false;

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $user = Filament::auth()->user();
        if (! $user->is_secret_code_required || ! $user->secret_code) {
            redirect(get_route_prefix_by_model($user));
        }

        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            \Filament\Forms\Components\TextInput::make('secret_code')
                ->autofocus()
                ->label(__('general.secret_code'))
                ->password()
                ->rules([
                    'required',
                    'string',
                ])
                ->columnSpanFull()
                ->required(),
        ])->statePath('data');
    }

    public function submit(): ?Redirector
    {
        try {
            $this->rateLimit(5);
        } catch (TooManyRequestsException $exception) {
            Notification::make()
                ->title(__('filament-panels::pages/auth/login.notifications.throttled.title', [
                    'seconds' => $exception->secondsUntilAvailable,
                    'minutes' => ceil($exception->secondsUntilAvailable / 60),
                ]))
                ->body(array_key_exists('body', __('filament-panels::pages/auth/login.notifications.throttled') ?: []) ? __('filament-panels::pages/auth/login.notifications.throttled.body', [
                    'seconds' => $exception->secondsUntilAvailable,
                    'minutes' => ceil($exception->secondsUntilAvailable / 60),
                ]) : null)
                ->danger()
                ->send();

            return null;
        }

        $data = $this->form->getState();
        $user = Filament::auth()->user();

        (new CheckSecretCode(\App\Models\Agent::class))->execute($user, $data['secret_code']);

        $user->update([
            'is_secret_code_required' => false,
        ]);

        return redirect(get_route_prefix_by_model($user));
    }

    protected function throwFailureValidationException(): never
    {
        throw ValidationException::withMessages([
            'data.secret_code' => __('auth.secret_code'),
        ]);
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }

    protected function hasFullWidthFormActions(): bool
    {
        return true;
    }
}
