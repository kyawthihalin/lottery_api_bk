<?php

namespace App\Filament\Pages;

use App\Settings\PointRateSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class PointRate extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    protected static string $settings = PointRateSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.point_rates');
    }

    public static function getNavigationLabel(): string
    {
        return ___('general.point_rates');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Fieldset::make('Senior')
                    ->label(___('general.seniors'))
                    ->schema([
                        Forms\Components\TextInput::make('min_senior')
                            ->label(__('general.minimum'))
                            ->numeric()
                            ->rules([
                                'required',
                                'integer',
                            ])
                            ->required(),
                        Forms\Components\TextInput::make('max_senior')
                            ->label(__('general.maximum'))
                            ->gt('min_senior')
                            ->numeric()
                            ->rules([
                                'required',
                                'integer',
                            ])
                            ->required(),
                    ])->columnSpan(1),
                Forms\Components\Fieldset::make('Master')
                    ->label(___('general.masters'))
                    ->schema([
                        Forms\Components\TextInput::make('min_master')
                            ->label(__('general.minimum'))
                            ->numeric()
                            ->rules([
                                'required',
                                'integer',
                            ])
                            ->required(),
                        Forms\Components\TextInput::make('max_master')
                            ->label(__('general.maximum'))
                            ->gt('min_master')
                            ->numeric()
                            ->rules([
                                'required',
                                'integer',
                            ])
                            ->required(),
                    ])->columnSpan(1),
                Forms\Components\Fieldset::make('Agent')
                    ->label(___('general.agents'))
                    ->schema([
                        Forms\Components\TextInput::make('min_agent')
                            ->label(__('general.minimum'))
                            ->numeric()
                            ->rules([
                                'required',
                                'integer',
                            ])
                            ->required(),
                        Forms\Components\TextInput::make('max_agent')
                            ->label(__('general.maximum'))
                            ->helperText(__('general.form.max_agent.hint'))
                            // ->hintColor('warning')
                            ->gt('min_agent')
                            ->numeric()
                            ->rules([
                                'required',
                                'integer',
                            ])
                            ->required(),
                    ])->columnSpan(2),
            ])->columns(2);
    }
}
