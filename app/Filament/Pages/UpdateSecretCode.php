<?php

namespace App\Filament\Pages;

use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Illuminate\Contracts\Support\Htmlable;

class UpdateSecretCode extends SetupSecretCode
{
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    protected static string $layout = 'filament-panels::components.layout.index';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.update_secret_code');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.update_secret_code');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(3)->schema(self::getFormComponents()),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $user = Filament::auth()->user();

        $user->update([
            'secret_code' => bcrypt($data['secret_code']),
        ]);

        Notification::make()
            ->title(__('general.notification.title.secret_code_updated', [
                'name' => __('general.your'),
            ]))
            ->success()
            ->send();

        return redirect(self::getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }

    protected function hasFullWidthFormActions(): bool
    {
        return false;
    }
}
