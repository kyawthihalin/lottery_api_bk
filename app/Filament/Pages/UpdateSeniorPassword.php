<?php

namespace App\Filament\Pages;

use App\Models\Senior;
use App\Settings\AuthSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Actions\Action;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Illuminate\Contracts\Support\Htmlable;
use Rawilk\FilamentPasswordInput\Password;

class UpdateSeniorPassword extends Page
{
    use HasPageShield;
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    protected static ?string $navigationGroup = 'Update Senior';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.seniors').' '.___('general.password');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.password');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(2)->schema([
                \Filament\Forms\Components\Select::make('senior_id')
                    ->label(___('general.seniors'))
                    ->options(Senior::all()->pluck('name', 'id'))
                    ->rules([
                        'required',
                        'string',
                    ])
                    ->searchable()
                    ->preload()
                    ->selectablePlaceholder(false)
                    ->required(),
                Password::make('your_password')
                    ->label(__('general.form.your_password.label'))
                    ->password()
                    ->rules([
                        'required',
                        'current_password',
                    ])
                    ->required(),
                Password::make('password')
                    ->label(__('general.password'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getPasswordRule(\App\Models\Agent::class),
                    ]),
            ]),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $senior = Senior::find($data['senior_id']);

        $senior->update([
            'password' => bcrypt($data['password']),
        ]);

        Notification::make()
            ->title(__('general.notification.title.password_updated', [
                'name' => $senior->name,
            ]))
            ->success()
            ->send();

        return redirect(self::getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }
}
