<?php

namespace App\Filament\Pages;

use App\Settings\FileSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class ManageFile extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'gmdi-insert-drive-file-o';

    protected static string $settings = FileSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.manage_files');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.manage_files');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\FileUpload::make('user_app')
                    ->label(__('general.user_app'))
                    ->acceptedFileTypes(['application/vnd.android.package-archive'])
                    ->downloadable()
                    ->directory('apk')
                    ->required(),
                Forms\Components\FileUpload::make('agent_app')
                    ->label(__('general.agent_app'))
                    ->acceptedFileTypes(['application/vnd.android.package-archive'])
                    ->downloadable()
                    ->directory('apk')
                    ->required(),
                Forms\Components\FileUpload::make('master_app')
                    ->label(__('general.master_app'))
                    ->acceptedFileTypes(['application/vnd.android.package-archive'])
                    ->downloadable()
                    ->directory('apk')
                    ->required(),
                Forms\Components\FileUpload::make('senior_app')
                    ->label(__('general.senior_app'))
                    ->acceptedFileTypes(['application/vnd.android.package-archive'])
                    ->downloadable()
                    ->directory('apk')
                    ->required(),
            ]);
    }
}
