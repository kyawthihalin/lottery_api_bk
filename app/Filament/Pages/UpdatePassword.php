<?php

namespace App\Filament\Pages;

use App\Settings\AuthSettings;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Illuminate\Contracts\Support\Htmlable;
use Rawilk\FilamentPasswordInput\Password;

class UpdatePassword extends Page
{
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    protected static string $layout = 'filament-panels::components.layout.index';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.update_password');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.update_password');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(3)->schema([
                Password::make('current_password')
                    ->label(__('general.form.current_password.label'))
                    ->password()
                    ->rules([
                        'required',
                        'current_password',
                    ])
                    ->autofocus()
                    ->required(),
                Password::make('password')
                    ->label(__('general.form.new_password.label'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getPasswordRule(\App\Models\Agent::class),
                    ]),
            ]),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $user = Filament::auth()->user();

        $user->update([
            'password' => bcrypt($data['password']),
        ]);

        Notification::make()
            ->title(__('general.notification.title.password_updated', [
                'name' => __('general.your'),
            ]))
            ->success()
            ->send()
            ->sendToDatabase($user);

        return redirect(Filament::getCurrentPanel()->login()->getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }

    protected function hasFullWidthFormActions(): bool
    {
        return false;
    }
}
