<?php

namespace App\Filament\Pages;

use App\Settings\GeneralSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class ManageContact extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-calendar-days';

    protected static string $settings = GeneralSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.manage_contacts');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.manage_contacts');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('contact_viber')
                    ->label(__('general.contacts.viber'))
                    ->placeholder('viber://pa?chatURI=XXXXX')
                    ->prefixIcon('si-viber')
                    ->rules([
                        'string',
                        'starts_with:viber://pa?chatURI=',
                        'max:50',
                    ]),
                Forms\Components\TextInput::make('contact_facebook')
                    ->label(__('general.contacts.facebook'))
                    ->placeholder('https://www.facebook.com/XXXXX')
                    ->prefixIcon('si-facebook')
                    ->rules([
                        'string',
                        'starts_with:https://www.facebook.com/',
                        'max:50',
                    ]),
                Forms\Components\TextInput::make('contact_linkedin')
                    ->label(__('general.contacts.linkedin'))
                    ->placeholder('https://www.linkedin.com/company/XXXXX')
                    ->prefixIcon('si-linkedin')
                    ->rules([
                        'string',
                        'starts_with:https://www.linkedin.com/company/',
                        'max:50',
                    ]),
                Forms\Components\TextInput::make('contact_youtube')
                    ->label(__('general.contacts.youtube'))
                    ->placeholder('https://www.youtube.com/channel/XXXXX')
                    ->prefixIcon('si-youtube')
                    ->rules([
                        'string',
                        'starts_with:https://www.youtube.com/channel/',
                        'max:50',
                    ]),
                Forms\Components\TextInput::make('contact_instagram')
                    ->label(__('general.contacts.instagram'))
                    ->placeholder('https://www.instagram.com/XXXXX')
                    ->prefixIcon('si-instagram')
                    ->rules([
                        'string',
                        'starts_with:https://www.instagram.com/',
                        'max:50',
                    ]),
                Forms\Components\TextInput::make('contact_twitter')
                    ->label(__('general.contacts.twitter'))
                    ->placeholder('https://twitter.com/XXXXX')
                    ->prefixIcon('si-twitter')
                    ->rules([
                        'string',
                        'starts_with:https://twitter.com/',
                        'max:50',
                    ]),
                Forms\Components\TextInput::make('contact_mail')
                    ->label(__('general.contacts.mail'))
                    ->prefixIcon('gmdi-mail-o')
                    ->email()
                    ->rules([
                        'string',
                        'email',
                        'max:255',
                    ]),
                Forms\Components\TextInput::make('contact_phone')
                    ->label(__('general.contacts.phone'))
                    ->prefixIcon('gmdi-phone')
                    ->tel()
                    ->rules([
                        'string',
                        'max:20',
                    ]),
            ]);
    }
}
