<?php

namespace App\Filament\Pages;

use App\Settings\AuthSettings;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Form;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Rawilk\FilamentPasswordInput\Password;

class SetupSecretCode extends Page
{
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $layout = 'filament-panels::components.layout.base';

    protected static string $view = 'filament.pages.setup-secret-code';

    protected static bool $shouldRegisterNavigation = false;

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $user = Filament::auth()->user();
        if ($user->secret_code) {
            redirect(get_route_prefix_by_model($user));
        }

        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema(self::getFormComponents())->statePath('data');
    }

    public static function getFormComponents(): array
    {
        return [
            Password::make('password')
                ->label(__('general.form.current_password.label'))
                ->password()
                ->rules([
                    'required',
                    'string',
                    'current_password',
                ])
                ->autofocus()
                ->required(),
            Password::make('secret_code')
                ->label(__('general.secret_code'))
                ->password()
                ->copyable()
                ->regeneratePassword()
                ->notifyOnPasswordRegenerate(false)
                ->required()
                ->rules([
                    'required',
                    'string',
                    app(AuthSettings::class)->getSecretCodeRule(\App\Models\Agent::class),
                ]),
        ];
    }

    public function submit()
    {
        $data = $this->form->getState();
        $user = Filament::auth()->user();

        $user->forceFill([
            'secret_code' => bcrypt($data['secret_code']),
        ]);

        $user->update([
            'is_secret_code_required' => false,
        ]);

        return redirect(get_route_prefix_by_model($user));
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }

    protected function hasFullWidthFormActions(): bool
    {
        return true;
    }
}
