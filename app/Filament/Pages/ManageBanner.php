<?php

namespace App\Filament\Pages;

use App\Settings\BannerSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class ManageBanner extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static string $settings = BannerSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.manage_banners');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.manage_banners');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\FileUpload::make('user_app')
                    ->label(__('general.user_app'))
                    ->image()
                    ->multiple()
                    ->reorderable()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(5000)
                    ->required(),
                Forms\Components\FileUpload::make('website')
                    ->label(__('general.website'))
                    ->image()
                    ->multiple()
                    ->reorderable()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(5000)
                    ->required(),
            ]);
    }
}
