<?php

namespace App\Filament\Pages;

use App\Settings\AuthSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class ManageSecurity extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    protected static string $settings = AuthSettings::class;

    public function getTitle(): string|Htmlable
    {
        return __('general.security');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.security');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('type')
                    ->schema([
                        $this->getAdminTab(),
                        $this->getAgentTab(),
                        $this->getUserTab(),
                    ])->columns(3),
            ])->columns(1);
    }

    public function getAdminTab(): Tabs\Tab
    {
        return Tabs\Tab::make(___('general.admins'))
            ->schema([
                Forms\Components\Fieldset::make(__('security.form.fieldset.password_rules'))
                    ->schema([
                        Forms\Components\TextInput::make('admin_password_max_attempt')
                            ->label(___('security.form.max_attempt.label'))
                            ->helperText(___('security.form.max_attempt.helper'))
                            ->minValue(3)
                            ->rules([
                                'min:3',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\TextInput::make('admin_password_min')
                            ->label(___('security.form.min.label'))
                            ->minValue(6)
                            ->rules([
                                'min:6',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\Checkbox::make('admin_password_letters')
                            ->label(___('security.form.letters.label'))
                            ->helperText(___('security.form.letters.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('admin_password_mixed_case')
                            ->label(___('security.form.mixed_case.label'))
                            ->helperText(___('security.form.mixed_case.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('admin_password_numbers')
                            ->label(___('security.form.numbers.label'))
                            ->helperText(___('security.form.numbers.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('admin_password_symbols')
                            ->label(___('security.form.symbols.label'))
                            ->helperText(___('security.form.symbols.helper'))
                            ->inline(),
                    ])->columns(3),
            ])->columnSpanFull();
    }

    public function getAgentTab(): Tabs\Tab
    {
        return Tabs\Tab::make(___('security.agents_tab'))
            ->schema([
                Forms\Components\TextInput::make('agent_secret_code_interval')
                    ->label(___('security.form.secret_code_interval.label'))
                    ->helperText(___('security.form.secret_code_interval.helper'))
                    ->minValue(1)
                    ->rules([
                        'min:1',
                        'integer',
                    ])
                    ->numeric(),
                Forms\Components\Fieldset::make(__('security.form.fieldset.password_rules'))
                    ->schema([
                        Forms\Components\TextInput::make('agent_password_max_attempt')
                            ->label(___('security.form.max_attempt.label'))
                            ->helperText(___('security.form.max_attempt.helper'))
                            ->minValue(3)
                            ->rules([
                                'min:3',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\TextInput::make('agent_password_min')
                            ->label(___('security.form.min.label'))
                            ->minValue(6)
                            ->rules([
                                'min:6',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\Checkbox::make('agent_password_letters')
                            ->label(___('security.form.letters.label'))
                            ->helperText(___('security.form.letters.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('agent_password_mixed_case')
                            ->label(___('security.form.mixed_case.label'))
                            ->helperText(___('security.form.mixed_case.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('agent_password_numbers')
                            ->label(___('security.form.numbers.label'))
                            ->helperText(___('security.form.numbers.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('agent_password_symbols')
                            ->label(___('security.form.symbols.label'))
                            ->helperText(___('security.form.symbols.helper'))
                            ->inline(),
                    ])->columns(3),
                Forms\Components\Fieldset::make(__('security.form.fieldset.secret_code_rules'))
                    ->schema([
                        Forms\Components\TextInput::make('agent_secret_code_max_attempt')
                            ->label(___('security.form.max_attempt.label'))
                            ->helperText(___('security.form.max_attempt.helper'))
                            ->minValue(3)
                            ->rules([
                                'min:3',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\TextInput::make('agent_secret_code_min')
                            ->label(___('security.form.min.label'))
                            ->minValue(6)
                            ->rules([
                                'min:6',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\Checkbox::make('agent_secret_code_letters')
                            ->label(___('security.form.letters.label'))
                            ->helperText(___('security.form.letters.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('agent_secret_code_mixed_case')
                            ->label(___('security.form.mixed_case.label'))
                            ->helperText(___('security.form.mixed_case.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('agent_secret_code_numbers')
                            ->label(___('security.form.numbers.label'))
                            ->helperText(___('security.form.numbers.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('agent_secret_code_symbols')
                            ->label(___('security.form.symbols.label'))
                            ->helperText(___('security.form.symbols.helper'))
                            ->inline(),
                    ])->columns(3),
            ])->columnSpanFull();
    }

    public function getUserTab(): Tabs\Tab
    {
        return Tabs\Tab::make(___('general.users'))
            ->schema([
                Forms\Components\Fieldset::make(__('security.form.fieldset.password_rules'))
                    ->schema([
                        Forms\Components\TextInput::make('user_password_max_attempt')
                            ->label(___('security.form.max_attempt.label'))
                            ->helperText(___('security.form.max_attempt.helper'))
                            ->minValue(3)
                            ->rules([
                                'min:3',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\TextInput::make('user_password_min')
                            ->label(___('security.form.min.label'))
                            ->minValue(6)
                            ->rules([
                                'min:6',
                                'integer',
                            ])
                            ->numeric(),
                        Forms\Components\Checkbox::make('user_password_letters')
                            ->label(___('security.form.letters.label'))
                            ->helperText(___('security.form.letters.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('user_password_mixed_case')
                            ->label(___('security.form.mixed_case.label'))
                            ->helperText(___('security.form.mixed_case.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('user_password_numbers')
                            ->label(___('security.form.numbers.label'))
                            ->helperText(___('security.form.numbers.helper'))
                            ->inline(),
                        Forms\Components\Checkbox::make('user_password_symbols')
                            ->label(___('security.form.symbols.label'))
                            ->helperText(___('security.form.symbols.helper'))
                            ->inline(),
                    ])->columns(3),
            ])->columnSpanFull();
    }
}
