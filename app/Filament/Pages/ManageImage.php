<?php

namespace App\Filament\Pages;

use App\Settings\ImageSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class ManageImage extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-photo';

    protected static string $settings = ImageSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.manage_images');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.manage_images');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\FileUpload::make('app_logo')
                    ->label(__('general.app_logo'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('default_avatar')
                    ->label(__('general.default_avatar'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('app_background')
                    ->label(__('general.app_background'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('website_background')
                    ->label(__('general.website_background'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('group_ticket_frame_website')
                    ->label(__('general.group_ticket_frame_website'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('group_ticket_frame_mobile')
                    ->label(__('general.group_ticket_frame_mobile'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('ticket_background')
                    ->label(__('general.ticket_background'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(1024),
                Forms\Components\FileUpload::make('won_image')
                    ->label(__('general.won_image'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(4000),
                Forms\Components\FileUpload::make('lost_image')
                    ->label(__('general.lost_image'))
                    ->image()
                    ->openable()
                    ->downloadable()
                    ->directory('images')
                    ->maxSize(4000),
            ]);
    }
}
