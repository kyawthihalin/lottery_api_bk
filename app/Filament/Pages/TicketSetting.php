<?php

namespace App\Filament\Pages;

use App\Settings\TicketSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Pages\SettingsPage;
use Illuminate\Contracts\Support\Htmlable;

class TicketSetting extends SettingsPage
{
    use HasPageShield;

    protected static ?string $navigationIcon = 'heroicon-o-ticket';

    protected static string $settings = TicketSettings::class;

    public function getTitle(): string|Htmlable
    {
        return ___('general.ticket_setting');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('general.settings');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.ticket_setting');
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TagsInput::make('restricted_tickets')
                    ->label(__('general.form.restricted_tickets.label'))
                    ->placeholder(__('general.form.restricted_tickets.placeholder'))
                    ->helperText(__('general.form.restricted_tickets.description'))
                    ->nestedRecursiveRules([
                        'digits:6',
                    ])
                    ->validationMessages([
                        '*.digits' => __('validation.digits', [
                            'attribute' => ___('general.tickets'),
                        ]),
                    ])
                    ->required(),
                Forms\Components\TextInput::make('purchase_limit_per_ticket')
                    ->label(__('general.form.purchase_limit_per_ticket.label'))
                    ->helperText(__('general.form.purchase_limit_per_ticket.description'))
                    ->numeric()
                    ->rules([
                        'required',
                        'integer',
                        'min:1',
                    ])
                    ->required(),
                Forms\Components\TextInput::make('tickets_limit_per_purchase')
                    ->label(__('general.form.tickets_limit_per_purchase.label'))
                    ->helperText(__('general.form.tickets_limit_per_purchase.description'))
                    ->numeric()
                    ->rules([
                        'required',
                        'integer',
                        'min:1',
                    ])
                    ->required(),
                Forms\Components\TextInput::make('random_ticket_groups_count_per_draw_id')
                    ->label(__('general.form.random_ticket_groups_count_per_draw_id.label'))
                    ->helperText(__('general.form.random_ticket_groups_count_per_draw_id.description'))
                    ->numeric()
                    ->rules([
                        'required',
                        'integer',
                        'min:1',
                        'max:20',
                    ])
                    ->required(),
            ]);
    }
}
