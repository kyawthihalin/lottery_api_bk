<?php

namespace App\Filament\Pages;

use App\Models\Admin;
use App\Models\Senior;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Components\Section;
use Filament\Forms\Form;
use Filament\Pages\Dashboard as BaseDashboard;
use Filament\Pages\Dashboard\Concerns\HasFiltersForm;

class Dashboard extends BaseDashboard
{
    use HasFiltersForm;

    public function filtersForm(Form $form): Form
    {
        $authUser = Filament::auth()->user();
        $childUsers = match (get_class($authUser)) {
            Admin::class => Senior::all(),
            Senior::class => Senior::where('id', $authUser->id)->get(),
            default => Senior::all(),
        };

        return $form
            ->schema([
                Section::make()
                    ->schema([
                        Forms\Components\Select::make('senior_id')
                            ->label('Senior')
                            ->options($childUsers->pluck('name', 'id'))
                            ->required(),
                    ])
                    ->columns(3),
            ]);
    }
}
