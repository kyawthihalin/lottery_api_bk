<?php

namespace App\Filament\Agent\Resources;

use App\Filament\Agent\Resources\BetResource\Pages;
use App\Filament\Resources\BetResource as AdminBetResource;
use App\Models\Bet;
use Filament\Facades\Filament;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class BetResource extends Resource
{
    protected static ?string $model = Bet::class;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    protected static ?string $slug = 'purchased-list';

    public static function getModelLabel(): string
    {
        return __('general.purchased_list');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function table(Table $table): Table
    {
        return AdminBetResource::table($table)
            ->modifyQueryUsing(fn (Builder $query) => $query->whereRelation('user', 'agent_id', '=', Filament::auth()->id()));
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBets::route('/'),
        ];
    }
}
