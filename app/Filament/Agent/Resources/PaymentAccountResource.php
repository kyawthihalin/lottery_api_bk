<?php

namespace App\Filament\Agent\Resources;

use App\Filament\Agent\Resources\PaymentAccountResource\Pages;
use App\Models\PaymentAccount;
use Filament\Facades\Filament;
use Filament\Resources\Resource;
use Filament\Support\Enums\IconPosition;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PaymentAccountResource extends Resource
{
    protected static ?string $model = PaymentAccount::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function getModelLabel(): string
    {
        return __('general.payment_accounts');
    }

    public static function table(Table $table): Table
    {
        return $table
            ->modifyQueryUsing(fn (Builder $query) => $query->whereRelation('user', 'agent_id', '=', Filament::auth()->id()))
            ->columns([
                Tables\Columns\TextColumn::make('user.name')
                    ->label(___('general.users'))
                    ->searchable(isIndividual: true)
                    ->description(fn (Model $record): string => $record->user->user_name)
                    ->url(function (Model $record) {
                        if (Filament::getAuthGuard() != 'admin') {
                            return null;
                        }
                        $user = $record->user;

                        $path = get_path_by_model($user);

                        return route("filament.admin.resources.{$path}.index", [
                            'tableSearch' => $user->user_name,
                        ]);
                    }, shouldOpenInNewTab: true),
                Tables\Columns\TextColumn::make('name')
                    ->label(__('general.name')),
                Tables\Columns\TextColumn::make('number')
                    ->label(__('general.number'))
                    ->searchable(isIndividual: true)
                    ->copyable()
                    ->icon('gmdi-copy-all-o')
                    ->iconPosition(IconPosition::After)
                    ->iconColor('success'),
            ])
            ->filters([
                //
            ])
            ->actions([
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPaymentAccounts::route('/'),
        ];
    }
}
