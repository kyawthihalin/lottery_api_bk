<?php

namespace App\Filament\Agent\Resources;

use App\Enums\Gender;
use App\Filament\Agent\Resources\UserResource\Pages;
use App\Filament\Resources\UserResource as AdminUserResource;
use App\Models\User;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Rawilk\FilamentPasswordInput\Password;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    protected static ?string $slug = 'account-management';

    public static function getModelLabel(): string
    {
        return __('general.users');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.account_management');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label(__('general.name'))
                    ->rules(['required', 'string', 'max:255'])
                    ->required(),
                Forms\Components\TextInput::make('user_name')
                    ->label(__('general.user_name'))
                    ->unique('users', ignoreRecord: true)
                    ->hidden(fn (string $operation): bool => $operation === 'edit')
                    ->rules(['required', 'alpha_dash'])
                    ->required(),
                Forms\Components\TextInput::make('phone')
                    ->label(__('general.phone'))
                    ->tel(),
                Password::make('password')
                    ->hidden(fn (string $operation) => $operation === 'edit')
                    ->label(__('general.password'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->dehydrateStateUsing(fn (string $state): string => bcrypt($state))
                    ->dehydrated(fn (?string $state): bool => filled($state))
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                    ]),
                Forms\Components\Select::make('gender')
                    ->label(__('general.gender'))
                    ->options(Gender::class)
                    ->native(false)
                    ->selectablePlaceholder(false)
                    ->default(Gender::Male)
                    ->required(),
            ])
            ->columns(3);
    }

    public static function table(Table $table): Table
    {
        return AdminUserResource::table($table)
            ->modifyQueryUsing(fn (Builder $query) => $query->where('agent_id', auth()->id()))
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
        ];
    }
}
