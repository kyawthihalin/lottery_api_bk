<?php

namespace App\Filament\Agent\Resources\BetResource\Pages;

use App\Filament\Agent\Resources\BetResource;
use Filament\Resources\Pages\ListRecords;

class ListBets extends ListRecords
{
    protected static string $resource = BetResource::class;
}
