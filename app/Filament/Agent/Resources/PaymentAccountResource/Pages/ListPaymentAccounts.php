<?php

namespace App\Filament\Agent\Resources\PaymentAccountResource\Pages;

use App\Filament\Agent\Resources\PaymentAccountResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListPaymentAccounts extends ListRecords
{
    protected static string $resource = PaymentAccountResource::class;
}
