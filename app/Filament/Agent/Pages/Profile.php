<?php

namespace App\Filament\Agent\Pages;

use App\Filament\Resources\AgentResource;
use Filament\Forms;
use Filament\Forms\Components\Component;
use Filament\Forms\Form;
use Filament\Pages\Auth\EditProfile;

class Profile extends EditProfile
{
    protected static string $layout = 'filament-panels::components.layout.index';

    /**
     * @var view-string
     */
    protected static string $view = 'filament.pages.edit-profile';

    protected function getPhoneFormComponent(): Component
    {
        return Forms\Components\TextInput::make('phone')
            ->label(__('general.phone'))
            ->tel();
    }

    /**
     * @return array<int | string, string | Form>
     */
    protected function getForms(): array
    {
        return [
            'form' => $this->form(
                $this->makeForm()
                    ->schema([
                        Forms\Components\Tabs::make('Form')
                            ->schema([
                                AgentResource::getProfileTab(),
                                AgentResource::getNrcTab(),
                            ])->columns(3),
                    ])
                    ->operation('edit')
                    ->model($this->getUser())
                    ->statePath('data'),
            ),
        ];
    }
}
