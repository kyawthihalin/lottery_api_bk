<?php

namespace App\Filament\Agent\Pages;

use App\Models\Agent;
use App\Settings\AuthSettings;
use Filament\Actions\Action;
use Filament\Facades\Filament;
use Filament\Forms\Components\Grid;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Pages\Concerns\InteractsWithFormActions;
use Filament\Pages\Page;
use Illuminate\Contracts\Support\Htmlable;
use Rawilk\FilamentPasswordInput\Password;

class UpdateUserPassword extends Page
{
    use InteractsWithFormActions;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    protected static string $layout = 'filament-panels::components.layout.index';

    protected static string $view = 'filament.pages.update-secret-code';

    protected static bool $shouldRegisterNavigation = true;

    public function getTitle(): string|Htmlable
    {
        return ___('general.update_user_password');
    }

    public static function getNavigationLabel(): string
    {
        return __('general.update_user_password');
    }

    /**
     * @var array<string, mixed> | null
     */
    public ?array $data = [];

    public function mount(): void
    {
        $this->form->fill();
    }

    public function form(Form $form): Form
    {
        return $form->schema([
            Grid::make(2)->schema([
                \Filament\Forms\Components\Select::make('user_id')
                    ->label(___('general.users'))
                    ->options(Agent::find(Filament::auth()->id())->users()->pluck('name', 'id'))
                    ->rules([
                        'required',
                        'string',
                    ])
                    ->searchable()
                    ->preload()
                    ->selectablePlaceholder(false)
                    ->required(),
                Password::make('your_password')
                    ->label(__('general.form.your_password.label'))
                    ->password()
                    ->rules([
                        'required',
                        'current_password',
                    ])
                    ->required(),
                Password::make('password')
                    ->label(__('general.password'))
                    ->password()
                    ->copyable()
                    ->regeneratePassword()
                    ->notifyOnPasswordRegenerate(false)
                    ->required()
                    ->rules([
                        'required',
                        'string',
                        app(AuthSettings::class)->getPasswordRule(\App\Models\User::class),
                    ]),
            ]),
        ])->statePath('data');
    }

    public function submit()
    {
        $data = $this->form->getState();
        $agent = Agent::find(Filament::auth()->id());
        $user = $agent->users()->find($data['user_id']);

        $user->update([
            'password' => bcrypt($data['password']),
        ]);

        Notification::make()
            ->title(__('general.notification.title.password_updated', [
                'name' => $user->name,
            ]))
            ->success()
            ->send();

        return redirect(self::getUrl());
    }

    /**
     * @return array<Action | ActionGroup>
     */
    protected function getFormActions(): array
    {
        return [
            Action::make('submit')
                ->label(__('general.submit'))
                ->submit('submit'),
        ];
    }
}
