<?php

namespace App\Filament\Agent\Widgets;

use App\Filament\Widgets\SettlementWidget as AdminSettlementWidget;
use App\Models\Agent;
use App\Models\Settlement;
use Filament\Facades\Filament;
use Filament\Tables\Table;
use Filament\Widgets\Concerns\InteractsWithPageFilters;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementWidget extends BaseWidget
{
    use InteractsWithPageFilters;

    protected int|string|array $columnSpan = 'full';

    protected static bool $isLazy = false;

    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.history');
    }

    public function table(Table $table): Table
    {
        return $table
            ->query(Settlement::where('user_id', Filament::auth()->id())->whereUserType(Agent::class))
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(AdminSettlementWidget::getColumns())
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.history').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }
}
