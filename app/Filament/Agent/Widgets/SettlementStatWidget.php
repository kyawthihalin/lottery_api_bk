<?php

namespace App\Filament\Agent\Widgets;

use App\Filament\Widgets\SettlementStatWidget as AdminSettlementStatWidget;
use App\Models\Agent;
use Filament\Facades\Filament;
use Filament\Tables\Table;
use Filament\Widgets\Concerns\InteractsWithPageFilters;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Contracts\Support\Htmlable;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use pxlrbt\FilamentExcel\Exports\ExcelExport;

class SettlementStatWidget extends BaseWidget
{
    use InteractsWithPageFilters;

    protected int|string|array $columnSpan = 'full';

    protected static bool $isLazy = false;

    protected function getTableHeading(): string|Htmlable|null
    {
        return __('settlement.stat');
    }

    public function table(Table $table): Table
    {
        return $table
            ->query(Agent::where('id', Filament::auth()->id()))
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns(AdminSettlementStatWidget::getStatColumns())
            ->bulkActions([
                ExportBulkAction::make()->exports([
                    ExcelExport::make()->withFilename(fn () => __('settlement.stat').' '.now()->format('Y-m-d h-i A'))->fromTable(),
                ]),
            ]);
    }
}
