<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait HasNrc
{
    protected function nrc(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                if (! $attributes['nrc_state'] || ! $attributes['nrc_township'] || ! $attributes['nrc_type'] || ! $attributes['nrc_number']) {
                    return '';
                }

                return "{$attributes['nrc_state']}/{$attributes['nrc_township']}({$attributes['nrc_type']}){$attributes['nrc_number']}";
            },
        );
    }
}
