<?php

namespace App\Traits;

trait ResponseHelper
{
    protected function responseSuccess(array $data = [], string $message = 'Success', int $status = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'data' => $data,
            'message' => $message,
            'status' => $status,
        ], $status);
    }

    protected function responseError(string $message, array $data, int $code): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'data' => $data,
            'message' => $message,
            'status' => $code,
        ], $code);
    }

    protected function responseBadRequest(string $message = 'Bad Request'): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'message' => $message,
            'status' => 400,
        ], 400);
    }

    protected function responseLocked(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'message' => __('auth.locked'),
            'status' => 403,
        ], 403);
    }

    protected function responseHiddenValidationError(array $errors)
    {
        $data['message'] = 'Unprocessable entity.';
        $data['status'] = 422;

        if (config('app.show_hidden_validation_errors')) {
            $data['message'] = 'Development validation error, missing hidden parameters';
            $data['notice'] = 'Errors will be null in production.';
            $data['errors'] = $errors;
        }

        return response()->json($data, 422);
    }
}
