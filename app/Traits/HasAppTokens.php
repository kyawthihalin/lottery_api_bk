<?php

namespace App\Traits;

use App\Models\AppToken;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasAppTokens
{
    public function appTokens(): MorphMany
    {
        return $this->morphMany(AppToken::class, 'user');
    }
}
