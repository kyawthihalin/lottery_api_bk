<?php

namespace App\Traits;

use App\Facades\Settlement;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait HasSettlements
{
    protected function boughtPoint(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getBoughtPoint(self::find($attributes['id'])),
        );
    }

    protected function rewithdrawnPoint(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getRewithdrawnPoint(self::find($attributes['id'])),
        );
    }

    protected function paidPoint(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getPaidPoint(self::find($attributes['id'])),
        );
    }

    protected function regainedPoint(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getRegainedPoint(self::find($attributes['id'])),
        );
    }

    protected function usedPoint(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getUsedPoint(self::find($attributes['id'])),
        );
    }

    protected function paidAbove(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getPaidAbove(self::find($attributes['id'])),
        );
    }

    protected function netPaidAbove(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Settlement::getNetPaidAbove(self::find($attributes['id'])),
        );
    }
}
