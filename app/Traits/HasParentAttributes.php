<?php

namespace App\Traits;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait HasParentAttributes
{
    public function parentType(Agent|Senior|Master|Admin|null $parent = null): Attribute
    {
        $parent = $parent ?? $this->parent;

        return new Attribute(
            get: fn () => match (get_class($parent)) {
                Agent::class => 'agent',
                Senior::class => 'senior',
                Master::class => 'master',
                Admin::class => 'admin',
            },
        );
    }
}
