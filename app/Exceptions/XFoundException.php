<?php

namespace App\Exceptions;

use App\Models\Admin;
use App\Notifications\LotteryResultFailed;
use Exception;
use Illuminate\Support\Facades\Notification;

class XFoundException extends Exception
{
    public function __construct(public array $results)
    {
    }

    public function render()
    {
        Notification::send(Admin::first(), new LotteryResultFailed('Fail to fetch lottery results. X character found.', json_encode($this->results)));
    }
}
