<?php

namespace App\Exceptions;

use App\Enums\TransactionType;
use Exception;

class PointNotEnoughException extends Exception
{
    public function __construct(public TransactionType $transactionType)
    {
        //
    }
}
