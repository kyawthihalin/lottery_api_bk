<?php

namespace App\Exceptions;

use App\Traits\ResponseHelper;
use Exception;

class InvalidSecretCodeException extends Exception
{
    use ResponseHelper;

    public function render()
    {
        return $this->responseBadRequest(message: __('auth.secret_code'));
    }
}
