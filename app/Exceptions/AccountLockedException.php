<?php

namespace App\Exceptions;

use App\Traits\ResponseHelper;
use Exception;

class AccountLockedException extends Exception
{
    use ResponseHelper;

    public function render()
    {
        return $this->responseLocked();
    }
}
