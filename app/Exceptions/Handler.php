<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        InvalidPasswordException::class,
        InvalidSecretCodeException::class,
        XFoundException::class,
    ];

    public function render($request, Throwable $exception)
    {
        if (! str_starts_with($request->path(), 'api')) {
            return parent::render($request, $exception);
        }

        $data = match (get_class($exception)) {
            ValidationException::class => [
                'message' => $exception->getMessage(),
                'errors' => $exception->validator->errors(),
                'status' => 422,
            ],
            PointNotEnoughException::class => [
                'message' => __('general.errors.point_not_enough', [
                    'transaction_type' => $exception->transactionType->getBreifLabel(),
                ]),
                'status' => 400,
            ],
            AuthenticationException::class => [
                'message' => 'Unauthenticated',
                'status' => 401,
            ],
            InvalidAppTokenException::class => [
                'message' => config('app.show_hidden_validation_errors') ? 'Invalid app token.' : 'Unprocessable entity.',
                'status' => 422,
            ],
            default => [],
        };

        if (count($data)) {
            return response()->json($data, $data['status']);
        }

        return parent::render($request, $exception);
    }
}
