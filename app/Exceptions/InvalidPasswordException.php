<?php

namespace App\Exceptions;

use App\Traits\ResponseHelper;
use Exception;

class InvalidPasswordException extends Exception
{
    use ResponseHelper;

    public function __construct()
    {
    }

    public function render()
    {
        return $this->responseBadRequest(message: __('auth.password'));
    }
}
