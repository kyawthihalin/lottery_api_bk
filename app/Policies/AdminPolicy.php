<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\PolicyUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the policyUser can view any models.
     */
    public function viewAny(PolicyUser $policyUser): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('view_any_admin');
    }

    /**
     * Determine whether the policyUser can view the model.
     */
    public function view(PolicyUser $policyUser, Admin $admin): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ View }}');
    }

    /**
     * Determine whether the policyUser can create models.
     */
    public function create(PolicyUser $policyUser): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('create_admin');
    }

    /**
     * Determine whether the policyUser can update the model.
     */
    public function update(PolicyUser $policyUser, Admin $admin): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('update_admin');
    }

    /**
     * Determine whether the policyUser can delete the model.
     */
    public function delete(PolicyUser $policyUser, Admin $admin): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ Delete }}');
    }

    /**
     * Determine whether the policyUser can bulk delete.
     */
    public function deleteAny(PolicyUser $policyUser): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ DeleteAny }}');
    }

    /**
     * Determine whether the policyUser can permanently delete.
     */
    public function forceDelete(PolicyUser $policyUser, Admin $admin): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ ForceDelete }}');
    }

    /**
     * Determine whether the policyUser can permanently bulk delete.
     */
    public function forceDeleteAny(PolicyUser $policyUser): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ ForceDeleteAny }}');
    }

    /**
     * Determine whether the policyUser can restore.
     */
    public function restore(PolicyUser $policyUser, Admin $admin): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ Restore }}');
    }

    /**
     * Determine whether the policyUser can bulk restore.
     */
    public function restoreAny(PolicyUser $policyUser): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ RestoreAny }}');
    }

    /**
     * Determine whether the policyUser can replicate.
     */
    public function replicate(PolicyUser $policyUser, Admin $admin): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ Replicate }}');
    }

    /**
     * Determine whether the policyUser can reorder.
     */
    public function reorder(PolicyUser $policyUser): bool
    {
        if (! $policyUser instanceof Admin) {
            return true;
        }

        return $policyUser->can('{{ Reorder }}');
    }

    public function lock(PolicyUser $policyUser): bool
    {
        return $policyUser->can('lock_admin');
    }
}
