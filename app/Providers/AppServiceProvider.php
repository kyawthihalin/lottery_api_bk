<?php

namespace App\Providers;

use App\Filament\Resources\RoleResource\Pages\CreateRole;
use App\Filament\Resources\RoleResource\Pages\EditRole;
use App\Services\DrawService;
use App\Services\SettlementService;
use BezhanSalleh\FilamentLanguageSwitch\LanguageSwitch;
use BezhanSalleh\FilamentShield\Resources\RoleResource\Pages\CreateRole as ShieldCreateRole;
use BezhanSalleh\FilamentShield\Resources\RoleResource\Pages\EditRole as ShieldEditRole;
use Filament\Notifications\Livewire\DatabaseNotifications;
use Filament\Notifications\Notification;
use Filament\Support\Facades\FilamentColor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ShieldEditRole::class, fn () => new EditRole());
        $this->app->bind(ShieldCreateRole::class, fn () => new CreateRole());
        $this->app->bind('draw', fn () => new DrawService);
        $this->app->bind('settlement', fn () => new SettlementService);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Model::unguard();

        DatabaseNotifications::pollingInterval(null);

        LanguageSwitch::configureUsing(function (LanguageSwitch $switch) {
            $switch
                ->locales(['en', 'my']); // also accepts a closure
                // ->flags([
                //     'en' => asset('flags/en.svg'),
                //     'my' => asset('flags/my.svg'),
                // ]);
        });

        FilamentColor::register([
            'info' => [
                '50' => '#ecf2ff',
                '100' => '#dde8ff',
                '200' => '#c2d4ff',
                '300' => '#9cb5ff',
                '400' => '#758cff',
                '500' => '#4254ff',
                '600' => '#3639f5',
                '700' => '#2a2ad8',
                '800' => '#2527ae',
                '900' => '#262a89',
                '950' => '#161650',
            ],
            'primary' => [
                '50' => '#eef9ff',
                '100' => '#daf0ff',
                '200' => '#bce7ff',
                '300' => '#8fd9ff',
                '400' => '#5ac1ff',
                '500' => '#33a4fe',
                '600' => '#1c85f3',
                '700' => '#156ee0',
                '800' => '#1859b5',
                '900' => '#1a4c8e',
                '950' => '#152f56',
            ],
            'warning' => [
                '50' => '#fef8ee',
                '100' => '#fcefd8',
                '200' => '#f8dcb0',
                '300' => '#f3c27e',
                '400' => '#ec9c46',
                '500' => '#e88327',
                '600' => '#d96a1d',
                '700' => '#b4511a',
                '800' => '#90411c',
                '900' => '#74371a',
                '950' => '#3e1a0c',
            ],
        ]);

        Notification::configureUsing(function (Notification $notification): void {
            $notification->view('notifications.notification');
        });
    }
}
