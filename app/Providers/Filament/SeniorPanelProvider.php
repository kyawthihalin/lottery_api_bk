<?php

namespace App\Providers\Filament;

use App\Filament\Agent\Pages\Login;
use App\Filament\Pages\SecretCode;
use App\Filament\Pages\SetupSecretCode;
use App\Filament\Pages\UpdatePassword;
use App\Filament\Pages\UpdateSecretCode;
use App\Filament\Senior\Pages\Dashboard;
use App\Http\Middleware\ForceSecretCodeMiddleware;
use App\Services\StorageService;
use App\Settings\ImageSettings;
use Filament\Http\Middleware\Authenticate;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Filament\Panel;
use Filament\PanelProvider;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class SeniorPanelProvider extends PanelProvider
{
    public function panel(Panel $panel): Panel
    {
        return $panel
            ->id('senior')
            ->path('senior')
            ->authGuard('senior')
            ->login(Login::class)
            ->discoverResources(in: app_path('Filament/Senior/Resources'), for: 'App\\Filament\\Senior\\Resources')
            ->discoverPages(in: app_path('Filament/Senior/Pages'), for: 'App\\Filament\\Senior\\Pages')
            ->pages([
                Dashboard::class,
                SecretCode::class,
                SetupSecretCode::class,
                UpdateSecretCode::class,
                UpdatePassword::class,
            ])
            ->discoverWidgets(in: app_path('Filament/Senior/Widgets'), for: 'App\\Filament\\Senior\\Widgets')
            ->widgets([
            ])
            ->middleware([
                EncryptCookies::class,
                AddQueuedCookiesToResponse::class,
                StartSession::class,
                AuthenticateSession::class,
                ShareErrorsFromSession::class,
                VerifyCsrfToken::class,
                SubstituteBindings::class,
                DisableBladeIconComponents::class,
                DispatchServingFilamentEvent::class,
            ])
            ->authMiddleware([
                Authenticate::class,
                ForceSecretCodeMiddleware::class,
            ])
            ->renderHook(
                'panels::global-search.after',
                fn () => view('components.filament.widgets.user-point')
            )
            ->databaseNotifications()
            ->viteTheme('resources/css/admin/theme.css')
            ->sidebarFullyCollapsibleOnDesktop()
            ->brandName(config('app.name'))
            // ->brandLogo(StorageService::getUrl(app(ImageSettings::class)->app_logo))
            ->brandLogoHeight('3rem');
    }
}
