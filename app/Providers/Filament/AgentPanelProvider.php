<?php

namespace App\Providers\Filament;

use App\Filament\Agent\Pages\Login;
use App\Filament\Agent\Pages\Profile;
use App\Filament\Pages\SecretCode;
use App\Filament\Pages\SetupSecretCode;
use App\Filament\Pages\UpdatePassword;
use App\Filament\Pages\UpdateSecretCode;
use App\Http\Middleware\ForceSecretCodeMiddleware;
use App\Services\StorageService;
use App\Settings\ImageSettings;
use Filament\Http\Middleware\Authenticate;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Filament\Pages;
use Filament\Panel;
use Filament\PanelProvider;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use pxlrbt\FilamentSpotlight\SpotlightPlugin;

class AgentPanelProvider extends PanelProvider
{
    public function panel(Panel $panel): Panel
    {
        return $panel
            ->id('agent')
            ->path('agent')
            ->authGuard('agent')
            ->login(Login::class)
            ->profile(Profile::class)
            ->discoverResources(in: app_path('Filament/Agent/Resources'), for: 'App\\Filament\\Agent\\Resources')
            ->discoverPages(in: app_path('Filament/Agent/Pages'), for: 'App\\Filament\\Agent\\Pages')
            ->pages([
                Pages\Dashboard::class,
                SecretCode::class,
                SetupSecretCode::class,
                UpdateSecretCode::class,
                UpdatePassword::class,
            ])
            ->discoverWidgets(in: app_path('Filament/Agent/Widgets'), for: 'App\\Filament\\Agent\\Widgets')
            ->widgets([
            ])
            ->middleware([
                EncryptCookies::class,
                AddQueuedCookiesToResponse::class,
                StartSession::class,
                AuthenticateSession::class,
                ShareErrorsFromSession::class,
                VerifyCsrfToken::class,
                SubstituteBindings::class,
                DisableBladeIconComponents::class,
                DispatchServingFilamentEvent::class,
            ])
            ->authMiddleware([
                Authenticate::class,
                ForceSecretCodeMiddleware::class,
            ])
            ->plugins([
                SpotlightPlugin::make(),
            ])
            ->renderHook(
                'panels::global-search.after',
                fn () => view('components.filament.widgets.user-point')
            )
            ->databaseNotifications()
            ->databaseNotificationsPolling('180s')
            ->viteTheme('resources/css/admin/theme.css')
            ->sidebarFullyCollapsibleOnDesktop()
            ->brandName(config('app.name'))
            // ->brandLogo(StorageService::getUrl(app(ImageSettings::class)->app_logo))
            ->brandLogoHeight('3rem');
    }
}
