<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        \App\Events\TicketPurchased::class => [
            \App\Listeners\SendTicketPurchasedNotification::class,
            \App\Listeners\ResetTicketGroup::class,
        ],
        \App\Events\DepositCompleted::class => [
            \App\Listeners\SendDepositCompletedNotification::class,
        ],
        \App\Events\WithdrawCompleted::class => [
            \App\Listeners\SendWithdrawCompletedNotification::class,
        ],
        \App\Events\LotteryWon::class => [
            \App\Listeners\SendLotteryWonNotification::class,
        ],
        \App\Events\SystemNotificationSent::class => [
            \App\Listeners\SendSystemNotification::class,
        ],
        \App\Events\SettlementCompleted::class => [
            \App\Listeners\SendSettlementCompletedNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
