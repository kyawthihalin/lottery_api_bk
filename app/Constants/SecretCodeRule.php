<?php

namespace App\Constants;

use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use Illuminate\Validation\Rules\Password;

class SecretCodeRule
{
    const MIN = [
        Agent::class => 8,
    ];

    const LETTERS = [
        Agent::class => true,
    ];

    const MIXED_CASE = [
        Agent::class => true,
    ];

    const NUMBERS = [
        Agent::class => true,
    ];

    const SYMBOLS = [
        Agent::class => true,
    ];

    public static function getRules(Senior|Master|Agent|string $class): Password
    {
        $rule = Password::min(self::MIN[$class]);

        if (self::LETTERS[$class]) {
            $rule->letters();
        }
        if (self::MIXED_CASE[$class]) {
            $rule->mixedCase();
        }
        if (self::NUMBERS[$class]) {
            $rule->numbers();
        }
        if (self::SYMBOLS[$class]) {
            $rule->symbols();
        }

        return $rule;
    }
}
