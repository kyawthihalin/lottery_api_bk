<?php

namespace App\Constants\Factory;

class Credential
{
    const PASSWORD = 'ALBBdefPwd#757';

    const SECRET_CODE = 'ALBBdefSc#757';

    public static function getPassword()
    {
        return bcrypt(app()->environment('local') ? 'password' : self::PASSWORD);
    }

    public static function getSecretCode()
    {
        return bcrypt(app()->environment('local') ? 'password' : self::SECRET_CODE);
    }
}
