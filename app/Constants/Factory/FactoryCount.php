<?php

namespace App\Constants\Factory;

class FactoryCount
{
    const MAX_SENIOR = 3;

    const MIN_MASTER = 1; // max for a senior

    const MAX_MASTER = 2; // max for a senior

    const MIN_AGENT = 1; // max for a senior

    const MAX_AGENT = 2; // max for a master

    const MIN_USER = 5; // max for an agent

    const MAX_USER = 10; // max for an agent

    const DRAW_ID_COUNT = 3;
}
