<?php

namespace App\Services\LotteryApi;

use App\Contracts\LotteryApi;
use App\Enums\LotteryApiError;
use App\Exceptions\XFoundException;
use App\Facades\Draw;
use App\Models\Admin;
use App\Models\ResultType;
use App\Notifications\LotteryResultFailed;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class LotteryService implements LotteryApi
{
    private $key_map = [
        'รางวัลที่1' => 'first',
        'รางวัลข้างเคียงรางวัลที่1' => 'closest_first',
        'รางวัลที่2' => 'second',
        'รางวัลที่3' => 'third',
        'รางวัลที่4' => 'forth',
        'รางวัลที่5' => 'fifth',
        'เลขหน้า3ตัว' => 'first_three',
        'เลขท้าย3ตัว' => 'last_three',
        'เลขท้าย2ตัว' => 'last_two',
    ];

    public function __construct()
    {
    }

    public function health(): bool
    {
        try {
            $client = new Client();
            $client->request('GET', 'https://thai-lottery1.p.rapidapi.com/reto', [
                'headers' => [
                    'X-RapidAPI-Host' => 'thai-lottery1.p.rapidapi.com',
                    'X-RapidAPI-Key' => config('services.lottery.api_key'),
                ],
            ]);
        } catch (Exception $e) {
            $this->warn('Health check failed.', "
Provider: Rapid Api,
Error Message: {$e->getMessage()},
");

            return false;
        }

        return true;
    }

    public function fetchResults(string $drawId): ?LotteryApiError
    {
        if (! $this->health()) {
            return LotteryApiError::FAILED;
        }

        $thailandDate = convert_to_thailand_date($drawId);

        try {

            $request = new Request('GET', "https://thai-lottery1.p.rapidapi.com/?date={$thailandDate}", [
                'X-RapidAPI-Host' => 'thai-lottery1.p.rapidapi.com',
                'X-RapidAPI-Key' => config('services.lottery.api_key'),
            ]);
            $res = (new Client())->sendAsync($request)->wait();

        } catch (Exception $e) {
            $this->warn('Fail to fetch lottery results.', "
Provider: Rapid Api,
Error Message: {$e->getMessage()},
Draw ID: {$drawId},
Thailand Date: {$thailandDate}
          ");

            return LotteryApiError::FAILED;
        }

        $statusCode = $res->getStatusCode();

        if ($res->getStatusCode() != 200) {
            $this->warn('Fail to fetch lottery results.', "
Provider: Rapid Api,
Error Code: $statusCode,
Draw ID: {$drawId},
Thailand Date: {$thailandDate}
          ");

            return LotteryApiError::FAILED;
        }

        $bodystream = $res->getBody();

        $data = json_decode($bodystream->getContents());

        if ($data[0][1] === 0) {
            $this->warn('Fail to fetch lottery results.', "
Provider: Rapid Api,
Error: No results,
Draw ID: {$drawId},
Thailand Date: {$thailandDate}
          ");

            return LotteryApiError::NOT_FOUND;
        }

        DB::transaction(function () use ($data, $drawId) {
            foreach ($data as $result) {
                $type = $this->key_map[$result[0]];
                array_shift($result);

                $resultType = ResultType::whereCode($type)->first();

                if (str_starts_with($result[0], 'x')) {
                    throw new XFoundException(results: $data);
                }

                //store lottery results
                if ($resultType) {
                    $resultType->results()->create([
                        'numbers' => $result,
                        'price' => $resultType->price,
                        'draw_id' => $drawId,
                    ]);
                }
            }

            Draw::generateWinners($drawId);
        });

        return null;
    }

    private function warn($title, $message)
    {
        Notification::send(Admin::first(), new LotteryResultFailed($title, $message));
    }
}
