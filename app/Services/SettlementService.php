<?php

namespace App\Services;

use App\Enums\TransactionType;
use App\Models\Agent;
use App\Models\Bet;
use App\Models\DepositWithdraw;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SettlementService
{
    public function getChildRelationship(Model $record): string
    {
        return match (get_class($record)) {
            Senior::class => 'masters',
            Master::class => 'agents',
            Agent::class => 'users',
        };
    }

    public function getBoughtTransactionType(Model $record): string
    {
        return match (get_class($record)) {
            Senior::class => TransactionType::SeniorDeposit->value,
            Master::class => TransactionType::MasterDeposit->value,
            Agent::class => TransactionType::AgentDeposit->value,
            User::class => TransactionType::UserDeposit->value,
        };
    }

    public function getWithdrawnTransactionType(Model $record): string
    {
        return match (get_class($record)) {
            Senior::class => TransactionType::SeniorWithdraw->value,
            Master::class => TransactionType::MasterWithdraw->value,
            Agent::class => TransactionType::AgentWithdraw->value,
            User::class => TransactionType::UserWithdraw->value,
        };
    }

    public function getPaidTransactionType(Model $record): string
    {
        return match (get_class($record)) {
            Senior::class => TransactionType::MasterDeposit->value,
            Master::class => TransactionType::AgentDeposit->value,
            Agent::class => TransactionType::UserDeposit->value,
        };
    }

    public function getRegainedTransactionType(Model $record): string
    {
        return match (get_class($record)) {
            Senior::class => TransactionType::MasterWithdraw->value,
            Master::class => TransactionType::AgentWithdraw->value,
            Agent::class => TransactionType::UserWithdraw->value,
        };
    }

    public function getSettlementClosure(Builder $query, Model $record)
    {
        $latestSettlement = $record->settlements()->latest()->first();

        if ($latestSettlement) {
            $query->where('created_at', '>', $latestSettlement->created_at);
        }
    }

    public function getBoughtPoint(Model $record): int
    {
        return $record->depositWithdraws()->where('transaction_type', $this->getBoughtTransactionType($record))
            ->where(fn ($query) => $this->getSettlementClosure($query, $record))
            ->sum('point');
    }

    public function getRewithdrawnPoint(Model $record): int
    {
        return $record->depositWithdraws()->where('transaction_type', $this->getWithdrawnTransactionType($record))
            ->where(fn ($query) => $this->getSettlementClosure($query, $record))
            ->sum('point');
    }

    public function getRegainedPoint(Model $record): int
    {
        $childRelation = $this->getChildRelationship($record);
        $childIds = $record->$childRelation()->pluck('id')->toArray();

        return DepositWithdraw::whereIn('user_id', $childIds)
            ->where('transaction_type', $this->getRegainedTransactionType($record))
            ->where(fn ($query) => $this->getSettlementClosure($query, $record))
            ->sum('point');
    }

    public function getPaidPoint(Model $record): int
    {
        $childRelation = $this->getChildRelationship($record);
        $childIds = $record->$childRelation()->pluck('id')->toArray();

        return DepositWithdraw::whereIn('user_id', $childIds)
            ->where('transaction_type', $this->getPaidTransactionType($record))
            ->where(fn ($query) => $this->getSettlementClosure($query, $record))
            ->sum('point');
    }

    public function getUsedPoint(Model $record): int
    {
        $userIds = $record->users()->pluck('users.id')->toArray();

        return Bet::whereIn('user_id', $userIds)
            ->where(fn ($query) => $this->getSettlementClosure($query, $record))
            ->sum('point');
    }

    public function getPaidAbove(Model $record): int
    {
        return $record->profit + $this->getNetPaidAbove($record);
    }

    public function getNetPaidAbove(Model $record): int
    {
        $usedPoint = $this->getUsedPoint($record);

        return $record->point_rate * $usedPoint;
    }
}
