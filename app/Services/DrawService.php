<?php

namespace App\Services;

use App\Enums\ResultTypeCode;
use App\Events\LotteryWon;
use App\Models\Agent;
use App\Models\Bet;
use App\Models\Master;
use App\Models\Result;
use App\Models\Senior;
use App\Models\User;
use App\Models\UserResult;
use App\Settings\GeneralSettings;
use App\Settings\TicketSettings;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DrawService
{
    private string $first;

    public function getPendingDrawId(?string $date = null): string
    {
        $pendingDate = $date ? Carbon::parse($date) : now();
        $monthDay = $pendingDate->format('m-d');
        $lotteryDates = app(GeneralSettings::class)->lottery_dates;

        foreach ($lotteryDates as $drawId) {
            if ($monthDay <= $drawId) {
                return $pendingDate->year.'-'.$drawId;
            }
        }

        // If the pending date is after the last draw, return the first draw of the next year
        return ($pendingDate->year + 1).'-01-17';
    }

    public function getPreviousDrawId(?string $date = null): string
    {
        $currentDate = $date ? Carbon::parse($date) : now();
        $monthDay = $currentDate->format('m-d');
        $lotteryDates = app(GeneralSettings::class)->lottery_dates;

        foreach (array_reverse($lotteryDates) as $drawId) {
            if ($monthDay >= $drawId) {
                return $currentDate->year.'-'.$drawId;
            }
        }

        // If the current date is before the first draw, return the last draw of the previous year
        return $currentDate->subYear()->year.'-12-16';
    }

    public function getPreviousDrawIds(int $count = 1, ?string $date = null, bool $isOptions = false): array
    {
        $date = $date ?? now()->format('Y-m-d');
        $currentLastDraw = $this->getPreviousDrawId($date);

        if ($isOptions) {
            $drawDates[$currentLastDraw] = $currentLastDraw;
        } else {
            $drawDates = [$currentLastDraw];
        }

        while ($count > 1) {
            $date = Carbon::parse($currentLastDraw)->subDay();

            $currentLastDraw = $this->getPreviousDrawId($date->format('Y-m-d'));

            if ($isOptions) {
                $drawDates[$currentLastDraw] = $currentLastDraw;
            } else {
                array_push($drawDates, $currentLastDraw);
            }

            $count--;
        }

        return $drawDates;
    }

    public function isLotteryDate(?string $date = null): bool
    {
        $date = $date ?? now()->format('Y-m-d');
        $lotteryDates = app(GeneralSettings::class)->lottery_dates;

        return in_array(Carbon::parse($date)->format('m-d'), $lotteryDates);
    }

    public function getDrawIdsByMonth(?string $date = null): array
    {
        $date = $date ?? now()->format('Y-m-d');
        $lotteryDates = app(GeneralSettings::class)->lottery_dates;

        $drawIds = [];
        foreach ($lotteryDates as $drawId) {
            $drawMonth = explode('-', $drawId)[0];
            if (Carbon::parse($date)->format('m') == $drawMonth) {
                array_push($drawIds, Carbon::parse($date)->year.'-'.$drawId);
            }
        }

        return $drawIds;
    }

    public function findWonNumbers(ResultTypeCode $resultTypeCode, array $resultTickets, array $tickets): array
    {
        $wonNumbers = [];
        foreach ($tickets as $ticketNumber) {
            if ($resultTypeCode === ResultTypeCode::FirstThree) {
                $firstThree = substr($ticketNumber, 0, 3);
                if (in_array($firstThree, $resultTickets)) {
                    array_push($wonNumbers, $ticketNumber);
                }

                continue;
            }

            if ($resultTypeCode === ResultTypeCode::LastThree) {
                $lastThree = substr($ticketNumber, -3);
                if (in_array($lastThree, $resultTickets)) {
                    array_push($wonNumbers, $ticketNumber);
                }

                continue;
            }

            if ($resultTypeCode === ResultTypeCode::LastTwo) {
                $lastTwo = substr($ticketNumber, -2);
                if (in_array($lastTwo, $resultTickets)) {
                    array_push($wonNumbers, $ticketNumber);
                }

                continue;
            }

            if (in_array($resultTypeCode, [
                ResultTypeCode::First,
                ResultTypeCode::Second,
                ResultTypeCode::Third,
                ResultTypeCode::Forth,
                ResultTypeCode::Fifth,
                ResultTypeCode::ClosetFirst]
            )) {
                if (in_array($ticketNumber, $resultTickets)) {
                    array_push($wonNumbers, $ticketNumber);
                }

                continue;
            }
        }

        return $wonNumbers;
    }

    public function generateWinners(string $drawId): void
    {
        $drawId = $drawId ?? now()->format('Y-m-d');
        $results = Result::where('draw_id', $drawId)->get();

        if (! count($results)) {
            return;
        }

        $users = DB::table('bets')
            ->select('user_id', DB::raw('JSON_ARRAYAGG(tickets) AS tickets'))
            ->where('draw_id', $drawId)
            ->groupBy('user_id')
            ->get();

        if (! count($users)) {
            return;
        }

        DB::table('user_result')->where('draw_id', $drawId)->delete();

        // calculate result by users' tickets
        foreach ($users as $bet) {
            $user = User::find($bet->user_id);
            $tickets = array_merge(...json_decode($bet->tickets, true));

            foreach ($results as $result) {
                $resultType = $result->resultType;
                $wonNumbers = $this->findWonNumbers($resultType->code, $result->numbers, $tickets);

                if (! count($wonNumbers)) {
                    continue;
                }

                app()->setLocale($user->language->value);

                $user->results()->attach($result->uuid, [
                    'tickets' => json_encode($wonNumbers),
                    'draw_id' => $drawId,
                ]);

                LotteryWon::dispatch($user, [
                    'code' => $resultType->code->value,
                    'price' => $result->price,
                    'numbers' => $wonNumbers,
                ]);
            }

        }
    }

    public function isPurchasedTicket(User $user, string $ticketNumber, ?string $drawId = null): bool
    {
        $drawId = $drawId ?? now()->format('Y-m-d');

        if ($user->bets()->where('draw_id', $drawId)->whereJsonContains('tickets', $ticketNumber)->exists()) {
            return true;
        }

        return false;
    }

    public function isPurchaseLimitReached(string $ticketNumber, ?string $drawId = null): bool
    {
        $drawId = $drawId ?? now()->format('Y-m-d');
        $betsCount = Bet::where('draw_id', $drawId)->whereJsonContains('tickets', $ticketNumber)->count();

        if ($betsCount >= app(TicketSettings::class)->purchase_limit_per_ticket) {
            return true;
        }

        return false;
    }

    public function isRestrictedTicket(string $ticketNumber): bool
    {
        if (in_array($ticketNumber, app(TicketSettings::class)->restricted_tickets)) {
            return true;
        }

        return false;
    }

    public function getFakeResultsByResultType(ResultTypeCode $resultTypeCode): array
    {
        if ($resultTypeCode == ResultTypeCode::First) {
            $this->first = get_random_digit();

            return [$this->first];
        }

        if ($resultTypeCode == ResultTypeCode::ClosetFirst) {
            $upNumber = (int) ltrim($this->first, '0') + 1;
            $downNumber = (int) ltrim($this->first, '0') - 1;
            $upNumber = str_pad((string) $upNumber, 6, '0', STR_PAD_LEFT);
            $downNumber = str_pad((string) $downNumber, 6, '0', STR_PAD_LEFT);

            return [$upNumber, $downNumber];
        }

        $resultCount = $resultTypeCode->getResultCount();

        $tickets = [];
        while ($resultCount > 0) {
            $tickets[] = get_random_digit($resultTypeCode->getDigitCount());
            $resultCount--;
        }

        return $tickets;
    }

    public function getRandomTickets(int $count, bool $includeRestricted = false, bool $includePurchased = false, ?string $drawId = null): array
    {
        $drawId = $drawId ?? $this->getPendingDrawId();
        $tickets = [];

        while ($count > 0) {
            $ticket = $this->getRandomTicket($includeRestricted, $includePurchased, $drawId);

            $tickets[] = $ticket;
            $count--;
        }

        return $tickets;
    }

    public function getRandomTicket(bool $includeRestricted = false, bool $includePurchased = false, ?string $drawId = null): string
    {
        $ticket = str_pad((string) mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
        $retries = 0;

        if ($retries >= 10) {
            Log::info('Generating tickets..., Tried 10 times and not found available number.');

            return $ticket;
        }

        if (! $includeRestricted && $this->isRestrictedTicket($ticket)) {
            $retries++;
            Log::info('Generating tickets..., Restricted ticket '.$ticket);

            return $this->getRandomTicket($includeRestricted, $includePurchased, $drawId);
        }

        if (! $includePurchased && $this->isPurchaseLimitReached($ticket, $drawId)) {
            $retries++;
            Log::info('Generating tickets..., Purchase limit reached for ticket '.$ticket);

            return $this->getRandomTicket($includeRestricted, $includePurchased, $drawId);
        }

        return $ticket;
    }

    public function getTotalWonLottery(Senior|Master|Agent $user): int
    {
        $userIds = $user->users()->pluck('users.id')->toArray();
        $previousDrawId = $this->getPreviousDrawId(now()->subDays(5));

        $userResults = UserResult::whereIn('user_id', $userIds)
            ->where('draw_id', $previousDrawId)
            ->get();

        $prices = 0;

        foreach ($userResults as $userResult) {
            $result = $userResult->result;

            $prices += count($userResult->tickets) * $result->price;
        }

        return $prices;
    }
}
