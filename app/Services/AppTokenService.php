<?php

namespace App\Services;

use App\Enums\AppTokenAction;
use App\Exceptions\InvalidAppTokenException;
use App\Models\Agent;
use App\Models\Master;
use App\Models\Senior;
use App\Models\User;
use Illuminate\Support\Str;

class AppTokenService
{
    public function __construct(private Agent|Master|Senior|User $user)
    {
    }

    public function generate(AppTokenAction $action): string
    {
        $token = Str::random(rand(180, 255));

        $this->deleteTokens();

        $this->user->appTokens()->create([
            'token' => $token,
            'expired_at' => now()->addMinutes($action->getLifetime()),
            'action' => $action,
        ]);

        return $token;
    }

    public function verify(AppTokenAction $action, string $token)
    {
        $appToken = $this->user->appTokens()->where('token', $token)->where('expired_at', '>', now())->where('action', $action->value)->first();

        if (! $appToken) {
            throw new InvalidAppTokenException();
        }
    }

    public function deleteTokens()
    {
        $this->user->appTokens()->delete();
    }
}
