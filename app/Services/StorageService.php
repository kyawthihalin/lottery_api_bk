<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class StorageService
{
    public static function getUrl(?string $path): ?string
    {
        if (! $path) {
            return null;
        }

        if (! Storage::exists($path)) {
            return null;
        }

        return Storage::url($path);
    }

    public static function upload(UploadedFile $file, string $path, string $fileName = ''): string
    {
        $fileName = (bool) $fileName ? $fileName : date('mdYHis').uniqid().$file->getClientOriginalName();

        $file->storeAs($path, $fileName);

        return $path.'/'.$fileName;
    }

    public static function delete(string $path)
    {
        Storage::delete($path);
    }
}
