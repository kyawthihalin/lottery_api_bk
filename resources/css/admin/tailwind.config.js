import preset from "../../../vendor/filament/filament/tailwind.config.preset";

/** @type {import('tailwindcss').Config} */
export default {
    presets: [preset],
    content: [
        "./app/Filament/**/*.php",
        "./resources/views/**/*.blade.php",
        "./vendor/filament/**/*.blade.php",
        './vendor/bezhansalleh/filament-language-switch/resources/views/language-switch.blade.php',
    ],
    plugins: [],
};
