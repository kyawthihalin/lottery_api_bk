<x-layout>
    <x-home.banner></x-home.banner>

    <x-home.count-down></x-home.count-down>

    <x-home.live-sale></x-home.live-sale>

    <x-home.block.index></x-home.block.index>

    <x-home.result></x-home.result>

    <x-home.new-on-ticket></x-home.new-on-ticket>

    <x-home.youtube></x-home.youtube>
</x-layout>
