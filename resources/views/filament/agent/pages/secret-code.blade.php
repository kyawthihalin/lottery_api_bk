<div class="flex justify-center h-screen">
    <x-filament-panels::page class="self-center">
        <x-filament-panels::form wire:submit="submit" class="w-full">
            {{ $this->form }}
            <x-filament-panels::form.actions :actions="$this->getCachedFormActions()" :full-width="$this->hasFullWidthFormActions()" />
        </x-filament-panels::form>
    </x-filament-panels::page>
</div>
