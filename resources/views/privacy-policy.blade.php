<x-layout>
    <main class="w-[85%] lg:w-[60%] mx-auto my-10 lg:my-20">
        <p class="text-base lg:text-lg font-semibold">Privacy Policy</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">This Privacy Policy describes our policies and procedures on the
            collection, use and
            disclosure of your information when you use the Service and tells you about your privacy rights and how
            the law protects you.</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">We use your Personal data to provide and improve the service. By
            using the service, you
            agree to the collection and use of information in accordance with this Privacy Policy.</p>
        <p class="mt-8 lg:mt-12 text-base lg:text-lg font-semibold">Information Which we collects :</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">For your Service only we collects your personal information as per
            we need. Only for
            provide and the improvement the App services we are committed to collecting the necessary information.
        </p>
        <p class="mt-8 lg:mt-12 text-base lg:text-lg font-semibold">Personal Information :</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">We do not collect Personal Information. Personal Information is
            information that
            identifies you or another person, such as your first name and last name, physical addresses, email
            addresses, telephone, fax, SSN, information stored within your device.</p>
        <p class="mt-8 lg:mt-12 text-base lg:text-lg font-semibold">Links to Other Sites :</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">This Service may contain links to other sites. If You click and
            third-party link, you
            will be directed to that site. Note that these external sites are not operated by Us. Therefore, we
            strongly advise you to review the Privacy Policy of these websites. We have no control over and assume
            no responsibility for the content, privacy policies, or practices of any third-party sites or services.
        </p>
        <p class="mt-8 lg:mt-12 text-base lg:text-lg font-semibold">Children’s Privacy :</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">These Services do not address anyone under the age of 17. We do not
            knowingly collect
            personally identifiable information from children under 17. In the case we discover that a child under
            17 has provided us with personal information, we immediately delete this from our servers. If you are a
            parent or guardian and you are aware that your child has provided us with personal information, please
            contact us so that we will be able to do necessary actions.</p>
        <p class="mt-8 lg:mt-12 text-base lg:text-lg font-semibold">Changes to This Privacy Policy :</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">We may update Our Privacy Policy from time to time. Thus, you are
            advised to review
            this page periodically for any changes. Will notify you of any change by posting the new Privacy Policy
            on this page. These changes are effective immediately after they are posted on this page.</p>
        <p class="mt-8 lg:mt-12 text-base lg:text-lg font-semibold">Contact Us :</p>
        <p class="mt-4 lg:mt-5 text:sm lg:text:base">If you have any questions or suggestions about our Privacy Policy,
            do not hesitate to
            contact us at myanthai.eticket@gmail.com</p>
    </main>
</x-layout>
