@php
    use Carbon\Carbon;
    use App\Facades\Draw;
    use App\Services\StorageService;
    use App\Settings\ImageSettings;
    use App\Models\TicketGroup;
    use App\Settings\TicketSettings;

    $pendingDraw = Carbon::parse(Draw::getPendingDrawId())->format('d M Y');
    $ticketGroups = TicketGroup::orderBy('draw_id', 'desc')->take(5)->get();
    $purchaseLimit = app(TicketSettings::class)->tickets_limit_per_purchase;
    $bundleTickets = $ticketGroups
        ->map(function ($group) {
            return array_map(function ($number) {
                return [
                    'value' => array_map(function ($digit) {
                        return [
                            'digit' => $digit,
                            'text' => get_string_by_number($digit),
                        ];
                    }, str_split((string) $number)),
                    'checked' => false,
                ];
            }, $group->numbers);
        })
        ->all();
@endphp

<x-layout>
    <div x-data="{
        bundleTickets: [],
        init() {
            this.bundleTickets = {{ json_encode($bundleTickets) }};
        },
        generateValue: function(digits) {
            return digits.map((digit) => {
                return {
                    digit: digit,
                    text: this.getStringByNumber(digit)
                };
            });
        },
        getStringByNumber: function(number) {
            switch (number) {
                case 0:
                    return 'ZER';
                case 1:
                    return 'ONE';
                case 2:
                    return 'TWO';
                case 3:
                    return 'THR';
                case 4:
                    return 'FOR';
                case 5:
                    return 'FIV';
                case 6:
                    return 'SIX';
                case 7:
                    return 'SEV';
                case 8:
                    return 'EIG';
                case 9:
                    return 'NIN';
                default:
                    return '';
            }
        },
        carts: [],
        updateToCart: function(bundleIndex, ticketIndex) {
            this.bundleTickets[bundleIndex][ticketIndex].checked = !this.bundleTickets[bundleIndex][ticketIndex].checked;
    
            this.bundleTickets.forEach((tickets, bundleIndex) => {
                tickets.forEach((ticket, ticketIndex) => {
                    if (ticket.checked && !this.carts.includes(ticket.value)) {
                        this.carts.push(ticket.value);
                    } else {
                        this.carts = this.carts.filter((cart, cartIndex) => {
                            return !(cart == ticket.value && !ticket.checked);
                        });
                    }
                });
            });
        },
        removeCart: function(index) {
            this.bundleTickets.forEach((tickets, bundleIndex) => {
                tickets.forEach((ticket, ticketIndex) => {
                    if (JSON.stringify(ticket.value) === JSON.stringify(this.carts[index])) {
                        this.bundleTickets[bundleIndex][ticketIndex].checked = false;
                    }
                });
            });
    
            this.carts.splice(index, 1);
        }
    }" x-init="init()">
        <div class="swiper liveSaleDetail mx-auto py-10">
            <div class="swiper-wrapper">
                <template x-for="(tickets, bundleIndex) in bundleTickets" :key="bundleIndex">
                    <div class="swiper-slide bg-center bg-cover w-[300px] xs:w-[522px] h-[500px] xs:h-[800px]">
                        <div
                            class="overflow-x-clip flex justify-center items-center flex-col relative top-[18px] bg-white p-3 rounded">
                            <template x-for="(ticket, ticketIndex) in tickets" :key="ticketIndex">
                                <div style="background-image: url('{{ StorageService::getUrl(app(ImageSettings::class)->ticket_background) }}')"
                                    class="bg-cover flex-none relative w-[278px] xs:w-[495px]"
                                    :class="{
                                        'h-[139px] xs:h-[247px]': ticketIndex == 9,
                                        'h-[32px] xs:h-[53px]': ticketIndex !=
                                            9
                                    }">
                                    <input @click="updateToCart(bundleIndex, ticketIndex)" type="checkbox"
                                        class="absolute top-3.5 xs:top-7 right-1.5 xs:right-2.5 cursor-pointer w-2.5 h-2.5 xs:w-4 xs:h-4"
                                        :checked="ticket.checked"
                                        x-bind:disabled="carts.length == {{ $purchaseLimit }}" />

                                    <span
                                        class="w-[122px] xs:w-[210px] absolute top-[8px] xs:top-[12px] left-[130px] xs:left-[236px] flex justify-evenly">
                                        <template x-for="(number, key) in ticket.value" :key="key">
                                            <div class="font-['Octin'] flex flex-col items-center">
                                                <span class="text-md xs:text-xl" x-text="number.digit ?? '-'"></span>
                                                <span class="hidden xs:block text-[10px]" x-text="number?.text"></span>
                                            </div>
                                        </template>
                                    </span>
                                    <template x-if="ticketIndex == 9">
                                        <span
                                            class="font-['Octin'] absolute top-[42%] right-[20%] text-xs sm:text-xl text-gray-800">
                                            {{ $pendingDraw }}
                                        </span>
                                    </template>
                                </div>
                            </template>
                        </div>
                    </div>
                </template>
            </div>
        </div>

        <template x-if="carts.length > 0">
            <div class="flex justify-center font-semibold mx-5">
                <div class="flex flex-col w-[400px]">
                    <div class="flex justify-between">
                        <div class="text-info-900">{{ __('web.chosen_ticket') }}</div>
                        <div class="text-info-900" x-text="carts.length + ` {{ __('web.point') }}`"></div>
                    </div>
                    <template x-for="(ticket, index) in carts" :key="index">
                        <div class="flex justify-between align-center mt-5">
                            <span
                                class="w-[219px] h-[50px] bg-[#D0BCFC] text-center text-[29px] text-gray-950 cursor-pointer"
                                @click.prevent="open = true">
                                <div class="flex justify-evenly">
                                    <template x-for="(number, key) in ticket" :key="key">
                                        <div class="flex flex-col font-['Octin']">
                                            <span :class="{ 'text-2xl': number != null }"
                                                x-text="number?.digit ?? '-'"></span>
                                            <span class="text-xs" x-text="number?.text"></span>
                                        </div>
                                    </template>
                                </div>
                            </span>
                            <div @click="removeCart(index)" class="flex items-center">
                                <i class="fa-solid fa-xmark text-info-900 cursor-pointer"></i>
                            </div>
                        </div>
                    </template>

                    <form method="POST" action="{{ route('go-to-cart') }}">
                        @csrf
                        <input type="hidden" name="carts" x-bind:value="JSON.stringify(carts)">
                        <div class="my-10">
                            <x-buttons.outline-button>{{ __('web.purchase_ticket') }}</x-buttons.outline-button>
                        </div>
                    </form>
                </div>
            </div>
        </template>
    </div>
</x-layout>

<script>
    window.onbeforeunload = function() {
        var checkboxes = document.querySelectorAll('input[type=checkbox]');
        checkboxes.forEach(function(checkbox) {
            checkbox.checked = false;
        });
    };
</script>
