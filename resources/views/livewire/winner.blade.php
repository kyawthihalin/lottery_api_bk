<div class="mx-20 max-sm:mx-3  my-10 max-sm:my-3">

    <div
        class="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700 ">
        <ul class="flex flex-wrap -mb-px">
            @foreach($filterDates as  $key => $filterDate )
                <li class="me-2">
                    <a href="#" onclick="showTab('tab{{$key}}')" id="tab{{$key}}Link"
                        class=" tab-link @if($key == 0 ) activeTabLink @endif  inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300"
                        aria-current="page">
                        {{$filterDate}}</a>
                </li>
            @endforeach
        </ul>
    </div>


    @foreach($filterDates as  $key => $filterDate )
        <div id="tab{{$key}}" class="tab-content @if($key == 0) active @endif">

            <div class="winner-prize-total mt-4 ">
                <div class="text-base max-sm:text-sm">{{ \Carbon\Carbon::parse($filterDate)->format('D, d F Y') }}</div>
                <div><i class="fa-solid fa-award mr-2 inline text-base max-sm:text-sm bg-info-600 text-white px-[6px] py-[4px]  rounded-full "></i> <span class="text-lg max-sm:text-sm">{{ count($winnerUsers[$filterDate]) }}</span>
                </div>
            </div>

            <x-winner.winner-price-section prizeType="first" prizeText="1-st Prize" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="closest_first" prizeText="Up/Down Prize" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="second"  prizeText="2-nd Prize" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="third" prizeText="3-rd Prize" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="forth" prizeText="4-th Prize" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="fifth" prizeText="5-th Prize" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="first_three" prizeText="First 3 Digits" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="last_three" prizeText="Last 3 Digits" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
            <x-winner.winner-price-section prizeType="last_two" prizeText="Last 2 Digits" :filterDate="$filterDate" :winnerUsers="$winnerUsers"/>
        </div>
    @endforeach
</div>
