@php
    use App\Enums\ResultTypeCode;
@endphp

<div class="max-sm:mx-1 mx-10  max-sm:my-1 my-10 ">
    <x-result.check-result :changeType="$changeType" :winOrLose="$winOrLose" :winAmount="$winAmount" :singleWinNumber="$singleWinNumber" :multiInputs="$multiInputs" :multiWinNumber="$multiWinNumber"   />

    <div wire:ignore
        class="clear-both max-sm:text-xs text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
        <ul class="flex flex-wrap -mb-px">
            <li class="me-2">
                <a href="#" onclick="showTab('tab1')" wire:click="filterResult(1,true)" id="tab1Link"
                    class=" tab-link @if ($month_count == '') activeTabLink @endif inline-block py-4 max-sm:px-3 px-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300"
                    aria-current="page">
                    LATEST</a>
            </li>
            <li class="me-2">
                <a href="#" onclick="showTab('tab2')" wire:click="filterResult(1)" id="tab2Link"
                    class="tab-link @if ($month_count == 1) activeTabLink @endif inline-block py-4 max-sm:px-3 px-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">MONTH</a>
            </li>
            <li class="me-2">
                <a href="#" onclick="showTab('tab3')" wire:click="filterResult(3)" id="tab3Link"
                    class=" tab-link @if ($month_count == 3) activeTabLink @endif inline-block py-4 max-sm:px-3 px-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">
                    3 MONTHS</a>
            </li>
            <li class="me-2">
                <a href="#" onclick="showTab('tab4')" wire:click="filterResult(6)" id="tab4Link"
                    class=" tab-link @if ($month_count == 6) activeTabLink @endif inline-block py-4 max-sm:px-3 px-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300">
                    6 MONTHS</a>
            </li>
        </ul>
    </div>

    <div class="tab-content pt-0 active">
        @foreach ($results as $result)
            @foreach ($result as $rows)
                @foreach ($rows as $row)
                    <!-- First Prize -->
                    @if ($row->resultType->code == ResultTypeCode::First)
                    <div class="mt-12">
                        <h3 class="flex justify-end">{{ \Carbon\Carbon::parse($row->draw_id)->format('D, d F Y') }}</h3>
                    </div>
                    <div>
                        <div class="grid sm:grid-cols-3 bg-transparent mx-40 result-number-row">
                            <div></div>
                            <div>
                                <h5 class="prize-title"> {{ $row->resultType->name }}
                                    {{ number_format($row->price, 0, '.', ',') }} ฿</h5>
                                <div class="result-number-first text-center">{{ $row->numbers[0] }}</div>
                            </div>
                            <div></div>
                        </div>

                        <div class="grid sm:grid-cols-3 bg-transparent mx-40">
                            <div></div>
                            <img src="https://www.myanthai.com/images/union.png" style="width: 100%;height:auto;" />
                            <div></div>
                        </div>
                    </div>
                    @endif
                    <!-- Closet First Prize -->
                    @if ($row->resultType->code == ResultTypeCode::ClosetFirst)
                        <div class="grid sm:grid-cols-4  bg-transparent mb-12">
                            <div></div>
                            <div class="text-center">
                                <div class="my-2"> {{ $row->resultType->name }}
                                    {{ number_format($row->price, 0, '.', ',') }} ฿</div>
                                <div>
                                    @foreach (str_split($row->numbers[0]) as $digit)
                                        <span class="@if($digit != 1) result-number @else result-number-one @endif  result-number-yellow">{{ $digit }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="my-2"> {{ $row->resultType->name }}
                                    {{ number_format($row->price, 0, '.', ',') }} ฿</div>
                                <div>
                                    @foreach (str_split($row->numbers[1]) as $digit)
                                        <span class="@if($digit != 1) result-number @else result-number-one @endif  result-number-yellow">{{ $digit }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div></div>
                        </div>
                    @endif

                    <div class="space-y-6 mt-4">
                        <!-- Second Prize -->
                        @if ($row->resultType->code == ResultTypeCode::Second)
                            <x-result.prize-section :row="$row" numberColorClass="result-second-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-second-prize-title"  firstThree={{false}} lastThree={{false}} lastTwo={{false}} />
                        @endif

                        <!-- Third Prize -->
                        @if ($row->resultType->code == ResultTypeCode::Third)
                            <x-result.prize-section :row="$row" numberColorClass="result-third-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-third-prize-title"  firstThree={{false}} lastThree={{false}} lastTwo={{false}}  />
                        @endif

                        <!-- Fourth Prize -->
                        @if ($row->resultType->code == ResultTypeCode::Forth)
                            <x-result.prize-section :row="$row" numberColorClass="result-fourth-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-fourth-prize-title"  firstThree={{false}} lastThree={{false}} lastTwo={{false}}  />
                        @endif

                        <!-- Fifth Prize -->
                        @if ($row->resultType->code == ResultTypeCode::Fifth)
                            <x-result.prize-section :row="$row" numberColorClass="result-fifth-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-fifth-prize-title"  firstThree={{false}} lastThree={{false}} lastTwo={{false}}  />
                        @endif

                        <!-- First Three Prize -->
                        @if ($row->resultType->code == ResultTypeCode::FirstThree)
                            <x-result.prize-section :row="$row" numberColorClass="result-first-three-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-first-three-prize-title"  firstThree={{true}} lastThree={{false}} lastTwo={{false}}  />
                        @endif

                        <!-- Last Three Prize -->
                        @if ($row->resultType->code == ResultTypeCode::LastThree)
                            <x-result.prize-section :row="$row" numberColorClass="result-last-three-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-last-three-prize-title"  firstThree={{false}} lastThree={{true}} lastTwo={{false}}  />
                        @endif

                        <!-- Last Two Prize -->
                        @if ($row->resultType->code == ResultTypeCode::LastTwo)
                            <x-result.prize-section :row="$row"  numberColorClass="result-last-two-prize" :resultTypeCode="$row->resultType->code" backgroundColor="result-last-two-prize-title"  firstThree={{false}} lastThree={{false}} lastTwo={{true}}  />
                        @endif
                    </div>
                @endforeach
            @endforeach
        @endforeach
    </div>
</div>
