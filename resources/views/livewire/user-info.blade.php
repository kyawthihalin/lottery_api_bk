
<div wire:ignore>

    <x-user-info.alert />
    <x-user-info.show-ticket />
    <x-user-info.show-one-ticket />

    <div class="bg-info-600 relative w-full  flex flex-wrap justify-center items-center " style="height: 130px;">
        <img src="{{ auth('user')->user()->gender->value  == 'male' ? 'https://www.myanthai.com/images/avatar.svg' : 'https://www.myanthai.com/images/avatar-female.svg' }}" alt="" class="w-24 absolute" style="top:80px;">
   </div>

    <x-user-info.info-section />
    <x-user-info.label-section />

    <div>
        <div class="tab-content active  mx-14 max-sm:mx-[20px] "  id="tabContent1" >
            <x-user-info.history :bet_infos="$bet_infos" :depositWithdrawes="$depositWithdrawes" />
        </div>
        <div class="tab-content"  id="tabContent2" >
            <x-user-info.profile_update />
        </div>
        <div class="tab-content"  id="tabContent3">
            <x-user-info.password_update />
        </div>
        <div class="tab-content"  id="tabContent4">
            <x-user-info.bank-info :paymentTypes="$paymentTypes" :authUserPaymentTypes="$authUserPaymentTypes" :kbzPayName="$kbzPayName" :kbzAccountName="$kbzAccountName" :yomaAccountName="$yomaAccountName" :cbAccountName="$cbAccountName" :ayaAccountName="$ayaAccountName" :waveMoneyAccountName="$waveMoneyAccountName"/>
        </div>
    </div>
</div>

