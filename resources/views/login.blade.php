<x-layout>
    <section class="flex justify-center">
        <div class="max-xl:flex-auto flex-1">
            <div class="px-8 py-10 sm:py-20">
                <h2 class="font-bold text-2xl text-info-900 text-center"> {{ __('web.login') }}</h2>

                <form action="/login" method="post" class="flex flex-col items-center gap-5 mt-8">
                    @csrf
                    <x-form.error name="user_name" />
                    <x-form.error name="password" />
                    <x-form.error name="data.user_name" />
                    <x-form.error />

                    <x-form.input name="user_name" placeholder="Username" />

                    <div x-data="{ show: false }" class="relative w-full text-center">
                        <x-form.input action="show ? 'text' : 'password'" name="password" placeholder="Password" />

                        <x-form.password-eye />
                    </div>

                    <x-buttons.background-button style="w-1/4 max-lg:w-1/2 max-sm:w-full">
                        {{ __('web.login') }}
                    </x-buttons.background-button>
                </form>
                <div class="flex justify-center items-center">
                    <div class="w-1/4 max-lg:w-1/2 max-sm:w-full my-5 grid grid-cols-3 items-center text-gray-400">
                        <hr class="border-gray-400">
                        <p class="text-center text-sm">OR</p>
                        <hr class="border-gray-400">
                    </div>
                </div>

                <div class="flex justify-center">
                    <a href="{{ route('register') }}" class="contents">
                        <x-buttons.background-button style="w-1/4 max-lg:w-1/2 max-sm:w-full">
                            {{ __('web.create_account') }}
                        </x-buttons.background-button>
                    </a>
                </div>
            </div>
        </div>
    </section>
</x-layout>
