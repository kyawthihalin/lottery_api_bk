@php
    use Carbon\Carbon;
    use App\Facades\Draw;
    use App\Settings\ImageSettings;
    use App\Services\StorageService;

    $pendingDraw = Carbon::parse(Draw::getPendingDrawId())->format('d M Y');
@endphp

<x-layout>
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>

    <div class="min-h-[52vh] mx-5 my-10">
        <h1 class="text-center text-xl text-info-900 font-semibold mb-5">
            {{ __('web.pick_lottery') }}
        </h1>

        <div x-cloak id="basicModal" x-init="init()" @open-me="open=true" @close-me="open=false"
            x-data="{
                open: false,
                init() {
                    pageRefresh = localStorage.getItem('pageRefresh') ? localStorage.getItem('pageRefresh') : true;
                    localStorage.setItem('pageRefresh', pageRefresh);
                },
                loginDialog: false,
                generalText: JSON.parse(localStorage.getItem('generalText')) || '',
                warningDialog: JSON.parse(localStorage.getItem('warningDialog')) || false,
                successDialog: false,
                confirmDialog: false,
                numbers: Array(6).fill(null),
                isUpdateNumber: false,
                tickets: JSON.parse(localStorage.getItem('tickets')) || {{ isset($carts) ? json_encode($carts) : '[Array(6).fill(null)]' }},
                showTicketList: JSON.parse(localStorage.getItem('showTicketList')) || {{ isset($carts) ? 'true' : 'false' }},
                focus: 0,
                count: 0,
                ticketCount: 1,
                width: 'w-0',
                addTicket(tickets) {
                    if (this.tickets.some(ticket => tickets.includes(ticket))) {
                        this.warning('{{ __('web.duplicate_ticket') }}');
                        return;
                    }
            
                    this.showTicketList ? this.tickets = this.tickets.concat(tickets) : this.tickets = JSON.parse(JSON.stringify(tickets));
                    for (let ticket of this.tickets[0]) {
                        if (ticket === null) {
                            this.showTicketList = false;
                            this.warning('{{ __('web.must_choose_six') }}');
                            return;
                        } else {
                            this.showTicketList = true;
                            this.numbers = Array(6).fill(null);
                        }
                    }
                },
                removeTicket(index) {
                    if (this.tickets.length == 1) {
                        this.tickets[index] = this.tickets[index].map(() => null);
                        this.showTicketList = false;
                        return;
                    }
                    this.tickets.splice(index, 1);
                },
                addToCart() {
                    if (this.tickets.length + 1 > 10) {
                        this.warning('{{ __('web.limit_ticket') }}');
                        return;
                    }
            
                    if (this.count < 6 && this.showTicketList) {
                        this.warning('{{ __('web.must_choose_six') }}');
                        return;
                    }
            
                    this.addTicket([this.numbers]);
                    this.open = false;
                },
                updateFocus(number) { this.focus = number; },
                addNumber(number) {
                    this.numbers[this.focus] = {
                        digit: String(number),
                        text: this.getStringByNumber(number)
                    };
            
                    if (this.focus > 5 || this.focus < 0) {
                        return;
                    }
            
                    this.focus = this.focus < 5 ? this.focus + 1 : this.focus;
                    this.updateCount();
                },
                backSpace() {
                    this.numbers[this.focus] = null;
                    this.focus = this.focus > 0 ? this.focus - 1 : this.focus;
                    this.updateCount();
                },
                clearAll() {
                    this.numbers = Array(6).fill(null);
                    this.focus = 0;
                    this.updateCount();
                },
                shuffle() {
                    if (this.showTicketList && (this.tickets.length + this.ticketCount) > 10) {
                        this.warning('{{ __('web.limit_ticket') }}');
                        return;
                    }
            
                    if (this.ticketCount > 1) {
                        let tickets = [];
            
                        for (let i = 0; i < this.ticketCount; i++) {
                            tickets.push(this.generateRandomNumbers());
                        }
            
                        this.addTicket(tickets);
                        this.numbers = Array(6).fill(null);
                        this.open = false;
                        return;
                    }
            
                    this.numbers = this.generateRandomNumbers();
                    this.updateCount();
                },
                generateRandomNumbers() {
                    return this.numbers = this.numbers.map(number => {
                        let randomNum = Math.floor(Math.random() * 10);
                        return {
                            digit: String(randomNum),
                            text: this.getStringByNumber(randomNum)
                        };
                    });
                },
                updateCount() {
                    this.count = 0;
                    this.count = this.numbers.filter(number => number !== null).length;
            
                    if (this.count === 0) {
                        this.width = 'w-0';
                        return;
                    }
                    if (this.count === 1) {
                        this.width = 'w-1/6';
                    }
                    if (this.count === 2) {
                        this.width = 'w-2/6';
                    }
                    if (this.count === 3) {
                        this.width = 'w-3/6';
                    }
                    if (this.count === 4) {
                        this.width = 'w-4/6';
                    }
                    if (this.count === 5) {
                        this.width = 'w-5/6';
                    }
                    if (this.count === 6) {
                        this.width = 'w-full';
                    }
                },
                getStringByNumber(number) {
                    switch (number) {
                        case 0:
                            return 'ZER';
                        case 1:
                            return 'ONE';
                        case 2:
                            return 'TWO';
                        case 3:
                            return 'THR';
                        case 4:
                            return 'FOR';
                        case 5:
                            return 'FIV';
                        case 6:
                            return 'SIX';
                        case 7:
                            return 'SEV';
                        case 8:
                            return 'EIG';
                        case 9:
                            return 'NIN';
                        default:
                            return '';
                    }
                },
                openDialog() {
                    this.isUpdateNumber = false;
                    this.numbers = Array(6).fill(null);
                    this.updateCount();
                    this.focus = 0;
                    this.open = true;
                },
                currentTicketIndex: '',
                openDialogByNumber(ticket, focus, ticketIndex) {
                    this.isUpdateNumber = true;
                    this.currentTicketIndex = ticketIndex;
                    this.numbers = JSON.parse(JSON.stringify(ticket));
                    this.updateCount();
                    this.focus = focus;
                    this.open = true;
                },
                prepareTicket() {
                    if (this.count < 6) {
                        this.warning('{{ __('web.must_choose_six') }}');
                        return;
                    }
            
                    this.tickets[this.currentTicketIndex] = this.numbers;
                    this.open = false;
                },
                warning(text) {
                    this.generalText = text;
                    this.open = false;
                    this.warningDialog = true;
                },
                user_name: '',
                password: '',
                errors: [],
                token: '{{ csrf_token() }}',
                isAuthenticated: {{ auth()->guard('user')->check() ? 'true' : 'false' }},
                login() {
                    if (!this.isAuthenticated) {
                        this.errors = [];
                        fetch('/login', {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json', 'X-CSRF-TOKEN': this.token },
                            body: JSON.stringify({ user_name: this.user_name, password: this.password, ajax: true })
                        }).then(response => response.json()).then(result => {
                            if (result.status == 200) {
                                this.token = result.data.token;
                                this.isAuthenticated = true;
                                this.loginDialog = false;
                                this.purchase();
                            } else {
                                this.errors = result.data.errors;
                            }
                        }).catch(error => {
                            this.errors = [error];
                        });
                    }
                },
                refreshAndStore() {
                    localStorage.setItem('tickets', JSON.stringify(this.tickets));
                    localStorage.setItem('showTicketList', JSON.stringify(this.showTicketList));
                    localStorage.setItem('pageRefresh', JSON.stringify(false));
                    location.reload();
                },
                purchase() {
                    this.confirmDialog = false;
                    if (this.isAuthenticated) {
                        purchaseTickets = [];
                        this.errors = [];
                        for (let ticket of this.tickets) {
                            purchaseTickets.push(ticket.map(number => number.digit).join(''));
                        }
                        fetch('/purchase', {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json', 'X-CSRF-TOKEN': this.token },
                            body: JSON.stringify({ tickets: purchaseTickets })
                        }).then(response => response.json()).then(result => {
                            if (result.status == 200) {
                                this.confirmDialog = false;
                                this.generalText = result.message;
                                this.successDialog = true;
                            } else {
                                this.errors = result.data.errors;
                                this.warning(this.errors);
                            }
                        }).catch(error => {
                            this.errors = ['An error occurred'];
                        });
                    } else {
                        this.loginDialog = true;
                    }
                }
            }">
            <div class="flex overflow-auto" :class="tickets.length > 2 ? 'justify-start' : 'justify-center'">
                <template x-for="(ticket, index) in tickets" :key="index" class="overflow-auto">
                    <div class="flex-none relative w-[375px] h-[182px] sm:w-[500px] sm:h-[252px] bg-cover my-5"
                        :class="index == 0 ? '' : 'ml-5'"
                        style="background-image: url('{{ StorageService::getUrl(app(ImageSettings::class)->ticket_background) }}')">
                        <span
                            class="w-[180px] h-[54px] sm:w-[219px] absolute top-[4%] right-[7%] sm:top-[6%] sm:right-[9%] text-center text-[29px] text-gray-800"
                            :class="!showTicketList ? 'cursor-pointer' : ''"
                            @click.prevent="!showTicketList ? open = true : null">
                            <div class="flex justify-evenly">
                                <template x-for="(number, key) in ticket" :key="key">
                                    <div class="flex flex-col font-['Octin']">
                                        <span :class="number != null ? 'text-lg sm:text-2xl' : ''"
                                            x-text="number != null ? number.digit : '-'"></span>
                                        <span class="text-[8px] sm:text-xs" x-text="number?.text"></span>
                                    </div>
                                </template>
                            </div>
                        </span>
                        <span
                            class="font-['Octin'] absolute top-[42%] right-[20%] text-xs sm:text-xl text-gray-800">{{ $pendingDraw }}</span>
                    </div>
                </template>
            </div>

            {{-- Purchase Ticket Dialog --}}
            <x-dialog.dialog name="open">
                <div class="mx-10 sm:mx-24">
                    <div class="p-5 text-info-900 text-center text-xl bg-white">
                        {{ __('web.pick_number') }}
                    </div>

                    <div class="flex flex-col items-center">
                        <div class="flex justify-center text-3xl">
                            <template x-for="(number, index) in numbers" :key="index">
                                <div class="flex w-[80%]">
                                    <div @click="updateFocus(index)"
                                        :class="focus == index ? 'text-info-600' : 'text-gray-700'"
                                        class="w-10 h-10 mr-2 text-center">
                                        <span :class="number != null ? 'text-2xl' : ''" class="cursor-pointer"
                                            x-text="number != null ? number.digit : '--'"></span>
                                        <div class="text-xs cursor-pointer" x-text="number?.text"></div>
                                    </div>
                                </div>
                            </template>
                        </div>

                        <div class="text-gray-800 text-center text-3xl w-full sm:w-[400px] mt-5">
                            <div class="relative pt-1">
                                <div class="flex items-center justify-between mb-2">
                                    <div>
                                        <span
                                            class="inline-block px-2 py-1 text-sm text-white bg-info-600 rounded-full">
                                            Numbers
                                        </span>
                                    </div>
                                    <div class="text-right">
                                        <span class="inline-block text-lg text-gray-700"
                                            x-text="count + ' of 6'"></span>
                                    </div>
                                </div>
                                <div class="relative h-2 max-w-xl overflow-hidden rounded-full">
                                    <div class="absolute w-full h-full bg-gray-200"></div>
                                    <div id="bar"
                                        :class="`transition-all duration-500 ease-in-out h-full bg-info-600 relative ${width}`">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3">
                            <div class="flex">
                                @for ($i = 0; $i < 5; $i++)
                                    <x-buttons.number-button :value="$i" />
                                @endfor
                            </div>
                            <div class="flex">
                                @for ($i = 5; $i < 10; $i++)
                                    <x-buttons.number-button :value="$i" />
                                @endfor
                            </div>
                        </div>
                    </div>

                    <div class="items-center px-4 py-3 flex sm:flex-row-reverse justify-evenly">
                        <a @click="clearAll()" type="button"
                            class="text-info-900 underline cursor-pointer hover:text-info-950">
                            {{ __('web.clear_all') }}
                        </a>
                        <a @click="backSpace()" type="button"
                            class="text-info-900 underline cursor-pointer hover:text-info-950">
                            {{ __('web.back_space') }}
                        </a>
                    </div>

                    <template x-if="isUpdateNumber">
                        <div class="items-center justify-center py-3 px-6 flex flex-row-reverse">
                            <x-buttons.background-button action="prepareTicket()"
                                style="w-full">{{ __('web.save') }}</x-buttons.background-button>
                        </div>
                    </template>

                    <template x-if="!isUpdateNumber">
                        <div class="items-center justify-center py-3 px-6 flex flex-row-reverse">
                            <button @click="addToCart()" type="button"
                                class="inline-flex justify-center px-4 py-2 font-medium text-white bg-info-600 border border-transparent rounded-md shadow-sm hover:bg-info-700 ml-3 w-auto text-sm">
                                Add
                            </button>
                            <button @click="shuffle()" type="button"
                                class="inline-flex justify-center px-4 py-2 font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50mt-0 ml-3 w-auto text-sm">
                                <i class="fa-solid fa-shuffle text-sm"></i>
                            </button>
                            <span
                                class="flex w-min justify-center px-4 py-1 font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50mt-0 ml-3 text-sm">
                                <button class="text-gray-950 text-xl"
                                    @click="ticketCount > 1 ? ticketCount-- : 1">-</button>
                                <input :value="ticketCount" type="number"
                                    class="inline-block align-top text-medium font-bold leading p-0 min-w-[35px] text-center bg-transparent text-gray-700 border-0 w-[2%]"
                                    disabled>
                                <button class="text-gray-950 text-xl"
                                    @click="ticketCount < 10 ? ticketCount++ : 10">+</button>
                            </span>
                        </div>
                    </template>
                </div>
            </x-dialog.dialog>

            {{-- Warning Dialog --}}
            <x-dialog.dialog name="warningDialog">
                <div class="flex flex-col text-gray-800 mx-5 sm:mx-20">
                    <div class="flex flex-col justify-center items-center">
                        <i class="fa-solid fa-circle-exclamation text-5xl text-amber-300"></i>
                        <h5 class="my-3">Error</h5>
                        <p class="my3" x-text="generalText"></p>
                    </div>
                    <x-buttons.background-button action="warningDialog = false"
                        style="mt-10">{{ __('web.back_to_purchase') }}</x-buttons.background-button>
                </div>
            </x-dialog.dialog>

            {{-- Success Dialog --}}
            <x-dialog.dialog name="successDialog">
                <div class="flex flex-col text-gray-800 mx-5 sm:mx-20">
                    <div class="flex flex-col justify-center items-center">
                        <i class="fa-regular fa-circle-check text-5xl text-green-500"></i>
                        <h5 class="my-3">Success</h5>
                        <p class="my3" x-text="generalText"></p>
                    </div>
                    <x-buttons.background-button action="successDialog = false"
                        style="mt-10">{{ __('web.back_to_purchase') }}</x-buttons.background-button>
                </div>
            </x-dialog.dialog>

            {{-- Confirm Dialog --}}
            <x-dialog.dialog name="confirmDialog">
                <div class="flex flex-col text-gray-800 mx-5 sm:mx-20">
                    <div class="flex flex-col justify-center items-center text-info-900">
                        <i class="fa-solid fa-circle-exclamation text-5xl text-amber-300"></i>
                        <h2 class="text-xl font-semibold mt-5">{{ __('web.confirm_title') }}</h2>

                        @php
                            $confirmTextParts = explode(':point', __('web.confirm_text'));
                        @endphp
                        <p class="mt-5">
                            {!! $confirmTextParts[0] !!}
                            <span x-text="tickets.length"></span>
                            {!! $confirmTextParts[1] !!}
                        </p>

                        <template x-for="(ticket, index) in tickets" :key="index">
                            <div class="flex mt-5">
                                <template x-for="(number, key) in ticket" :key="key">
                                    <span x-text="number != null ? number.digit : '--'" class="text-md"></span>
                                </template>
                            </div>
                        </template>
                    </div>
                    <div class="flex mt-10">
                        <x-buttons.background-button action="confirmDialog = false"
                            style="w-full mr-2">{{ __('web.cancel') }}</x-buttons.background-button>
                        <x-buttons.background-button action="purchase"
                            style="w-full">{{ __('web.purchase') }}</x-buttons.background-button>

                    </div>
                </div>
            </x-dialog.dialog>

            {{-- Login Dialog --}}
            <x-dialog.dialog name="loginDialog">
                <div class="flex flex-col text-gray-800">
                    <div class="mx-5 sm:mx-16 mb-5 max-w-[16rem] w-[16rem]">
                        <h2 class="font-bold text-xl text-info-900 text-center mb-5"> {{ __('web.login') }}</h2>

                        <template x-for="(error, index) in errors" :key="index">
                            <p class="text-sm text-red-500 my-2 text-center" x-text="error"></p>
                        </template>

                        <x-form.input name="user_name" placeholder="Username" style="w-full" attribute="x-model" />

                        <div x-data="{ show: false }" class="relative mt-5">
                            <x-form.input action="show ? 'text' : 'password'" name="password" placeholder="Password"
                                style="w-full" attribute="x-model" />
                            <x-form.password-eye style="right-3" />
                        </div>

                        <x-buttons.background-button action="login"
                            style="w-full mt-5">{{ __('web.login') }}</x-buttons.background-button>
                    </div>
                </div>
            </x-dialog.dialog>

            <template x-if="showTicketList">
                <div class="flex justify-center font-semibold mt-10">
                    <div class="flex flex-col w-[400px]">
                        <div class="flex justify-between">
                            <div class="text-info-900">{{ __('web.chosen_ticket') }}</div>
                            <div class="text-info-900" x-text="tickets.length + ` {{ __('web.point') }}`"></div>
                        </div>
                        <template x-for="(ticket, index) in tickets" :key="index">
                            <div class="flex justify-between align-center mt-5">
                                <span class="w-[219px] h-[50px] bg-[#D0BCFC] text-center text-[29px] text-gray-800">
                                    <div class="flex justify-evenly">
                                        <template x-for="(number, key) in ticket" :key="key">
                                            <div @click="openDialogByNumber(ticket, key, index)"
                                                class="flex flex-col font-['Octin'] cursor-pointer">
                                                <span :class="number != null ? 'text-2xl' : ''"
                                                    x-text="number != null ? number.digit : '-'"></span>
                                                <span class="text-xs" x-text="number?.text"></span>
                                            </div>
                                        </template>
                                    </div>
                                </span>
                                <div @click="removeTicket(index)" class="flex items-center">
                                    <i class="fa-solid fa-xmark text-info-900 cursor-pointer"></i>
                                </div>
                            </div>
                        </template>

                        <div class="grid gap-4 mt-10">
                            <x-buttons.outline-button
                                action="confirmDialog = true">{{ __('web.purchase_ticket') }}</x-buttons.outline-button>
                            <x-buttons.background-button
                                action="openDialog">{{ __('web.choose_number') }}</x-buttons.background-button>
                        </div>
                    </div>
                </div>
            </template>
        </div>
    </div>
</x-layout>
