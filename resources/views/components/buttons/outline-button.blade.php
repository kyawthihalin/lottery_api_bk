@php
    $action = $action ?? null;
@endphp

<button type="submit" @if ($action) @click.prevent="{{ $action }}" @endif
    class="w-full flex justify-center py-5 bg-white text-info-500 border-2 border-info-500 rounded-lg hover:bg-info-500 hover:text-white duration-500">
    {{ $slot }}
</button>
