<div
    class="flex-1 px-2 py-2 justify-center flex items-center text-gray-600 text-lg sm:text-xl font-semibold cursor-pointer">
    <div @click="addNumber({{ $value }})"
        class="rounded-full h-10 w-10 sm:h-16 sm:w-16 flex items-center bg-gray-200 justify-center shadow-lg border-2 border-gray-400 hover:border-2 hover:border-info-500 hover:text-info-900 focus:outline-none">
        {{ $value }}</div>
</div>
