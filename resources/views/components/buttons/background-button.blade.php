@php
    $style = $style ?? '';
    $action = $action ?? null;
@endphp

<button type="submit"
    @if ($action) @click.prevent="{{ $action }}; if(JSON.parse(localStorage.getItem('pageRefresh')) && isAuthenticated) refreshAndStore();" @endif
    class="flex justify-center py-5 bg-info-500 rounded-lg text-white hover:scale-105 duration-300 {{ $style }}">
    {{ $slot }}
</button>
