@php
    use Filament\Facades\Filament;
@endphp
<span x-data="{ open: false }" class="inline-flex items-center text-sm gap-x-2 text-warning-400 dark:text-warning-400">
    <x-heroicon-o-eye x-on:click="open = ! open" class="inline w-4 h-4 cursor-pointer text-primary-500" />
    <span x-show="open">
        {{ number_format(Filament::auth()->user()->point) }}
    </span>
    <span x-show="!open">***</span>
    <x-gmdi-monetization-on-o class="inline w-5 h-5" />
</span>
