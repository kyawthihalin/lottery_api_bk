@props([
    'paymentTypes',
    'authUserPaymentTypes',
    'kbzPayName',
    'kbzAccountName',
    'yomaAccountName',
    'cbAccountName',
    'ayaAccountName',
    'waveMoneyAccountName',
])

<section class="flex">
    <div class="max-xl:flex-auto flex-1 p-4">
        <h1 class="text-xl text-center text-info-900 font-semibold">{{ __('web.bank_information') }}</h1>

        <form wire:submit.prevent="updateBankInfo">
            <div class="flex items-center flex-col  max-sm:mx-4">
                {{-- Account Type --}}
                <div class="flex items-center flex-col w-full " id="container">
                    {{-- Kpay --}}
                    @if ($kbzPayName != null)
                        <div class="w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full" id="KBZPay-main">
                            <div class="flex justify-between">
                                <div>
                                    <i class="fa-solid fa-building-columns"></i>
                                    <span>Kpay</span>
                                </div>
                                <div wire:click.prevent="deletePaymentAccount('KBZPay')"
                                    class="delete-payment cursor-pointer" id="KBZPay">
                                    <i class="fa-solid fa-circle-minus text-red-500"></i>
                                    <span class="text-sm">{{ __('web.remove_text') }}</span>
                                </div>
                            </div>
                            <div class="flex">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300 	"
                                    type="text" wire:model="kbzPayName">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="kbzPayNumber">

                            </div>
                        </div>
                    @endif

                    {{-- KBZ Account --}}
                    @if ($kbzAccountName != null)
                        <div class="w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full" id="KBZ-Account-main">
                            <div class="flex justify-between">
                                <div>
                                    <i class="fa-solid fa-building-columns"></i>
                                    <span>KBZ Account</span>
                                </div>
                                <div wire:click.prevent="deletePaymentAccount('KBZ Account')"
                                    class="delete-payment cursor-pointer" id="KBZ-Account">
                                    <i class="fa-solid fa-circle-minus  text-red-500"></i>
                                    <span class="text-sm">{{ __('web.remove_text') }}</span>
                                </div>
                            </div>
                            <div class="flex">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="kbzAccountName">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300""
                                    type="text" wire:model="kbzAccountNumber">

                            </div>
                        </div>
                    @endif

                    {{-- Yoma Account --}}
                    @if ($yomaAccountName != null)
                        <div class="w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full" id="Yoma-Account-main">
                            <div class="flex justify-between">
                                <div>
                                    <i class="fa-solid fa-building-columns"></i>
                                    <span>Yoma Account</span>
                                </div>
                                <div wire:click.prevent="deletePaymentAccount('Yoma Account')"
                                    class="delete-payment cursor-pointer" id="Yoma-Account">
                                    <i class="fa-solid fa-circle-minus  text-red-500"></i>
                                    <span class="text-sm">{{ __('web.remove_text') }}</span>
                                </div>
                            </div>
                            <div class="flex">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="yomaAccountName">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="yomaAccountNumber">

                            </div>
                        </div>
                    @endif

                    {{-- CB Account --}}
                    @if ($cbAccountName != null)
                        <div class="w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full" id="CB-Account-main">
                            <div class="flex justify-between">
                                <div>
                                    <i class="fa-solid fa-building-columns"></i>
                                    <span>CB Account</span>
                                </div>
                                <div wire:click.prevent="deletePaymentAccount('CB Account')"
                                    class="delete-payment cursor-pointer" id="CB-Account">
                                    <i class="fa-solid fa-circle-minus  text-red-500"></i>
                                    <span class="text-sm">{{ __('web.remove_text') }}</span>
                                </div>
                            </div>
                            <div class="flex">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="cbAccountName">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="cdAccountNumber">

                            </div>
                        </div>
                    @endif

                    {{-- AYA Account --}}
                    @if ($ayaAccountName != null)
                        <div class="w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full" id="AYA-Account-main">
                            <div class="flex justify-between">
                                <div>
                                    <i class="fa-solid fa-building-columns"></i>
                                    <span>AYA Account</span>
                                </div>
                                <div wire:click.prevent="deletePaymentAccount('AYA Account')"
                                    class="delete-payment cursor-pointer" id="AYA-Account">
                                    <i class="fa-solid fa-circle-minus  text-red-500"></i>
                                    <span class="text-sm">{{ __('web.remove_text') }}</span>
                                </div>
                            </div>
                            <div class="flex">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="ayaAccountName">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="ayaAccountNumber">

                            </div>
                        </div>
                    @endif

                    {{-- WaveMoney --}}
                    @if ($waveMoneyAccountName != null)
                        <div class="w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full" id="WaveMoney-main">
                            <div class="flex justify-between">
                                <div>
                                    <i class="fa-solid fa-building-columns"></i>
                                    <span>WaveMoney</span>
                                </div>
                                <div wire:click.prevent="deletePaymentAccount('WaveMoney')"
                                    class="delete-payment cursor-pointer" id="WaveMoney">
                                    <i class="fa-solid fa-circle-minus  text-red-500"></i>
                                    <span class="text-sm">{{ __('web.remove_text') }}</span>
                                </div>
                            </div>
                            <div class="flex">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="waveMoneyAccountName">
                                <input
                                    class="w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-2 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
                                    type="text" wire:model="waveMoneyAccountNumber">

                            </div>
                        </div>
                    @endif
                </div>

                <div class=" flex  w-1/4  max-lg:w-1/2 max-sm:w-full ">
                    <select id="accountType"
                        class="w-3/4  p-4 rounded-lg border text-base max-sm:text-sm mt-5 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300">
                        <option value="null" selected>Choose Account Type</option>
                        @foreach ($paymentTypes as $paymentType)
                            <option value="{{ $paymentType->name }}"> {{ $paymentType->name }}</option>
                        @endforeach
                    </select>

                    <button type="button" onclick="increatePaymentType()"
                        class="border-2 border-info-600 text-info-600 w-1/4 mt-5   py-2 hover:scale-105 duration-300 rounded-lg ">
                        <span class="pr-[1px] text-base max-sm:text-sm ">{{ __('web.add_text') }}</span><i
                            class="fa-solid fa-plus text-base max-sm:text-sm"></i>
                    </button>
                </div>

                <x-buttons.background-button style="mt-5 w-1/4 max-lg:w-1/2 max-sm:w-full">
                    {{ __('web.comfirm_update_info') }}
                </x-buttons.background-button>
            </div>
        </form>
    </div>
</section>
