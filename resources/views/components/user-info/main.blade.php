<style>
    .tab-link {}

    .result-toggle-button {
        cursor: pointer;
    }

    .tab-link.activeTabLink {
        border-color: #365BEF;
    }

    .tab-content {
        display: none;
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 20px;
        padding-top: 0px;
    }

    .tab-content.active {
        display: block;
    }

    .result-list {
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.8s ease-in-out;
    }

    .result-list-show {
        max-height: 500px;
        transition: max-height 0.8s ease-in-out;
    }

    .in-left {
        -webkit-animation-name: fadeInLeft;
        -moz-animation-name: fadeInLeft;
        -o-animation-name: fadeInLeft;
        animation-name: fadeInLeft;
        -webkit-animation-fill-mode: both;
        -moz-animation-fill-mode: both;
        -o-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.7s;
        -moz-animation-duration: 0.7s;
        -o-animation-duration: 0.7s;
        animation-duration: 0.7s;
        -webkit-animation-delay: 0.7s;
        -moz-animation-delay: 0.7s;
        -o-animation-duration: 0.7s;
        animation-delay: 0.7s;
    }

    .input-select-box-shake {
        animation: shake 0.5s;
        border-color: red;
        color:red;
    }

    .input-shake {
        animation: shake 0.5s;
    }


  .watermark {
    position: relative;
    display: inline-block;
  }

  .watermark::after {
    content: "Your Watermark Text";
    position: absolute;
    bottom: 10px;
    right: 10px;
    color: rgba(255, 255, 255, 0.5); /* Adjust the color and opacity as needed */
    font-size: 16px; /* Adjust the font size as needed */
    transform: rotate(-45deg);
    pointer-events: none; /* Ensure the watermark text doesn't interfere with clicks */
  }

  .center-text {
    text-align: center;
    margin: 0 0; /* Centers the element horizontally */
}


    @keyframes shake {
        0% { transform: translateY(0); }
        20% { transform: translateY(-5pY); }
        40% { transform: translateY(5px); }
        60% { transform: translateY(-5px); }
        80% { transform: translateY(5px); }
        100% { transform: translateY(0); }
    }

</style>

@livewire('user-info')

<script>


    function showTab(tabId) {
        event.preventDefault();

        var tabsLink = document.querySelectorAll('.tab-link');

        tabsLink.forEach(function(tab) {
            tab.classList.remove('activeTabLink');
        });
        document.getElementById("tab"+tabId).classList.add('activeTabLink');

        var tabsLink = document.querySelectorAll('.tab-content');
        tabsLink.forEach(function(tab) {
            tab.classList.remove('active');
        });
        document.getElementById("tabContent"+tabId).classList.add('active');
    }



    document.addEventListener('click', (event) => {
        const result = event.target.closest('.result-toggle-button');
        if (result) {
            const showResult = result.nextElementSibling;

            showResult.classList.toggle('result-list-show');
            showResult.classList.toggle('in-left');

        }

        const deletePayment = event.target.closest('.delete-payment');
        if (deletePayment) {
            const hasId = deletePayment.id;
            const elementToRemove = document.getElementById(hasId+"-main");
            elementToRemove.remove();
        }

    });

    function convertToWord(digit) {
            const words = ["ZER", "ONE", "TWO", "THR", "FOU", "FIV", "SIX", "SEV", "EIG", "NIN"];
            return words[digit] || "";
    }

    function replaceSpacesWithHyphens(str) {
        return str.replace(/\s+/g, '-');
    }

    function getInputAccountName(id) {
        const idMapping = {
            'KBZPay': 'kbzPayName',
            'KBZ-Account': 'kbzAccountName',
            'Yoma-Account': 'yomaAccountName',
            'CB-Account': 'cbAccountName',
            'AYA-Account': 'ayaAccountName',
            'WaveMoney': 'waveMoneyAccountName'
        };

        return idMapping[id] || null;
    }

    function getInputAccountNumber(id) {
        const idMapping = {
            'KBZPay': 'kbzPayNumber',
            'KBZ-Account': 'kbzAccountNumber',
            'Yoma-Account': 'yomaAccountNumber',
            'CB-Account': 'cdAccountNumber',
            'AYA-Account': 'ayaAccountNumber',
            'WaveMoney': 'waveMoneyAccountNumber'
        };

        return idMapping[id] || null;
    }

    function increatePaymentType()
    {
        var selectElement = document.getElementById('accountType');
        var selectedValue = selectElement.value;

        var getSelectedId = replaceSpacesWithHyphens(selectElement.value);
        var checkElementExit = document.getElementById(getSelectedId);



        if(!checkElementExit && selectedValue != 'null' )
        {
             // Create the outer div with class "w-1/4 mt-4" and id "WaveMoney"
            var outerDiv = document.createElement('div');
            outerDiv.className = 'w-1/4 mt-4 max-lg:w-1/2 max-sm:w-full';
            outerDiv.id = getSelectedId+"-main";
            // outerDiv.id = getSelectedId;

            // Create the first inner div with class "flex justify-between"
            var firstInnerDiv = document.createElement('div');
            firstInnerDiv.className = 'flex justify-between';

            // Create the first div inside the first inner div with icon and text
            var iconDiv = document.createElement('div');
            var icon = document.createElement('i');
            icon.className = 'fa-solid fa-building-columns';
            var textSpan = document.createElement('span');
            textSpan.textContent = " "+selectedValue;
            iconDiv.appendChild(icon);
            iconDiv.appendChild(textSpan);

            // Create the second div inside the first inner div with icon and text
            var statusDiv = document.createElement('div');
            statusDiv.id = getSelectedId;
            statusDiv.className = 'delete-payment cursor-pointer';
            var statusIcon = document.createElement('i');
            statusIcon.className = 'fa-solid fa-circle-minus text-red-500';
            var statusSpan = document.createElement('span');
            statusSpan.className = 'text-sm';
            statusSpan.textContent = " "+ @json(trans('web.remove_text'));
            statusDiv.appendChild(statusIcon);
            statusDiv.appendChild(statusSpan);

            // Append the iconDiv and statusDiv to the firstInnerDiv
            firstInnerDiv.appendChild(iconDiv);
            firstInnerDiv.appendChild(statusDiv);

            // Create the second inner div with class "flex"
            var secondInnerDiv = document.createElement('div');
            secondInnerDiv.className = 'flex';

            // Create the first input element with class "w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-3 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300 mr-1"
            var input1 = document.createElement('input');
            input1.className = 'w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-3 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300';
            input1.type = 'text';
            input1.setAttribute('wire:model',getInputAccountName(getSelectedId));
            input1.setAttribute('placeholder', 'Account Number');

            // Create the second input element with class "w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-3 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300"
            var input2 = document.createElement('input');
            input2.className = 'w-1/2 p-4 rounded-lg border text-base max-sm:text-sm mt-3 mr-1 border-none focus:ring-2 focus:ring-info-300 transition duration-300';
            input2.type = 'text';
            input2.setAttribute('wire:model',getInputAccountNumber(getSelectedId));
            input2.setAttribute('placeholder', 'Name');

            // Append the input1 and input2 to the secondInnerDiv
            secondInnerDiv.appendChild(input1);
            secondInnerDiv.appendChild(input2);

            // Append the firstInnerDiv and secondInnerDiv to the outerDiv
            outerDiv.appendChild(firstInnerDiv);
            outerDiv.appendChild(secondInnerDiv);

            // Append the dynamically created element to the container div
            var container = document.getElementById('container');
            container.appendChild(outerDiv);
        }
        else
        {
            if(selectedValue == 'null')
            {
                var selectAccountType = document.getElementById("accountType");
                selectAccountType.classList.add("input-select-box-shake");
                selectAccountType.classList.remove("border-none");
                setTimeout(function() {
                    selectAccountType.classList.remove("input-select-box-shake");
                    selectAccountType.classList.add("border-none");
                }, 500);
            }

            if(checkElementExit)
            {
                var exitInput = document.getElementById(getSelectedId+"-main");
                exitInput.classList.add("input-shake");
                setTimeout(function() {
                    exitInput.classList.remove("input-shake");
                }, 500);
            }
        }
    }

    function downloadImage(imageWidth,imageHeight,ticketDate,ticketNumber)
    {
        var img = document.getElementById('imageToDownload');

        var canvas = document.createElement('canvas');
        canvas.width = imageWidth;
        canvas.height = imageHeight;

        var ctx = canvas.getContext('2d');
        var downloadImageName= "";
        downloadImageName = ticketDate.replace(/ /g, '_');

        //Background Image Style Property
        ctx.fillStyle = '#FFFFFF';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0);

        // ကံထူးရှင်ဖြစ်ပါစေ Style Property
        ctx.fillStyle = 'rgb(163 230 53';
        ctx.fillRect(900,400, 400, 70);
        ctx.font = '40px Arial';
        ctx.fillStyle = 'red';
        ctx.textAlign = 'center';
        ctx.fillText('ကံထူးရှင်ဖြစ်ပါစေ',1100, 450);

        // Ticket Date Style Property
        ctx.font = '50px Chakra Petch,sans-serif';
        ctx.fillStyle = 'black';
        ctx.textAlign = 'center';
        ctx.fillText(ticketDate,1100, 350);

        var x = 855;
        var y = 120; // number character y position
        ctx.font = '80px Chakra Petch,sans-serif';
        ctx.fillStyle = 'black';
        ctx.textAlign = 'center';
        var characters = ticketNumber;
        for (var i = 0; i < characters.length; i++) {
            downloadImageName = downloadImageName+"_"+characters[i];
            ctx.fillText(characters[i], x, y,60);
            var charWidth = ctx.measureText(characters[i]).width;
            x +=  100; // distance between character
        }

        var x = 855;
        var y = 170; // number text character y position
        ctx.font = '40px Chakra Petch,sans-serif';
        ctx.fillStyle = 'black';
        ctx.textAlign = 'center';
        var text = getWord(ticketNumber);
        var characters = text.split(' ');
        for (var i = 0; i < characters.length; i++) {
            ctx.fillText(characters[i], x, y,200);
            var charWidth = ctx.measureText(characters[i]).width;
            x += 100; // distance between character
        }

        var link = document.createElement('a');
        link.download = downloadImageName+'.jpg';
        link.href = canvas.toDataURL('image/jpeg', 1.0);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    function getWord(digits) {
        const digitWords = {
            0: "ZER",
            1: "ONE",
            2: "TWO",
            3: "THR",
            4: "FOU",
            5: "FIV",
            6: "SIX",
            7: "SEV",
            8: "EIG",
            9: "NIN"
        };

        let stringOfDigits = '';
        for (const digit of digits) {
            stringOfDigits += digitWords[digit] + ' ';
        }
        return stringOfDigits;
    }

</script>
