<div x-cloak id="basicModal" x-data="{
    open: @entangle('showMessage'),
    alert_title: @entangle('alert_title'),
    alert_message: @entangle('alert_message'),
    alert_type: @entangle('alert_type'),
}" @open-me="open=true" @close-me="open=false">
    <x-dialog.dialog name="open">
        <div class="flex flex-col text-gray-800 mx-5 sm:mx-20">
            <div class="flex flex-col justify-center items-center text-center">
                <template x-if="alert_type === 'success'">
                    <i class="fa-regular fa-circle-check text-5xl text-green-500"></i>
                </template>
                <template x-if="alert_type === 'error'">
                    <i class="fa-solid fa-circle-exclamation text-5xl text-amber-300"></i>
                </template>

                <h2 class="pt-6 text-xl" x-text="alert_title"></h2>

                <p class="my-3" x-text="alert_message"></p>
            </div>
            <x-buttons.background-button action="open = false"
                style="mt-5">{{ __('web.close') }}</x-buttons.background-button>
        </div>
    </x-dialog.dialog>
</div>
