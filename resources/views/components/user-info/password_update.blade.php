<section class="flex">
    <div class="max-xl:flex-auto flex-1 p-4">
        <h1 class="text-xl text-center text-info-900 font-semibold">{{ __('web.password_changing') }}</h1>

        <form wire:submit.prevent="updatePassword" class="flex flex-col gap-5 mt-6 items-center">
            {{-- old password --}}
            <div x-data="{ show: false }" class="relative w-full text-center">
                <x-form.input action="show ? 'text' : 'password'" name="old_password"
                    placeholder="{{ __('web.old_password') }}" />
                <x-form.password-eye />
            </div>

            {{-- new password --}}
            <div x-data="{ show: false }" class="relative w-full text-center">
                <x-form.input action="show ? 'text' : 'password'" name="new_password"
                    placeholder="{{ __('web.new_password') }}" />
                <x-form.password-eye />
            </div>

            {{-- confirm password --}}
            <div x-data="{ show: false }" class="relative w-full text-center">
                <x-form.input action="show ? 'text' : 'password'" name="confirm_password"
                    placeholder="{{ __('web.confirm_password') }}" />
                <x-form.password-eye />
            </div>

            <x-buttons.background-button style="w-1/4 max-lg:w-1/2 max-sm:w-full">
                {{ __('web.password_changing') }}
            </x-buttons.background-button>
        </form>
    </div>
</section>
