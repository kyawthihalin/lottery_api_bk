<div class="text-center mt-6">
    <h1 class="mt-5 pt-5 font-black	">{{ auth('user')->user()->name}}</h1>
    <h1 class="mt-2"><span class="text-2xl mr-2">{{ auth('user')->user()->point}}</span><span class="text-lg"> {{ __('web.point') }}</span></h1>
    <h1 class="mt-2"><i class="fa-regular fa-circle-check text-lg mr-1 text-green-400"></i> <span class="text-green-400 text-sm">Verified</span></h1>
    <h1 class="mt-2 "><span class=" bg-info-600 text-white text-xl p-1 rounded">Agent Code - {{ auth('user')->user()->agent->code}} </span></h1>
</div>
