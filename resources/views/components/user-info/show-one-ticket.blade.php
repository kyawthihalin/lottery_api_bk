
<div x-cloak  id="basicModal" x-data="{
    openPurchaseOneTicket: @entangle('showPurchaseOneTicket'),
    showTicketNumbers : @entangle('showTicketNumbers'),
    showTicketDate :  @entangle('showTicketDate'),
    totalTicket:  @entangle('showTicketCount'),
 }"
    @open-me="openPurchaseOneTicket=true" @close-me="openPurchaseOneTicket=false">

    <div @keydown.window.escape="openPurchaseOneTicket = false" x-show="openPurchaseOneTicket" class="relative z-10" aria-labelledby="modal-title"
        x-ref="dialog" aria-modal="true">

        <div x-show="openPurchaseOneTicket" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
            x-description="Background backdrop, show/hide based on modal state."
            class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-end items-center justify-center min-h-full max-sm:p-0 p-4 text-center sm:p-0">

                <div x-show="openPurchaseOneTicket" x-transition:enter="ease-out duration-300"
                    x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave="ease-in duration-200"
                    x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-description="Modal panel, show/hide based on modal state."
                    @click.away="openPurchaseOneTicket = false">

                    <button @click.prevent="openPurchaseOneTicket = false" type="button" class=" float-right close-show-wining-number"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-5xl  text-white  cursor-pointer">×</span>
                    </button>


                    <div class="pt-[20px] pb-[50px] rotate-container max-sm:p-[0px] w-full relative ">
                        <template x-for="(value, index) in showTicketNumbers" >
                            <div style="background-image: url(https://www.myanthai.com/storage/images/ticket/2023-12-16.jpg);" class="w-[500px] h-[255px] max-sm:w-[350px] max-sm:h-[170px] bg-cover bg-no-repeat bg-center mt-11 " >

                                <div class="ticket-number flex w-[219px] max-sm:w-[100px] h-[54px] absolute  text-gray-800 text-[31px] max-sm:text-[20px] cursor-pointer text-center top-23 mt-[10px] max-sm:mt-[5px] max-sm:top-0 right-10 max-sm:right-[82px]">
                                    <template x-for="(number, columnIndex) in value" :key="columnIndex">
                                        <div class="m-0.5">
                                            <div class="text-[20px] max-sm:text-[15px] p-[-4px] ml-[1px] mr-[1px] w-[27px] max-sm:w-[19px]  text-center"   x-text="number" ></div>
                                            <div class="text-[14px] max-sm:text-[9px]  p-[-4px] ml-[1px] mr-[1px] w-[27px] max-sm:w-[19px]  text-center"    x-text="convertToWord(number)"></div>
                                        </div>
                                    </template>
                                </div>

                                <div class="absolute top-36 right-16 px-1 w-[180px] max-sm:top-[60px] max-sm:right-[30px]">
                                    <h1 class=" text-[18px]   max-sm:text-[15px] text-center font-black " x-text="showTicketDate"></h1>
                                </div>

                                <div class="bg-lime-400 absolute top-44 right-16 w-[180px] max-sm:w-[150px] max-sm:top-[90px] max-sm:right-[30px]">
                                    <h1 class=" text-[15px] max-sm:text-[10px] text-center font-semibold my-1">ကံထူးရှင်ဖြစ်ပါစေ</h1>
                                </div>
                            </div>
                        </template>
                    </div>

                    <div class="watermark" style="display: none">
                        <img src={{ asset('img/ticket.jpg') }} width="500px" height="500px" alt="Your Image" id="imageToDownload">
                    </div>


                    <button class="bg-emerald-500 w-full py-[14px] text-white download_one"><i class="fa-solid fa-cloud-arrow-down mr-1 text-lg"></i> Download</button>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('click', (event) => {
        const download_one = event.target.closest('.download_one');

        if (download_one)
        {
            const showTicketNumbers = @this.showTicketNumbers;
            const date = @this.showTicketDate;
            const distanceBetweenImage = 200;
            const imageWidth = 1600;
            const imageHeight= 800;

            for (const showTicketNumber of showTicketNumbers) {
                // function has in main blade
                downloadImage( imageWidth, imageHeight, date, showTicketNumber)
            }
        }
    });
</script>

