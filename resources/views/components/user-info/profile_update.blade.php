<section class="flex">
    <div class="max-xl:flex-auto flex-1 p-4">
        <h1 class="text-xl text-center text-info-900 font-semibold">{{ __('web.profile') }}</h1>

        <form wire:submit.prevent="updateInfo" class="flex flex-col gap-5 mt-6 items-center">
            <x-form.input name="user_name" placeholder="Username" />
            <x-form.input name="phone" placeholder="Phone" />

            <div class="w-1/4 max-lg:w-1/2 max-sm:w-full duration-300">
                <h1 class="text-left">Gender:</h1>

                <input type="radio" wire:model="gender" value="male" id="male" class="mr-2">
                <label for="male" class="mr-5">Male</label>

                <input type="radio" wire:model="gender" value="female" id="female" class="mr-2">
                <label for="female">Female</label>
            </div>

            <x-buttons.background-button style="w-1/4 max-lg:w-1/2 max-sm:w-full">
                {{ __('web.comfirm_update_info') }}
            </x-buttons.background-button>
        </form>
    </div>
</section>
