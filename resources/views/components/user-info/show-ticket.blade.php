<div x-cloak id="basicModal" x-data="{
    openTicket: @entangle('showTicketBox'),
    showTicketNumbers : @entangle('showTicketNumbers'),
    showTicketDate :  @entangle('showTicketDate'),
    totalTicket:  @entangle('showTicketCount'),
}" @open-me="openTicket=true" @close-me="openTicket=false">


<div @keydown.window.escape="openTicket = false" x-show="openTicket" class="relative z-10" aria-labelledby="modal-title"
    x-ref="dialog" aria-modal="true">

    <div x-show="openTicket" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
        x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
        x-description="Background backdrop, show/hide based on modal state."
        class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

    <div class="fixed z-10 inset-0 overflow-y-auto">
        <div class="flex items-center justify-center min-h-full p-4 text-center sm:p-0">
            <div x-show="openTicket" x-transition:enter="ease-out duration-300"
                x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                x-transition:leave="ease-in duration-200"
                x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                x-description="Modal panel, show/hide based on modal state."
                class="relative rounded-lg bg-white w-[700px] max-sm:w-[365px] text-left  transform transition-all  "
                @click.away="openTicket = false">

                <div>
                    <button @click.prevent="openTicket = false" type="button" class="close float-right"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-4xl font-black p-5">×</span>
                    </button>
                    <div class="clear-both flex flex-col items-center justify-center text-center p-4">

                        <div class="max-h-[550px] max-sm:max-h-[400px] relative overflow-auto ">
                            <div class="overflow-x-clip flex justify-center items-center flex-col relative top-[18px] bg-white rounded pb-4  ">
                                <template x-for="(value, index) in showTicketNumbers" class=" top-20 right-16  ">
                                    <div style="background-image: url(https://www.myanthai.com/storage/images/ticket/2023-12-16.jpg);" class="w-[500px] max-sm:w-[350px] h-[63px] flex-none relative  bg-cover bg-no-repeat  "
                                    :class="{ 'h-[170px]  shadow-xl': index == totalTicket, 'h-[63px]': index != totalTicket }">
                                        <div class="ticket-number flex w-[219px] max-sm:w-[100px] h-[54px] absolute  text-gray-800 text-[31px] max-sm:text-[20px] cursor-pointer text-center top-3 max-sm:top-[0.4rem] right-10 max-sm:right-[5.3rem]">
                                            <template x-for="(number, columnIndex) in value" :key="columnIndex">
                                                <div class="m-0.5">
                                                    <div class="text-[20px] max-sm:text-[15px] p-[-4px] ml-[1px] mr-[1px] w-[27px] max-sm:w-[19px]  text-center"   x-text="number" ></div>
                                                    <div class="text-[14px] max-sm:text-[9px]  p-[-4px] ml-[1px] mr-[1px] w-[27px] max-sm:w-[19px]  text-center"    x-text="convertToWord(number)"></div>
                                                </div>
                                            </template>
                                        </div>
                                        <div class="absolute top-20 max-sm:top-16 right-16 max-sm:right-6  px-1 w-[180px]">
                                            <h1 class=" text-[18px]  max-sm:text-[15px] text-center font-black " x-text="showTicketDate"></h1>
                                        </div>

                                        <div class="bg-lime-400 absolute top-28 max-sm:top-24 right-16 max-sm:right-14 w-[180px] max-sm:w-[120px]">
                                            <h1 class=" text-[15px] max-sm:text-[10px] text-center font-semibold my-1">ကံထူးရှင်ဖြစ်ပါစေ</h1>
                                        </div>
                                    </div>
                                </template>
                            </div>

                            <template x-for="(value, index) in showTicketNumbers" >
                                <div style="background-image: url(https://www.myanthai.com/storage/images/ticket/2023-12-16.jpg);" class="w-[500px] max-sm:w-[350px] h-[252px] max-sm:h-[170px]  bg-cover bg-no-repeat  relative mt-8 shadow-xl">
                                    <div class="ticket-number flex w-[219px] max-sm:w-[100px] h-[54px] absolute  text-gray-800 text-[31px] max-sm:text-[20px] cursor-pointer text-center top-3 max-sm:top-[0.4rem] right-10 max-sm:right-[5.3rem]">
                                        <template x-for="(number, columnIndex) in value" :key="columnIndex">
                                            <div class="m-0.5">
                                                <div class="text-[20px] max-sm:text-[15px] p-[-4px] ml-[1px] mr-[1px] w-[27px] max-sm:w-[19px]  text-center"   x-text="number" ></div>
                                                <div class="text-[14px] max-sm:text-[9px]  p-[-4px] ml-[1px] mr-[1px] w-[27px] max-sm:w-[19px]  text-center"    x-text="convertToWord(number)"></div>
                                            </div>
                                        </template>
                                    </div>

                                    <div class="absolute top-20 max-sm:top-16 right-16 max-sm:right-7 px-1 w-[180px]">
                                        <h1 class=" text-[18px] max-sm:text-[15px] text-center font-black " x-text="showTicketDate"></h1>
                                    </div>

                                    <div class="bg-lime-400 absolute top-28 max-sm:top-24 right-16 max-sm:right-14 w-[180px] max-sm:w-[120px]">
                                        <h1 class=" text-[15px] max-sm:text-[10px] text-center font-semibold my-1">ကံထူးရှင်ဖြစ်ပါစေ</h1>
                                    </div>
                                </div>
                            </template>

                            <div class="watermark" style="display: none">
                                <img src={{ asset('img/ticket.jpg') }} width="500px" height="500px" alt="Your Image" id="imageToDownload">
                            </div>

                        </div>
                        <div class="flex justify-around  w-[570px] max-sm:w-[365px] mt-3">
                            <button @click.prevent="openTicket = false" class=" w-[45%] px-4 bg-white border-2	max-sm:text-[10px] border-info-500 text-info-500 py-2">
                                {{ __('web.close') }}
                            </button>

                            <button id="downloadButton" @click.prevent="openTicket = false" class="download_all max-sm:text-[10px] w-[45%] px-4 bg-info-500 text-white py-2">
                                {{ __('web.download_all') }}
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<script>

    document.addEventListener('click', (event) => {
        const download_all = event.target.closest('.download_all');

        if (download_all)
        {
            const showTicketNumbers = @this.showTicketNumbers;
            const date = @this.showTicketDate;
            const imageCount = showTicketNumbers.length;
            const distanceBetweenImage = 200;
            const imageWidth = 1600;
            const imageHeight= 800;

            for (const showTicketNumber of showTicketNumbers) {
                // function has in main blade
                downloadImage( imageWidth, imageHeight, date, showTicketNumber)
            }

            //Combining Image
            var downloadImageName= date.replace(/ /g, '_')+"all";
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var img = document.getElementById('imageToDownload');
            canvas.width  = imageWidth;
            canvas.height = (imageHeight - distanceBetweenImage) + ( distanceBetweenImage * imageCount );

            ctx.fillStyle = '#FFFFFF';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var adjustYPosition = 0;

            for (const showTicketNumber of showTicketNumbers) {
                ctx.drawImage(img, 0, adjustYPosition);

                var x = 855;
                var y = 120 + adjustYPosition;
                ctx.font = '80px Chakra Petch,sans-serif';
                ctx.fillStyle = 'black';
                ctx.textAlign = 'center';
                var characters = showTicketNumber;
                for (var i = 0; i < characters.length; i++) {
                    // 60 is font Size
                    ctx.fillText(characters[i], x, y,60);
                    var charWidth = ctx.measureText(characters[i]).width;
                    // 100 is distance between two world
                    x +=  100;
                }

                var x = 855;
                var y = 170 + adjustYPosition;
                ctx.font = '40px Chakra Petch,sans-serif';
                ctx.fillStyle = 'black';
                ctx.textAlign = 'center';
                var text = getWord(showTicketNumber);
                var characters = text.split(' ');
                for (var i = 0; i < characters.length; i++) {
                    // 200 is font Size
                    ctx.fillText(characters[i], x, y,200);
                    var charWidth = ctx.measureText(characters[i]).width;
                    // 100 is distance between two world
                    x += 100;
                }

                adjustYPosition += distanceBetweenImage;

            }

            ctx.fillStyle = 'rgb(163 230 53';
            // X Position,Y Position , Width, Height
            ctx.fillRect(900, 200 + (distanceBetweenImage * imageCount ), 400, 70);
            ctx.font = '40px Arial';
            ctx.fillStyle = 'red';
            ctx.textAlign = 'center';
            // text , X Position,Y Position
            ctx.fillText('ကံထူးရှင်ဖြစ်ပါစေ',1100, 250 + (distanceBetweenImage * imageCount ));

            ctx.font = '50px Chakra Petch,sans-serif';
            ctx.fillStyle = 'black';
            ctx.textAlign = 'center';
            // text , X Position,Y Position
            ctx.fillText(date,1100, 150 + (distanceBetweenImage * imageCount ));


            var link = document.createElement('a');
            link.download = downloadImageName + '.jpg';
            link.href = canvas.toDataURL('image/jpeg', 1.0);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);


        }
    });


</script>
