@props(['betInfo','depositWithdrawes'])
@php
    use Carbon\Carbon;
@endphp

<div x-data="{  lastDate: @entangle('last_date'),
                betCount : @entangle('bet_count'),
                depositWithdrawCount : @entangle('depositWithdrawCount')}">
    <h6 x-text="lastDate" class="mt-[-20px] max-sm:mt-[15px] text-sm"></h6>


    <div class="result-toggle-button flex justify-between  bg-slate-300 p-4 px-8  max-sm:px-2 rounded mt-5">
        <div>
            <i class="fa-solid fa-ticket text-2xl max-sm:text-lg  rounded-full py-[5px] px-[8px] text-white bg-red-600"></i>
            <span class="ml-3 max-sm:text-sm">Purchased Tickets (<span x-text="betCount"></span>)</span>
        </div>
        <h2 class="flex items-center text-2xl max-sm:text-lg"><span x-show="betCount !== 0">-</span><span x-text="betCount"></span></h2>
    </div>
    <div class="result-list">
        @foreach ($betInfos as $betInfo)
            <div class="flex justify-between max-sm:flex-col   bg-slate-300 p-4 px-8 max-sm:px-2 rounded mt-5">
                <div class="flex items-center">
                    <div class="items-center">
                        <i class="fa-solid fa-ticket text-2xl max-sm:text-lg  rounded-full py-[5px] px-[8px] text-white items-center bg-red-600"></i>
                    </div>
                    <div class="ml-3">
                        <div class="max-sm:text-sm">{{ Carbon::parse($betInfo->created_at)->format('M j, Y h:m:i')}}</div>
                        <div class="max-sm:text-sm">{{$betInfo->point}} {{ __('web.ticket') }}</div>
                    </div>
                </div>
                <div class="flex items-center max-sm:justify-center	cursor-pointer" x-on:click="$wire.showTickets({{$betInfo->id}})">
                    <div style="background-image: url(https://www.myanthai.com/storage/images/ticket/2023-12-16.jpg);" class="w-[135px] h-[70px]  bg-cover bg-no-repeat shadow-xl relative ">

                        <div class="absolute top-1 right-3 px-1">
                            <h1 class=" text-[10px] font-black">
                                <span class="tracking-[2.5px]">{{$betInfo->tickets[0]}}</span>
                            </h1>
                        </div>

                        <div class="absolute top-7 right-4 px-1">
                            <h1 class=" text-[7px] font-black">{{ Carbon::parse($betInfo->created_at)->format('d M Y ')}}</h1>
                        </div>

                        <div class="bg-lime-400 absolute top-10 right-4 px-1">
                            <h1 class=" text-[5px]">ကံထူးရှင်ဖြစ်ပါစေ</h1>
                        </div>

                    </div>

                </div>
            </div>
        @endforeach
    </div>





    <div class="result-toggle-button flex justify-between  bg-slate-300 p-4 px-8 max-sm:px-2 rounded mt-5">
        <div>
            <i class="fa-solid fa-circle-dollar-to-slot text-2xl max-sm:text-lg  rounded-full py-[5px] px-[9px] text-white bg-emerald-400"></i>
            <span class="ml-3 max-sm:text-sm">Refill Units</span>
        </div>
        <h2 class="flex items-center text-2xl max-sm:text-lg"><span x-text="depositWithdrawCount"></span></h2>
    </div>

    <div class="result-list">
        @foreach ($depositWithdrawes as $depositWithdraw)
            <div class="flex justify-between  bg-slate-300 p-4 px-8 max-sm:px-2 rounded mt-5">
                <div class="flex items-center">
                    <div class="items-center">
                        <i class="fa-solid fa-circle-dollar-to-slot text-2xl max-sm:text-lg  rounded-full py-[5px] px-[8px] text-white items-center bg-emerald-400"></i>
                    </div>
                    <div class="ml-3">
                        <div class="max-sm:text-sm">{{ Carbon::parse($depositWithdraw->created_at)->format('M j, Y h:m:i')}}</div>
                        <div class="max-sm:text-sm">Transaction ID #{{$depositWithdraw->reference_id}}</div>
                    </div>
                </div>
                <div class="flex items-center cursor-pointer" >
                    <h2 class="flex items-center text-2xl max-sm:text-lg"><span>+{{$depositWithdraw->point}}</span></h2>
                </div>
            </div>
        @endforeach
    </div>



</div>
