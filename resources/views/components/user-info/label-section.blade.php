<div class="mx-20 max-sm:mx-3  my-10 max-sm:my-3 " >
    <div class="text-sm max-sm:text-[12px] font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700 ">
        <ul class="flex flex-wrap -mb-px mt-6">
                <li class="me-2">
                    <a href="#"  onclick="showTab('1')" id="tab1" class=" tab-link activeTabLink  inline-block p-4 max-sm:p-[5px]  border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" aria-current="page">
                        {{ __('web.transactions') }}
                    </a>
                </li>

                <li class="me-2">
                    <a href="#" onclick="showTab('2')" id="tab2" class=" tab-link   inline-block p-4 max-sm:p-[5px] border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" aria-current="page">
                        {{ __('web.profile_info') }}
                    </a>
                </li>


                <li class="me-2">
                    <a href="#" onclick="showTab('3')" id="tab3" class=" tab-link   inline-block p-4 max-sm:p-[5px] border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" aria-current="page">
                        {{ __('web.password') }}
                    </a>
                </li>

                <li class="me-2">
                    <a href="#" onclick="showTab('4')" id="tab4" class=" tab-link   inline-block p-4 max-sm:p-[5px] border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" aria-current="page">
                        {{ __('web.bank') }}
                    </a>
                </li>
        </ul>
    </div>
</div>
