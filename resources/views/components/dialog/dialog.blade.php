<div @keydown.window.escape="{{ $name }} = false; if(JSON.parse(localStorage.getItem('pageRefresh')) && isAuthenticated) refreshAndStore();"
    x-show="{{ $name }}" class="relative z-10" aria-labelledby="modal-title" x-ref="dialog" aria-modal="true">
    <div x-show="{{ $name }}" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
        x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
        x-description="Background backdrop, show/hide based on modal state."
        class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>
    <div class="fixed z-10 inset-0 overflow-y-auto">
        <div class="flex items-center justify-center min-h-full p-4 text-center">
            <div x-show="{{ $name }}" x-transition:enter="ease-out duration-300"
                x-transition:enter-start="opacity-0 translate-y-4 translate-y-0 scale-95"
                x-transition:enter-end="opacity-100 translate-y-0 scale-100" x-transition:leave="ease-in duration-200"
                x-transition:leave-start="opacity-100 translate-y-0 scale-100"
                x-transition:leave-end="opacity-0 translate-y-4 translate-y-0 scale-95"
                x-description="Modal panel, show/hide based on modal state."
                class="relative bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all my-8"
                @click.away="{{ $name }} = false; if(JSON.parse(localStorage.getItem('pageRefresh')) && isAuthenticated) refreshAndStore();">
                <div class="bg-white px-4 pt-5 pb-4 p-6">
                    <div class="flex justify-end text-gray-800">
                        <button
                            @click.prevent="{{ $name }} = false; if(JSON.parse(localStorage.getItem('pageRefresh')) && isAuthenticated) refreshAndStore();">
                            <span class="text-3xl" aria-hidden="true">×</span>
                        </button>
                    </div>
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>
