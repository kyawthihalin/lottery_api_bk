<div>

    @if(!empty($winnerUsers[$filterDate][$prizeType]))
                @foreach($winnerUsers[$filterDate][$prizeType] as $key => $data)
                    @if($key == 0)
                        <div class="winner-prize mt-4">
                            <div class="text-base max-sm:text-sm">{{$prizeText}}</div>
                            <div><i class="fa-solid fa-trophy mr-2 inline text-base max-sm:text-sm bg-yellow-500 p-1 rounded-full "></i> <span class="text-base max-sm:text-sm">{{$data["amount"]}}</span>
                            </div>
                        </div>
                    @endif
                    <div class="winner-info">
                        <div class="winner-left">
                            <img src="{{ $data['gender']->value == 'male' ? 'https://www.myanthai.com/images/avatar.svg' : 'https://www.myanthai.com/images/avatar-female.svg' }}" alt="" class="w-20 max-sm:w-14">
                            <div class="user-code text-base max-sm:text-sm">
                                <div>{{$data["name"]}}</div>
                                <div class="agent-code">{{$data["agent_code"]}}</div>
                            </div>
                        </div>

                        <div class="winner-mid flex justify-center items-center "><i class="fa-solid text-3xl max-sm:text-lg fa-ticket mr-2 text-primary-600"></i>
                               <span class="text-xl max-sm:text-sm "> {{$data["ticket_numbers"][0]}}</span>
                        </div>
                    </div>
                @endforeach
     @endif
</div>
