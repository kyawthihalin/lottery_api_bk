<style>
    .tab-content {
        display: none;
        padding-top: 20px;
    }

    .tab-content.active {
        display: block;
    }

    .tab-link {}

    .tab-link.activeTabLink {
        border-color: #365BEF;
    }

    .winner-header {
        padding: 1rem;
        color: #081557;
        background: #eaeefe;
        border-radius: 5px;
        display: flex;
        justify-content: space-between;
    }

    .winner-prize {
        padding: 1rem;
        color: #081557;
        background: #fffde7;
        border-radius: 5px;
        display: flex;
        justify-content: space-between;
    }

    .winner-prize-total {
        padding: 1rem;
        color: #081557;
        background: #eaeefe;
        border-radius: 5px;
        display: flex;
        justify-content: space-between;
    }

    .win-prize-icon {
        width: 406px;
    }

    .winner-info {
        display: flex;
        padding: 1rem 0;
        border-bottom: 1px solid #C9C9C9;
        align-items: center;
        justify-content: space-between;
        padding-left: 0.4rem;
        padding-right: 1rem;
    }

    .winner-left {
        display: flex;
        align-items: center;
        min-width: 156px;
    }

    /* .winner-avatar {
        width: 80px;
    } */

    .user-code {
        color: #081557;
        text-align: left;
    }

    .agent-code {
        width: fit-content;
        padding: 0.2rem;
        color: #ffffff;
        background-color: #365BEF;
        border-radius: 0.2rem;
    }

    .winner-mid {
        color: #081557;
        font-size: 18px;
        min-width: 109px;
    }

    /* .ticket-blue {
        width: 30px;
        margin-right: 0.5rem;
    } */

    img {
        display: inline;
    }
</style>


@livewire('winner')



<script>
    function showTab(tabId) {
        event.preventDefault();
        var tabs = document.querySelectorAll('.tab-content');
        tabs.forEach(function(tab) {
            tab.classList.remove('active');
        });

        var tabsLink = document.querySelectorAll('.tab-link');
        tabsLink.forEach(function(tab) {
            tab.classList.remove('activeTabLink');
        });

        document.getElementById(tabId).classList.add('active');
        document.getElementById(tabId + "Link").classList.add('activeTabLink');
    }
</script>
