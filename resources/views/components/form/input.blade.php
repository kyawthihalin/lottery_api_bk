@php
    $type = $type ?? 'text';
    $action = $action ?? null;
    $style = $style ?? 'w-1/4 max-lg:w-1/2 max-sm:w-full';
    $attribute = isset($attribute) ? $attributes->merge([$attribute => $name]) : '';
@endphp

<input type="{{ $type }}" @if ($action) :type="{{ $action }}" @endif
    name="{{ $name }}" placeholder="{{ $placeholder }}" wire:model="{{ $name }}" {{ $attribute }}
    class="{{ $style }} p-4 rounded-lg text-base placeholder:text-base border-none focus:ring-2 focus:ring-info-300 transition duration-300">
