@php
    $style = $style ?? 'right-3 sm:right-[27%] lg:right-[39%]';
@endphp

<i @click="show = !show" x-show="show"
    class="fa-regular fa-eye top-1/2 {{ $style }} -translate-y-1/2 absolute text-sm text-gray-600 cursor-pointer"></i>
<i @click="show = !show" x-show="!show"
    class="fa-regular fa-eye-slash top-1/2 {{ $style }} -translate-y-1/2 text-sm absolute text-gray-600 cursor-pointer"></i>
