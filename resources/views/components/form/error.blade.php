@php
    $name = $name ?? null;
    $style = 'text-red-500 text-sm font-semibold text-center';
@endphp

@if ($name)
    @error($name)
        <span class="{{ $style }}">{{ $message }}</span>
    @enderror
@else
    @if (session('error'))
        <span class="{{ $style }}">
            {{ session('error') }}
        </span>
    @endif
@endif
