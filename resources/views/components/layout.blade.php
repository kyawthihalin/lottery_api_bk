<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@php
    use App\Services\StorageService;
    use App\Settings\ImageSettings;
    use App\Models\Notification;
    use App\Settings\GeneralSettings;

    $viberLink = app(GeneralSettings::class)->contact_viber ?? null;

    if (Auth::guard('user')->check()) {
        $notifications = auth('user')->user()->notifications()->latest()->limit(5)->get();
    } else {
        $notifications = '';
    }
@endphp


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alibaba</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Manrope' rel='stylesheet'>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"> --}}

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />
    <link href="https://unpkg.com/@pqina/flip/dist/flip.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css"
        integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Chakra+Petch:wght@700&display=swap" rel="stylesheet">

    <style>
        @font-face {
            font-family: 'Octin';
            src: url('{{ asset('fonts/octin college rg.otf') }}') format('opentype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            background-image: url({{ StorageService::getUrl(app(ImageSettings::class)->website_background) }});
        }

        .swiper-pagination-bullet-active {
            width: 40px;
            transition: width .5s;
            border-radius: 5px;
            background: #fff;
            border: 1px solid transparent;
        }

        .swiper-3d .swiper-slide-shadow-right,
        .swiper-3d .swiper-slide-shadow-left {
            background-image: none;
        }

        /* scroll bar style */
        /* width */
        ::-webkit-scrollbar {
            width: 12px;
            height: 12px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            border-radius: 100vh;
            background: #edf2f7;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #cbd5e0;
            border-radius: 100vh;
            border: 3px solid #edf2f7;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #a0aec0;
        }
    </style>

    @vite('resources/css/app.css')

    @livewireStyles
</head>

<body class="bg-gray-200 flex flex-col min-h-screen justify-between font-['Manrope']">
    <header>
        <x-nav.navbar :notifications="$notifications"></x-nav.navbar>
        <x-nav.navbar-second :notifications="$notifications"></x-nav.navbar-second>
    </header>

    {{ $slot }}

    <footer class="mt-auto">
        <x-footer.footer></x-footer.footer>
    </footer>

    <a href="{{ $viberLink }}" target="_blank" class="fixed bottom-3 right-3 z-50 cursor-pointer">
        <img src="https://www.myanthai.com/images/viber.png" alt="ViberLogo" width="60px" height="60px">
    </a>

    @livewireScripts
</body>

<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
<script src="https://unpkg.com/@pqina/flip/dist/flip.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    if (!window.location.pathname.includes('purchase-tickets')) {
        localStorage.clear();
    }

    document.addEventListener('click', (event) => {
        const navMobile = event.target.closest('.nav-mobile-button');

        if (navMobile) {
            const navBar = document.getElementById("nav-mobile");
            navBar.classList.toggle('nav-mobile-list-show');
        }
    });

    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 30,
        autoplay: {
            delay: 3000,
        },
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });

    var swiper = new Swiper(".liveSale", {
        effect: "coverflow",
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: "auto",
        coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
        },
        autoplay: {
            delay: 3000,
        },
    });

    var swiper = new Swiper(".liveSaleDetail", {
        effect: "coverflow",
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: "auto",
        coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
        },
    });

    @if (session()->has('success'))
        message = @json(session()->pull('success'));

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        Toast.fire({
            icon: 'success',
            title: message
        });
    @endif
</script>

</html>
