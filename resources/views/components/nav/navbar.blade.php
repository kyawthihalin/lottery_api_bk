@php
    use App\Services\StorageService;
    use App\Settings\ImageSettings;
    use Carbon\Carbon;
@endphp

<nav class="flex  w-full py-4 sm:py-0 px-4 text-sm text-gray-900  border-b">
    <div class="container mx-auto my-0 sm:my-5 flex justify-between items-center max-sm:items-start max-sm:flex-col ">
        <div class=" w-full flex sm:items-center sm:w-auto  max-sm:flex-col" id="menu">
            <a href="{{ route('home') }}"
                class="contents box-border h-20 w-20 overflow-hidden transform hover:scale-90 m-2">
                <img src="{{ StorageService::getUrl(app(ImageSettings::class)->app_logo) }}" alt="logo" class="w-24">
            </a>
        </div>

        <div class="max-sm:flex max-sm:items-center max-sm:w-full">
            <div class="flex max-sm:flex-col w-full sm:items-center sm:w-auto mt-5 sm:mt-0" id="menu">
                <div class="flex items-center">
                    <ul class="text-sm flex md:justify-between max-sm:pt-0 max-sm:flex-col">
                        <li>
                            @if (Auth::guard('user')->check())
                                <ul class="p-0 pl-0 md:pt-4 flex items-center cursor-pointer">
                                    <li class="mr-[10px] max-sm:mr-[5px]">
                                        <div class="text-info-900" >
                                            <i class="fa-solid fa-coins text-[18px] text-yellow-500	"></i>
                                            <span class="text-[17px] ml-1 font-bold">{{ auth('user')->user()->point}}</span>
                                        </div>
                                    </li>

                                    <li class="mx-[10px]  max-sm:mr-[5px]">
                                        <div class="text-info-900" >
                                            <i class="fa-solid fa-sack-dollar text-[18px] text-zinc-700	"></i>
                                            <span class="text-[17px] ml-1 font-bold">0</span>
                                        </div>
                                    </li>
                                </ul>
                            @endif
                        </li>
                        <li>
                            <div class="relative text-info-900 max-sm:flex-col" x-data="{ dropdown: false }" x-cloak>
                                <a @click="dropdown = !dropdown"
                                    class="p-0 pl-0 md:p-4 md:pl-4 max-sm:mt-4 flex items-center cursor-pointer">
                                    <i class="fa-solid fa-earth-asia"></i>
                                    <span class="mx-1">{{ __('web.lang') }}</span>
                                    <i class="fa-solid fa-sort-down mb-1"></i>
                                </a>
                                <ul class="absolute top-8 sm:top-12 right-0 max-sm:left-3/4  transform max-sm:-translate-x-1/4 w-48 rounded-md shadow-lg bg-white z-50"
                                    x-show="dropdown" @click.away="dropdown = false">
                                    <li>
                                        <a href="{{ route('lang.change', 'en') }}"
                                            class="flex px-4 py-2 text-sm text-gray-700 hover:bg-gray-700 hover:text-white rounded">
                                            <img src="{{ asset('flags/en.svg') }}" alt="en_flag" class="w-5 mr-2">
                                            {{ __('general.english') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('lang.change', 'my') }}"
                                            class="flex px-4 py-2 text-sm text-gray-700 hover:bg-gray-700 hover:text-white rounded">
                                            <img src="{{ asset('flags/my.svg') }}" alt="en_flag" class="w-5 mr-2">
                                            {{ __('general.myanmar') }}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>

                    <div x-cloak id="basicModal" x-data="{
                        open: false,
                        title: '',
                        message: '',
                        date: '',
                    }" @open-me="open=true" @close-me="open=false">

                        <div @keydown.window.escape="open = false" x-show="open" class="relative z-10"
                            aria-labelledby="modal-title" x-ref="dialog" aria-modal="true">

                            <div x-show="open" x-transition:enter="ease-out duration-300"
                                x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
                                x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100"
                                x-transition:leave-end="opacity-0"
                                x-description="Background backdrop, show/hide based on modal state."
                                class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

                            <div class="fixed z-10 inset-0 overflow-y-auto">
                                <div class="flex items-center justify-center min-h-full p-4 text-center sm:p-0">
                                    <div x-show="open" x-transition:enter="ease-out duration-300"
                                        x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                                        x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                                        x-transition:leave="ease-in duration-200"
                                        x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                                        x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                                        x-description="Modal panel, show/hide based on modal state."
                                        class="relative bg-white rounded-lg  text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:max-w-lg sm:w-full"
                                        @click.away="open = false">

                                        <div>
                                            <div class="bg-slate-100 relative h-[60px]">
                                                <div class="py-3 px-3 flex justify-between ">
                                                    <h1 class="text-base" x-text="date"></h1>
                                                    <button @click.prevent="open = false" type="button">
                                                        <span aria-hidden="true" class="text-4xl font-black">×</span>
                                                    </button>

                                                </div>
                                                <div class=" absolute bottom-[-20px] w-full ">
                                                    <div class="flex object-center  justify-center">
                                                        <span class="bg-info-500 rounded-full">
                                                            <i
                                                                class="fa-regular fa-bell text-3xl text-center text-white my-[6px] mx-[12px]"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <h1 class="text-center text-base font-extrabold	py-0 pt-14" x-text="title">
                                            </h1>
                                            <p class="m-2 py-5 px-6 text-base text-center" x-text="message"></p>
                                            <hr>
                                            <div class="m-3 flex justify-end">
                                                <button @click.prevent="open = false"
                                                    class="bg-slate-600 p-2 rounded-md text-white">{{ __('web.close') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (Auth::guard('user')->check())
                            <ul class="text-sm flex md:justify-between max-sm:pt-10">
                                <li>
                                    <div class="relative text-info-900 text-base" x-data="{ dropdown: false }" x-cloak>
                                        <a @click="dropdown = !dropdown"
                                            class="p-0 pl-4 md:p-4 md:pl-4 max-sm:p-0 flex items-center cursor-pointer">
                                            <i class="fa-solid fa-bell"></i>
                                            <i class="fa-solid fa-sort-down mb-2 ml-1"></i>
                                        </a>
                                        <ul x-show="dropdown" @click.away="dropdown = false" id="noti-list"

                                            class="absolute top-8 sm:top-12 right-0 max-sm:left-1/4 transform max-sm:-translate-x-1/4 w-80 rounded-md shadow-lg bg-white z-50 p-5">

                                            <h1 class="text-xl mb-5">Notifications</h1>

                                            @if($notifications != null)
                                                @foreach ($notifications as $notification)
                                                    @php
                                                        $title = $notification->data['title'];
                                                        $decodedTitle = json_decode($title);
                                                        $key = isset($decodedTitle->key) ? $decodedTitle->key : null;
                                                        $pointValue = $decodedTitle->replace->point;
                                                        $translatedString = trans($key, ['point' => $pointValue]);

                                                        $body = $notification->data['body'];
                                                        $decodedBody = json_decode($body);
                                                        $keyBody = isset($decodedBody->key) ? $decodedBody->key : null;
                                                        $nameValue = isset($decodedBody->replace->name) ? $decodedBody->replace->name : null;
                                                        $ticketValue = isset($decodedBody->replace->tickets) ? $decodedBody->replace->tickets : null;

                                                        $message = trans($keyBody, ['point' => $pointValue, 'name' => $nameValue, 'tickets' => $ticketValue]);
                                                        $date = Carbon::parse($notification->created_at)->format('M d, Y h:i A');

                                                    @endphp
                                                    <li class="read-notification flex my-1 cursor-pointer" id={{$notification->id}}
                                                        @click.prevent="open = true,dropdown = false,message='{{ $message }}',title='{{ $translatedString }}', date='{{ $date }}'">
                                                        <div class="flex items-center justify-center pr-3"><i
                                                                class="fa-regular fa-bell py-[5px] px-[11px] text-lg text-info-900 bg-blue-100	 rounded-full"></i>
                                                        </div>
                                                        <div>
                                                            {{ Carbon::parse($notification->created_at)->format('M j, Y') }}<br>
                                                            {{ $translatedString }}</div>
                                                    </li>
                                                    <hr class="my-2">
                                                @endforeach
                                            @else
                                                <hr class="mb-3">
                                                    <h1 class="text-center text-red-700	">
                                                        <i class="fa-solid fa-triangle-exclamation "></i>
                                                        There is no notification.
                                                    </h1>
                                                <hr class="mt-3 mb-2">
                                            @endif
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        @endif
                    </div>
                </div>

                @if (!Auth::guard('user')->check())
                    <ul class="pt-4 sm:pt-0 text-sm text-info-900 flex md:justify-between">
                        <li>
                            <div class="h-full w-full flex items-center rounded-full  ">
                                <a href=""
                                    class="py-2 px-3 bg-transparent	 rounded-full transition border-2 border-info-900 duration-300 flex hover:bg-info-900  hover:border-blue-950 hover:text-slate-200">
                                    <i class="fa-brands fa-android pt-1"></i>
                                    <span class="ml-2 whitespace-nowrap">{{ __('web.download') }} APK</span>
                                </a>
                            </div>
                        </li>
                        <li class="ml-2">
                            <div class="h-full w-full flex items-center rounded-full">
                                <a href=""
                                    class="py-2 px-3 bg-transparent	 rounded-full transition border-2 border-info-900 duration-300 flex hover:bg-info-900  hover:border-blue-950 hover:text-slate-200">
                                    <i class="fa-brands fa-apple" style="font-size: 17px;"></i>
                                    <span class="ml-2 whitespace-nowrap">{{ __('web.download') }} APK</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                @endif
            </div>

            @if (Auth::guard('user')->check())
                <div class="text-[16px] text-black font-bold max-sm:mt-[70px]">
                    <div style="white-space: nowrap;">1 baht : <span class="text-info-900"> 70 </span>  mmk</div>
                </div>
            @endif
        </div>
    </div>
    <div class=" flex-row-reverse nav-mobile-button hidden  max-sm:block ">
        <i class="fa-solid cursor-pointer fa-bars text-3xl border-radius border-2 px-2 rounded-sm"></i>
    </div>
</nav>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
     document.addEventListener('click', (event) => {
        const read_Notification = event.target.closest('.read-notification');
        if (read_Notification) {
            const notificationId = read_Notification.id;
            $.ajax({
                url: '/notifications/' + notificationId,
                type: 'PUT',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // Add CSRF token
                },
                data: {
                    read_at: '{{ now() }}'
                },
                success: function(response) {
                    console.log('Notification updated successfully');
                },
                error: function(xhr, status, error) {
                    console.error('Error updating notification:', error);
                }
            });

        }
    });
</script>
