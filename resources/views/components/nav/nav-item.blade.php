@php
    $current = Route::currentRouteName();
    $current == 'login' || $current == 'register' ? ($current = 'auth') : $current;
@endphp

<li class="py-2 sm:py-0 sm:pb-2 sm:px-3">
    <a class="relative whitespace-nowrap" href="{{ $href }}">
        {{ $value }}
        @if ($current == $route)
            <span x-data="{ loaded: false }" x-init="setTimeout(() => loaded = true, 500)"
                class="block transition-all duration-300 transform absolute h-1 rounded bg-amber-300 bottom-[-10px] left-0"
                :class="loaded ? 'scale-x-100 w-full' : 'scale-x-0'"></span>
        @endif
    </a>
</li>
