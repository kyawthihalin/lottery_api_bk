<nav>
    <div class="bg-info-600 mx-auto flex justify-center items-center max-sm:hidden">
        <ul
            class="flex max-sm:flex-col flex-wrap justify-center text-base text-white my-6 w-full transition-max-height duration-500 ease-in-out ">
            <x-nav.nav-item href="{{ route('home') }}" value="{{ __('web.home') }}" route="home" />
            <x-nav.nav-item href="{{ route('purchase-ticket') }}" value="{{ __('web.purchase_ticket') }}"
                route="purchase-ticket" />
            <x-nav.nav-item href="{{ route('result') }}" value="{{ __('web.results') }}" route="result" />
            <x-nav.nav-item href="{{ route('winner') }}" value="{{ __('web.winner_list') }}" route="winner" />
            @if (Auth::guard('user')->check())
                <x-nav.nav-item href="{{ route('user-info') }}" value="{{ auth('user')->user()->name }}"
                    route="user-info" />
                <li class="sm:px-3">
                    <form method="post" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit">
                            {{ __('web.logout') }}
                        </button>
                    </form>
                </li>
            @else
                <x-nav.nav-item href="{{ route('login') }}" value="{{ __('web.login') }}" route="auth" />
            @endif
        </ul>
    </div>

    {{-- Mobile View --}}
    <style>
        .nav-mobile-list {
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.8s ease-in-out;
        }

        .nav-mobile-list-show {
            max-height: 500px;
            transition: max-height 0.8s ease-in-out;
        }
    </style>

    <div id="nav-mobile" class="nav-mobile-list bg-info-600 justify-center items-center hidden max-sm:block">
        <ul
            class="flex max-sm:flex-col justify-between text-base text-white w-[50%]  transition-max-height duration-500 ease-in-out py-3 pl-6">
            <x-nav.nav-item href="{{ route('home') }}" value="{{ __('web.home') }}" route="home" />
            <x-nav.nav-item href="{{ route('purchase-ticket') }}" value="{{ __('web.purchase_ticket') }}"
                route="purchase-ticket" />
            <x-nav.nav-item href="{{ route('result') }}" value="{{ __('web.results') }}" route="result" />
            <x-nav.nav-item href="{{ route('winner') }}" value="{{ __('web.winner_list') }}" route="winner" />
            @if (Auth::guard('user')->check())
                <x-nav.nav-item href="{{ route('user-info') }}" value="{{ auth('user')->user()->name }}"
                    route="user-info" />
                <li>
                    <form method="post" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit">
                            {{ __('web.logout') }}
                        </button>
                    </form>
                </li>
            @else
                <x-nav.nav-item href="{{ route('login') }}" value="{{ __('web.login') }}" route="auth" />
            @endif
        </ul>
    </div>
</nav>
