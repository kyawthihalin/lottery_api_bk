@php
    use App\Services\StorageService;
    use App\Settings\ImageSettings;
    use App\Settings\GeneralSettings;

    $year = date('Y');

    $instagramLink = app(GeneralSettings::class)->contact_instagram ?? null;
    $youtubeLink = app(GeneralSettings::class)->contact_youtube ?? null;
    $facebookLink = app(GeneralSettings::class)->contact_facebook ?? null;
    $twitterLink = app(GeneralSettings::class)->contact_twitter ?? null;
@endphp

<div class="flex flex-col mx-auto h-auto text-base">
    <div
        class="flex items-center justify-evenly sm:justify-between flex-col sm:flex-row sm:px-[30px] md:px-[40px] lg:px-[50px] xl:px-[75px] w-full h-[100px] bg-info-600">
        <a href="{{ route('home') }}"
            class="block box-border h-10 w-10 md:h-16 md:w-16 overflow-hidden transform hover:scale-90 m-2 max-sm:hidden">
            <img src="{{ StorageService::getUrl(app(ImageSettings::class)->app_logo) }}" alt="logo">
        </a>

        <div>
            <ul class="flex max-sm:gap[7px] gap-[20px] text-white">
                <li class="ease-in duration-200 hover:underline mx-2 whitespace-nowrap max-sm:ml-1">
                    <a href="{{ route('privacy-policy') }}">Privacy Policy</a>
                </li>
                <li class="ease-in duration-200 hover:underline mx-2 whitespace-nowrap max-sm:ml-1">
                    <a href="{{ route('terms-and-conditions') }}">Terms & Conditions</a>
                </li>
                <li class="ease-in duration-200 hover:underline mx-2 whitespace-nowrap max-sm:ml-1">
                    <a href="{{ route('about') }}">About</a>
                </li>
            </ul>
            <div class="text-white text-center mt-1 sm:mt-3 max-sm:hidden">© {{ $year }} Alibaba - All Rights
                Reserved.</div>
        </div>

        <ul class="flex gap-[15px] text-white cursor-pointer text-xl ">
            <a href="{{ $instagramLink }}" target="_blank">
                <li class="ease-in duration-200 hover:text-gray-300"><i class="fa-brands fa-instagram"></i></li>
            </a>
            <a href="{{ $youtubeLink }}" target="_blank">
                <li class="ease-in duration-200 hover:text-gray-300"><i class="fa-brands fa-youtube"></i></li>
            </a>
            <a href="{{ $facebookLink }}" target="_blank">
                <li class="ease-in duration-200 hover:text-gray-300"><i class="fa-brands fa-facebook"></i></li>
            </a>
            <a href="{{ $twitterLink }}" target="_blank">
                <li class="ease-in duration-200 hover:text-gray-300"><i class="fa-brands fa-twitter"></i></li>
            </a>
        </ul>
    </div>
</div>
