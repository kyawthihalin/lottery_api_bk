
<div x-cloak  id="basicModal" x-data="{ openWiningTicket: @entangle('showWiningTicket') }" @open-me="openWiningTicket=true" @close-me="openWiningTicket=false">

    <div @keydown.window.escape="openWiningTicket = false" x-show="openWiningTicket" class="relative z-10" aria-labelledby="modal-title"
        x-ref="dialog" aria-modal="true">

        <div x-show="openWiningTicket" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
            x-description="Background backdrop, show/hide based on modal state."
            class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-end items-center justify-center min-h-full max-sm:p-0 p-4 text-center sm:p-0">

                <div x-show="openWiningTicket" x-transition:enter="ease-out duration-300"
                    x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave="ease-in duration-200"
                    x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-description="Modal panel, show/hide based on modal state."
                    @click.away="openWiningTicket = false">

                    <button @click.prevent="openWiningTicket = false" type="button" class=" float-right close-show-wining-number"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-4xl  m-5 mt-12 cursor-pointer">×</span>
                    </button>


                    <div class="p-[100px] rotate-container max-sm:p-[0px] w-full ">
                        <img src="https://www.myanthai.com/images/winning-text.png" class="winning-text ml-[-40px] w-[200px] left-[70px] max-sm:w-[150px] max-sm:ml-[20px]">
                        <img src="https://www.myanthai.com/images/left-bingo.png"  class=" absolute w-[125px] max-sm:w-[80px] left-[25px] max-sm:left-[-49px] bottom-[100px] max-sm:bottom-[0px] z-[-100]">
                        <img src="https://www.myanthai.com/images/right-bingo.png" class=" absolute w-[125px] max-sm:w-[80px] right-[25px] max-sm:right-[-49px] bottom-[100px] max-sm:bottom-[0px] z-[-100]">

                        <div style="background-image: url(https://www.myanthai.com/storage/images/ticket/2023-12-16.jpg);" class="w-[500px] h-[255px] max-sm:w-[350px] max-sm:h-[170px] bg-cover bg-no-repeat bg-center mt-10 " >


                            <div class="ticket-number flex w-[219px] max-sm:w-[100px] h-[54px] absolute  text-gray-800 text-[31px] max-sm:text-[20px] cursor-pointer text-center top-[45%] max-sm:top-[55%] right-[20%] max-sm:right-[24%]">
                                <?php foreach ($singleWinNumber as $digit): ?>
                                    <div class="m-0.5">
                                        <div class="text-[20px] max-sm:text-[15px] p-[-4px] m-[-4px]"><?php echo $digit; ?></div>
                                        <div class="text-[15px] max-sm:text-[9px] p-[-6px] m-[2px]"><?php echo convertToWord($digit); ?></div>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <?php
                                function convertToWord($digit) {
                                    $words = ["ZER", "ONE", "TWO", "THR", "FOU", "FIV", "SIX", "SEV", "EIG", "NIN"];
                                    return isset($words[$digit]) ? $words[$digit] : "";
                                }
                            ?>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
