<style>
    .result-toggle-button {
        cursor: pointer;
    }

    .result-list {
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.8s ease-in-out;
    }

    .result-list-show {
        max-height: 500px;
        transition: max-height 0.8s ease-in-out;
    }

    .result-number {
        min-width: 34px;
        font-size: 16px;
        color: #23326D;
        padding: 4px 10px;
        font-weight: bold;
        border-radius: 50%;
        margin-right: 5px;
    }

    .result-number-one {
        min-width: 34px;
        font-size: 16px;
        color: #23326D;
        padding: 4px 12px;
        font-weight: bold;
        border-radius: 50%;
        margin-right: 5px;
    }

    .result-number-row {
        max-width: 600px;
        margin: 16px auto;
        justify-content: center;
        text-align: center
    }

    .result-number-first {
        letter-spacing: 0.3em;
        background: #FFE81C;
        font-size: 30px;
        font-weight: bold;
        padding: 0.5rem;
        padding-left: 1rem;
    }

    .result-second-prize {
        background: radial-gradient(at 24% 24%, #E0C6FE 0%, #9B5FE0 50%, #E0C6FE 100%);
    }

    .result-second-prize-title {
        background-image: linear-gradient(to left bottom, #9b5fe0, #ad79e8, #be93f0, #cfacf7, #e0c6fe);
    }
    .result-third-prize {
        background: radial-gradient(at 24% 24%, #d2e7ef 0%, #16A4D8 50%, #d2e7ef 100%);
    }

    .result-third-prize-title {
        /* background-image: linear-gradient(to left bottom, #16a4d8, #64ade9, #94b5f4, #bdbdfb, #e0c6fe); */
        background-image: linear-gradient(to left bottom, #16a4d8, #5db5dd, #88c6e2, #aed7e8, #d2e7ef);
    }

    .result-fourth-prize {
        background: radial-gradient(at 24% 24%, #BAF4FA 0%, #60DBE8 50%, #BAF4FA 100%);
    }

    .result-fourth-prize-title {
        background-image: linear-gradient(to left bottom, #60dbe8, #7be1ec, #91e8f1, #a6eef5, #baf4fa);
    }

    .result-fifth-prize {
        background: radial-gradient(at 24% 24%, #D2F9AD 0%, #8BD346 50%, #D2F9AD 100%);
    }

    .result-fifth-prize-title {
        background-image: linear-gradient(to left bottom, #8bd346, #9ddd62, #afe67b, #c1f094, #d2f9ad);
    }

    .result-first-three-prize {
        background: radial-gradient(at 24% 24%, #fcfcfc 0%, #FFBFDF 50%, #FFBFDF 100%);
    }

    .result-first-three-prize-title {
        background-image: linear-gradient(to left bottom, #dfca00, #e6d32c, #eddc43, #f5e557, #fcee6a);
    }

    .result-first-three-prize {
        background: radial-gradient(at 24% 24%, #FCEE6A 0%, #DFCA00 50%, #FCEE6A 100%);
    }

    .result-first-three-prize-title {
        /* background-image: linear-gradient(to left bottom, #fcee6a, #f7eb59, #f3e847, #ede631, #e8e30a); */
        background-image: linear-gradient(to left bottom, #fcee6a, #ece159, #ddd447, #cdc834, #bebb1e);
    }

    .result-last-three-prize {
        background: radial-gradient(at 24% 24%, #FDD192 0%, #F9A52C 50%, #FDD192 100%);
    }

    .result-last-three-prize-title {
        background: radial-gradient(at 24% 24%, #FDD192 0%, #F9A52C 50%, #FDD192 100%);
    }

    .result-last-two-prize {
        background: radial-gradient(at 24% 24%, #FA8F5F 0%, #D64E12 50%, #FA8F5F 100%);
    }

    .result-last-two-prize-title {
        background-image: linear-gradient(to left bottom, #d64e12, #e05f27, #e96f3a, #f27f4d, #fa8f5f);
    }

    .result-number-yellow {
        background: radial-gradient(at 24% 24%, #fcfcfc 0%, #ffe609 50%, #ffe609 100%);
    }


    @-webkit-keyframes fadeInLeft {
        from {
            opacity: 0;
            -webkit-transform: translatex(-10px);
            -moz-transform: translatex(-10px);
            -o-transform: translatex(-10px);
            transform: translatex(-10px);
        }

        to {
            opacity: 1;
            -webkit-transform: translatex(0);
            -moz-transform: translatex(0);
            -o-transform: translatex(0);
            transform: translatex(0);
        }
    }


    @-webkit-keyframes fadeInRight {
        from {
            opacity: 1;
            -webkit-transform: translatex(0);
            -moz-transform: translatex(0);
            -o-transform: translatex(0);
            transform: translatex(0);
        }

        to {
            opacity: 0;
            -webkit-transform: translatex(-10px);
            -moz-transform: translatex(-10px);
            -o-transform: translatex(-10px);
            transform: translatex(-10px);
        }
    }

    .in-right {
        -webkit-animation-name: fadeInRight;
        -moz-animation-name: fadeInRight;
        -o-animation-name: fadeInRight;
        animation-name: fadeInRight;
        -webkit-animation-fill-mode: both;
        -moz-animation-fill-mode: both;
        -o-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.7s;
        -moz-animation-duration: 0.7s;
        -o-animation-duration: 0.7s;
        animation-duration: 0.7s;
        -webkit-animation-delay: 0.7s;
        -moz-animation-delay: 0.7s;
        -o-animation-duration: 0.7s;
        animation-delay: 0.7s;
    }

    .in-left {
        -webkit-animation-name: fadeInLeft;
        -moz-animation-name: fadeInLeft;
        -o-animation-name: fadeInLeft;
        animation-name: fadeInLeft;
        -webkit-animation-fill-mode: both;
        -moz-animation-fill-mode: both;
        -o-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 0.7s;
        -moz-animation-duration: 0.7s;
        -o-animation-duration: 0.7s;
        animation-duration: 0.7s;
        -webkit-animation-delay: 0.7s;
        -moz-animation-delay: 0.7s;
        -o-animation-duration: 0.7s;
        animation-delay: 0.7s;
    }

    .rotate-chevron {
        transform: rotate(180deg);
        transition: transform 0.3s ease-in-out;
    }

    .rotate-chevron-up {
        transform: rotate(0deg);
        transition: transform 0.3s ease-in-out;
    }

    .prize-title {
        text-align: center;
        font-size: 16px;
        color: #081557;
        margin-bottom: 4px;
    }

    .tab-content {
        display: none;
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 20px;
        padding-top: 0px;
    }

    .tab-content.active {
        display: block;
    }

    .tab-link {}

    .tab-link.activeTabLink {
        border-color: #365BEF;
    }

    .login-input {
        border-radius: 0;
        background-color: #F2F2F2;
        border: 1px solid #F2F2F2;
        padding: 16px 10px;
        color: #081557;
    }

    .btn-check {
        background-color: #365BEF;
        color: #ffffff;
        height: 58px;
        border-radius: unset;
    }

    /* Add these custom width classes to your styles */
    .custom-width-80 {
        width: 77%;
    }

    .custom-width-20 {
        width: 20%;
    }

    .custom-width-50 {
        width: 48%;
    }

    .custom-width-90{
        width: 80%;
    }

    .win-img {
        width: 290px;
        vertical-align: middle;
        border-style: none;
    }

    .center-container {
      display: flex;
      justify-content: center;
      align-items: center;
      padding:20px;
    }



    .wining-img
    {
        width:500px;
        height:255px;
    }

    .rotate-container {
        position: relative;
        -ms-transform: rotate(-10deg);
        transform: rotate(-10deg);
    }

    .close-show-wining-number
    {
        margin-top: 15%;
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #F2F2F2;
    }

    .close-show-wining-number:hover
    {
        color: #292929;
    }

    .winning-text {
        clear: both;
        transform: rotate(10deg);
    }
</style>

@livewire('results')

<script>
    function showTab(tabId) {
        event.preventDefault();
        var tabsLink = document.querySelectorAll('.tab-link');
        tabsLink.forEach(function(tab) {
            tab.classList.remove('activeTabLink');
        });
        document.getElementById(tabId + "Link").classList.add('activeTabLink');
    }


    document.addEventListener('click', (event) => {
        const result = event.target.closest('.result-toggle-button');

        if (result) {
            const showResult = result.nextElementSibling;
            const chevron = result.querySelector('i.fa-chevron-down');

            showResult.classList.toggle('result-list-show');
            showResult.classList.toggle('in-left');

            const isShowResult = showResult.classList.contains('result-list-show');

            chevron.classList.toggle('rotate-chevron', isShowResult);
            chevron.classList.toggle('rotate-chevron-up', !isShowResult);
        }
    });


    var checkboxes = document.querySelectorAll('.control-checkbox');
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener('click', function() {
            checkboxes.forEach(function(otherCheckbox) {
                if (otherCheckbox !== checkbox) {
                    otherCheckbox.checked = false;
                }
            });
        });
    });
</script>
