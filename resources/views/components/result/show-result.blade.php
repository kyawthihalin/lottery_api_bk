
<div  x-cloak id="basicModal" x-data="{ openResult: @entangle('showResult') }" @open-me="openResult=true" @close-me="openResult=false">

    <div @keydown.window.escape="openResult = false" x-show="openResult" class="relative z-10" aria-labelledby="modal-title"
        x-ref="dialog" aria-modal="true">

        <div x-show="openResult" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
            x-description="Background backdrop, show/hide based on modal state."
            class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-end items-center justify-center min-h-full p-4 text-center sm:p-0">

                <div x-show="openResult" x-transition:enter="ease-out duration-300"
                    x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave="ease-in duration-200"
                    x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-description="Modal panel, show/hide based on modal state."
                    class="relative bg-white  text-left overflow-hidden shadow-xl transform transition-all "
                    @click.away="openResult = false">

                    <div class="p-6">
                        <button @click.prevent="openResult = false" type="button" class="close float-right"
                            data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="text-2xl font-black m-5 mt-12">×</span>
                        </button>

                        @if($winOrLose)
                            <div class="p-6">
                                <div class="flex items-center justify-center clear-both ">
                                    <img src="https://www.myanthai.com/images/congrats.jpg" class="modal-icon win-img">
                                </div>
                                @if($winAmount != 0)
                                    <p class="text-center text-md/[17px]">You win ฿ {{ number_format($winAmount, 0, '.', ',') }}  from this ticket!</p>
                                    <button type="button" wire:click="viewWiningTicket()"  class="bg-indigo-500 w-96 p-3 m-3 max-sm:w-full">CHECK MY TICKET</button>
                                @elseif(!empty($multiWinNumber))
                                    @foreach($multiWinNumber as $WinNumber)
                                        <p class="text-center text-md/[17px]">You win ฿ {{ number_format($WinNumber->price, 0, '.', ',') }} from ticket number {{$WinNumber->winNumber}} !</p>
                                    @endforeach
                                @endif
                        </div>
                        @else
                            <div class="p-6">
                                <div class="flex items-center justify-center clear-both ">
                                    <img src="https://www.myanthai.com/images/lose.jpg" class="modal-icon win-img">
                                </div>
                                <p class="text-center text-md/[17px]">The lottery number you entered is not a winning number.</p>
                                <button type="button" wire:click="tryAgainCheck()"  class="bg-indigo-500 w-96 p-3 m-3 max-sm:w-full">TRY AGAIN</button>
                            </div>
                        @endif

                   </div>

                </div>
            </div>
        </div>
    </div>
</div>
