<div>
    <x-result.checking-number :changeType="$changeType"  :multiInputs="$multiInputs" />
    <x-result.show-result :winOrLose="$winOrLose" :winAmount="$winAmount" :multiWinNumber="$multiWinNumber"  />
    <x-result.show-wining-ticket :winOrLose="$winOrLose" :winAmount="$winAmount" :singleWinNumber="$singleWinNumber"/>
</div>
