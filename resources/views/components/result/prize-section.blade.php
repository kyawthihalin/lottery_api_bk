@props(['row', 'resultTypeCode', 'backgroundColor','numberColorClass','firstThree','lastThree','lastTwo'])

<div class="shadow">

    <div  class="result-toggle-button p-4 grid grid-cols-6 {{ $backgroundColor }} ">
        <button class="col-start-0 col-end-1"><i class="fa-solid fa-chevron-down"></i></button>
        <h2 class="text-lg  col-start-1 col-end-8 text-center">{{ $row->resultType->name }} {{ number_format($row->price, 0, '.', ',') }} ฿ Each</h2>
    </div>

    <div class="result-list">
        <div class="p-3 pt-8 grid @if($lastThree || $firstThree ) lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-2 @elseif($lastTwo) sm:grid-cols-1 @else lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 @endif gap-4 bg-inherit bg-transparent ">
            @foreach($row->numbers as $number)
                <div class="mb-1 text-center">

                    @if($lastThree)
                            <span class="result-number {{$numberColorClass}}">X</span>
                            <span class="result-number {{$numberColorClass}}">X</span>
                            <span class="result-number {{$numberColorClass}}">X</span>
                    @endif
                    @if($lastTwo)
                            <span class="result-number {{$numberColorClass}}">X</span>
                            <span class="result-number {{$numberColorClass}}">X</span>
                            <span class="result-number {{$numberColorClass}}">X</span>
                            <span class="result-number {{$numberColorClass}}">X</span>
                    @endif

                    @foreach(str_split($number) as $digit)
                        <span class="@if($digit != 1) result-number @else result-number-one @endif {{$numberColorClass}}">{{ $digit }}</span>
                    @endforeach

                    @if($firstThree)
                        <span class="result-number {{$numberColorClass}}">X</span>
                        <span class="result-number {{$numberColorClass}}">X</span>
                        <span class="result-number {{$numberColorClass}}">X</span>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>
