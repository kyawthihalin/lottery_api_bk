<div x-cloak id="basicModal" x-data="{
        open: @entangle('showCheck'),
        changeType: 'single',
    }" @open-me="open=true" @close-me="open=false">

    <span @click.prevent="open = true"
        class="float-right bg-blue-600 hover:bg-blue-700 text-white font-bold  py-2 px-4 rounded max-sm:mt-4 mt-2 max-sm:mr-4 mr-12 cursor-pointer">
        Check My Lottery Number
    </span>

    <div @keydown.window.escape="open = false" x-show="open" class="relative z-10" aria-labelledby="modal-title"
        x-ref="dialog" aria-modal="true">

        <div x-show="open" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
            x-description="Background backdrop, show/hide based on modal state."
            class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-center justify-center min-h-full p-4 text-center sm:p-0">
                <div x-show="open" x-transition:enter="ease-out duration-300"
                    x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave="ease-in duration-200"
                    x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
                    x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    x-description="Modal panel, show/hide based on modal state."
                    class="relative bg-white  text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:max-w-lg sm:w-full"
                    @click.away="open = false">

                    <div>
                        <button @click.prevent="open = false" type="button" class="close float-right"
                            data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="text-2xl font-black m-3 mt-12">×</span>
                        </button>

                        <h2 class="text-center clear-both pt-6 text-xl">Check Lottery Number </h2>

                        <div class="mx-14 max-sm:mx-6 ">
                            <div class="flex items-center justify-center my-12">
                                <div @click="changeType = 'single'" class="flex items-center cursor-pointer">
                                    <input type="checkbox" id="single-check" value="single"
                                        :checked="changeType == 'single'" class="cursor-pointer">
                                    <span
                                        class="ml-2 mr-4 max-sm:mr-1  max-sm:ml-1 max-sm:text-xs whitespace-nowrap">Single
                                        Ticket</span>
                                </div>

                                <div @click="changeType = 'multiple'" class="flex items-center cursor-pointer">
                                    <input type="checkbox" id="multiple-check" value="multiple"
                                        :checked="changeType == 'multiple'" class="cursor-pointer">
                                    <span
                                        class="ml-2 mr-4 max-sm:mr-1 max-sm:ml-1 max-sm:text-xs whitespace-nowrap">Multiple
                                        Ticket</span>
                                </div>

                                <div @click="changeType = 'series'" class="flex items-center cursor-pointer">
                                    <input type="checkbox" id="series-check" value="series"
                                        :checked="changeType == 'series'" class="cursor-pointer">
                                    <span
                                        class="ml-2 mr-4 max-sm:mr-1 max-sm:ml-1 max-sm:text-xs whitespace-nowrap">Series
                                        Ticket </span>
                                </div>
                            </div>

                            <form wire:submit.prevent="checkSingleResult">
                                <div x-show="changeType === 'single'"
                                    class="flex items-center   w-full justify-center my-4">
                                    <input wire:model="number" type="number" placeholder="Enter Ticket Number"
                                        autofocus="autofocus"
                                        class="form-control text-base max-sm:text-sm login-input w-3/4">
                                    <button type="submit"
                                        class="btn text-base max-sm:text-sm btn-check  w-1/4 ">Check</button>
                                </div>
                                @error('number')
                                    <span x-show="changeType === 'single'"
                                        class="flex items-center justify-center text-red-500">{{ $message }}</span>
                                @enderror
                            </form>

                            <form wire:submit.prevent="checkMultipleResult">
                                <div x-show="changeType === 'multiple'" class="items-center w-full justify-center my-4"  wire:ignore >
                                    <div class="input-group">
                                        @if (empty($multiInputs))
                                            <input type="number" wire:model="multiInputs.0"
                                                placeholder="Enter Ticket Number" autofocus="autofocus"
                                                class="form-control login-input custom-width-80 text-base max-sm:text-sm">
                                            <button type="button"
                                                class="bg-slate-100 px-2 rounded-full ml-3 text-red-500 "
                                                disabled>x</button>
                                        @else
                                            @foreach ($multiInputs as $key => $input)
                                                <input type="number" wire:model="multiInputs.{{ $key }}"
                                                    placeholder="Enter Ticket Number" autofocus="autofocus"
                                                    id="{{'multiInput'.$key}}"
                                                    class="form-control login-input custom-width-80 text-base max-sm:text-sm mt-2">
                                                @if ($key != 0)
                                                    <button type="button" class="bg-slate-100 px-2 rounded-full ml-3 remove-multi-input-item"
                                                    id="{{$key}}"
                                                    wire:click.prevent="removeMultiInput({{ $key }})" >x</button>
                                                @else
                                                    <button type="button"
                                                        class="bg-slate-100 px-2 rounded-full ml-3 text-red-500 "
                                                        disabled>x</button>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>

                                    <div class="items-center justify-center my-4 w-full">
                                        <button type="submit"
                                            class="btn btn-check custom-width-80 text-base max-sm:text-sm">Check</button>
                                        <button type="button"
                                            class="btn btn-check custom-width-20 add-input ">+</button>
                                    </div>
                                </div>
                                @error('multiInputs')
                                    <span x-show="changeType === 'multiple'"
                                        class="flex items-center text-base max-sm:text-sm justify-center text-red-500">{{ $message }}</span>
                                @enderror
                                @error('multiInputs.*')
                                    <span x-show="changeType === 'multiple'"
                                        class="flex items-center text-base max-sm:text-sm justify-center text-red-500">{{ $message }}</span>
                                @enderror
                            </form>


                            <form wire:submit.prevent="checkSeriesResult">
                                <div x-show="changeType === 'series'" class="items-center justify-center my-4">
                                    <div class="flex items-center justify-center my-4 ">
                                        <input type="number" wire:model="seriesFrom" placeholder="From"
                                            autofocus="autofocus"
                                            class="form-control login-input custom-width-50 text-base max-sm:text-sm">
                                        <input type="number" wire:model="seriesTo" placeholder="To"
                                            autofocus="autofocus"
                                            class="form-control login-input custom-width-50 text-base max-sm:text-sm ml-2">
                                    </div>
                                    <button type="submit"
                                        class="btn btn-check w-full text-base max-sm:text-sm">Check</button>
                                </div>
                                @error('seriesFrom')
                                    <span x-show="changeType === 'series'"
                                        class="flex items-center text-base max-sm:text-sm justify-center text-red-500">{{ $message }}</span>
                                @enderror
                                @error('seriesTo')
                                    <span x-show="changeType === 'series'"
                                        class="flex items-center text-base max-sm:text-sm justify-center text-red-500">{{ $message }}</span>
                                @enderror
                            </form>

                        </div>
                        <div class="mt-12"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>

    const container = document.querySelector('.input-group');
    const addBtn = document.querySelector('.add-input');
    const removeBtn = document.querySelector('.remove-input');
    const inputs = container.querySelectorAll('input');
    let inputCount = 1;

    document.addEventListener('click', (event) => {
        const result = event.target.closest('.remove-multi-input-item');
        if (result) {
            var key = event.target.id;
            $('#multiInput'+key).remove();
            $('#'+key).remove();
        }
    });

    addBtn.addEventListener('click', function() {
        const newInput = document.createElement('input');
        newInput.type = 'number';
        newInput.placeholder = 'Enter Ticket Number';
        newInput.className = 'form-control login-input custom-width-80 text-base max-sm:text-sm mt-2';
        newInput.setAttribute('wire:model', 'multiInputs.' + inputCount);
        newInput.setAttribute('id', 'multiInput' + inputCount);
        container.appendChild(newInput);

        const newButton = document.createElement('button');
        newButton.className = 'bg-slate-100 px-2 rounded-full ml-3 remove-multi-input-item';
        newButton.type = 'button';
        newButton.setAttribute('wire:click.prevent', 'removeMultiInput(' + inputCount + ')');
        newButton.textContent = 'x';
        newButton.setAttribute('id',inputCount);
        container.appendChild(newButton);
        inputCount++;
    });
</script>
