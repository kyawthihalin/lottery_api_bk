<div class="mt-10 mb-16">
    <div class="flex justify-center items-center text-center mx-3">
        <hr class="w-full sm:w-[300px] border-info-400">
        <h2 class="text-lg sm:text-2xl w-full sm:w-[300px] mx-2 sm:mx-0 text-info-900 font-semibold">
            {{ __('web.new_on_ticket') }}</h2>
        <hr class="w-full sm:w-[300px] border-info-400">
    </div>
    <div class="mt-8 grid sm:gap-8 grid-cols-5 md:grid-cols-11 justify-center items-center mx-auto px-4 sm:px-28">
        <div class="flex items-center w-full col-span-2">
            <img src="	https://www.myanthai.com/images/viber-contact.svg" class="icon-choose">
            <div class="w-full ml-3">
                <p class="text-center">{{ __('web.new_on_first') }}</p>
            </div>
        </div>
        <i class="fa-solid fa-arrow-right text-xl text-center"></i>
        <div class="flex items-center w-full col-span-2">
            <img src="https://www.myanthai.com/images/buy-point.svg" class="icon-choose">
            <div class="w-full ml-3">
                <p class="text-center">{{ __('web.new_on_second') }}</p>
            </div>
        </div>
        <i class="fa-solid fa-arrow-right text-xl text-center hidden md:block"></i>
        <div class="flex items-center w-full col-span-2  max-sm:mt-3">
            <img src="https://www.myanthai.com/images/buy-ticket.svg" class="icon-choose">
            <div class="w-full ml-3">
                <p class="text-center">{{ __('web.new_on_third') }}</p>
            </div>
        </div>
        <i class="fa-solid fa-arrow-right text-xl text-center"></i>
        <div class="flex items-center w-full col-span-2 max-sm:mt-3">
            <img src="https://www.myanthai.com/images/win-prize.svg" class="icon-choose">
            <div class="w-full ml-3">
                <p class="text-center">{{ __('web.new_on_fourth') }}</p>
            </div>
        </div>
    </div>
</div>
