@php
    use App\Settings\GeneralSettings;

    $link = app(GeneralSettings::class)->live_link;

    $regExp = '/^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/';
    preg_match($regExp, $link, $match);

    $embed = null;
    if (!empty($match) && strlen($match[1]) == 11) {
        $embed = $match[1];
    }
@endphp

@if ($link)
    <div class="h-[425px]">
        <iframe src="//www.youtube.com/embed/{{ $embed }}" width="100%" height="100%"></iframe>
    </div>
@endif
