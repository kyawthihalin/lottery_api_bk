<div class="mx-4">
    <div
        class="mx-auto grid gap-8 max-sm:grid-cols-2 grid-cols-4 bg-white max-w-3xl rounded-lg my-5 sm:my-10 max-sm:p-10 p-16 shadow-xl">
        <x-home.block.block-item title="{{ __('web.e_ticket_title') }}" text="{{ __('web.e_ticket_text') }}"
            icon="fa-solid fa-ticket" iconColor="text-primary-600 rotate-12" />
        <x-home.block.block-item title="{{ __('web.favorite_number_title') }}" text="{{ __('web.favorite_number_text') }}"
            icon="fa-solid fa-star" iconColor="text-yellow-400" />
        <x-home.block.block-item title="{{ __('web.safe_title') }}" text="{{ __('web.safe_text') }}"
            icon="fa-solid fa-shield-halved" iconColor="text-cyan-600" />
        <x-home.block.block-item title="{{ __('web.result_title') }}" text="{{ __('web.result_text') }}"
            icon="fa-solid fa-thumbs-up" iconColor="text-info-600" />
    </div>
</div>
