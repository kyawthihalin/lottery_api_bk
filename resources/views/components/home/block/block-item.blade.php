<div class="flex flex-col items-center w-full ">
    <div class="text-2xl py-4 px-5 rounded-full {{ $iconColor }}" style="background-color: #E7EBF9">
        <i class="{{ $icon }}"></i>
    </div>
    <div class="w-full">
        <p class="guarantee-slider-sub-title mt-3 text-center font-extrabold">
            {{ $title }}
        </p>
        <p class="guarantee-slider-text mt-3 text-center ">
            {{ $text }}
        </p>
    </div>
</div>
