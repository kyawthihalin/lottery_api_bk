@php
    use App\Services\StorageService;
    use App\Settings\BannerSettings;
@endphp

<div class="swiper mySwiper w-full h-64 sm:h-auto mx-auto">
    <div class="swiper-wrapper">
        @foreach (app(BannerSettings::class)->website as $img)
            <div class="swiper-slide text-center text-18 bg-white flex justify-center items-center">
                <img class="block w-full h-full object-cover" src="{{ StorageService::getUrl($img) }}" />
            </div>
        @endforeach
    </div>
    <div class="swiper-pagination"></div>
</div>
