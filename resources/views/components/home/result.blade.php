@php
    use Carbon\Carbon;
    use App\Facades\Draw;
    use App\Models\Result;
    use App\Models\ResultType;
    use App\Enums\ResultTypeCode;

    $lastDraw = Carbon::parse(Draw::getPreviousDrawId());
    $firstResultType = ResultType::where('code', ResultTypeCode::First)->first();

    $firstResult = Result::where('draw_id', $lastDraw)
        ->where('result_type_id', $firstResultType->id)
        ->first();

    $pendingDraw = Carbon::parse(Draw::getPendingDrawId())->format('d M Y');
    $number = $firstResult?->numbers[0];
    $price = number_format($firstResultType->price);
@endphp

<div class="px-4">
    <div
        class="flex flex-col items-center text-center bg-info-500 text-white mx-auto max-w-3xl rounded-lg my-5 sm:my-10 max-sm:p-10 p-12 shadow-xl">
        <h2 class="text-xl sm:text-3xl">Latest Thai Lottery Result</h2>
        <h3 class="mt-5 text-lg sm:text-2xl">{{ $lastDraw->format('D, d M Y') }}</h3>
        <h5 class="text-primary-950 text-base sm:text-xl mt-8">1-st Prize {{ $price }} ฿</h5>
        <p class="mt-3 bg-info-900 text-3xl p-5 tracking-[.25em]">{{ $number ?? 'xxxxxx' }}</p>
        <a href="{{ route('result') }}" class="self-center sm:self-end mt-5 text-sm sm:text-lg cursor-pointer">
            VIEW ALL RESULTS
            <i class="fa-solid fa-chevron-right ml-1"></i></a>
    </div>
</div>
