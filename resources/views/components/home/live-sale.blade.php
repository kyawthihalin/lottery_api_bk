@php
    use Carbon\Carbon;
    use App\Models\TicketGroup;
    use App\Settings\ImageSettings;
    use App\Services\StorageService;
    use App\Settings\TicketSettings;
    use App\Http\Resources\Api\User\TicketGroupResource;

    $ticketGroups = TicketGroup::orderBy('draw_id', 'desc')
        ->take(5)
        ->get();
    $groups = TicketGroupResource::collection($ticketGroups);
@endphp

<div class="swiper liveSale w-[100%] sm:w-[90%] py-10">
    <div class="swiper-wrapper">
        @foreach ($groups as $group)
            <div class="swiper-slide bg-center bg-cover w-[285px] sm:w-[515px] h-[450px] sm:h-[805px]">
                <a href="{{ route('live-details') }}" class="cursor-grab">
                    <img class="top-0 left-0 absolute w-full z-10"
                        src="{{ StorageService::getUrl(app(ImageSettings::class)->group_ticket_frame_website) }}" />
                </a>
                <div
                    class="w-[94%] sm:w-full overflow-x-clip flex flex-col justify-center items-center relative top-[13px] sm:top-[18px] left-[12px] sm:left-0 rounded-3xl">
                    @foreach ($group->numbers as $ticket)
                        <div style="background-image: url('{{ StorageService::getUrl(app(ImageSettings::class)->ticket_background) }}')"
                            class="bg-cover flex-none relative w-full sm:w-[495px] {{ $ticket == last($group->numbers) ? 'h-[125px] sm:h-[225px]' : 'h-[32px] sm:h-[53px]' }}">
                            <span
                                class="font-['Octin'] w-[125px] sm:w-[210px] absolute top-[9px] sm:top-[12px] left-[122px] sm:left-[236px] flex justify-evenly">
                                @foreach (str_split($ticket) as $number)
                                    <div class="flex flex-col items-center">
                                        <div class="text-md sm:text-xl">{{ $number }}</div>
                                        <span
                                            class="hidden sm:block text-[10px]">{{ get_string_by_number($number) }}</span>
                                    </div>
                                @endforeach
                            </span>
                            @if ($ticket == last($group->numbers))
                                <span
                                    class="font-['Octin'] absolute top-[42%] right-[20%] text-xs sm:text-xl text-gray-800">{{ Carbon::parse($group->draw_id)->format('d M Y') }}</span>
                            @endif

                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>
