@php
    use App\Facades\Draw;
    use Carbon\Carbon;

    $currentDraw = Carbon::parse(Draw::getPendingDrawId());
@endphp

<style>
    .tick-credits {
        display: none;
    }

    .tick-flip-panel {
        color: #edebeb;
        background-color: #262a89
    }
</style>

<div class="container grid grid-cols-1 gap-6 sm:gap-12 mx-auto mt-8 sm:mt-16 text-center">
    <h1 id="headline" class="text-lg sm:text-2xl font-bold text-info-900">{{ __('web.countdown_title') }}</h1>
    <div class="tick max-w-[600px] text-[1.5rem] sm:text-[3rem] m-auto" data-did-init="handleTickInit">
        <div data-repeat="true" data-layout="horizontal center fit" data-transform="preset(d, h, m, s) -> delay">
            <div class="my-0 mx-3">
                <div data-key="value" data-repeat="true" data-transform="pad(00) -> split -> delay">
                    <span data-view="flip" class="bg-primary-500"></span>
                </div>

                <span data-key="label" data-view="text" class="text-xs sm:text-lg text-center text-info-900"></span>
            </div>
        </div>
    </div>
</div>

<script>
    function handleTickInit(tick) {
        var date = @json($currentDraw->format('Y-m-d'));

        Tick.count.down(date).onupdate = function(value) {
            tick.value = value;
        };
    }
</script>
