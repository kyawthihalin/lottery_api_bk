<x-layout>
    <section class="flex">
        <div class="max-md:flex-auto flex-1">
            <div class="px-8 py-10 sm:py-20">
                <h2 class="font-bold text-2xl text-info-900 text-center">{{ __('web.create_account') }}</h2>

                @php
                    $inputs = ['user_name' => 'Username', 'name' => 'Name', 'password' => 'Password', 'agent_code' => 'Agent Code'];
                @endphp

                <form action="/register" method="post" class="flex flex-col items-center gap-5 mt-8">
                    @csrf
                    <x-form.error />
                    @foreach ($inputs as $field => $placeholder)
                        <x-form.error name="{{ $field }}" />
                    @endforeach

                    @foreach ($inputs as $field => $placeholder)
                        <div x-data="{ show: false }" class="relative w-full text-center">
                            <x-form.input name="{{ $field }}" placeholder="{{ $placeholder }}"
                                action="show ? 'text' : 'password'" />
                            @if ($field === 'password')
                                <x-form.password-eye />
                            @endif
                        </div>
                    @endforeach

                    <x-buttons.background-button style="w-1/4 max-lg:w-1/2 max-sm:w-full">
                        {{ __('web.create_account') }}
                    </x-buttons.background-button>
                </form>

                <p class="text-sm text-info-900 mt-3 text-center">
                    {{ __('web.already_account') }}
                    <a href="{{ route('login') }}" class="login-link underline-link active">{{ __('web.login') }}</a>
                </p>
            </div>
        </div>
    </section>
</x-layout>
