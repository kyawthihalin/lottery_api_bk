<?php

return [
    'shield_resource' => [
        'should_register_navigation' => true,
        'slug' => 'roles',
        'navigation_sort' => -1,
        'navigation_badge' => false,
        'navigation_group' => true,
        'is_globally_searchable' => false,
        'show_model_path' => false,
    ],

    'auth_provider_model' => [
        'fqcn' => 'App\\Models\\PolicyUser',
    ],

    'super_admin' => [
        'enabled' => true,
        'name' => 'Super admin',
        'define_via_gate' => false,
        'intercept_gate' => 'before', // after
    ],

    'panel_user' => [
        'enabled' => false,
        'name' => 'panel_user',
    ],

    'permission_prefixes' => [
        'resource' => [
            'view_any',
            // 'create',
            // 'update',
            // 'delete',
            // 'delete_any',
        ],

        'page' => 'page',
        'widget' => 'widget',
    ],

    'entities' => [
        'pages' => true,
        'widgets' => true,
        'resources' => true,
        'custom_permissions' => false,
    ],

    'generator' => [
        // 'option' => 'policies_and_permissions',
        'option' => 'permissions',
    ],

    'exclude' => [
        'enabled' => true,

        'pages' => [
            'Dashboard',
            'SecretCode',
            'SetupSecretCode',
            'UpdateSecretCode',
            'UpdatePassword',
        ],

        'widgets' => [
            'AccountWidget', 'FilamentInfoWidget',
        ],

        'resources' => [],
    ],

    'discovery' => [
        'discover_all_resources' => false,
        'discover_all_widgets' => false,
        'discover_all_pages' => false,
    ],

    'register_role_policy' => [
        'enabled' => true,
    ],

];
