import preset from "./vendor/filament/filament/tailwind.config.preset";
const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
export default {
    presets: [preset],
    content: [
        "./app/Filament/**/*.php",
        "./resources/views/**/*.blade.php",
        "./vendor/filament/**/*.blade.php",
        "./vendor/bezhansalleh/filament-language-switch/resources/views/language-switch.blade.php",
    ],
    theme: {
        extend: {
            colors: {
                white: "#fffafa",
                danger: colors.red,
                gray: colors.gray,
                // gray: {
                //     50: "#f6f7f8",
                //     100: "#eaecef",
                //     200: "#d9dee4",
                //     300: "#bfc7d1",
                //     400: "#aeb8c4",
                //     500: "#8995a8",
                //     600: "#778299",
                //     700: "#6b738a",
                //     800: "#5a6073",
                //     900: "#4b505d",
                //     950: "#30333b",
                // },
                info: {
                    50: "#ecf2ff",
                    100: "#dde8ff",
                    200: "#c2d4ff",
                    300: "#9cb5ff",
                    400: "#758cff",
                    500: "#4254ff",
                    600: "#3639f5",
                    700: "#2a2ad8",
                    800: "#2527ae",
                    900: "#262a89",
                    950: "#161650",
                },
                primary: {
                    50: "#eef9ff",
                    100: "#daf0ff",
                    200: "#bce7ff",
                    300: "#8fd9ff",
                    400: "#5ac1ff",
                    500: "#33a4fe",
                    600: "#1c85f3",
                    700: "#156ee0",
                    800: "#1859b5",
                    900: "#1a4c8e",
                    950: "#152f56",
                },
                success: colors.green,
                warning: {
                    50: "#fef8ee",
                    100: "#fcefd8",
                    200: "#f8dcb0",
                    300: "#f3c27e",
                    400: "#ec9c46",
                    500: "#e88327",
                    600: "#d96a1d",
                    700: "#b4511a",
                    800: "#90411c",
                    900: "#74371a",
                    950: "#3e1a0c",
                },
            },
            screens: {
                xs: "576px", // => @media (min-width: 576px) { ... }
            },
        },
    },
    plugins: [],
};
